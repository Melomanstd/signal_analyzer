/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include "renderthread.h"

RenderThread::RenderThread(WorkerObject *w,
                           QString strIp,
                           QString strOscIp,
                           QObject *parent)
    : QThread(parent)
{
    //process=0;
    //m_Ch1=false;
    //m_Ch2= false;
    m_abort = false;
    m_bDelWorker=false;
    suspended = false;
    stopThreadF = false;
    exitThreadF = false;
    suspF = false;
    m_strIp=strIp;
    m_strOscIp=strOscIp;
    m_worker=w;
/*    if(!m_worker)
    {
        m_worker = new WorkerObject(m_strIp, m_strOscIp);
        m_worker->open();
        m_bDelWorker=true;
    }
*/
    m_data2.resize(RAW_SAMPLES_NUM*4);
    //m_data2.fill(1);
    m_data.resize(RAW_SAMPLES_NUM*4);

    m_nReadPtr=0;
    m_nFftGain=0;
#ifdef unused
    d_plot=NULL;
#endif
    //m_mw=mw;

    //m_bCycle=false;

    udp_client=NULL;
    mainThread = 0;
//    udp_client=new QUdpSocket();
//    udp_client->bind(QHostAddress(/*"192.168.100.1"*/m_strIp), 50001);
    //udp_client->bind(QHostAddress::LocalHost, 50001);
    //udp_client->connectToHost(ui->edit_IP->text(), ui->edit_Port->text().toInt()/*QHostAddress::LocalHost, 7755*/);
    //udp_client->moveToThread(this);

    //initPLL();
}

RenderThread::RenderThread(ProcessObject *p,
                           WorkerObject *w,
                           QString strIp,
                           QString strOscIp,
                           QObject *parent)
    : QThread(parent)
{
    process=p;
    //m_Ch1=false;
    //m_Ch2= false;
    mainThread = 0;
    m_abort = false;
    m_bDelWorker=false;
    suspended = false;
    stopThreadF = false;
    exitThreadF = false;
    suspF = false;
    m_strIp=strIp;
    m_strOscIp=strOscIp;
    m_worker=new WorkerObject(m_strIp, m_strOscIp);//w;
    m_data2.resize(RAW_SAMPLES_NUM*4);
    //m_data2.fill(1);
    m_data.resize(RAW_SAMPLES_NUM*4);
    m_nReadPtr=0;
    m_nFftGain=0;
    modeConf=0;
#ifdef unused
    d_plot=NULL;
#endif
    //m_bCycle=false;

    udp_client=NULL;
    //initPLL();
}


RenderThread::~RenderThread()
{
    mutex.lock();
    m_abort = true;
    mutex.unlock();

    wait();

    if(udp_client)
    {
        udp_client->close();
        delete udp_client;
    }

    if(m_worker && m_bDelWorker)
    {
        delete m_worker;
        m_worker=NULL;
    }
}

//![processing the image (start)]
void RenderThread::startProcess(quint8 nMeasMode,
                                quint8 nCh,
                                quint8 nChType,
                                quint8 nChAtten,
                                quint8 nTaktADC,
                                quint8 nGenMode,
                                double fFc,
                                quint32 nAmax,
                                quint32 nAmin,
                                double fFdev,
                                double fFsym,
                                double fFmod,
                                quint8 nSSBType,
                                qreal  fK,
                                double fFs,
                                double fFcentr,
                                double fBand,
                                int nFftWindow,//*pFftWindow,
#ifdef unused
                                Plot *pPlot,
#endif
                                bool bCycle)
{
    qDebug("render starting ");
    m_nMeasMode=nMeasMode;
    //m_nCh=nCh;
    //m_nChType=nChType;
    //m_nChAtten=nChAtten;
    //m_nTaktADC=nTaktADC;
    //m_nGenMode=nGenMode;
    //m_fFc=fFc;
    //m_nAmax=nAmax;
    //m_nAmin=nAmin;
    //m_fFdev=fFdev;
    //m_fFsym=fFsym;
    //m_fFmod=fFmod;
    //m_nSSBType=nSSBType;
    //m_fK=fK;

    //Fs=fFs;
    //Fcentr=fFcentr;
    //Band=fBand;
    //window=pFftWindow;
    fft_window=&m_worker->fft_window[nFftWindow][0];//pFftWindow;
#ifdef unused
    d_plot=pPlot;
#endif
    //m_bCycle=bCycle;

    //Fleft=modeConf->fFcentr-modeConf->fBand/2;
    //Fright=modeConf->fFcentr+modeConf->fBand/2;
    //Nfft=FFT_SAMPLES_NUM;

    //udp_client=pSock;
    if(udp_client)
        udp_client->moveToThread(this);

    //m_condition = condition;
    m_abort = false;
    start();
}

void RenderThread::run()
{
//    emit renderStarted();
  while (1)
  {
#ifdef unused
//    for (int i=0;i<modeConf->plots->size();i++) {
//        modeConf->plots->at(i)->signalData(0).clearValues();
//        modeConf->plots->at(i)->signalData(1).clearValues();
//        modeConf->plots->at(i)->signalData(2).clearValues();
//    }
#endif
    initPLL();

    process->initRenderVar(0);
    int nFrameLen;
    m_worker->initDevice();
    msleep(500);
    m_worker->initTakt(modeConf->nTaktADC);
    msleep(1000);
//process->processDataDDC(nFrameLen);
    //m_worker->processQueue();

    while(!m_abort)
    {
        //m_worker->processQueue();
        if(modeConf->nPass==0)
            process->initRenderVar(1);
        process->processDataDDC(nFrameLen);

//------------->
        int c=0; //chanel counter
        bool bResult;
        bool ch1Used=false;
        bool ch2Used=false;
        if (modeConf->Ch1) {c++;
            ch1Used=true;
        }
        if (modeConf->Ch2) {c++;
            ch2Used=true;
        }

      for (int x=0; x<c; x++) {
        if (ch1Used) {
            currentData = &m_data;
            m_worker->initChannel(0, modeConf->nChType,
                        modeConf->nChAtten);
        }
        else if (ch2Used) {
            currentData = &m_data2;
            m_worker->initChannel(1, modeConf->nChType2,
                        modeConf->nChAtten2);
        }

        bResult=true;
        for(int i=0;i<3;i++)
        {
        //!!!!!!!!!!!!!
        // start write data BUGED
            if (ch1Used)
                startDecoding(0);
            else if (ch2Used)
                startDecoding(1);
            //startDecoding(x);
//---------------
        // delay for fft frame to be captured
        if(nFrameLen==FFT_SAMPLES_NUM) // fft
            msleep(100+FFT_SAMPLES_NUM*1000/modeConf->Fdec);

qDebug("render start %d ", m_nReadPtr);
        //makeTestDataFFT();
        //m_nReadPtr=FFT_SAMPLES_NUM;
//#ifdef sdsfsdfs
        while(m_nReadPtr<nFrameLen)
        {
            mutex.lock();
            int n=m_nReadPtr;
            readData();
            qDebug("render ............ %d ", m_nReadPtr);
            //m_nReadPtr+=128;
           //m_worker->waitFor();

            //if(n==m_nReadPtr)
            //m_codition.wait(&mutex, 1000);
                    //msleep(100);
            mutex.unlock();

            if(m_abort)break;
        }
//#endif
qDebug("render stop %d ", m_nReadPtr);
        if(m_abort)break;

        if(m_nMeasMode!=4)
            break;
        }
        //bool bResult=true;
        if(m_nReadPtr>=nFrameLen)
        {
            //bwait = true
            if (ch1Used) {
                bResult=processData(0);
                ch1Used=false;
            }
            else if (ch2Used) {
                bResult=processData(1);
                ch2Used=false;
            }
            //bResult=processData(x);

            if( bResult) // todo remove m_nMeasMode==4
            {
            //m_mw->dataReady();
                modeConf->max_y=0;
                modeConf->max_y2=0;
                modeConf->min_y=0xffff;
                modeConf->min_y2=0xffff;

                //msleep(100);
                mutex.lock();
                //if bwait
                if (process->bWait)
                    m_codition.wait(&mutex, 100);//
                mutex.unlock();
            }
        }
        m_nReadPtr=0;
      }
//-----------------------<

        if(m_abort)break;

        if(/*!modeConf->bCycle &&*/ bResult/*nPass>=(nPassNum-1)*/)break;

        if(m_nMeasMode==4)
        {
            if(!bResult/*nPass<(nPassNum-1)*/)modeConf->nPass++;
            else modeConf->nPass=0;
        }

    }
    enableGEN(false);
//    while(!stopThreadF)
//    {

//    }
    if (suspF)
    {
        suspended = true;
        m_codition_run.wait(&mutex);
        suspended = false;
    }
    if(exitThreadF)
        break;
    else m_abort = false;
  }

    //udp_client->close();
    // return socket to the main thread
    if(udp_client)
        udp_client->moveToThread(QApplication::instance()->thread());

    qDebug("render QUIT *********************************************************8 ");

//    qDebug()<<"move to "<<thread();
    //m_worker->moveToThread(mainThread/*m_worker->getMainThread()*/);
}
//![processing the image (finish)]

void RenderThread::stopProcess()
{
    mutex.lock();
    m_abort = true;
    exitThreadF = true;
    mutex.unlock();
    wait();
}

void RenderThread::resumeProcess()
{
    mutex.lock();
    //bwait =false
    process->bWait = false;
    m_codition.wakeAll();
    mutex.unlock();
}

void RenderThread::initDevice()
{
    qDebug("initDevice");

    QList<QPoint> args;

    // установка регистра направления
    args.append(QPoint(3,0));
    WriteReg(1, args);
    WriteReg(2, args);
    //WriteReg(3, args);

    // todo...
    // параметры adc
    args.clear();
    args.append(QPoint(0x14,0x09));
    args.append(QPoint(0xff,0x01));
    WriteReg(4, args);
    WriteReg(6, args);
    WriteReg(8, args);
    WriteReg(10, args);
}

void RenderThread::initChannel(int nCh, int nChType, int nChAtten)
{
    qDebug("initChannel");

    quint8 type[3]={0x08, 0x00, 0x01};
    quint8 atten_600[2]={0x06, 0x00};
    quint8 atten_1M[4]={0x30, 0x10, 0x20, 0x00};

    quint8 b=0x00;
    b|=type[nChType];

    switch(nChType)
    {
    case 0: // 50 Ohm
        break;
    case 1: // 600 Ohm
        b|=atten_600[nChAtten];
        break;
    case 2: // 1 MOhm
        b|=atten_1M[nChAtten];
        break;
    }

    QList<QPoint> args;
    args.append(QPoint(1, b));
    WriteReg(nCh+1, args); // коммутация канала
}

void RenderThread::initTakt()
{
    int nTaktADC=modeConf->nTaktADC;
    qDebug()<< "takting ADC " << nTaktADC;
    quint8 type[2]={0x05, 0x15};

    quint8 b=0x00;
    b|=type[nTaktADC];

    QList<QPoint> args;
    args.append(QPoint(3, 0));
    args.append(QPoint(1, b));
    WriteReg(3, args); //

    //if(nTaktADC==1)
    //    initPLL();
}

void RenderThread::initDDC(bool bFft)
{
    /*double */modeConf->Dec_ideal=modeConf->fFs/(2*modeConf->fBand);
    /*int */modeConf->Dec_3=(modeConf->Dec_ideal>=2)? 2 : 1;

    /*int */modeConf->Dec_1=qFloor(modeConf->Dec_ideal/modeConf->Dec_3); //41.2 -> 41
    if(modeConf->Dec_1>=256)modeConf->Dec_1=256;

    /*int */modeConf->Dec_2=qFloor(modeConf->Dec_ideal/(modeConf->Dec_1*modeConf->Dec_3));
    if(modeConf->Dec_2>=256)modeConf->Dec_2=256;

    /*unsigned long */modeConf->Dec_sum=modeConf->Dec_1*modeConf->Dec_2*modeConf->Dec_3;

    /*double */modeConf->Fdec=qRound64(modeConf->fFs/modeConf->Dec_sum);

    /*int */modeConf->Nband=(modeConf->fBand*modeConf->Nfft)/modeConf->Fdec;

    quint8 nBADDR=bFft?0x22:0x21;

    qDebug("Init DDC Fdec=%f Fcentr=%f Nband=%d Band=%f", modeConf->Fdec ,
           modeConf->fFcentr, modeConf->Nband, modeConf->fBand);
    //////////
    // формирование команд
    QList<QPoint> args;

    // 1
    {
    qint64 Step=qRound64((1-modeConf->fFcentr/modeConf->fFs)*qPow(2,32));
    int Mode=1;

    args.clear();
    args.append(QPoint(0,Step&0xff));    //PARAM0
    args.append(QPoint(1,(Step>>8)&0xff));    //PARAM1
    args.append(QPoint(2,(Step>>16)&0xff));    //PARAM2
    args.append(QPoint(3,(Step>>24)&0xff));    //PARAM3
    args.append(QPoint(4,Mode));   //MAIN_PARAM младш
    args.append(QPoint(5,0));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,17));  //CMD_NUM
    WriteReg(nBADDR, args);
    }

    //////////////
    {
    // 2

    int Mode=modeConf->Dec_1>1 ? 1 : 0;
    int Atten=48-qCeil(6*(qLn(modeConf->Dec_1)/qLn(2)));
    quint8 Last_Dcm_Val=modeConf->Dec_1-1;

    args.clear();
    args.append(QPoint(0,Last_Dcm_Val));    //PARAM0
    args.append(QPoint(1,0));    //PARAM1
    args.append(QPoint(2,Atten&0x3f));    //PARAM2
    args.append(QPoint(3,0));    //PARAM3
    args.append(QPoint(4,Mode));   //MAIN_PARAM младш
    args.append(QPoint(5,0));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,18));  //CMD_NUM
    WriteReg(nBADDR, args);

    // 3
    if(modeConf->Dec_1!=1)
    {
        double Ap=(6*(qLn(modeConf->Dec_1)/qLn(2)));
        //int Ap=qCeil(6*(qLn(Dec_1)/qLn(2)))-1;
        qreal S1=qPow(2,qCeil(Ap)-Ap-1);
        //qreal S1=qPow(2,-Ap);

        double K[6]={9152.01418415251,
                     -51560.9092863082,
                     107960.6904140670,
                     107960.6904140670,
                     -51560.9092863082,
                     9152.01418415251};

        for(int i=0;i<6;i++)
        {
            int K1=qRound(S1*K[i]);
            args.clear();
            args.append(QPoint(0,K1&0xff));    //PARAM0
            args.append(QPoint(1,(K1>>8)&0xff));    //PARAM1
            args.append(QPoint(2,(K1>>16)&0x03));    //PARAM2
            args.append(QPoint(3,0));    //PARAM3
            args.append(QPoint(4,i&0x0f));   //MAIN_PARAM младш
            args.append(QPoint(5,0));   //MAIN_PARAM старш
            args.append(QPoint(6,0));   //BLOCK_NUM
            args.append(QPoint(7,19));  //CMD_NUM
            WriteReg(nBADDR, args);
        }
    }
    }

    //////////////
    {
    // 4

    int Mode=modeConf->Dec_2>1 ? 1 : 0;
    int Atten=48-qCeil(6*(qLn(modeConf->Dec_2)/qLn(2)));
    quint8 Last_Dcm_Val=modeConf->Dec_2-1;
    quint8 Last_Coef_Addr=5;

    args.clear();
    args.append(QPoint(0,Last_Dcm_Val));    //PARAM0
    args.append(QPoint(1,0));    //PARAM1
    args.append(QPoint(2,Atten&0x3f));    //PARAM2
    args.append(QPoint(3,Last_Coef_Addr));    //PARAM3
    args.append(QPoint(4,Mode));   //MAIN_PARAM младш
    args.append(QPoint(5,0));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,20));  //CMD_NUM
    WriteReg(nBADDR, args);

    // 5
    if(modeConf->Dec_2!=1)
    {
        //int Ap=qCeil(6*(qLn(Dec_2)/qLn(2)))-1;
        double Ap=(6*(qLn(modeConf->Dec_2)/qLn(2)));
        qreal S2=qPow(2,qCeil(Ap)-Ap-1);
        //qreal S2=qPow(2,-Ap);

        double K[6]={9152.01418415251,
                     -51560.9092863082,
                     107960.6904140670,
                     107960.6904140670,
                     -51560.9092863082,
                     9152.01418415251};

        for(int i=0;i<6;i++)
        {
            int K2=qRound(S2*K[i]);
            args.clear();
            args.append(QPoint(0,K2&0xff));    //PARAM0
            args.append(QPoint(1,(K2>>8)&0xff));    //PARAM1
            args.append(QPoint(2,(K2>>16)&0x03));    //PARAM2
            args.append(QPoint(3,0));    //PARAM3
            args.append(QPoint(4,i&0x0f));   //MAIN_PARAM младш
            args.append(QPoint(5,0));   //MAIN_PARAM старш
            args.append(QPoint(6,0));   //BLOCK_NUM
            args.append(QPoint(7,21));  //CMD_NUM
            WriteReg(nBADDR, args);
        }
    }

    }

    // 6
    {
        int Mode=modeConf->Dec_3>1 ? 1 : 0;

        args.clear();
        args.append(QPoint(0,0));    //PARAM0
        args.append(QPoint(1,0));    //PARAM1
        args.append(QPoint(2,0));    //PARAM2
        args.append(QPoint(3,0));    //PARAM3
        args.append(QPoint(4,Mode));   //MAIN_PARAM младш
        args.append(QPoint(5,0));   //MAIN_PARAM старш
        args.append(QPoint(6,0));   //BLOCK_NUM
        args.append(QPoint(7,22));  //CMD_NUM
        WriteReg(nBADDR, args);

    }

    if(!bFft) return; // ddc only
    ///////////////////////////////////////////////////
    // 7
    if(fft_window)
    {
        for(int i=0;i<FFT_SAMPLES_NUM;i++)
        {
            short K=fft_window[i];
            args.clear();
            args.append(QPoint(0,K&0xff));    //PARAM0
            args.append(QPoint(1,(K>>8)&0xff));    //PARAM1
            args.append(QPoint(2,0));    //PARAM2
            args.append(QPoint(3,0));    //PARAM3
            args.append(QPoint(4,i&0xff));   //MAIN_PARAM младш
            args.append(QPoint(5,(i>>8)&0x0f));   //MAIN_PARAM старш
            args.append(QPoint(6,0));   //BLOCK_NUM
            args.append(QPoint(7,24));  //CMD_NUM
            WriteReg(nBADDR, args);
        }
    }

    // 8
    {
        int Mode=2;
        //int Delay=0;

        args.clear();
        args.append(QPoint(0,0));    //PARAM0
        args.append(QPoint(1,0));    //PARAM1
        args.append(QPoint(2,0));    //PARAM2
        args.append(QPoint(3,0));    //PARAM3
        args.append(QPoint(4,Mode));   //MAIN_PARAM младш
        args.append(QPoint(5,0));   //MAIN_PARAM старш
        args.append(QPoint(6,0));   //BLOCK_NUM
        args.append(QPoint(7,25));  //CMD_NUM
        WriteReg(nBADDR, args);

    }

    // 9
    {
        int Mode=0;
        int Order=0;

        args.clear();
        args.append(QPoint(0,0));    //PARAM0
        args.append(QPoint(1,0));    //PARAM1
        args.append(QPoint(2,0));    //PARAM2
        args.append(QPoint(3,0));    //PARAM3
        args.append(QPoint(4,Order));   //MAIN_PARAM младш
        args.append(QPoint(5,Mode));   //MAIN_PARAM старш
        args.append(QPoint(6,0));   //BLOCK_NUM
        args.append(QPoint(7,26));  //CMD_NUM
        WriteReg(nBADDR, args);

    }
}

void RenderThread::enableGEN(bool bEnable)
{
    return; // unused !!!!!

    qDebug() << "enable gen " << bEnable;

    QList<QPoint> args;
    args.clear();
    args.append(QPoint(10, bEnable?0x80:0)); // выбор канала
    WriteReg(16, args);
}

void RenderThread::initGEN()
{
    qDebug("Init GEN Fcentr=%f Mode=%d Amax=%d", modeConf->fFc, modeConf->nGenMode, modeConf->m_nSc);
    QMetaObject::invokeMethod(m_worker, "initGEN",  Qt::QueuedConnection ,
                              Q_ARG(quint8, modeConf->nGenMode),
                              Q_ARG(quint64, (quint64)modeConf->fFc),
                              Q_ARG(quint32, modeConf->m_nSc/*m_nAmax*/),
                              Q_ARG(quint32, 0/*m_nAmin*/),
                              Q_ARG(quint64, (quint64)modeConf->fFmod),
                              Q_ARG(quint64, (quint64)modeConf->fFsym),
                              Q_ARG(quint64, (quint64)modeConf->fFdev),
                              Q_ARG(quint8, modeConf->nSSBType),
                              Q_ARG(qreal, modeConf->fK));
}

// 0 - F1B
// 1 - G1B
// 2 - J3E

void RenderThread::initGEN(quint8 nMode)
{
    return; // unused !!!!!

    if(nMode==0) return;

    double Fdev=modeConf->fFdev;//85.0; // F1B 0.0;//
    double Fmod=modeConf->fFmod;//100.0; // F1B/G1B 0.0;//

    // J3E
    quint8 SSBType=modeConf->nSSBType;//1; // (0 нижняя или 1 верхняя). // todo ...
    double Fb=1000.0; // todo ....
    double dF= SSBType==1 ? Fb : -Fb;

    double Fc=1600000.0; //1.6 Mhz

    quint8 nBADDR=0x23;

    qDebug("Init GEN Fcentr=%f Mode=%d", Fc, nMode);
    //////////
    // формирование команд
    QList<QPoint> args;

    // 1
    {
    qint64 Step = nMode==2 ? qRound64(((Fc+dF)/modeConf->fFs)*qPow(2,32)) : (nMode==1 ?  0 : qRound64((Fc/modeConf->fFs)*qPow(2,32)));
    int Mode=0;

    args.clear();
    args.append(QPoint(0,Step&0xff));    //PARAM0
    args.append(QPoint(1,(Step>>8)&0xff));    //PARAM1
    args.append(QPoint(2,(Step>>16)&0xff));    //PARAM2
    args.append(QPoint(3,(Step>>24)&0xff));    //PARAM3
    args.append(QPoint(4,Mode));   //MAIN_PARAM младш
    args.append(QPoint(5,0));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,17));  //CMD_NUM
    WriteReg(nBADDR, args);
    }

    //////////////
    {
    // 2

    int Mode=0;
    qint64 Dev_Step = nMode==2 ? 0 : ( nMode==1 ? qRound64((Fc/modeConf->fFs)*qPow(2,32)) : qRound64((Fdev/modeConf->fFs)*qPow(2,32)) );

    args.clear();
    args.append(QPoint(0,Dev_Step&0xff));    //PARAM0
    args.append(QPoint(1,(Dev_Step>>8)&0xff));    //PARAM1
    args.append(QPoint(2,(Dev_Step>>16)&0xff));    //PARAM2
    args.append(QPoint(3,(Dev_Step>>24)&0xff));    //PARAM3
    args.append(QPoint(4,Mode));   //MAIN_PARAM младш
    args.append(QPoint(5,0));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,27));  //CMD_NUM
    WriteReg(nBADDR, args);
    }

    //////////////
    {
    // 3

        int Mode=0;
        qint64 Last_SC_NUM = nMode==2 ? 0 : ( nMode==1 ? (qRound64(modeConf->fFs/Fmod)-1)  : (qRound64(modeConf->fFs/Fmod)-1) );

        args.clear();
        args.append(QPoint(0,Last_SC_NUM&0xff));    //PARAM0
        args.append(QPoint(1,(Last_SC_NUM>>8)&0xff));    //PARAM1
        args.append(QPoint(2,(Last_SC_NUM>>16)&0xff));    //PARAM2
        args.append(QPoint(3,(Last_SC_NUM>>24)&0xff));    //PARAM3
        args.append(QPoint(4,Mode));   //MAIN_PARAM младш
        args.append(QPoint(5,0));   //MAIN_PARAM старш
        args.append(QPoint(6,0));   //BLOCK_NUM
        args.append(QPoint(7,28));  //CMD_NUM
        WriteReg(nBADDR, args);
    }

}


void RenderThread::initFFT_Freq(int nPass)
{

    modeConf->Ft0=modeConf->fFs/4.0;
    // вынести в инициализацию
    switch(nPass)
    {
    case 0://1-е приближение частоты
        modeConf->fFcentr=modeConf->Ft0;
        modeConf->fBand=modeConf->fFs/2;
        break;
    case 1://2-е приближение частоты
        modeConf->fFcentr=qRound64(modeConf->Ft1);
        modeConf->fBand=qRound64(modeConf->fFs/1024);
        break;
    case 2://3-е приближение частоты
        modeConf->Ft2=qRound64((modeConf->Ft2*qPow(2,32))/modeConf->fFs)*(modeConf->fFs/qPow(2,32));
        modeConf->fFcentr=qRound64(modeConf->Ft2);//qRound((Ft2*qPow(2,32))/Fs)*(Fs/qPow(2,32));
        modeConf->fBand=qRound64(modeConf->fFs/262144);
        break;
    case 3:
        break;
    }

    modeConf->Fleft=modeConf->fFcentr-modeConf->fBand/2;
    modeConf->Fright=modeConf->fFcentr+modeConf->fBand/2;

    initDDC(true);
}

void RenderThread::initFFT_KNI()
{
    //Fcentr=0.0;
    //Fleft=300.0;
    //Fright=3000.0;
    //Band=2*Fright;
    //window=4;

    initDDC(true);
}

void RenderThread::initDDC_TLG()
{
    quint32 Fsym=100; //todo 	//– символьная скорость (50, 100, 150, 300, 500, 1200, 2400, 4800, 9600, 16000 бод);
    quint8 Nsym=16; 	//– число отсчётов на символ модуляции = 16.
    quint8 Mode=3;   // 0:0 1:-1 2:+1	3:x

    //Band=Fsym*Nsym/2;
    //Fcentr=0.0;
    //Fleft=0.0;
    //Fright=Band;

    initDDC(false);
}

void RenderThread::initFFT_PSOPHO()
{
    modeConf->fFcentr=0.0;
    modeConf->Fleft=300.0;
    modeConf->Fright=3400.0;
    modeConf->fBand=2*modeConf->Fright;
    //window=4;

    initDDC(true);
}

void RenderThread::initFFT_AFC()
{
    modeConf->fFcentr=modeConf->fFc;
    modeConf->fBand=50000.0;

    initDDC(true);
}

void RenderThread::initPLL()
{
    qDebug("initPLL");

    quint8 nBADDR=0x0C;

    //////////
    // формирование команд
    QList<QPoint> args;

    args.clear();
    args.append(QPoint(3, 0));
    args.append(QPoint(1, 0x05));
    m_worker->WriteReg(3, args); //

    //- пауза 0.5 сек
    this->msleep(500);
    //- 0 4 1 0x0C 0x20 0x03 0x80 0xEB
    args.clear();
    args.append(QPoint(0x20, 0x03));
    args.append(QPoint(0x80, 0xEB));
    m_worker->WriteReg(nBADDR, args);

    //- 0 4 1 0x0C 0x21 0x00 0x40 0xEA
    args.clear();
    args.append(QPoint(0x21, 0x00));
    args.append(QPoint(0x40, 0xEA));
    m_worker->WriteReg(nBADDR, args);

    //- 0 4 1 0x0C 0x02 0x00 0x40 0xEA
    args.clear();
    args.append(QPoint(0x02, 0x00));
    args.append(QPoint(0x40, 0xEA));
    m_worker->WriteReg(nBADDR, args);

    //- 0 4 1 0x0C 0x03 0x03 0x02 0xEA
    args.clear();
    args.append(QPoint(0x03, 0x03));
    args.append(QPoint(0x02, 0xEA));
    m_worker->WriteReg(nBADDR, args);
/*
    //- 0 4 1 0x0C 0x14 0x01 0x40 0xEB
    args.clear();
    args.append(QPoint(0x14, 0x01));
    args.append(QPoint(0x40, 0xEB));
    WriteReg(nBADDR, args);
*/
    //- 0 4 1 0x0C 0x14 0x03 0x1E 0xEB
    args.clear();
    args.append(QPoint(0x14, 0x03));
    args.append(QPoint(0x1E, 0xEB));
    m_worker->WriteReg(nBADDR, args);

    //- 0 4 1 0x0C 0xB5 0x0E 0x0C 0x90
    args.clear();
    args.append(QPoint(0xB5, 0x0E));
    args.append(QPoint(0x0C, 0x90));
    m_worker->WriteReg(nBADDR, args);

    //- 0 4 1 0x0C 0x06 0x01 0x8E 0xA4
    args.clear();
    args.append(QPoint(0x06, 0x01));
    args.append(QPoint(0x8E, 0xA4));
    m_worker->WriteReg(nBADDR, args);

    //- 0 4 1 0x0C 0xF7 0x3D 0x93 0x0x95
    args.clear();
    args.append(QPoint(0xF7, 0x3D));
    args.append(QPoint(0x93, 0x95));
    m_worker->WriteReg(nBADDR, args);

    //- 0 4 1 0x0C 0xD8 0x9C 0x00 0x20
    args.clear();
    args.append(QPoint(0xD8, 0x9C));
    args.append(QPoint(0x00, 0x20));
    m_worker->WriteReg(nBADDR, args);

    //- пауза 0.5 сек
    this->msleep(500);
    //- 0 4 1 0x0C 0x06 0x01 0x8E 0xA0
    args.clear();
    args.append(QPoint(0x06, 0x01));
    args.append(QPoint(0x8E, 0xA0));
    m_worker->WriteReg(nBADDR, args);

    //- пауза 0.5 сек
    this->msleep(500);
    //- 0 4 1 0x0C 0x06 0x01 0x8E 0xA4
    args.clear();
    args.append(QPoint(0x06, 0x01));
    args.append(QPoint(0x8E, 0xA4));
    m_worker->WriteReg(nBADDR, args);
    //m_worker->processQueue();
    qDebug("initPLL <<<<<<<<<<<<<<<<<<<<<<");
}

void RenderThread::startDecoding(int nCh)
{
    qDebug("start decoding %i chn",nCh+1);

    QList<QPoint> args;
    args.clear();
    args.append(QPoint(7, 0x80/*|m_mw->m_nCh*/)); // выбор канала
    args.append(QPoint(m_nMeasMode==3||m_nMeasMode==4||m_nMeasMode==5||m_nMeasMode==7||m_nMeasMode==8||m_nMeasMode==10?9:8, nCh)); // выбор канала
    args.append(QPoint(1, 0));           // сброс бита записи
    quint8 start=1;
    switch(m_nMeasMode)
    {
    case 0:
    case 1:
    case 6:
    case 9:
        start=1;
        break;
    case 2:
        start=2;
        break;
    case 3:
    case 4:
    case 5:
    case 7:
    case 8:
    case 10:
        start=4;
        break;
    }
    args.append(QPoint(1,start/*m_mw->m_nMeasMode==0?1:1<<(m_mw->m_nMeasMode-1)*/));           // установка бита записи
    WriteReg(16, args);

    currentData->fill(0);
    m_nReadPtr=0;
    // use timer for getting status updates
}

void RenderThread::readData()
{ // timer for read data
    QList<QPoint> args;
    args.clear();
    args.append(QPoint(3,m_nReadPtr&0xff)); // адрес0
    args.append(QPoint(4,(m_nReadPtr>>8)&0xff)); // адрес1
    args.append(QPoint(5,0x80)); // количество0
    args.append(QPoint(6,0x00)); // количество1
    args.append(QPoint(2,0));           // сброс бита чтения
    quint8 start=1;
    switch(m_nMeasMode)
    {
    case 0:
    case 1:
    case 6:
    case 9:
        start=1;
        break;
    case 2:
        start=2;
        break;
    case 3:
    case 4:
    case 5:
    case 7:
    case 8:
    case 10:
        start=3;
        break;
    }
    args.append(QPoint(2, start/*m_mw->m_nMeasMode==0?1:m_mw->m_nMeasMode*/));           // установка бита чтения
    m_worker->WriteReg(16, args);

    m_worker->readData(this);


    // read
#ifdef asddasd
    if (udp_client->waitForReadyRead(/*-1*/1000))
    {
        while(udp_client->hasPendingDatagrams()) {
            QByteArray datagram;
            datagram.resize(udp_client->pendingDatagramSize());
            QHostAddress sender;
            quint16 senderPort;

            udp_client->readDatagram(datagram.data(), datagram.size(),
                                     &sender, &senderPort);
            //qout << "datagram received from " << sender.toString() << endl;

            processDatagram(datagram);
        }
    }
#endif
}

void RenderThread::WriteReg(quint8 BADDR, const QList<QPoint> &WDATA)
{
//#ifdef qweqwewq
    bool b2byte=false;
    quint16 LDATA=0;
    quint8 CMD=1;

    //if(BADDR>3 && BADDR<8) b2byte=true; // упр АЦП 2байт адрес

    LDATA=b2byte ? WDATA.size()*3 : WDATA.size()*2;

    //QByteArray msg;
    //msg.resize(LDATA+4);
    //unsigned char* msg=(uchar*)datagram.data();


    unsigned char msg[1024]={0};

    //qToBigEndian(LDATA, msg);
    //memcpy(msg, &LDATA, 2);
    msg[0]=LDATA>>8;
    msg[1]=LDATA&0x00ff;
    msg[2]=CMD;
    msg[3]=BADDR;

    for(int i=0;i<WDATA.size();i++)
    {
        QPoint item=WDATA.at(i);

        quint16 a=item.x();
        quint8 d=item.y();

        if(b2byte)
        {
            msg[4+i*3]=a>>8;
            msg[4+i*3+1]=a&0x00FF;
            msg[4+i*3+2]=d;
        }
        else
        {
            msg[4+i*2]=a&0x00FF;
            msg[4+i*2+1]=d;
        }
    }
// todo if !release
    //WriteLog(LDATA, CMD, BADDR, WDATA);
    QByteArray datagram((const char*)msg, LDATA+4);
    //QMetaObject::invokeMethod(m_worker, "writeData",  Qt::BlockingQueuedConnection,
    //                          Q_ARG(QByteArray, datagram) );
    m_worker->writeData(datagram);
#ifdef sdasdasd
    if(udp_client)
        //udp_client->writeDatagram((const char*)msg, LDATA+4, QHostAddress::LocalHost, 50000);
    udp_client->writeDatagram((const char*)msg, LDATA+4, QHostAddress(/*"192.168.100.2"*/m_strOscIp), 50000);
#endif
}

bool RenderThread::processData(int nCh)
{
    bool bRes=false;
    QByteArray *temp;
    if (nCh==0) temp=&m_data;
    if (nCh==1) temp=&m_data2;

    bRes=process->processData(currentData,nCh);
    /*case 0:// глубина ам //iqan
    case 1:// девиация чм  //iqan
    case 2: // вч напряжение  //osci
    case 3: // спектр  //spec
    case 4: // частота  //spec
    case 5: // кни  //spec
    case 6: // тлг //iqan
    case 7: // псоф  //spec
    case 8: // чувствит
    case 9: // чувствит
        bRes=processFftDataSens(nCh);
    case 10:// ачх
        bRes=processFftDataAFC(nCh);*/

    return bRes;
}

bool RenderThread::processDdcData(int nCh)
{
    calculateDeep(nCh);
    return true;
}

bool RenderThread::processDdcData2(int nCh)
{
    calculateDeviation(nCh);
    return true;
}

bool RenderThread::processRawData(int nCh)
{
    static int ma=1;
    if(ma==1)ma=2;
    else ma=1;
    //ui->edit_GlubAM->setText(QString::number(ma,'f', 2));
    qDebug("raw data ready ............ %d ", ma);
    ///if(m_bCycle)startDecoding();
    //else on_pushButton_2_clicked(); // stop meas
    calculatePower(nCh); // todo
    return true;
}

bool RenderThread::processFftData(int nCh)
{

    qDebug("fft data ready ............");

    quint32 y_max=0;
    quint32 x_max=0;
    double f=0.0;
    QByteArray *temp=0;
    if (nCh==0)
        temp=&m_data;
    if (nCh==1)
        temp=&m_data2;
//return;
    // todo switch by data type/mode
    if(m_nReadPtr>=FFT_SAMPLES_NUM)
    {
        // calculate
         // todo
        // перестновка
        str1="";
        str2="";
        str3="";
#ifdef unused
        if(d_plot)
        {
        d_plot->signalData(nCh).clearValues();
        //d_plot->signalData(0).clearValues(); //d_plot[0]->incrementInterval();
        //d_plot->signalData(1).clearValues(); //d_plot[0]->incrementInterval();
        //d_plot->signalData(2).clearValues();
        }
#endif
        double gain=getGainY(nCh);//1.25*1/qPow(2,28);// // todo...
        double gain_mks=getGainX();
        double gain_x=modeConf->fBand/modeConf->Nband;
        QPointF s;
        qDebug()<<"gain_x="<<gain_x<<" Band="<<modeConf->fBand<<" NBand="<<modeConf->Nband<<endl;
        int x=0;
#ifdef gjhkj
        for(int j=0; j<FFT_SAMPLES_NUM; j++)
        {
            s.setX(gain_mks*x);
            x++;

          /*  unsigned short y=m_data[j*4]<<8;
            y|=0xff&m_data[j*4+1];
            short g=m_data[j*4+2]<<8;
            g|=0xff&m_data[j*4+3];  */

            unsigned short y=temp->at(j*4)<<8;
            y|=0xff&temp->at(j*4+1);
            short g=temp->at(j*4+2)<<8;
            g|=0xff&temp->at(j*4+3);

            double dB=20*qLn(/*g**/y/65535)/qLn(10); // ???????
            s.setY(/*dB*/gain*y);

            m_mw->d_plot[0]->signalData(nCh).append(s);
            //m_mw->d_plot[0]->signalData(0).append(s);
        }
#endif
//#ifdef sdfsf
        for(int j=FFT_SAMPLES_NUM-modeConf->Nband/2; j<FFT_SAMPLES_NUM; j++)
        {
            s.setX((modeConf->Fleft+gain_x*x)/1000000);
            x++;

          /*  unsigned int y=m_data[j*4]<<24;
            y|=(0xff&m_data[j*4+1])<<16;
            y|=(0xff&m_data[j*4+2])<<8;
            y|=0xff&m_data[j*4+3]; */

            unsigned int y=temp->at(j*4)<<24;
            y|=(0xff&temp->at(j*4+1))<<16;
            y|=(0xff&temp->at(j*4+2))<<8;
            y|=0xff&temp->at(j*4+3);
/*
            unsigned short y=m_data[j*4]<<8;
            y|=0xff&m_data[j*4+1];
            short g=m_data[j*4+2]<<8;
            g|=0xff&m_data[j*4+3];

            double Kfft=1/(qPow(2,12-g));
*/
            if(y>y_max)
            {
                y_max=y;
                x_max=j;  // or x
                f=((modeConf->Fdec*j)/FFT_SAMPLES_NUM)-modeConf->Fdec;//(Fleft+gain_x*x);//
            }

            double dB=(y==0)?-170:20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=(y==0)?-170:10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????
            //s.setY(/*dB*/gain*y/qPow(2,12-g));
            s.setY(dBV/*gain*y*/);
            //qDebug()<<"FFT x="<<s.x()<< " y=" << s.y()<< ":"<< x<< endl;
#ifdef unused
            if(d_plot)
            {
                d_plot->signalData(nCh).append(s);
            //d_plot->signalData(0).append(s);
            }
#endif
        }

        for(int i=0; i<modeConf->Nband/2; i++)
        {
            s.setX((modeConf->Fleft+gain_x*x)/1000000);
            x++;

          /*  unsigned int y=m_data[i*4]<<24;
            y|=(0xff&m_data[i*4+1])<<16;
            y|=(0xff&m_data[i*4+2])<<8;
            y|=0xff&m_data[i*4+3];  */

            unsigned int y=temp->at(i*4)<<24;
            y|=(0xff&temp->at(i*4+1))<<16;
            y|=(0xff&temp->at(i*4+2))<<8;
            y|=0xff&temp->at(i*4+3);
/*
            unsigned short y=m_data[i*4]<<8;
            y|=0xff&m_data[i*4+1];
            short g=m_data[i*4+2]<<8;
            g|=0xff&m_data[i*4+3];

            double Kfft=1/(qPow(2,12-g));
*/
            if(y>y_max)
            {
                y_max=y;
                x_max=i;  // or x
                f=((modeConf->Fdec*i)/FFT_SAMPLES_NUM)-modeConf->Fdec;//(Fleft+gain_x*x);//
            }

            double dB=(y==0)?-170:20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=(y==0)?-170:10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????
            //s.setY(/*dB*/gain*y/qPow(2,12-g));
            s.setY(dBV/*gain*y*/);
            //qDebug()<<"FFT x="<<s.x()<< " y=" << s.y()<< ":"<< x<<  endl;
#ifdef unused
            if(d_plot)
            {
                d_plot->signalData(nCh).append(s);
            //d_plot->signalData(0).append(s);
            }
#endif
        }
//#endif

        qDebug()<<"FFT x_max="<<x_max<< " y_max=" << y_max<< ":f="<< f<<  endl;

    }
    return true;
}

bool RenderThread::processFftDataFreq(int nCh)
{

    //qDebug("fft freq data ready ............");

    quint32 y_max=0;
    quint32 x_max=0;
    double f=0.0;

    QByteArray *temp=0;
    if (nCh==0)
        temp=&m_data;
    if (nCh==1)
        temp=&m_data2;
//return;
    // todo switch by data type/mode
    if(m_nReadPtr>=FFT_SAMPLES_NUM)
    {
        // calculate
         // todo
        // перестновка
        str1="";
        str2="";
        str3="";

#ifdef unused
        if(d_plot)
        {
        d_plot->signalData(nCh).clearValues(); //d_plot[0]->incrementInterval();
        //d_plot->signalData(1).clearValues(); //d_plot[0]->incrementInterval();
        //d_plot->signalData(2).clearValues();
        }
#endif
        double gain=getGainY(nCh);//1.25*1/qPow(2,28);// // todo...
        double gain_mks=getGainX();
        double gain_x=/*m_mw->*/modeConf->fBand/modeConf->Nband;
        QPointF s;
        //qDebug()<<"gain_x="<<gain_x<<" Band="<<Band<<" NBand="<<Nband<<endl;
        int x=0;
#ifdef gjhkj
        for(int j=0; j<FFT_SAMPLES_NUM; j++)
        {
            s.setX(gain_mks*x);
            x++;

           /* unsigned short y=m_data[j*4]<<8;
            y|=0xff&m_data[j*4+1];
            short g=m_data[j*4+2]<<8;
            g|=0xff&m_data[j*4+3];  */

            unsigned short y=temp->at(j*4)<<8;
            y|=0xff&temp->at(j*4+1);
            short g=temp->at(j*4+2)<<8;
            g|=0xff&temp->at(j*4+3);

            double dB=20*qLn(/*g**/y/65535)/qLn(10); // ???????
            s.setY(/*dB*/gain*y);

            m_mw->d_plot[0]->signalData(nCh).append(s);
        }
#endif
//#ifdef sdfsf
        for(int j=FFT_SAMPLES_NUM-modeConf->Nband/2; j<FFT_SAMPLES_NUM; j++)
        {
            s.setX(x);

/*
            unsigned int y=m_data[j*4]<<24;
            y|=(0xff&m_data[j*4+1])<<16;
            y|=(0xff&m_data[j*4+2])<<8;
            y|=0xff&m_data[j*4+3]; */

            unsigned int y=temp->at(j*4)<<24;
            y|=(0xff&temp->at(j*4+1))<<16;
            y|=(0xff&temp->at(j*4+2))<<8;
            y|=0xff&temp->at(j*4+3);

            if(y>y_max)
            {
                y_max=y;
                x_max=j;  // or x
                f=((modeConf->Fdec*j)/FFT_SAMPLES_NUM)-modeConf->Fdec;//(Fleft+gain_x*x);//
            }
            x++;
/*
            unsigned short y=m_data[j*4]<<8;
            y|=0xff&m_data[j*4+1];
            short g=m_data[j*4+2]<<8;
            g|=0xff&m_data[j*4+3];

            double Kfft=1/(qPow(2,12-g));
*/
            double dB=20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????
            //s.setY(/*dB*/gain*y/qPow(2,12-g));
            s.setY(dBV/*gain*y*/);
            //qDebug()<<"FFT x="<<s.x()<< " y=" << s.y()<< ":"<< x<< endl;
#ifdef unused
            if(d_plot)
            {
            d_plot->signalData(nCh).append(s);
            }
#endif
        }

        for(int i=0; i<modeConf->Nband/2; i++)
        {
            s.setX(x);

/*
            unsigned int y=m_data[i*4]<<24;
            y|=(0xff&m_data[i*4+1])<<16;
            y|=(0xff&m_data[i*4+2])<<8;
            y|=0xff&m_data[i*4+3];  */

            unsigned int y=temp->at(i*4)<<24;
            y|=(0xff&temp->at(i*4+1))<<16;
            y|=(0xff&temp->at(i*4+2))<<8;
            y|=0xff&temp->at(i*4+3);

            if(y>y_max)
            {
                y_max=y;
                x_max=i;  // or x
                f=(modeConf->Fdec*i)/FFT_SAMPLES_NUM;//(Fleft+gain_x*x);//
            }
            x++;
/*
            unsigned short y=m_data[i*4]<<8;
            y|=0xff&m_data[i*4+1];
            short g=m_data[i*4+2]<<8;
            g|=0xff&m_data[i*4+3];

            double Kfft=1/(qPow(2,12-g));
*/
            double dB=20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????
            //s.setY(/*dB*/gain*y/qPow(2,12-g));
            s.setY(dBV/*gain*y*/);
            //qDebug()<<"FFT x="<<s.x()<< " y=" << s.y()<< ":"<< x<<  endl;
#ifdef unused
            if(d_plot)
            {
            d_plot->signalData(nCh).append(s);
            }
#endif
        }
//#endif
    }

    if(modeConf->nPass==0)
    {
        modeConf->dFt1=f;
        modeConf->Ft1=modeConf->Ft0+modeConf->dFt1;//f;//
        str1=QString::number(modeConf->Ft1,'f', 2);
        qDebug("pass 1 dFt1=%f Ft0=%f Ft1=%f x_max=%d", modeConf->dFt1 , modeConf->Ft0 ,modeConf->Ft1, x_max);
    }
    else if(modeConf->nPass==1)
    {
        modeConf->dFt2=f;
        modeConf->Ft2=modeConf->Ft1+modeConf->dFt2;//f;//
        str2=QString::number(modeConf->Ft2,'f', 2);
        qDebug("pass 2 dFt2=%f Ft1=%f Ft2=%f x_max=%d", modeConf->dFt2 , modeConf->Ft1 ,modeConf->Ft2, x_max);
    }
    else if(modeConf->nPass==2)
    {
        modeConf->dFt3=f;
        modeConf->Ft3=modeConf->Ft2+modeConf->dFt3;//f;//
        str3=QString::number(modeConf->Ft3,'f', 2);
        qDebug("pass 3 dFt3=%f Ft2=%f Ft3=%f x_max=%d y_max=%d", modeConf->dFt3 , modeConf->Ft2 ,modeConf->Ft3, x_max, y_max);
    }
/*
    str3=QString::number(Ft3,'f', 2);
    str2=QString::number(Ft2,'f', 2);
    str1=QString::number(Ft1,'f', 2);
*/
    return modeConf->nPass==2? true: false;
}

bool RenderThread::processFftDataKNI(int nCh)
{

    qDebug("fft kni data ready ............");

    unsigned int F[FFT_SAMPLES_NUM]={0};

    quint32 y_max=0;
    quint32 g=0;
    double f=0.0;
    QByteArray *temp=0;
    if (nCh==0)
        temp=&m_data;
    if (nCh==1)
        temp=&m_data2;

    const quint8 dF=8;
    int N=32;
    quint64 U[32]={0};
    quint64 S0=0;
    quint64 S1=0;
    int a_left=qRound((modeConf->Fleft*modeConf->Nfft)/modeConf->Fdec);//10;//
    int a_right=qRound((modeConf->Fright*modeConf->Nfft)/modeConf->Fdec);//3000;//
    if(a_left==0)a_left=1;
//return;
    // todo switch by data type/mode
    if(m_nReadPtr>=FFT_SAMPLES_NUM)
    {
        // calculate
         // todo
        // перестновка
        str1="";
        str2="";
        str3="";

#ifdef unused
        if(d_plot)
        {
        d_plot->signalData(nCh).clearValues();
        //d_plot->signalData(0).clearValues(); //d_plot[0]->incrementInterval();
        //d_plot->signalData(1).clearValues(); //d_plot[0]->incrementInterval();
        //d_plot->signalData(2).clearValues();
        }
#endif
        double gain=getGainY(nCh);//1.25*1/qPow(2,28);// // todo...
        double gain_mks=getGainX();
        double gain_x=/*m_mw->*/modeConf->fBand/modeConf->Nband;
        QPointF s;
/*
        for(int i=0; i<FFT_SAMPLES_NUM; i++)
        {
            unsigned int y=m_data[i*4]<<24;
            y|=(0xff&m_data[i*4+1])<<16;
            y|=(0xff&m_data[i*4+2])<<8;
            y|=0xff&m_data[i*4+3];

            F[i]=y;
        }*/
        int m=0;
        for(int jj=FFT_SAMPLES_NUM-modeConf->Nband/2; jj<FFT_SAMPLES_NUM; jj++)
        {
            /*unsigned int y=m_data[jj*4]<<24;
            y|=(0xff&m_data[jj*4+1])<<16;
            y|=(0xff&m_data[jj*4+2])<<8;
            y|=0xff&m_data[jj*4+3]; */

            unsigned int y=temp->at(jj*4)<<24;
            y|=(0xff&temp->at(jj*4+1))<<16;
            y|=(0xff&temp->at(jj*4+2))<<8;
            y|=0xff&temp->at(jj*4+3);

            F[m]=y;
            m++;

            double f=modeConf->fFcentr+((modeConf->Fdec*jj)/FFT_SAMPLES_NUM)-modeConf->Fdec;
            double dB=(y==0)?-170:20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=(y==0)?-170:10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????

            s.setX(f/1000000);
            s.setY(dBV/*gain*y*/);
            //qDebug()<<"FFT x="<<s.x()<< " y=" << s.y()<< ":"<< m<< endl;
#ifdef unused
            if(d_plot) d_plot->signalData(nCh).append(s);
            //if(d_plot) d_plot->signalData(0).append(s);
#endif
        }

        for(int ii=0; ii<modeConf->Nband/2; ii++)
        {
           /* unsigned int y=m_data[ii*4]<<24;
            y|=(0xff&m_data[ii*4+1])<<16;
            y|=(0xff&m_data[ii*4+2])<<8;
            y|=0xff&m_data[ii*4+3];  */

            unsigned int y=temp->at(ii*4)<<24;
            y|=(0xff&temp->at(ii*4+1))<<16;
            y|=(0xff&temp->at(ii*4+2))<<8;
            y|=0xff&temp->at(ii*4+3);

            F[m]=y;
            m++;

            double f=modeConf->fFcentr+(modeConf->Fdec*ii)/FFT_SAMPLES_NUM;
            double dB=(y==0)?-170:20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=(y==0)?-170:10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????

            s.setX(f/1000000);
            s.setY(dBV/*gain*y*/);
            //qDebug()<<"FFT x="<<s.x()<< " y=" << s.y()<< ":"<< m<< endl;
#ifdef unused
            if(d_plot) d_plot->signalData(nCh).append(s);
            //if(d_plot) d_plot->signalData(0).append(s);
#endif
        }
/////////////////
//        makeTestDataFFT(F);
//        m=4096;
///////////////
        a_left=1;
        a_right=m-1;
        for(int n=0; n<N; n++)
        {
            y_max=0;
            g=a_left;
            for(int j=a_left; j<=a_right; j++)
            {
                if(F[j]>y_max)
                {
                    y_max=F[j];
                    g=j;

                }

            }

            int g_left=g-dF/2;
            int g_right=g+dF/2;
            if(g_left<a_left)g_left=a_left;
            if(g_right>a_right)g_right=a_right;

            for(int x=g_left; x<=g_right; x++)
            {
                U[n]+=F[x];
                F[x]=0;
            }
            qDebug()<<n<<" ymax="<<y_max<<" g="<< g << " "<< g_left << " " << g_right;

            S0+=qPow(U[n],2);
            if(n>0)S1+=qPow(U[n],2);
        }

        qreal Kgi=qSqrt(S1)/U[0];
        qreal Kni=qSqrt(S1)/qSqrt(S0);
        qreal SINAD=20*(qLn(U[0]/qSqrt(S1)/*Kgi*/)/qLn(10));

        str3=QString::number(SINAD,'f', 3);
        str2=QString::number(100*Kgi,'f', 3);
        str1=QString::number(100*Kni,'f', 3);

        qDebug()<< "SINAD=" << SINAD <<" Kgi=" << Kgi << " Kni=" << Kni;
    }
    return true;
}

bool RenderThread::processDdcDataTLG(int nCh)
{
    double gain=getGainY(nCh);
    double gain_mks=getGainX();
    double Rmin=65767.0;
    double Rmax=-65767.0;
    double M=0.0; // avg
    double Mmin=0.0;
    double Mmax=0.0;

    QByteArray *temp=0;
    if (nCh==0)
        temp=&m_data;
    if (nCh==1)
        temp=&m_data2;

    short re_a[RAW_SAMPLES_NUM]={0};
    int   l[RAW_SAMPLES_NUM]={0};
    int Lc=0;

    double ss=0.0;
    double ss2=0.0;
#ifdef unused
    if(d_plot)
    {
    d_plot->signalData(nCh).clearValues();
    //d_plot->signalData(0).clearValues();
    //d_plot->signalData(1).clearValues();
    //d_plot->signalData(2).clearValues();
    }
#endif

    QPointF s;
    QPointF s1;

    for(int i=0; i<RAW_SAMPLES_NUM; i++)
    {
        s.setX(gain_mks*i);
        s1.setX(gain_mks*i);

      /*  short im=m_data[i*4]<<8;
        im |= 0xff&m_data[i*4+1];
        short re=m_data[i*4+2]<<8;
        re |= 0xff&m_data[i*4+3]; */

        short im=temp->at(i*4)<<8;
        im |= 0xff&temp->at(i*4+1);
        short re=temp->at(i*4+2)<<8;
        re |= 0xff&temp->at(i*4+3);

        re_a[i]=re;
        M+=re;

        double A=qSqrt(qPow(re,2)+qPow(im,2));

        s.setY(gain*re/*val*/);
        s1.setY(gain*A/*re*//*val2*/);
#ifdef unused
        if(d_plot)
        {
        d_plot->signalData(nCh).append(s);
        //d_plot->signalData(1).append(s1);
        }
#endif

    }

    M=M/RAW_SAMPLES_NUM;

    for(int i1=0; i1<RAW_SAMPLES_NUM; i1++)
    {
        re_a[i1]=re_a[i1]-M;
    }

    // определяем количество переходов через 0
    for(int i2=0; i2<RAW_SAMPLES_NUM-1; i2++)
    {
        if(re_a[i2]<=0 && re_a[i2+1]>0)
        {
            l[Lc]=i2;
            Lc++;
        }
    }

    for(int k=0; k<Lc; k++)
    {
        Rmin=65767.0;
        Rmax=-65767.0;

        for(int m=l[k]; m<l[k+1]; m++)
        {
            if(re_a[m]<Rmin)Rmin=re_a[m];
            if(re_a[m]>Rmax)Rmax=re_a[m];
        }
        Mmin+=Rmin;
        Mmax+=Rmax;
    }
    Mmin=Mmin/Lc;
    Mmax=Mmax/Lc;

    quint8 Mode=3;//0;//   // 0:0 1:-1 2:+1	3:x
    double Bound=0.0;

    if(Mode==0)
    {
       Bound=0.0;
    }
    else if(Mode==1)
    {
        Bound=Mmin/2;
    }
    else if(Mode==2)
    {
        Bound=Mmax/2;
    }
    else if(Mode==3)
    {
        if(Mmin>-Mmax/2) Bound=Mmax/2;
        if(Mmax<-Mmin/2) Bound=Mmin/2;
    }

    double T1=0.0;
    double T0=0.0;
    for(int m=l[0]; m<l[Lc-1]; m++)
    {
        if(re_a[m]<Bound)T0++;
        if(re_a[m]>=Bound)T1++;
    }


    double mk=qAbs(100*(T1-T0)/(T1+T0));
    qDebug() << "КВП " << mk << endl;


    //ui->edit_GlubAM->setText(QString::number(m,'f', 2));
    str1=QString::number(mk,'f', 2);
    str2="";//QString::number(SKZ,'f', 2);
    str3="";//QString::number(Am,'f', 2);;
    /////////////////////////////////////

    return true;
}

bool RenderThread::processFftDataPSOPHO(int nCh)
{
    qDebug("fft psopho data ready ............");

    const double Ppsoph[32]={
    /*300*/     0.295,
    /*400*/     0.484,
    /*500*/     0.661,
    /*600*/     0.794,
    /*700*/     0.902,
    /*800*/     1.0,
    /*900*/     1.072,
    /*1000*/	1.22,
    /*1100*/	1.072,
    /*1200*/	1.0,
    /*1300*/	0.955,
    /*1400*/	0.905,
    /*1500*/	0.861,
    /*1600*/	0.824,
    /*1700*/	0.791,
    /*1800*/	0.76,
    /*1900*/	0.723,
    /*2000*/	0.708,
    /*2100*/	0.689,
    /*2200*/	0.67,
    /*2300*/	0.652,
    /*2400*/	0.634,
    /*2500*/	0.617,
    /*2600*/	0.598,
    /*2700*/	0.58,
    /*2800*/	0.562,
    /*2900*/	0.543,
    /*3000*/	0.525,
    /*3100*/	0.501,
    /*3200*/	0.473,
    /*3300*/	0.444,
    /*3400*/	0.412};

    unsigned int F[FFT_SAMPLES_NUM]={0};

    int N=32;
    double U[32]={0};
    double S=0.0;

    QByteArray *temp=0;
    if (nCh==0)
        temp=&m_data;
    if (nCh==1)
        temp=&m_data2;

    // calculate
    str1="";
    str2="";
    str3="";

#ifdef unused
    if(d_plot)
    {
        d_plot->signalData(nCh).clearValues();
        //d_plot->signalData(0).clearValues();
        //d_plot->signalData(1).clearValues();
        //d_plot->signalData(2).clearValues();
    }
#endif
    double gain=getGainY(nCh);
    double gain_mks=getGainX();
    double gain_x=modeConf->fBand/modeConf->Nband;

    QPointF s;

    int m=0;
    for(int jj=FFT_SAMPLES_NUM-modeConf->Nband/2; jj<FFT_SAMPLES_NUM; jj++)
    {
        s.setX(m);

      /*  unsigned int y=m_data[jj*4]<<24;
        y|=(0xff&m_data[jj*4+1])<<16;
        y|=(0xff&m_data[jj*4+2])<<8;
        y|=0xff&m_data[jj*4+3];  */

        unsigned int y=temp->at(jj*4)<<24;
        y|=(0xff&temp->at(jj*4+1))<<16;
        y|=(0xff&temp->at(jj*4+2))<<8;
        y|=0xff&temp->at(jj*4+3);

        F[m]=y;
        m++;

        double dB=20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
        double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
        double dBm=10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????

        s.setY(dBV);

#ifdef unused
        if(d_plot)
        {
        d_plot->signalData(nCh).append(s);
        }
#endif
    }

    for(int ii=0; ii<modeConf->Nband/2; ii++)
    {
        s.setX(m);

     /*   unsigned int y=m_data[ii*4]<<24;
        y|=(0xff&m_data[ii*4+1])<<16;
        y|=(0xff&m_data[ii*4+2])<<8;
        y|=0xff&m_data[ii*4+3];  */

        unsigned int y=temp->at(ii*4)<<24;
        y|=(0xff&temp->at(ii*4+1))<<16;
        y|=(0xff&temp->at(ii*4+2))<<8;
        y|=0xff&temp->at(ii*4+3);

        F[m]=y;
        m++;

        double dB=20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
        double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
        double dBm=10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????

        s.setY(dBV);

#ifdef unused
        if(d_plot)
        {
        d_plot->signalData(nCh).append(s);
        }
#endif
    }


    for(int n=0; n<N; n++)
    {
        int a=qRound(modeConf->Nfft*(300+100*n)/modeConf->Fdec);
        if(a>=0 && a<FFT_SAMPLES_NUM)
            modeConf->m_Fa[n]+=F[a];

        if(modeConf->nPass==16-1)
        {
            modeConf->m_Fa[n]=modeConf->m_Fa[n]/16;
            U[n]=gain*modeConf->m_Fa[n];          // ????? перевод в вольты
            S+=qPow(Ppsoph[n]*U[n],2);
        }
    }


    if(modeConf->nPass==16-1)
    {
        qreal Upsoph_800=qSqrt(S);

        str3="";
        str2="";
        str1=QString::number(Upsoph_800,'f', 3);
    }

    return modeConf->nPass==16-1? true: false;
}

bool RenderThread::processFftDataSens(int nCh)
{
    qDebug("SENS data ready ............");
    // todo ... averaging
    if(modeConf->nGenMode==DDS_A1A ||
            modeConf->nGenMode==DDS_F1B ||
            modeConf->nGenMode==DDS_G1B)
    {
        if(processDdcDataTLG(nCh))
        {
            double Kvp=str1.toDouble();
            qDebug("SENS: Kvp=%f Kvp_bound=%f",Kvp,modeConf->m_fKvp_bound);
            qDebug("SENS: m_nSc=%d m_nSmin=%d m_nSmax=%d", modeConf->m_nSc, modeConf->m_nSmin, modeConf->m_nSmax);
            if(Kvp>modeConf->m_fKvp_bound && Kvp<100.0)// todo
            {
                modeConf->m_nSmax=modeConf->m_nSc;
                modeConf->m_nSc=modeConf->m_nSmax-(modeConf->m_nSmax-modeConf->m_nSmin)/2;
                qDebug("SENS: 1: m_nSc=%d m_nSmin=%d m_nSmax=%d", modeConf->m_nSc, modeConf->m_nSmin, modeConf->m_nSmax);
            }
            else
            {
                modeConf->m_nSmin=modeConf->m_nSc;
                modeConf->m_nSc=modeConf->m_nSmin+(modeConf->m_nSmax-modeConf->m_nSmin)/2;
                qDebug("SENS: 2: m_nSc=%d m_nSmin=%d m_nSmax=%d", modeConf->m_nSc, modeConf->m_nSmin, modeConf->m_nSmax);
            }

        }
    }
    else
    {
        if(processFftDataKNI(nCh))
        {  
            //qreal Kgi=str2.toDouble();
            //qreal Kni=str1.toDouble();
            qreal SINAD=str3.toDouble();

            qDebug("SENS: SINAD=%f SINAD_bound=%f",SINAD,modeConf->m_fSINAD_bound);
            qDebug("SENS: m_nSc=%d m_nSmin=%d m_nSmax=%d", modeConf->m_nSc, modeConf->m_nSmin, modeConf->m_nSmax);
            if(SINAD>=modeConf->m_fSINAD_bound)// todo
            {
                modeConf->m_nSmax=modeConf->m_nSc;
                modeConf->m_nSc=modeConf->m_nSmax-(modeConf->m_nSmax-modeConf->m_nSmin)/2;
                qDebug("SENS: 1: m_nSc=%d m_nSmin=%d m_nSmax=%d", modeConf->m_nSc, modeConf->m_nSmin, modeConf->m_nSmax);
            }
            else
            {
                modeConf->m_nSmin=modeConf->m_nSc;
                modeConf->m_nSc=modeConf->m_nSmin+(modeConf->m_nSmax-modeConf->m_nSmin)/2;
                qDebug("SENS: 2: m_nSc=%d m_nSmin=%d m_nSmax=%d", modeConf->m_nSc, modeConf->m_nSmin, modeConf->m_nSmax);
            }

        }
    }

    if(modeConf->m_nSc-modeConf->m_nSmin < 17 || modeConf->m_nSmax-modeConf->m_nSc < 17)
    {
        qreal U=(qreal)modeConf->m_nSc*25.0/16383.0;
        //qreal dBmkV = (U==0.0)? -170 : 20*qLn(U)/qLn(10);
        //qreal dB = (U==0.0)? -170 : (20*qLn(U)/qLn(10)-107);
        str1=QString::number(U,'f', 3);
        str2="";//QString::number(dB,'f', 3);
        str3="";//QString::number(dBmkV,'f', 3);
        qDebug("SENS: quit !!!!!!!!!!!!!!!!!!");
        return true;
    }

    return false;
}

bool RenderThread::processFftDataAFC(int nCh)
{
    qDebug("fft AFC data ready ............");

    unsigned int F[FFT_SAMPLES_NUM]={0};

    quint32 y_max=0;
    quint32 g=0;
    double f=0.0;

    QByteArray *temp=0;
    if (nCh==0)
        temp=&m_data;
    if (nCh==1)
        temp=&m_data2;

    const quint8 dF=8;
    int N=32;
    quint64 U[32]={0};
    quint64 S0=0;
    quint64 S1=0;
    int a_left=qRound((modeConf->Fleft*modeConf->Nfft)/modeConf->Fdec);//10;//
    int a_right=qRound((modeConf->Fright*modeConf->Nfft)/modeConf->Fdec);//3000;//
    if(a_left==0)a_left=1;
//return;

    if(m_nReadPtr>=FFT_SAMPLES_NUM)
    {
        // calculate
        str1="";
        str2="";
        str3="";

        if(modeConf->nPass==0)
        {
#ifdef unused
        if(d_plot)
        {
        d_plot->signalData(nCh).clearValues(); //d_plot[0]->incrementInterval();
        //d_plot->signalData(1).clearValues(); //d_plot[0]->incrementInterval();
        //d_plot->signalData(2).clearValues();
        }
#endif
        }
        double gain=getGainY(nCh);//1.25*1/qPow(2,28);// // todo...
        double gain_mks=getGainX();
        double gain_x=/*m_mw->*/modeConf->fBand/modeConf->Nband;
        QPointF s;

        int m=0;
        for(int jj=FFT_SAMPLES_NUM-modeConf->Nband/2; jj<FFT_SAMPLES_NUM; jj++)
        {
           /* unsigned int y=m_data[jj*4]<<24;
            y|=(0xff&m_data[jj*4+1])<<16;
            y|=(0xff&m_data[jj*4+2])<<8;
            y|=0xff&m_data[jj*4+3];*/

            unsigned int y=temp->at(jj*4)<<24;
            y|=(0xff&temp->at(jj*4+1))<<16;
            y|=(0xff&temp->at(jj*4+2))<<8;
            y|=0xff&temp->at(jj*4+3);

            F[m]=y;
            m++;
        }

        for(int ii=0; ii<modeConf->Nband/2; ii++)
        {
           /* unsigned int y=m_data[ii*4]<<24;
            y|=(0xff&m_data[ii*4+1])<<16;
            y|=(0xff&m_data[ii*4+2])<<8;
            y|=0xff&m_data[ii*4+3];*/

            unsigned int y=temp->at(ii*4)<<24;
            y|=(0xff&temp->at(ii*4+1))<<16;
            y|=(0xff&temp->at(ii*4+2))<<8;
            y|=0xff&temp->at(ii*4+3);

            F[m]=y;
            m++;
        }
/////////////////
//        makeTestDataFFT(F);
//        m=4096;
///////////////
        a_left=1;
        a_right=m-1;
        for(int n=0; n<1/*N*/; n++)
        {
            y_max=0;
            g=a_left;
            for(int j=a_left; j<=a_right; j++)
            {
                if(F[j]>y_max)
                {
                    y_max=F[j];
                    g=j;
                }

            }

            int g_left=g-dF/2;
            int g_right=g+dF/2;
            if(g_left<a_left)g_left=a_left;
            if(g_right>a_right)g_right=a_right;

            for(int x=g_left; x<=g_right; x++)
            {
                U[n]+=F[x];
                F[x]=0;
            }
            qDebug()<<n<<" ymax="<<y_max<<" g="<< g << " "<< g_left << " " << g_right;

        }


        qreal dB=20*(qLn(0.125/U[0])/qLn(10)); // todo учесть затухание !!!!!!!!
        modeConf->m_FdB[modeConf->nPass]=dB;

        qDebug()<< "dB=" << dB ;

        s.setX(modeConf->fFc/1000000);
        s.setY(dB);
#ifdef unused
        if(d_plot) d_plot->signalData(nCh).append(s);
#endif

        if(modeConf->nPass==modeConf->nPassNum-1)
        {
            qreal max_dB=modeConf->m_FdB[0];
            qreal min_dB=modeConf->m_FdB[0];
            for(int n=0; n<modeConf->nPassNum; n++)
            {
                if(modeConf->m_FdB[n]<min_dB) min_dB=modeConf->m_FdB[n];
                if(modeConf->m_FdB[n]>max_dB) max_dB=modeConf->m_FdB[n];
            }

            qreal NonLinear=max_dB-min_dB;
            qDebug()<< "NonLinear AFC dB=" << NonLinear ;

            str3="";
            str2="";
            str1=QString::number(NonLinear,'f', 3);
        }

    }

    return modeConf->nPass==modeConf->nPassNum-1? true: false;
}

void RenderThread::processDatagram(const QByteArray &datagram)
{
    if(!this->isRunning())return;

    //mutex.lock();

    //return;
    quint8 FRAMEID=datagram[0];

    switch(FRAMEID)
    {
    case 1: // register data
        break;
    case 2: // osc
        processOscDatagram(datagram);
        break;
    case 3: // ddc
        processDdcDatagram(datagram);
        break;
    case 4: // spect
         processFftDatagram(datagram);
        break;
    case 5: // raw adc
        processRawDatagram(datagram);
        break;
    }
    // todo if !release
    //WriteLog(datagram);

     //mutex.unlock();
}

//void RenderThread::processDatagram(const char *datagram)
//{
//    if(!this->isRunning())return;
//    QByteArray tempDatagram(datagram);
//    //mutex.lock();

//    //return;
//    quint8 FRAMEID=tempDatagram[0];

//    switch(FRAMEID)
//    {
//    case 1: // register data
//        break;
//    case 2: // osc
//        processOscDatagram(tempDatagram);
//        break;
//    case 3: // ddc
//        processDdcDatagram(tempDatagram);
//        break;
//    case 4: // spect
//         processFftDatagram(tempDatagram);
//        break;
//    case 5: // raw adc
//        processRawDatagram(tempDatagram);
//        break;
//    }
//    // todo if !release
//    //WriteLog(datagram);

//     //mutex.unlock();
//}

void RenderThread::processOscDatagram(QByteArray datagram)
{
    quint8 FDATA=datagram[3];
    bool b16bit=false;

    if(FDATA&0x07 > 0) // 16 bit data
        b16bit=true;
    //else // 8 bit data

    quint8 nCh=(FDATA&0x10) > 0 ? 2 : 1; // chan num


}

void RenderThread::processDdcDatagram(QByteArray datagram)
{
    quint16 addr=datagram[1]<<8;
    addr |= 0xff&datagram[2];

    quint16 len=datagram[3]<<8;
    len |= 0xff&datagram[4];

    qDebug() << "DDC1 data recieved: ADDR=" << addr << " LEN=" << len << " READ_PTR=" << m_nReadPtr << endl;

    if(len>0)
    {
        if(m_nReadPtr!=addr)
            qDebug() << "DDC1 data recieved: error addr\n";
        else
        {
            currentData->replace(m_nReadPtr*4, datagram.size()-5, datagram.right(datagram.size()-5));
            m_nReadPtr+=len;
        }
    }
}

void RenderThread::processFftDatagram(QByteArray datagram)
{
    quint16 addr=datagram[1]<<8;
    addr |= 0xff&datagram[2];

    quint16 len=datagram[3]<<8;
    len |= 0xff&datagram[4];

    //m_nFftGain=datagram[5];

    qDebug() << "FFT1 data recieved: ADDR=" << addr << " LEN=" << len << " READ_PTR=" << m_nReadPtr << endl;

    if(len>0)
    {
        if(m_nReadPtr!=addr)
            qDebug() << "FFT1 data recieved: error addr\n";
        else
        {
            currentData->replace(m_nReadPtr*4, datagram.size()-5, datagram.right(datagram.size()-5));
            m_nReadPtr+=len;
        }
    }
}

void RenderThread::processRawDatagram(QByteArray datagram)
{
    quint16 addr=datagram[1]<<8;
    addr |= 0xff&datagram[2];

    quint16 len=datagram[3]<<8;
    len |= 0xff&datagram[4];

    qDebug() << "RAW data recieved: ADDR=" << addr << " LEN=" << len << " READ_PTR=" << m_nReadPtr <<endl;

    if(len>0)
    {
        if(m_nReadPtr!=addr)
            qDebug() << "RAW data recieved: error addr\n";
        else
        {
            currentData->replace(m_nReadPtr*4, datagram.size()-5, datagram.right(datagram.size()-5));
            m_nReadPtr+=len;
        }
    }
}

void RenderThread::calculateDeep(int nCh)
{
    double gain=getGainY(nCh);
    double gain_mks=getGainX();
    double Amin=65767.0;
    double Amax=-65767.0;
    QByteArray *temp=0;
    if (nCh==0)
        temp=&m_data;
    if (nCh==1)
        temp=&m_data2;

    double ss=0.0;
    double ss2=0.0;
#ifdef unused
    if(d_plot)
    {
    d_plot->signalData(nCh).clearValues();
    }
#endif

    QPointF s;
    QPointF s1;

    for(int i=/*5*/0; i<RAW_SAMPLES_NUM; i++)
    {
        s.setX(gain_mks*i);
        //s1.setX(gain_mks*i);

     /*
        short im=m_data[i*4]<<8;
        im |= 0xff&m_data[i*4+1];
        short re=m_data[i*4+2]<<8;
        re |= 0xff&m_data[i*4+3];

        //------------
        short im2=m_data2[i*4]<<8;
        im2 |= 0xff&m_data2[i*4+1];
        short re2=m_data2[i*4+2]<<8;
        re2 |= 0xff&m_data2[i*4+3];
        //-------------
        */

        short im=temp->at(i*4)<<8;
        im |= 0xff&temp->at(i*4+1);
        short re=temp->at(i*4+2)<<8;
        re |= 0xff&temp->at(i*4+3);

        double A=qSqrt(qPow(re,2)+qPow(im,2));
        if(A<Amin)Amin=A;
        if(A>Amax)Amax=A;

        s.setY(gain*re/*val*/);
        //s1.setY(gain*re2/*re*//*val2*/);
#ifdef unused
        if(d_plot)
        {
         /*   if (m_Ch1)
                d_plot->signalData(0).append(s);
            if (m_Ch2)
                d_plot->signalData(1).append(s1);
         */
            d_plot->signalData(nCh).append(s);
        }
#endif

        double val=gain*re;

        ss+=val*val;
    }

    double m=100*(Amax-Amin)/(Amax+Amin);
    qDebug() << "Глубина модуляции " << m << endl;

    double SKZ=qSqrt(ss/RAW_SAMPLES_NUM);

    double Am=gain*Amax;

    //ui->edit_GlubAM->setText(QString::number(m,'f', 2));
    str1=QString::number((int)m);//,'f', 2);
    str2=QString::number(SKZ,'f', 2);
    str3=QString::number(Am,'f', 2);
    /////////////////////////////////////


}

void RenderThread::calculateDeviation(int nCh)
{
    double gain=getGainY(nCh);
    double gain_mks=getGainX();

    double Amin=65767.0;
    double Amax=-65767.0;

    short re_1[RAW_SAMPLES_NUM]={0};
    short im_1[RAW_SAMPLES_NUM]={0};

    short re_prev=0;
    short im_prev=0;
    double W[RAW_SAMPLES_NUM]={0.0};

    QPointF s2;

    QByteArray *temp=0;
    if (nCh==0)
        temp=&m_data;
    if (nCh==1)
        temp=&m_data2;

#ifdef unused
    if(d_plot)
    {
    d_plot->signalData(nCh).clearValues();
    //d_plot->signalData(0).clearValues();
    //d_plot->signalData(1).clearValues();
    //d_plot->signalData(2).clearValues();
    }
#endif

    for(int i=0; i<RAW_SAMPLES_NUM; i++)
    {
        //s2.setX(gain_mks*i);

        /*short im=m_data[i*4]<<8;
        im |= 0xff&m_data[i*4+1];
        short re=m_data[i*4+2]<<8;
        re |= 0xff&m_data[i*4+3];*/

        short im=temp->at(i*4)<<8;
        im |= 0xff&temp->at(i*4+1);
        short re=temp->at(i*4+2)<<8;
        re |= 0xff&temp->at(i*4+3);

        re_1[i]=(i==0?0:re-re_prev);
        im_1[i]=(i==0?0:im-im_prev);

        W[i]=(re==0&&im==0)?0:((re*im_1[i])-(im*re_1[i]))/(qPow(re,2)+qPow(im,2));
        if(W[i]<Amin)Amin=W[i];
        if(W[i]>Amax)Amax=W[i];

        re_prev=re;
        im_prev=im;

        //s2.setY(gain*W[i]/*val2*/);
        //m_mw->d_plot[0]->signalData(2).append(s2);
    }

    double K=modeConf->Fdec/(2.0*3.14159265358);

    double w=((Amax-Amin)*K)/2.0;
    qDebug() << "Девиация " << w << endl;

    //ui->edit_SKZ->setText(QString::number(w,'f', 2));
    str1=QString::number(w,'f', 2);
    str2="";
    str3="";
    /////////////////////////////////////

    for(int j=0; j<RAW_SAMPLES_NUM; j++)
    {
        s2.setX(gain_mks*j);


        double koef=modeConf->Fdec/(2.0*3.14159265358*w);
        s2.setY(koef*W[j]/*val2*/);
#ifdef unused
        if(d_plot)
        {
        d_plot->signalData(nCh).append(s2);
        }
#endif
    }

}

void RenderThread::calculatePower(int nCh)
{
    double gain=getGainY(nCh);//1.0;//
    double gain_mks=getGainX();
    //int *m_dataPtr=0;
    QByteArray *temp=0;
    if (nCh==0)
        temp=&m_data;
        //m_dataPtr=reinterpret_cast<int*>(&m_data.data()[1]);
    if (nCh==1)
        temp=&m_data2;
        //m_dataPtr=reinterpret_cast<int*>(&m_data2.data()[1]);

#ifdef unused
    if(d_plot)
    {
    d_plot->signalData(nCh).clearValues();
    //d_plot->signalData(0).clearValues(); //d_plot[0]->incrementInterval();
    //d_plot->signalData(1).clearValues(); //d_plot[0]->incrementInterval();
    //d_plot->signalData(2).clearValues();
    }
#endif
    QPointF s;
    QPointF s2;

    double ss=0.0;
    double ss2=0.0;
    for(int i=/*5*/0; i<RAW_SAMPLES_NUM; i++)
    {
        s.setX(gain_mks*i);
        s2.setX(gain_mks*i);

      /*  short y2=m_data[i*4]<<8;
        y2|=0xff&m_data[i*4+1];
        short y=m_data[i*4+2]<<8;
        y|=0xff&m_data[i*4+3];*/

        short y2=temp->at(i*4)<<8;
        y2|=0xff&temp->at(i*4+1);
        short y=temp->at(i*4+2)<<8;
        y|=0xff&temp->at(i*4+3);


        if(y>modeConf->max_y)modeConf->max_y=y;
        if(y2>modeConf->max_y2)modeConf->max_y2=y2;

        if(y<modeConf->min_y)modeConf->min_y=y;
        if(y2<modeConf->min_y2)modeConf->min_y2=y2;
        //перевод в вольты

        double val=gain*y/**7/8*/;
        double val2=gain*y2/**7/8*/;

        //val=(val*1.25*N)/32767;
        //val2=(val2*1.25*N)/32767;

        s.setY(gain*y/**7/8/**ma*/);
        s2.setY(gain*y2/**7/8*/);

#ifdef unused
        if(d_plot)
            d_plot->signalData(nCh).append(s);
#endif
        ss+=val*val;
        ss2+=val2*val2;
    }

    double max=gain*modeConf->max_y;
    double max2=gain*modeConf->max_y2;

    double min=gain*modeConf->min_y;
    double min2=gain*modeConf->min_y2;

    //double P;
    double SKZ=qSqrt((nCh==1?ss2:ss)/RAW_SAMPLES_NUM);
    //double SKZ=qSqrt((m_Ch1?ss:0)/RAW_SAMPLES_NUM);
    //double SKZ2=qSqrt((m_Ch2?ss2:0)/RAW_SAMPLES_NUM);
    //double SKZ2=qSqrt(s2/RAW_SAMPLES_NUM);

    double P=10*(qLn(1000*qPow(SKZ,2)/50)/qLn(10));

//    ui->edit_SKZ->setText(QString::number(/*m_nCh==1?SKZ2:*/SKZ,'f', 2));
//    ui->edit_Power->setText(QString::number(P,'f', 2));

    str1=QString::number(/*m_nCh==1?SKZ2:*/SKZ,'f', 3);
    str2=QString::number(P,'f', 2);
    str3=QString::number(nCh==1?max2:max,'f', 3);

    qDebug() << "Напряжение ВЧ " <<  SKZ << endl;

    /////////////////////////////////////


}

void RenderThread::WriteLog(quint16 LDATA, quint8 CMD, quint8 BADDR, const QList<QPoint> &WDATA)
{
    //if(!ui->text_Log->isVisible())return;

    QString str, str1;
    str.sprintf("PC->OSC: LDATA=0x%04X CMD=0x%02X BADDR=0x%02X | ", LDATA, CMD, BADDR);

    for(int i=0;i<WDATA.size();i++)
    {
        QPoint item=WDATA.at(i);

        quint16 a=item.x();
        quint8 d=item.y();

        str1.sprintf("0x%04X 0x%02X ", a,d);
        str+=str1;
    }
    //str+="\n";

    //ui->text_Log->appendPlainText(str);
    //ui->text_Log->ensureCursorVisible();
    qDebug()<<str<<endl;
}

void RenderThread::WriteLog(QByteArray datagram)
{
    //if(!ui->text_Log->isVisible())return;

    QString str;
    QTextStream stream(&str);
    stream << hex << qSetFieldWidth(2) << qSetPadChar('0') << right;
    for(int i=0;i<datagram.size();i++)
    {
        stream << qSetFieldWidth(2) << static_cast<unsigned char>(datagram[i]) << qSetFieldWidth(0) <<" ";
        //stream << static_cast<unsigned char>(datagram[i]) <<" ";
    }
    str.prepend("OSC->PC: ");
    //ui->text_Log->appendPlainText("OSC->PC: "+/*datagram*/str + QString("\n"));
    //ui->text_Log->ensureCursorVisible();
    qDebug()<<str<<endl;
}

double RenderThread::getGainY(int ch)
{
    if (!modeConf) return 0;
    //return 1.0;
    //перевод в вольты
    int nAtten;//ui->combo_Ch1Atten->currentIndex();
    int nChType=-1;
    int chScale;
    mutex.lock();
    if (ch==0) {
        nAtten = modeConf->nChAtten;
        nChType=modeConf->nChType;
        chScale = modeConf->sC1;}
    if (ch==1) {
        nAtten = modeConf->nChAtten2;
        nChType=modeConf->nChType2;
        chScale = modeConf->sC2;}
    mutex.unlock();
    int N=1;
    double V=0;
    switch(nAtten)
    {
    case 0:N=1;
        break;
    case 1:N=10;
        break;
    case 2:N=30;
        break;
    case 3:N=300;
        break;
    }

    switch (chScale) {
        case 0:
            V=0.001; break;
        case 1:
            V=0.002; break;
        case 2:
            V=0.005; break;
        case 3:
            V=0.01; break;
        case 4:
            V=0.02; break;
        case 5:
            V=0.05; break;
        case 6:
            V=0.1; break;
        case 7:
            V=0.2; break;
        case 8:
            V=0.5; break;
        case 9:
            V= 1; break;
        case 10:
            V= 2; break;
        case 11:
            V= 5; break;
        case 12:
            V= 10; break;
    }

    double val=(1.25*N)/(m_nMeasMode==3||m_nMeasMode==4||m_nMeasMode==5||m_nMeasMode==7||m_nMeasMode==8||m_nMeasMode==10?qPow(2,28):32767); // todo *2 for spectr

    if(nChType==0)val=val*7/8; // 50 ohm
    else if(nChType==1)val=val*0.5; //600 ohm
    else if(nChType==2)val=val*0.5; //1 Mohm
    //val*10/цена деления В
    //val=val*10/V;
    return val;
}

double RenderThread::getGainX()
{
    if (!modeConf) return 0;
    double val=0.005;  // 200 MHz
    //ms val/1000

    if (process->getID()==2)
    {
        if (modeConf->tSc>8 && modeConf->tSc<18)
           val=val/1000;
        if (modeConf->tSc>17)
           val=val/1000000;
    }

    //перевод в mks
    switch(m_nMeasMode)
    {
    case 0:// глубина ам
    case 1:// девиация чм
    case 6:// квп
    case 9:// чувств тлг квп
        val=1000000/modeConf->Fdec;
        break;
    case 2: // вч напряжение
        break;
    case 3: // спектр
    case 4:// частота
    case 5:// кни
    case 7:// псоф
    case 8:// чувств
    case 10:// ачх
        val=modeConf->fBand/modeConf->Nband;
        break;
    }

    return val;
}

void RenderThread::makeTestDataFFT(unsigned int *F)
{
    QStringList files;
    files.append("KNI_data_4096.txt");

    for(int i=0;i<files.size();i++)
    {
        QString dir=QCoreApplication::applicationDirPath ();
        QString fileName = dir+"/"+files.at(i);//QFileDialog::getOpenFileName(this);
        if (fileName.isEmpty())
            return;

        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            //QMessageBox::warning(this,tr("File error"),tr("Failed to open\n%1").arg(fileName));
            return;
        }
        int j=0;
        while (!file.atEnd())
        {
            QString str = file.readLine().trimmed();
            if (!str.isEmpty())
            {
                unsigned int y=str.toInt();

                m_data[j*4]=(y>>24)&0xff; //m_data[jj*4]<<24;
                m_data[j*4+1]=(y>>16)&0xff; //y|=(0xff&m_data[jj*4+1])<<16;
                m_data[j*4+2]=(y>>8)&0xff; //y|=(0xff&m_data[jj*4+2])<<8;
                m_data[j*4+3]=(y)&0xff; //y|=0xff&m_data[jj*4+3];

                F[j]=y;
                unsigned int y1=m_data[j*4]<<24;
                y1|=(0xff&m_data[j*4+1])<<16;
                y1|=(0xff&m_data[j*4+2])<<8;
                y1|=0xff&m_data[j*4+3];

                if(y!=y1)
                {
                    y=0;
                    return;
                }
            }
            j++;
        }
        file.close();
    }
}

//REALISATION OF STARTPROCESS COPY
void RenderThread::startProcess2(
                  MeasureConfigItem *conf)
{
    qDebug("copy of render starting ");
    modeConf = conf;

    m_nMeasMode=process->getID();
    fft_window=&m_worker->fft_window[conf->nFftWindow][0];//pFftWindow;
#ifdef unused
//    d_plot=conf->plots->last();
#endif
    //m_bCycle=conf->bCycle;

    modeConf->Fleft=modeConf->fFcentr-modeConf->fBand/2;
    modeConf->Fright=modeConf->fFcentr+modeConf->fBand/2;
    modeConf->Nfft=FFT_SAMPLES_NUM;

    //udp_client=pSock;
    if(udp_client)
        udp_client->moveToThread(this);

    //m_condition = condition;
    m_abort = false;
    exitThreadF = !conf->bCycle;
    start();
}

void RenderThread::initRender(MeasureConfigItem *conf)
{
    modeConf = conf;

    m_nMeasMode=process->getID();
    fft_window=&m_worker->fft_window[conf->nFftWindow][0];//pFftWindow;
#ifdef unused
//    d_plot=conf->plots->last();
#endif
    //m_bCycle=conf->bCycle;

    modeConf->Fleft=modeConf->fFcentr-modeConf->fBand/2;
    modeConf->Fright=modeConf->fFcentr+modeConf->fBand/2;
    modeConf->Nfft=FFT_SAMPLES_NUM;

    //udp_client=pSock;
//    if(udp_client)
//        udp_client->moveToThread(this);

//    m_condition = condition;
//    m_abort = false;
//    exitThreadF = !conf->bCycle;
}

void RenderThread::setScale(int ch ,int sc) {
    if (!modeConf) return;
    //if (modeConf->empty)
    //    return;
    mutex.lock();
    if (ch==0) {
        //m_Scale1 = sc;
        modeConf->sC1=sc;
    }

    if (ch==1) {
        //m_Scale2 = sc;
        modeConf->sC2=sc;
    }
    mutex.unlock();
}

void RenderThread::setTimeScale(int val) {
    if (!modeConf) return;
    //if (modeConf->empty)
    //    return;
    mutex.lock();
    //m_TimeScale = val;
    modeConf->tSc=val;
    mutex.unlock();
}

QByteArray *RenderThread::getData() {
    return &m_data;
}

QByteArray *RenderThread::getData2() {
    return &m_data2;
}

QByteArray *RenderThread::getCurrentData() {
    return currentData;
}

void RenderThread::setMainThread(QThread *thr)
{
    mainThread = thr;
}

void RenderThread::setProcess(ProcessObject *p)
{
    process = p;
}

void RenderThread::stop()
{
    suspendProcess();
    exitThreadF = true;
    resume();
}

void RenderThread::resume()
{
    suspF = false;
    m_codition_run.wakeAll();
}

void RenderThread::suspendProcess()
{
    suspF = true;
    m_abort = true;
    //stopThreadF = true;
}

void RenderThread::setUdpPropetries(QString oscIp, QString ip, quint32 port)
{
    m_strOscIp = oscIp;
    m_strIp = ip;
    this->port = port;
}

WorkerObject* RenderThread::getWorker()
{
    return m_worker;
}
