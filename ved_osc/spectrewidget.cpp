#include "spectrewidget.h"

SpectreWidget::SpectreWidget(QWidget *parent):
    baseTabWidget(parent)
{
    setName(tr("Spectre"));
}

QString SpectreWidget::getName() {
    return name;
}
