/******************************************************************************
 * Copyright (c) Aaronia AG, http://www.aaronia.de
 * This source is subject to the Microsoft Reference Source License (Ms-RSL).
 * Please see the license.txt file for more information.
 * All other rights reserved.
 ******************************************************************************/

#include "adcfdemomainwindow.h"
#include "ui_adcfdemomainwindow.h"

//#include "plot.h" dawdww
#include "knob.h"
#include "wheelbox.h"
#include <qwt_scale_engine.h>
#include <qwt_legend.h>
#include "signaldata.h"

#include <QAbstractState>
#include <QStateMachine>
#include <QFinalState>
#include <QTimer>
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QTableWidget>
#include <QTreeWidget>
#include <QtNetwork>
#include <QDebug>



//#include <ASpectranVariables>

// platform includes for setting system time
//#ifdef Q_OS_WIN32
//#include <Windows.h>
//#endif

#define DATA_DIR "D:\\Andreev\\Projects\\Nick\\KTS_DRS\\"

#define DPCh1 "DPCh1.txt"
#define DPCh3 "DPCh3.txt"
#define KMS "Kms.txt"
#define PERECH "Perech.txt"
#define PERECH_US1 "PerechUS1.txt"
#define PERECH_US3 "PerechUS3.txt"
#define RS_B "RS_B.txt"
#define RS_M "RS_M.txt"
#define RS_ObU "RS_ObU.txt"
#define RS_OBU3 "RS_OBU3.txt"

ADcfDemoMainWindow::ADcfDemoMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ADcfDemoMainWindow)
{   
    // create UI from Qt-Designer/moc generated header file
    ui->setupUi(this);

    QTextCodec::setCodecForCStrings( QTextCodec::codecForName("utf8") ); // todo ....
    QTextCodec::setCodecForTr( QTextCodec::codecForName("utf8") ); // todo ....

    showCursors = false;
    sideMainTab = 0;
    sideModeTab = 0;
    chThread = 0;
    QStringList items;
    renderThread = 0;

    //-------------------
    genState = new QAction(tr("Show Gen"),0);
    genState->setCheckable(true);

    //ui->groupBox_5->setEnabled(false);
//    ui->edit_IP->setMinimumWidth(50);;
//    ui->edit_Port->setMinimumWidth(50);
    //ui->verticalLayout_7->setGeometry(QRect(10,10,10,10));
    //ui->verticalLayout_7->setSizeConstraint(QLayout::SetMinimumSize);
//    QList<QAction*> l;
//    l.append(new QAction("AWDD",0));
//    l.append(new QAction("DSAW",0));
//    ui->mainToolBar->addActions(l);

    /*sideModeTab = new QWidget;
    sideMainTab = new QWidget;
    QHBoxLayout *l = new QHBoxLayout;
    checkingAll = new QCheckBox("Check all");
    startCheckingButton = new QPushButton("StartChecking");
    checkingCycleBox = new QCheckBox("Repeat");
    checkingCycleBox->setChecked(false);
    l->addWidget(checkingCycleBox);
    l->addWidget(checkingAll);
    l->addWidget(startCheckingButton);
    sideMainTab->setLayout(l);
    sideMainTab->setVisible(false);
    QVBoxLayout *l1 =new QVBoxLayout;
    l1->setContentsMargins(0,0,0,0);
    //ui->groupBox_3->setMinimumHeight(145);

    l1->addWidget(ui->groupBox_3);
    l1->addWidget(ui->groupBox_2);
    l1->addWidget(ui->timescale);
    l1->addWidget(ui->triggerBox);
    //l1->addWidget(ui->groupBox);
    l1->addWidget(ui->groupBox_6);
    l1->addWidget(ui->groupBox_4);
    l1->addWidget(ui->markersColorGrp);

    sideModeTab->setLayout(l1);
    sideModeTab->setVisible(false);
    //sideModeTab->setMinimumHeight(0);
    //sideModeTab->setMaximumHeight(1000);
    //ui->groupBox_2->setVisible(false);
    ui->verticalLayout_7->insertLayout(0,ui->horizontalLayout);
    ui->verticalLayout_7->insertWidget(1,sideMainTab);
    ui->verticalLayout_7->insertWidget(2,sideModeTab);
//    ui->verticalLayout_7->insertWidget(3, ui->groupBox_3);
//    ui->verticalLayout_7->insertWidget(4,ui->timescale);
//    ui->verticalLayout_7->insertWidget(5,ui->triggerBox);
//    ui->verticalLayout_7->insertWidget(6,ui->groupBox_6);
//    ui->verticalLayout_7->insertWidget(7,ui->groupBox_2);
//    ui->verticalLayout_7->insertWidget(ui->verticalLayout_7->count(),ui->groupBox_4);
    //for (int i=0;i<Mode::COUNT_ID; i++)
      //  currMode[i]=0;
    //modCnt=0;
*/
    //TRIGGER ELEMENTS INIT
    items.clear();
    items.append(tr("Auto"));
    items.append(tr("Normal"));
    ui->triggerMode->addItems(items);
    items.clear();
    items.append(tr("Chanel 1"));
    items.append(tr("Chanel 2"));
    items.append(tr("External"));
    ui->triggerSource->addItems(items);
    items.clear();
    items.append("/");
    items.append("\\");
    items.append("/\\");
    ui->triggerSlope->addItems(items);
    items.clear();
    //

    markColor = new colorLabel;
    markColor2 = new colorLabel;
    markColor->setChanel(0);    //1st marker
    markColor2->setChanel(1);   //2nd marker
    QHBoxLayout *hl = new QHBoxLayout;
    QHBoxLayout *hl1 = new QHBoxLayout;
    QVBoxLayout *vl = new QVBoxLayout;

    hl->setContentsMargins(0,0,0,0);
    hl1->setContentsMargins(0,0,0,0);
    vl->setContentsMargins(0,0,0,0);

    //QCheckBox *chBox = new QCheckBox("Markers Freeze");
    mrkFreezeChb = new QCheckBox(tr("Freeze Cursors"));
    connect(mrkFreezeChb, SIGNAL(toggled(bool)), this,SLOT(setMarkersFreeze(bool)));
    mrkFreezeChb->setMinimumWidth(50);


    QPushButton *btn = new QPushButton(tr("Default"));
    connect(btn,SIGNAL(clicked()),this, SLOT(setDefaultMarkers()));
    btn->setMinimumWidth(50);

    //hl->addWidget(chBox);
    hl->addWidget(new QLabel(tr("Cursor 1")));
    hl->addWidget(markColor);
    hl->addWidget(new QLabel(tr("Cursor 2")));
    hl->addWidget(markColor2);

    hl1->addWidget(mrkFreezeChb);
    hl1->addWidget(btn);


    vl->addLayout(hl);
    vl->addLayout(hl1);

    markColor->setCurrentColor(Qt::white);
    markColor2->setCurrentColor(Qt::green);
    ui->markersColorGrp->setLayout(vl);
    ui->markersColorGrp->setVisible(false);

    connect(markColor, SIGNAL(colorChanged(colorLabel*)), this ,SLOT(markersColorChanged(colorLabel*)));
    connect(markColor2, SIGNAL(colorChanged(colorLabel*)), this, SLOT(markersColorChanged(colorLabel*)));


    initDone=false;
    //color init
    QPalette p;
    p.setColor(QPalette::Window,Qt::red);
    colorLabel *colorLbl1 = new colorLabel;
    colorLbl1->setPalette(p);
    p.setColor(QPalette::Window,Qt::yellow);
    colorLabel *colorLbl2 = new colorLabel;
    colorLbl2->setPalette(p);
    colorLbl1->setChanel(0);
    colorLbl2->setChanel(1);
    lbl1=colorLbl1; lbl2=colorLbl2;

    ui->ch1Lay->addWidget(colorLbl1);
    ui->ch2Lay->addWidget(colorLbl2);
    //ui->colorLay->addWidget(colorLbl1);
    //ui->colorLay->addWidget((colorLbl2));
    //

    loadConfig();

    m_Ch1=true;
    m_Ch2=false;
    //isStarted=false;
    //m_nCh=0;
    m_last_captured_f=0.0;
    m_bWork=false;
    m_bManual=false;


    m_bStarted=false;
    //m_data.resize(16384*4);
    m_data_test.resize(RAW_SAMPLES_NUM*4);

    udp_client=NULL;
    udp_server=NULL;

    m_worker = 0;//new WorkerObject(m_strIp, m_strOscIp);
    //m_worker->setMainThread(thread());
    m_thread = NULL;//new Thread;
    renderThread = new RenderThread(0,0/*m_worker*/, m_strIp, m_strOscIp, this);
    renderThread->setMainThread(thread());
    initOscope();

//    m_thread->launchWorker(m_worker);
    //connect(this, SIGNAL(send(Message*, bool)), m_worker, SLOT(onSendMessage(Message*, bool)));
    //connect(m_worker, SIGNAL(recvPacket(const QByteArray& )), this, SLOT(onRecvPacket(const QByteArray& )));
    //QMetaObject::invokeMethod(m_worker, "doWork", Qt::QueuedConnection);
//    m_worker->doWork();

    //renderThread=new RenderThread(m_worker, m_strIp, m_strOscIp, this);

    connect(ui->plotTabWidget, SIGNAL(currentChanged(int)),this, SLOT(measModeChange(int)));
    connect(tabBtn, SIGNAL(clicked()), this, SLOT(addNewTab()));
    connect(ui->plotTabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(deleteTab(int)));
    //!!!!chanel checkbox connect
    connect(ui->ChanelBox1, SIGNAL(clicked()), this, SLOT(chanelSelected()));
    connect(ui->ChanelBox2, SIGNAL(clicked()), this, SLOT(chanelSelected()));
    connect(ui->check_Cycle, SIGNAL(toggled(bool)), this,SLOT(setCycle(bool)));
    //connect(ui->ChanelBox1, SIGNAL(clicked()), this, SLOT(setM_nCh()));
    //
    connect(lbl1, SIGNAL(colorChanged(colorLabel*)), this, SLOT(onColorChanged(colorLabel*)));
    connect(lbl2, SIGNAL(colorChanged(colorLabel*)), this, SLOT(onColorChanged(colorLabel*)));

    //connect(renderThread, SIGNAL(sendBlock(QString, QString, QString)), this, SLOT(dataReady(QString, QString, QString)));
    //connect(renderThread, SIGNAL(started()), this, SLOT(on_processStarted()));
    //connect(renderThread, SIGNAL(finished()), this, SLOT(on_processStopped()));
    //connect(renderThread, SIGNAL(writeLog(QString)), this, SLOT(WriteLog(QString)));

    //connect(m_worker, SIGNAL(recvPacket(const QByteArray& )), renderThread, SLOT(processDatagram(const QByteArray& )), Qt::BlockingQueuedConnection);
    //connect(m_worker, SIGNAL(resumeRead()), renderThread, SLOT(resumeProcess()), Qt::BlockingQueuedConnection);

    //ui->actionQuit->setCheckable(true);
    //ui->actionQuit->setChecked(true);

    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));    
    connect(genState, SIGNAL(triggered(bool)), this ,SLOT(showGenerator(bool)));

    connect(ui->edit_GenFc, SIGNAL(textEdited(QString)),this,SLOT(genFcChanged(QString)));
    connect(ui->edit_Fcentr, SIGNAL(textEdited(QString)), this,SLOT(uiFCentrChanged(QString)));
    connect(ui->edit_Band, SIGNAL(textEdited(QString)),this, SLOT(uiBandChanged(QString)));
    //connect(ui->combo_Ch1Type,SIGNAL(currentIndexChanged(int)), this,SLOT(uiChTypeChanged(int)));
    //connect(ui->combo_Ch2Type,SIGNAL(currentIndexChanged(int)), this,SLOT(uiChTypeChanged(int)));
    //connect(ui->combo_Ch1Atten, SIGNAL(currentIndexChanged(int)),this,SLOT(attChanged(int)));
    //connect(ui->combo_Ch2Atten, SIGNAL(currentIndexChanged(int)),this,SLOT(attChanged(int)));

    // start state machine
    //sm.start();
    ///////////////////////////////////////
    //QTextCodec::setCodecForCStrings( QTextCodec::codecForName("Windows-1251"));


    ui->combo_BlockId->addItem("0x01  Управление цепями коммутации канала 1", (int)1);
    ui->combo_BlockId->addItem("0x02  Управление цепями коммутации канала 2", (int)2);
    ui->combo_BlockId->addItem("0x03  Управление коммутацией тактовых частот", (int)3);
    ui->combo_BlockId->addItem("0x04  Управление АЦП 0, канал 1", (int)4);
    ui->combo_BlockId->addItem("0x05  Управление АЦП 1, канал 1", (int)5);
    ui->combo_BlockId->addItem("0x06  Управление АЦП 0, канал 2", (int)6);
    ui->combo_BlockId->addItem("0x07  Управление АЦП 1, канал 2", (int)7);
    ui->combo_BlockId->addItem("0x08  Управление PLL", (int)8);
    ui->combo_BlockId->addItem("0x10  xxx", (int)16);
    ui->combo_BlockId->addItem("0x20  Блок осциллографа 1", (int)32);
    ui->combo_BlockId->addItem("0x21  Блок DDC 1", (int)33);
    ui->combo_BlockId->addItem("0x22  Блок анализатора спектра 1", (int)34);


/*
    0x01	Управление цепями коммутации канала 1
    0x02	Управление цепями коммутации канала 2
    0x03	Управление коммутацией тактовых частот
    0x04	Управление АЦП 0, канал 1
    0x05	Управление АЦП 1, канал 1
    0x06	Управление АЦП 0, канал 2
    0x07	Управление АЦП 1, канал 2
    0x08	Управление PLL

    0x20	Блок осциллографа 1
    0x21	Блок DDC 1
    0x22	Блок анализатора спектра 1
  */
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   // ui->combo_ChanSelect->addItem(tr("Channel 1"));
   // ui->combo_ChanSelect->addItem(tr("Channel 2"));
//
    ui->combo_Ch1Type->addItem(tr("50 Ohm"));
    ui->combo_Ch1Type->addItem(tr("600 Ohm Diff."));
    ui->combo_Ch1Type->addItem(tr("1 MOhm/20pF"));
    //ui->combo_Ch1Atten->setEnabled(false);

    ui->combo_Ch2Type->addItem(tr("50 Ohm"));
    ui->combo_Ch2Type->addItem(tr("600 Ohm Diff."));
    ui->combo_Ch2Type->addItem(tr("1 MOhm/20pF"));
    //ui->combo_Ch2Type->setVisible(false);
    //ui->combo_Ch2Atten->setVisible(false);


    ui->combo_taktADC->addItem("XO 200МГц");
    ui->combo_taktADC->addItem("PLL");
    ui->combo_taktPLL->addItem("VXI 10МГц");
    ui->combo_taktPLL->addItem("CLK IN");
    ui->combo_taktPLL->addItem("OCXO 10/100МГц");
    ui->combo_ClkOut->addItem("PLL");
    ui->combo_ClkOut->addItem("CLK IN");
    ui->combo_ClkOut->addItem("OCXO");

    ui->combo_taktADC->setCurrentIndex(1);

    ui->combo_taktPLL->setVisible(false);
    ui->combo_ClkOut->setVisible(false);
    ui->label_8->setVisible(false);
    ui->label_9->setVisible(false);
    ui->groupBox->setVisible(false);




    /*
    Регистр сброса настроек	x	0	0	1	1	x	x	x	нормальная работа
        x	0	1	1	1	x	x	x	сброс всех настроек на дефолтные
    Управление Vref	0	x	x	x	x	0	x	x	внешний Vref выключен
        1	x	x	x	x	0	x	x	внешний Vref включен
    Формат данных	x	0	x	0	1	0	0	0	двоичный смещенный
        x	0	x	0	1	0	0	1	дополнительный код
    Задержка клока	1	x	x	0	0	0	0	0	задержка 0 пс
        1	x	x	0	0	0	0	1	задержка 100 пс
        1	x	x	1	1	1	1	1	задержка 3200 пс
    Режим развязки	x	x	x	x	x	0	x	x	развязка по переменному току
        x	x	x	x	x	1	x	x	развязка по постоянному току
    Тока буфера 1	1	1	0	1	0	1	x	x	ток буфера +530% (максимум)
        1	1	0	1	0	0	x	x	ток буфера +520%
        0	0	0	0	0	0	x	x	ток буфера 0% (номинальное значение)
        1	1	0	1	1	0	x	x	ток буфера -100% (минимум)
    Ток буфера 2	1	1	0	1	0	1	x	x	ток буфера +530% (максимум)
        1	1	0	1	0	0	x	x	ток буфера +520%
        0	0	0	0	0	0	x	x	ток буфера 0% (номинальное значение)
        1	1	0	1	1	0	x	x	ток буфера -100% (минимум)
    Обновление данных	x	x	x	x	x	x	x	0	в данное состояние переходит автоматически
        x	x	x	x	x	x	x	1	обновление данных в регистрах АЦП

    */
 /*   ui->combo_adc_select->addItem("АЦП 0 канал 1");
    ui->combo_adc_select->addItem("АЦП 1 канал 1");
    ui->combo_adc_select->addItem("АЦП 0 канал 2");
    ui->combo_adc_select->addItem("АЦП 1 канал 2");
    ui->combo_adc_reset->addItem("нормальная работа");
    ui->combo_adc_reset->addItem("сброс настроек на дефолтные");
    ui->combo_adc_vref->addItem("внешний Vref выключен");
    ui->combo_adc_vref->addItem("внешний Vref включен");
    ui->combo_adc_dataformat->addItem("двоичный смещенный");
    ui->combo_adc_dataformat->addItem("дополнительный код");
    ui->combo_adc_clockdelay->addItem("задержка клока 0 пс");
    ui->combo_adc_clockdelay->addItem("задержка клока 100 пс");
    ui->combo_adc_clockdelay->addItem("задержка клока 3200 пс");
    ui->combo_adc_razvmode->addItem("развязка по переменному току");
    ui->combo_adc_razvmode->addItem("развязка по постоянному току");
    ui->combo_adc_tokbuf1->addItem("ток буфера1 +530% (максимум)");
    ui->combo_adc_tokbuf1->addItem("ток буфера1 +520%");
    ui->combo_adc_tokbuf1->addItem("ток буфера1 0% (номинальное значение)");
    ui->combo_adc_tokbuf1->addItem("ток буфера1 -100% (минимум)");
    ui->combo_adc_tokbuf2->addItem("ток буфера2 +530% (максимум)");
    ui->combo_adc_tokbuf2->addItem("ток буфера2 +520%");
    ui->combo_adc_tokbuf2->addItem("ток буфера2 0% (номинальное значение)");
    ui->combo_adc_tokbuf2->addItem("ток буфера2 -100% (минимум)");
    ui->combo_adc_regrefresh->addItem("0");
    ui->combo_adc_regrefresh->addItem("1 - обновление");
*/
    //TIMESCALE WIDGETS INIT
    ui->timescaleBox->setVisible(false);
    ui->timescaleLbl->setVisible(false);
    ui->timescaleBox->addItem(tr("1 Mks"));
    ui->timescaleBox->addItem(tr("2 Mks"));
    ui->timescaleBox->addItem(tr("5 Mks"));
    ui->timescaleBox->addItem(tr("10 Mks"));
    ui->timescaleBox->addItem(tr("20 Mks"));
    ui->timescaleBox->addItem(tr("50 Mks"));
    ui->timescaleBox->addItem(tr("100 Mks"));
    ui->timescaleBox->addItem(tr("200 Mks"));
    ui->timescaleBox->addItem(tr("500 Mks"));
    ui->timescaleBox->addItem(tr("1 Ms"));
    ui->timescaleBox->addItem(tr("2 Ms"));
    ui->timescaleBox->addItem(tr("5 Ms"));
    ui->timescaleBox->addItem(tr("10 Ms"));
    ui->timescaleBox->addItem(tr("20 Ms"));
    ui->timescaleBox->addItem(tr("50 Ms"));
    ui->timescaleBox->addItem(tr("100 Ms"));
    ui->timescaleBox->addItem(tr("200 Ms"));
    ui->timescaleBox->addItem(tr("500 Ms"));
    ui->timescaleBox->addItem(tr("1 Sec"));
    ui->timescaleBox->addItem(tr("2 Sec"));
    ui->timescaleBox->addItem(tr("5 Sec"));

    connect(ui->timescaleBox, SIGNAL(currentIndexChanged(int)),this, SLOT(setOscInterval(int)));
    //--------

    //CHANEL SCALE WIDGET INIT
    items.clear();
    items.append(tr("1 mV/d"));
    items.append(tr("2 mV/d"));
    items.append(tr("5 mV/d"));
    items.append(tr("10 mV/d"));
    items.append(tr("20 mV/d"));
    items.append(tr("50 mV/d"));
    items.append(tr("100 mV/d"));
    items.append(tr("200 mV/d"));
    items.append(tr("500 mV/d"));
    items.append(tr("1 V/d"));
    items.append(tr("2 V/d"));
    items.append(tr("5 V/d"));
    items.append(tr("10 V/d"));
    ui->ch1ScaleBox->setVisible(false);
    ui->ch2ScaleBox->setVisible(false);
    ui->ch1ScaleBox->addItems(items);
    ui->ch2ScaleBox->addItems(items);

    connect(ui->ch1ScaleBox, SIGNAL(itemChanged(int)), this, SLOT(setOscScale1(int)));
    connect(ui->ch2ScaleBox, SIGNAL(itemChanged(int)), this, SLOT(setOscScale2(int)));


    //-------------

    //COUPLING BOXES INIT
    items.clear();
    items.append(tr("DC"));
    items.append(tr("AC"));
    items.append(tr("GND"));
    ui->coupBox1->addItems(items);
    ui->coupBox2->addItems(items);
    ui->coupBox1->setVisible(false);
    ui->coupBox2->setVisible(false);

    connect(ui->coupBox1, SIGNAL(currentIndexChanged(int)), this, SLOT(setCoupling1(int)));
    connect(ui->coupBox2, SIGNAL(currentIndexChanged(int)), this, SLOT(setCoupling2(int)));
    //---------------------

    ui->combo_WindowType->addItem(tr("rectangular"));//Отсутствует");
    ui->combo_WindowType->addItem(tr("hann"));//"Ханна");
    ui->combo_WindowType->addItem(tr("hamming"));//"Хэмминга");
    ui->combo_WindowType->addItem(tr("blackman"));//"Блэкмена");
    ui->combo_WindowType->addItem(tr("blackmanharris"));//"Блэкмена-Харриса");
    ui->combo_WindowType->addItem(tr("flattopwin"));//"С плоской вершиной");
    //ui->combo_WindowType->setEnabled(false);

    QDoubleValidator *v=new QDoubleValidator(0.0, 500000000.0, 5, this);
    v->setNotation(QDoubleValidator::StandardNotation);
    ui->edit_Fs->setValidator(v);
    ui->edit_Fcentr->setValidator(v);
    ui->edit_Band->setValidator(v);

    ui->edit_Fs->setText(QString::number(200));
    ui->edit_Fcentr->setText(QString::number(1));
    ui->edit_Band->setText(QString::number(500));

    ui->edit_Fs->setEnabled(false);

    ui->combo_Gen->addItem("OFF");
    ui->combo_Gen->addItem("SIN");
    ui->combo_Gen->addItem("F1B");
    ui->combo_Gen->addItem("G1B");
    ui->combo_Gen->addItem("J3E");
    ui->combo_Gen->addItem("R3E");
    ui->combo_Gen->addItem("A3E");
    ui->combo_Gen->addItem("H3E");
    ui->combo_Gen->addItem("A1A");
    ui->combo_Gen->addItem("F3EA");
    ui->combo_Gen->addItem("F3EJ");


    m_nGenFsym=100;
    m_nGenFmod=1000;
    m_nGenFdev=85;    
    m_nGenFc=1600000;
    m_nAmax=16383;
    m_nAmin=0;
    m_nGenSSBType=0;
    m_fGenK=0.5;
    ui->edit_GenFc->setText(QString::number(m_nGenFc));
    ui->edit_GenKmod->setText(QString::number(m_fGenK));
    ui->edit_GenFsym->setText(QString::number(m_nGenFsym));
    ui->edit_GenFmod->setText(QString::number(m_nGenFmod));
    ui->edit_GenFdev->setText(QString::number(m_nGenFdev));
    ui->combo_GenNegPos->addItem(tr("Negative"));//"Негатив");
    ui->combo_GenNegPos->addItem(tr("Positive"));//"Позитив");
    ui->combo_GenNegPos->setCurrentIndex(m_nGenSSBType);

    //connect(ui->combo_Gen, SIGNAL(activated(int)), this, SLOT(on_combo_GenMode_activated(int)));

    /*ui->combo_MeasMode->addItem(tr("Am"));//"Глубина АМ");
    ui->combo_MeasMode->addItem(tr("Fm"));//"Девиация ЧМ");
    ui->combo_MeasMode->addItem(tr("Osc"));//"Осциллограмма");
    ui->combo_MeasMode->addItem(tr("Fft"));//"Спектр");
    ui->combo_MeasMode->addItem(tr("Freq"));//"Частотомер");
    ui->combo_MeasMode->addItem(tr("SINAD"));//"КНИ/SINAD");
    ui->combo_MeasMode->addItem(tr("TLG"));//"Искажения ТЛГ");
    ui->combo_MeasMode->addItem(tr("Psopho"));//"Псофометр");
    ui->combo_MeasMode->addItem(tr("Sens SINAD"));//"Чувствительность SINAD");
    ui->combo_MeasMode->addItem(tr("Sens TLG"));//"Чувствительность ТЛГ");
    ui->combo_MeasMode->addItem(tr("AFC"));//"АЧХ");
*/
    //ui->combo_MeasMode->setCurrentIndex(1);
    //ui->combo_MeasMode->setEnabled(false);

    //on_combo_MeasMode_activated(0);

    m_nAmax=16383;
    ui->slider_Amax->setRange(0, 16383);
    ui->slider_Amax->setSingleStep(1);
    ui->slider_Amax->setValue(m_nAmax);

    ///////////////////////////////////////
    udp_client=NULL;
    udp_server=NULL;

    // find out which IP to connect to
    QString ipAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // if we did not find one, use IPv4 localhost
    if (ipAddress.isEmpty())
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();

    ui->edit_IP->setText(/*ipAddress*//*"192.168.100.2"*/m_strOscIp);
    ui->edit_Port->setText("50000");
    ui->edit_Port->setValidator(new QIntValidator(1, 65535, this));

    ui->edit_IP->setEnabled(false);
    ui->edit_Port->setEnabled(false);

    initSocket();

    ui->pushButton->setVisible(false);

    //////////////////////////////////////
/*    if (!server.listen(QHostAddress::Any, 55555)) {
           QMessageBox::critical(this, tr("Threaded Fortune Server"),
                                 tr("Unable to start the server: %1.")
                                 .arg(server.errorString()));

           return;
    }
*/
    loadProfiles();

    //QMetaObject::invokeMethod(m_worker, "initDDS",  Qt::BlockingQueuedConnection );
//    m_worker->initDDS();
    renderThread->getWorker()->initDDS();
    //auto_window=new AutoWindow(m_worker, this);
    //ui->verticalLayout->addWidget(auto_window);
    //ui->widget_zxc->hide();

    //ui->label_Header->hide();
    this->setWindowTitle("AKID");
    //ui->combo_MeasMode->setVisible(false);
    //ui->edit_SKZ->setVisible(false);
    //ui->edit_Power->setVisible(false);
    chThread = new CheckingThread(mainTabPtr,this);
    connect(chThread, SIGNAL(finished()),this, SLOT(checkingProcessStoped()));
    connect(ui->repeatBtn,SIGNAL(clicked()), this, SLOT(checkingCycle()));
    connect(ui->startCheckBtn, SIGNAL(clicked()), this, SLOT(startChecking()));

    connect(ui->cursorBtn, SIGNAL(clicked()), this,SLOT(setZoomerState()));
    connect(ui->peakBtn, SIGNAL(clicked()), this, SLOT(showPeaksTab()));


    for (int i =0;i<mode.size();i++)
        ui->plotTabWidget->setCurrentIndex(mode.at(i)->getTabIndex());
    ui->plotTabWidget->setCurrentIndex(0);
    for (int i=0;i<mode.size();i++) {
        currentMode = mode.at(i);
        if (i==0) {
            setOscInterval(0);
            setOscScale1(0);
            setOscScale2(0);
        }
        on_combo_Ch1Type_activated(0);
        on_combo_Ch2Type_activated(0);
        setCoupling1(0);
        setCoupling2(0);
        markersColorChanged(markColor);
        markersColorChanged(markColor2);
        showGenerator(false);
        //setZoomerState();
    }

}

ADcfDemoMainWindow::~ADcfDemoMainWindow()
{
    closeSocket();
    delete ui;
}

void ADcfDemoMainWindow::closeEvent(QCloseEvent *e)
{
    Q_UNUSED(e);
    if (chThread->isRunning())
        chThread->stopProcess();
    mode.last()->renderSuspend();
//    for (int i=0; i<mode.size();i++) {
//        mode.at(i)->renderSuspend();
//        delete mode.at(i);
//    }
    //if (currentMode)
    //    currentMode->renderSuspend();
    //renderThread->stopProcess();
    closeSocket();

    if(m_thread)
    {
        m_thread->stop();
        if(!m_thread->wait(5000))
            qDebug() << "Client: can't stop thread!";

        delete m_thread;
        m_thread=NULL;
    }
    if(m_worker)
    {
        delete m_worker;
        m_worker=NULL;
    }
}

void ADcfDemoMainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void ADcfDemoMainWindow::on_combo_Ch1Type_activated(int index)
{
    ui->combo_Ch1Atten->clear();
    //ui->combo_Ch1Atten->setEnabled(index>0);

    if(index==0)
    {
        ui->combo_Ch1Atten->addItem("1/1");
    }
    else if(index==1)
    {
        ui->combo_Ch1Atten->addItem("1/1");
        ui->combo_Ch1Atten->addItem("1/10");
    }
    else if(index==2)
    {
        ui->combo_Ch1Atten->addItem("1/1");
        ui->combo_Ch1Atten->addItem("1/10");
        ui->combo_Ch1Atten->addItem("1/30");
        ui->combo_Ch1Atten->addItem("1/300");
    }
    currentMode->getRenderConfig()->nChType=index;
    QString text = ui->combo_Ch1Type->itemText(index);
    currentMode->setLegText(0,0,text);
    on_combo_Ch1Atten_activated(0);
}

void ADcfDemoMainWindow::on_combo_Ch2Type_activated(int index)
{
    ui->combo_Ch2Atten->clear();

    if(index==0)
    {
        ui->combo_Ch2Atten->addItem("1/1");
    }
    else if(index==1)
    {
        ui->combo_Ch2Atten->addItem("1/1");
        ui->combo_Ch2Atten->addItem("1/10");
    }
    else if(index==2)
    {
        ui->combo_Ch2Atten->addItem("1/1");
        ui->combo_Ch2Atten->addItem("1/10");
        ui->combo_Ch2Atten->addItem("1/30");
        ui->combo_Ch2Atten->addItem("1/300");
    }
    currentMode->getRenderConfig()->nChType2=index;
    QString text = ui->combo_Ch2Type->itemText(index);
    currentMode->setLegText(0,1,text);
    on_combo_Ch2Atten_activated(0);
}

void ADcfDemoMainWindow::on_combo_MeasMode_activated(int index)
{
    m_nMeasMode=currentMode->getProcess()->getID();//index;

    on_button_SpecWrite_clicked();

    //ui->edit_Power->setText("");
    //ui->edit_SKZ->setText("");

    //currentMode->getPlot()->signalData(0).clearValues();
    //currentMode->getPlot()->signalData(1).clearValues();
    //currentMode->getPlot()->signalData(2).clearValues();
    //d_plot[0]->signalData(0).clearValues();
    //d_plot[0]->signalData(1).clearValues();
    //d_plot[0]->signalData(2).clearValues();

    //ui->label_16->setVisible(false);
    //ui->edit_Power->setVisible(false);
    //ui->label_10->setVisible(false);
    //ui->edit_SKZ->setVisible(false);
    ui->timescaleBox->setVisible(false);
    ui->timescaleLbl->setVisible(false);
    ui->ch1ScaleBox->setVisible(false);
    ui->ch2ScaleBox->setVisible(false);
    ui->coupBox1->setVisible(false);
    ui->coupBox2->setVisible(false);
    ui->groupBox_2->setVisible(false);
    ui->triggerBox->setVisible(false);
    ui->timescale->setVisible(false);
    //ui->groupBox_2->setVisible(false);

    //on_combo_Ch1Type_activated(ui->combo_Ch1Type->currentIndex());
    //on_combo_Ch2Type_activated(ui->combo_Ch2Type->currentIndex());
    if(index==0)
    {
        //renderThread=mode[2]->getRender();
        //currentMode=mode[2];
        //ui->label_10->setText(tr("Deep AM, %:"));//"Глубина АМ, %:");
        //on_combo_Ch1Atten_activated(m_nChAtten);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        ui->groupBox_2->setVisible(true);
        //measModeChange(2);

        //ui->plotTabWidget->setCurrentIndex(3);
        if (currentMode->getRenderConfig()->subMeasMode == 3) {
            ui->timescaleBox->setVisible(true);
            ui->timescale->setVisible(true);
        }
        else ui->timescale->hide();
        QList<Plot*> *l = currentMode->getPlotList();
        if (currentMode->getRenderConfig()->subMeasMode == 4) {
            l->first()->hide();
            currentMode->setZoomerEnable(false);
            l->last()->setZoomScale(-1,1,QwtPlot::xBottom);
            l->last()->setZoomScale(-1,1,QwtPlot::yLeft);
            l->last()->setZoomScale(-1,1,QwtPlot::yRight);
        }
        else{
            currentMode->setZoomerEnable(true);
            if (l->first()->isHidden()) l->first()->show();
            for (int i=0;i<l->size();i++) {
            l->at(i)->setInterval(0, RAW_SAMPLES_NUM/(Fdec/1000000));
            l->at(i)->setScale(-1*32767*getGainY(m_nChAtten), 32767*getGainY(m_nChAtten));
            //l->at(i)->setAxisTitle(QwtPlot::yLeft, tr("V"));
            }
        }
        //l->at(0)->setAxisTitle(QwtPlot::xBottom, tr(""));
        //l->at(1)->setAxisTitle(QwtPlot::xBottom, tr("mks"));

        /*currentMode->getPlot()->setInterval(0, RAW_SAMPLES_NUM/(Fdec/1000000));
        currentMode->getPlot()->setScale(-1*32767*getGainY(m_nChAtten), 32767*getGainY(m_nChAtten));
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("V"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("mks"));
*/
        /*mode[Mode::IQAN_ID]->getPlot()->setInterval(0, RAW_SAMPLES_NUM/(Fdec/1000000));
        mode[Mode::IQAN_ID]->getPlot()->setScale(-1*32767*getGainY(m_nChAtten), 32767*getGainY(m_nChAtten));
        mode[Mode::IQAN_ID]->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("V"));
        mode[Mode::IQAN_ID]->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("mks"));*/
    }
    else if(index==1)
    {
        //ui->label_10->setText(tr("Deviation FM, Hz:"));//"Девиация ЧМ, Гц:");

        //on_combo_Ch1Atten_activated(m_nChAtten);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        ui->groupBox_2->setVisible(true);
        //todo
        currentMode->getPlot()->setInterval(0, RAW_SAMPLES_NUM/(Fdec/1000000));
        currentMode->getPlot()->setScale(-1*32767*getGainY(m_nChAtten), 32767*getGainY(m_nChAtten));
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("V"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("mks"));
    }
    else if(index==2)
    {   //Osci
        //currentMode=mode[0];
        //ui->label_10->setText(tr("SKZ, V:"));//"СКЗ, В:");
        //renderThread=mode[0]->getRender();

        //ui->plotTabWidget->setCurrentIndex(1);
        //measModeChange(0);

        //ui->label_16->setVisible(true);
        //ui->edit_Power->setVisible(true);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        ui->timescaleBox->setVisible(true);
        ui->timescaleLbl->setVisible(true);
        ui->ch1ScaleBox->setVisible(true);
        ui->ch2ScaleBox->setVisible(true);
        ui->coupBox1->setVisible(true);
        ui->coupBox2->setVisible(true);
        ui->triggerBox->setVisible(true);
        ui->timescale->setVisible(true);
        //todo

        //d_plot[0]->setYScale();
        //currentMode->getPlot()->setYScale(ui->ch1ScaleBox->currentIndex(),QwtPlot::yLeft);
        //currentMode->getPlot()->setYScale(ui->ch2ScaleBox->currentIndex(),QwtPlot::yRight);
        //mode[Mode::OSCI_ID]->getPlot()->setYScale();
        setOscInterval(ui->timescaleBox->currentIndex());
        setOscScale1(ui->ch1ScaleBox->currentIndex());
        setOscScale2(ui->ch2ScaleBox->currentIndex());
    }
    else if(index==3)
    {
        //Spec
        //renderThread=mode[1]->getRender();
        //currentMode=mode[1];
        //measModeChange(2);
        //ui->plotTabWidget->setCurrentIndex(2);

        currentMode->getPlot()->setInterval(Fleft/1000000, Fright/1000000);
        //currentMode->getPlot()->setInterval(0, 2);
        currentMode->getPlot()->setScale(-200, 200);
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("dBV"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("MHz"));
        ui->groupBox_2->setVisible(true);

        /*mode[Mode::SPEC_ID]->getPlot()->setInterval(Fleft/1000000, Fright/1000000);
        mode[Mode::SPEC_ID]->getPlot()->setScale(-200, 120);
        mode[Mode::SPEC_ID]->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("dBV"));
        mode[Mode::SPEC_ID]->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("MHz"));*/
        //d_plot[0]->setAxisLabelRotation(QwtPlot::yLeft, 90);
    }
    else if(index==4)
    {
        //ui->label_10->setText(tr("Freq, Hz:"));//"Частота, Гц:");

        //on_combo_Ch1Atten_activated(m_nChAtten);
        //d_plot[0]->setAxisTitle(QwtPlot::yLeft, "V");

        //ui->label_16->setVisible(false);
        //ui->edit_Power->setVisible(false);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        /*ui->timescaleBox->setVisible(false);
        ui->timescaleLbl->setVisible(false);
        ui->ch1ScaleBox->setVisible(false);
        ui->ch2ScaleBox->setVisible(false);*/

        currentMode->getPlot()->setInterval(0, FFT_SAMPLES_NUM);
        currentMode->getPlot()->setScale(-200, 120);
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("dBV"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, " ");
    }
    else if(index==5)
    {
        //ui->label_10->setText(tr("KNI, %:"));//"КНИ,КГИ:");

        //on_combo_Ch1Atten_activated(m_nChAtten);
        //d_plot[0]->setAxisTitle(QwtPlot::yLeft, "V");

        //ui->label_16->setVisible(false);
        //ui->edit_Power->setVisible(false);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        /*ui->timescaleBox->setVisible(false);
        ui->timescaleLbl->setVisible(false);
        ui->ch1ScaleBox->setVisible(false);
        ui->ch2ScaleBox->setVisible(false);*/

        //d_plot[0]->setInterval(0, FFT_SAMPLES_NUM);
        currentMode->getPlot()->setInterval(Fleft/1000000, Fright/1000000);
        currentMode->getPlot()->setScale(-200, 120);
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("dBV"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("MHz"));
    }
    else if(index == 6)
    {
        //ui->label_10->setText(tr("KVP, %:"));//"КВП, %:");
        //on_combo_Ch1Atten_activated(m_nChAtten);
        //d_plot[0]->setAxisTitle(QwtPlot::yLeft, "V");

        //ui->label_16->setVisible(false);
        //ui->edit_Power->setVisible(false);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        /*ui->timescaleBox->setVisible(false);
        ui->timescaleLbl->setVisible(false);
        ui->ch1ScaleBox->setVisible(false);
        ui->ch2ScaleBox->setVisible(false);*/
        // todo
        ui->groupBox_2->setVisible(true);
        currentMode->getPlot()->setInterval(0, RAW_SAMPLES_NUM/(Fdec/1000000));
        currentMode->getPlot()->setScale(-1*32767*getGainY(m_nChAtten), 32767*getGainY(m_nChAtten));
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("V"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("mks"));
    }
    else if(index==7)
    {
        //ui->label_10->setText(tr("Upsoph, V:"));

        //ui->label_16->setVisible(false);
        //ui->edit_Power->setVisible(false);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        /*ui->timescaleBox->setVisible(false);
        ui->timescaleLbl->setVisible(false);
        ui->ch1ScaleBox->setVisible(false);
        ui->ch2ScaleBox->setVisible(false);*/

        currentMode->getPlot()->setInterval(0, FFT_SAMPLES_NUM);
        currentMode->getPlot()->setScale(-200, 120);
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("dBV"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, " ");

        ui->edit_Fcentr->setText("0");
        ui->edit_Band->setText("6.8");
        ui->combo_WindowType->setCurrentIndex(4);
    }
    else if(index==8) // sens sinad
    {
        //ui->label_10->setText(tr("U, mkV:"));

        //ui->label_16->setVisible(false);
        //ui->edit_Power->setVisible(false);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        /*ui->timescaleBox->setVisible(false);
        ui->timescaleLbl->setVisible(false);
        ui->ch1ScaleBox->setVisible(false);
        ui->ch2ScaleBox->setVisible(false);*/

        //d_plot[0]->setInterval(0, FFT_SAMPLES_NUM);
        currentMode->getPlot()->setInterval(Fleft/1000000, Fright/1000000);
        currentMode->getPlot()->setScale(-200, 120);
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("dBV"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("MHz"));
    }
    else if(index==9) // sens tlg
    {
        //ui->label_10->setText(tr("U, mkV:"));

        //ui->label_16->setVisible(false);
        //ui->edit_Power->setVisible(false);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        /*ui->timescaleBox->setVisible(false);
        ui->timescaleLbl->setVisible(false);
        ui->ch1ScaleBox->setVisible(false);
        ui->ch2ScaleBox->setVisible(false);*/
        // todo
        currentMode->getPlot()->setInterval(0, RAW_SAMPLES_NUM/(Fdec/1000000));
        currentMode->getPlot()->setScale(-1*32767*getGainY(m_nChAtten), 32767*getGainY(m_nChAtten));
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("V"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("mks"));
    }
    else if(index==10)
    {
        //ui->label_10->setText(tr("Nonliner, dB:"));//"Нелинейность АЧХ, dB:");

        //ui->label_16->setVisible(false);
        //ui->edit_Power->setVisible(false);
        //ui->label_10->setVisible(true);
        //ui->edit_SKZ->setVisible(true);
        /*ui->timescaleBox->setVisible(false);
        ui->timescaleLbl->setVisible(false);
        ui->ch1ScaleBox->setVisible(false);
        ui->ch2ScaleBox->setVisible(false);*/

        //d_plot[0]->setInterval(0, FFT_SAMPLES_NUM);
        currentMode->getPlot()->setInterval(Fleft/1000000, Fright/1000000);
        currentMode->getPlot()->setScale(-200, 120);
        currentMode->getPlot()->setAxisTitle(QwtPlot::yLeft, tr("dBV"));
        currentMode->getPlot()->setAxisTitle(QwtPlot::xBottom, tr("MHz"));
    }
}

void ADcfDemoMainWindow::on_combo_Ch1Atten_activated(int index)
{
    //перевод в вольты
    //QMessageBox::information(0,",",",",QMessageBox::Ok);
    m_nChAtten=index==-1?0:index;
    currentMode->getRenderConfig()->nChAtten=m_nChAtten;
    //m_nChAtten2=index==-1?0:index;
    QString text = ui->combo_Ch1Atten->itemText(index);
    currentMode->setLegText(1,0,text);
    on_combo_MeasMode_activated(currentMode->getProcess()->getID());//ui->combo_MeasMode->currentIndex());
    /*
    double val=0.0;
    double val2=0.0;

    val=32767*getGainY(m_nChAtten);
    val2=-1*32767*getGainY(m_nChAtten);

    d_plot[0]->setScale(val2, val);
*/
}

void ADcfDemoMainWindow::on_combo_Ch2Atten_activated(int index)
{
    //перевод в вольты
    //m_nChAtten=index==-1?0:index;
    //QMessageBox::information(0,",",",",QMessageBox::Ok);
    m_nChAtten2=index==-1?0:index;
    currentMode->getRenderConfig()->nChAtten2=m_nChAtten2;
    QString text = ui->combo_Ch2Atten->itemText(index);
    currentMode->setLegText(1,1,text);
    on_combo_MeasMode_activated(currentMode->getProcess()->getID());//ui->combo_MeasMode->currentIndex());
    /*
    double val=0.0;
    double val2=0.0;

    val=32767*getGainY(m_nChAtten);
    val2=-1*32767*getGainY(m_nChAtten);

    d_plot[0]->setScale(val2, val);
*/
}

void ADcfDemoMainWindow::on_combo_Gen_activated(int index)
{
   m_nGenMode=index;
   currentMode->getRenderConfig()->nGenMode=index;

   ui->edit_GenKmod->setVisible(m_nGenMode==DDS_A3E);
   ui->edit_GenFsym->setVisible(m_nGenMode==DDS_A1A || m_nGenMode==DDS_F1B || m_nGenMode==DDS_G1B);
   ui->edit_GenFmod->setVisible(m_nGenMode==DDS_J3E || m_nGenMode==DDS_A3E || m_nGenMode==DDS_H3E ||
                                m_nGenMode==DDS_R3E || m_nGenMode==DDS_F3EA || m_nGenMode==DDS_F3EJ);
   ui->edit_GenFdev->setVisible(m_nGenMode==DDS_F1B || m_nGenMode==DDS_F3EA || m_nGenMode==DDS_F3EJ);
   ui->combo_GenNegPos->setVisible(m_nGenMode==DDS_J3E || m_nGenMode==DDS_R3E || m_nGenMode==DDS_H3E);

   ui->label_K->setVisible(ui->edit_GenKmod->isVisible());
   ui->label_Fsym->setVisible(ui->edit_GenFsym->isVisible());
   ui->label_Fmod->setVisible(ui->edit_GenFmod->isVisible());
   ui->label_Fdev->setVisible(ui->edit_GenFdev->isVisible());
   ui->label_NegPos->setVisible(ui->combo_GenNegPos->isVisible());

   m_nGenFc=ui->edit_GenFc->text().toLongLong();
   m_nGenFsym=ui->edit_GenFsym->text().toLongLong();
   m_nGenFmod=ui->edit_GenFmod->text().toLongLong();
   m_nGenFdev=ui->edit_GenFdev->text().toLongLong();
   m_fGenK=ui->edit_GenKmod->text().toDouble();
   m_nGenSSBType=ui->combo_GenNegPos->currentIndex();
   //m_worker->initGEN(m_nGenMode);

//   if(ui->combo_MeasMode->currentIndex()==8) // тут в зависимости от типа генератора разные измерения
//       on_combo_MeasMode_activated(ui->combo_MeasMode->currentIndex());

   /*QMetaObject::invokeMethod(m_worker, "initGEN",  Qt::QueuedConnection ,
                             Q_ARG(quint8, m_nGenMode),
                             Q_ARG(quint64, m_nGenFc),
                             Q_ARG(quint32, m_nAmax),
                             Q_ARG(quint32, m_nAmin),
                             Q_ARG(quint64, m_nGenFmod),
                             Q_ARG(quint64, m_nGenFsym),
                             Q_ARG(quint64, m_nGenFdev),
                             Q_ARG(quint8, m_nGenSSBType),
                             Q_ARG(qreal, m_fGenK));
                             */

//   m_worker->initGEN(m_nGenMode,
//                     m_nGenFc,
//                     m_nAmax,
//                     m_nAmin,
//                     m_nGenFmod,
//                     m_nGenFsym,
//                     m_nGenFdev,
//                     m_nGenSSBType,
//                     m_fGenK);
}

void ADcfDemoMainWindow::connectDevice()
{

}

void ADcfDemoMainWindow::disconnectDevice()
{


    qDebug("disconnect device");

    //ui->editState->setText(tr("not connected"));
}

// oscilloscope
void ADcfDemoMainWindow::initOscope()
{
    //const double intervalLength = 100000000.0; // seconds
    titles.append(tr("f1"));
    titles.append(tr("f2"));
    titles.append(tr("f3"));
    titles.append(tr("f4"));

    QStringList headers[4];
    headers[0].append(tr("Number"));
    headers[0].append(tr("Frequency"));
    headers[0].append(tr("dBm"));
    headers[1].append(tr("Net"));
    headers[1].append(tr("Frequency"));
    headers[1].append(tr("dBm"));
    headers[2].append(tr("Number"));
    headers[2].append(tr("Frequency"));
    headers[2].append(tr("dBm"));
    headers[3].append(tr("Net"));
    headers[3].append(tr("Frequency"));
    headers[3].append(tr("dBm"));

    QStringList list;
    list.append(tr("MAIN"));
    list.append(tr("Osci"));
    list.append(tr("Spec"));
    list.append(tr("IQ Analyzer"));
    ui->plotTabWidget->clear();
    ui->plotTabWidget->setTabsClosable(true);
    tabBtn = new QPushButton(tr("Add Tab"));
    ui->plotTabWidget->setCornerWidget(tabBtn);

    mainModeWigdetInit();
    for(int i=0;i<Mode::COUNT_ID;i++)
    {
        modeInit(i);
        //configInit(mode.last());
        measModeChange(mode.at(i)->getTabIndex());
    }
    connect(renderThread, SIGNAL(started()), this, SLOT(on_processStarted()));
    connect(renderThread, SIGNAL(finished()), this, SLOT(on_processStopped()));
//    connect(renderThread, SIGNAL(renderStarted()), this ,SLOT(rendStarted()), Qt::BlockingQueuedConnection);
    for (int i=0;i<3;i++) {
        insertMode(i,mode.at(i));
        currentMode = mode.at(i); //
        //on_combo_Ch1Type_activated(0);
        //on_combo_Ch2Type_activated(0);
        //on_combo_Ch1Atten_activated(0);
        //on_combo_Ch2Atten_activated(0);
    }
    //renderThread = mode.last()->getRender();
    /*for (int i=0;i<mode.size();i++) {
        mode.at(i)->getWidget()->getOutputLay()->addWidget(
                mode.at(i)->getDeltaTab());
        mode.at(i)->getWidget()->getOutputLay()->addWidget(
                mode.at(i)->getPeaksTab());
    }*/
    measModeChange(0);
    ui->widget_Debug->setVisible(false);
    ui->text_Log->setMaximumBlockCount(5000);
    //this->resize(this->width()-100 ,this->height()-300);
    //this->updateGeometry();
    initDone=true;

/*
    connect(d_amplitudeKnob, SIGNAL(valueChanged(double)),
        SIGNAL(amplitudeChanged(double)));
    connect(d_frequencyKnob, SIGNAL(valueChanged(double)),
        SIGNAL(frequencyChanged(double)));
    connect(d_timerWheel, SIGNAL(valueChanged(double)),
        SIGNAL(signalIntervalChanged(double)));
*/
//    connect(d_intervalWheel, SIGNAL(valueChanged(double)),
//        d_plot, SLOT(setIntervalLength(double)) );

}

void ADcfDemoMainWindow::start()
{
    //isStarted = true;
    for(int i=0; i<Mode::COUNT_ID;i++) ;
    //d_plot[i]->start();
}

double ADcfDemoMainWindow::frequency() const
{
    return 0.0;//d_frequencyKnob->value();
}

double ADcfDemoMainWindow::amplitude() const
{
    return 0.0;//d_amplitudeKnob->value();
}

double ADcfDemoMainWindow::signalInterval() const
{
    return 0.0;//d_timerWheel->value();
}

void ADcfDemoMainWindow::on_pushButton_clicked()
{
    m_bWork=!m_bWork;
    ui->pushButton->setText(m_bWork?(tr("Close")):(tr("Open")));

    if(m_bWork)
    {
        initSocket();
        //initDevice();

        //renderThread->processImage();
    }
    else
    {
        //renderThread->stopProcess();
        closeSocket();
    }
}

//////////////////////////////////////////////////////////
void ADcfDemoMainWindow::initSocket()
{
    //udp_client=new QUdpSocket(this);
    //udp_client->bind(/*QHostAddress("192.168.100.1")*/QHostAddress::LocalHost, 50001);
    //udp_client->connectToHost(ui->edit_IP->text(), ui->edit_Port->text().toInt()/*QHostAddress::LocalHost, 7755*/);

    udp_server=new QUdpSocket(this);
    udp_server->bind(QHostAddress::Any, 50000);

    //connect(udp_client, SIGNAL(readyRead()),this, SLOT(readPendingDatagrams()));
    connect(udp_server, SIGNAL(readyRead()),this, SLOT(readPendingDatagramsSrv()));
}

void ADcfDemoMainWindow::closeSocket()
{
    if(udp_client)
    {
        udp_client->close();
        delete udp_client;
    }

    if(udp_server)
    {
        udp_server->close();
        delete udp_server;
    }
    udp_client=NULL;
    udp_server=NULL;
}

void ADcfDemoMainWindow::readPendingDatagramsSrv()
{
    while (udp_server->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udp_server->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        udp_server->readDatagram(datagram.data(), datagram.size(),
                                 &sender, &senderPort);

        processDatagramSrv(datagram, sender, senderPort);
    }
}

void ADcfDemoMainWindow::processDatagramSrv(QByteArray datagram,QHostAddress sender, quint16 senderPort)
{
    qDebug("SRV: LDATA=%X%X CMD=%X BADDR=%X", (int)datagram[0], (int)datagram[1], (int)datagram[2], (int)datagram[3]);

    if((char)datagram[3]==0x10)
    {
  /*      args.append(QPoint(3,m_nReadPtr&0xff)); // адрес0
        args.append(QPoint(4,(m_nReadPtr>>8)&0xff)); // адрес1
        args.append(QPoint(5,0x80)); // количество0
        args.append(QPoint(6,0x00)); // количество1
        args.append(QPoint(2,0));           // сброс бита чтения
        args.append(QPoint(2,m_nMeasMode+1));           // установка бита чтения
    */
        quint8 addr1=datagram[4];
        quint8 data1=datagram[5]; // read ptr 0

        quint8 addr2=datagram[6];
        quint8 data2=datagram[7]; // read ptr 1

        quint8 addr3=datagram[8];
        quint8 data3=datagram[9];

        int addr=data2<<8;
        addr|=data1;

        quint8 frame_id=0;
        if(m_nMeasMode==0 || m_nMeasMode==1|| m_nMeasMode==6|| m_nMeasMode==9) frame_id=0x03; // coplex ddc data
        else if(m_nMeasMode==2) frame_id=0x05; // raw osc data
        else if(m_nMeasMode==3 || m_nMeasMode==4|| m_nMeasMode==5|| m_nMeasMode==7 || m_nMeasMode==8 || m_nMeasMode==10) frame_id=0x04; // fft data

        QByteArray data;
        data.append(/*m_nMeasMode==0?0x03:0x05*/frame_id);
        data.append(datagram[7]);
        data.append(datagram[5]);
        data.append((char)0x00);
        data.append((char)0x80);
        data.append(m_data_test.mid(addr*4,128*4));

        // send to client
        udp_server->writeDatagram(data.data(), data.size(),sender,senderPort);
    }
    else
    {
        // echo to client
        //udp_server->writeDatagram(datagram.data(), datagram.size(),sender,senderPort);
    }
}

void ADcfDemoMainWindow::on_button_WriteReg_clicked()
{
    QList<QPoint> param_list;
    QString str=ui->edit_WDATA->toPlainText();
    QStringList list=str.split(QChar('\n'), QString::SkipEmptyParts);

    for(int i=0;i<list.size();i++)
    {
        QString item=list.at(i);
        QStringList args=item.split(QChar(' '), QString::SkipEmptyParts);
        QString addr("0");
        QString data("0");

        if(args.size()>=1) addr=args.at(0);
        if(args.size()>=2) data=args.at(1);

        uint a=addr.toUInt(0, 16);
        uint d=data.toUInt(0, 16);

        param_list.append(QPoint(a,d));
    }

    quint8 BADDR=ui->combo_BlockId->itemData(ui->combo_BlockId->currentIndex()).toInt();
    //WriteReg(BADDR, param_list);
    currentMode->getRender()->WriteReg(BADDR, param_list);
}

void ADcfDemoMainWindow::WriteLog(quint16 LDATA, quint8 CMD, quint8 BADDR, const QList<QPoint> &WDATA)
{
    if(!ui->text_Log->isVisible())return;

    QString str, str1;
    str.sprintf("PC->OSC: LDATA=0x%04X CMD=0x%02X BADDR=0x%02X | ", LDATA, CMD, BADDR);

    for(int i=0;i<WDATA.size();i++)
    {
        QPoint item=WDATA.at(i);

        quint16 a=item.x();
        quint8 d=item.y();

        str1.sprintf("0x%04X 0x%02X ", a,d);
        str+=str1;
    }
    str+="\n";

    ui->text_Log->appendPlainText(str);
    ui->text_Log->ensureCursorVisible();
}

void ADcfDemoMainWindow::WriteLog(QByteArray datagram)
{
    if(!ui->text_Log->isVisible())return;

    QString str;
    QTextStream stream(&str);
    stream << hex << qSetFieldWidth(2) << qSetPadChar('0') << right;
    for(int i=0;i<datagram.size();i++)
    {
        stream << qSetFieldWidth(2) << static_cast<unsigned char>(datagram[i]) << qSetFieldWidth(0) <<" ";
        //stream << static_cast<unsigned char>(datagram[i]) <<" ";
    }

    ui->text_Log->appendPlainText("OSC->PC: "+/*datagram*/str + QString("\n"));
    ui->text_Log->ensureCursorVisible();
}

void ADcfDemoMainWindow::WriteLog(QString str)
{
    if(!ui->text_Log->isVisible())return;

    ui->text_Log->appendPlainText(str);
    ui->text_Log->ensureCursorVisible();
}

void ADcfDemoMainWindow::on_button_SpecWrite_clicked()
{
    /*double */Fs=1000000*ui->edit_Fs->text().toDouble();
        Fcentr=1000000*ui->edit_Fcentr->text().toDouble();
        Band=1000*ui->edit_Band->text().toDouble();
    /*double */
        //Fleft=ui->edit_Fleft->text().toDouble();
        Fleft=Fcentr-Band/2;
    /*double */
        //Fright=ui->edit_Fright->text().toDouble();
        Fright=Fcentr+Band/2;
    /*int */window=ui->combo_WindowType->currentIndex();
    /*int */Nfft=FFT_SAMPLES_NUM;

    /*double *//*Fcentr=(Fleft+Fright)/2;*/
    /*double *//*Band=Fright-Fleft;*/

    /*double */Dec_ideal=Fs/(2*Band);
    /*int */Dec_3=(Dec_ideal>=2)? 2 : 1;

    /*int */Dec_1=qFloor(Dec_ideal/Dec_3); //41.2 -> 41
    if(Dec_1>=256)Dec_1=256;

    /*int */Dec_2=qFloor(Dec_ideal/(Dec_1*Dec_3));
    if(Dec_2>=256)Dec_2=256;

    /*unsigned long */Dec_sum=Dec_1*Dec_2*Dec_3;

    /*double */Fdec=Fs/Dec_sum;

    /*int */Nband=(Band*Nfft)/Fdec;

return;

}

void ADcfDemoMainWindow::on_pushButton_2_clicked()
{ // start
    if (currentMode->getRenderConfig()->subMeasMode==-1) {
        QMessageBox::critical(this,"Attention","Meas mode not selected",QMessageBox::Ok);
        return;
    }

    if (!ui->ChanelBox1->isChecked() && !ui->ChanelBox2->isChecked()) {
        QMessageBox::critical(0, "Attention","Chanel not selected",
                                 QMessageBox::Ok);
        return;
    }

    if (!ui->plotTabWidget->currentIndex())
        return;
    //isStarted = true;
    m_bCycle=ui->check_Cycle->isChecked();

    //m_nCh=ui->combo_ChanSelect->currentIndex();
    m_nChType=ui->combo_Ch1Type->currentIndex();
    m_nChType2=ui->combo_Ch2Type->currentIndex();
    m_nChAtten=ui->combo_Ch1Atten->currentIndex();
    m_nChAtten2=ui->combo_Ch2Atten->currentIndex();
    m_nMeasMode=currentMode->getProcess()->getID();//ui->combo_MeasMode->currentIndex();
    m_nChScale1=ui->ch1ScaleBox->currentIndex();
    m_nChScale2=ui->ch2ScaleBox->currentIndex();
    m_nTimeScale=ui->timescaleBox->currentIndex();

    m_nTaktADC=ui->combo_taktADC->currentIndex();
    m_nTaktPLL=ui->combo_taktPLL->currentIndex();

    m_nGenMode=ui->combo_Gen->currentIndex();
    m_nGenFsym=ui->edit_GenFsym->text().toLongLong();
    m_nGenFmod=ui->edit_GenFmod->text().toLongLong();
    m_nGenFdev=ui->edit_GenFdev->text().toLongLong();

    //m_bStart=!m_bStart;
    //ui->pushButton_2->setText(m_bStart?tr("Stop"):tr("Start"));

    if(!m_bStarted)
    {
        //SetParameters();
        currentMode->getProcess()->clearStatistic();
        on_button_SpecWrite_clicked();
        makeTestData();
        //makeTestDataFFT();

        //m_worker->initDDS();
        //m_worker->initDDS_J3E(100000000 ,1600000, 16383, 1000, 1);
        //m_worker->initDDS_F1B(100000000,1000000,16383,300000,16000);
        //m_worker->initDDS_A3E(100000000,1600000,16383,1000,0.5,32);

        m_Ch1 = currentMode->getRenderConfig()->Ch1;
        m_Ch2 = currentMode->getRenderConfig()->Ch2;

//        currentMode->getRenderConfig()->plots=
//                currentMode->getPlotList();
        currentMode->renderConfInit(m_Ch1,
                            m_Ch2,
                           m_nMeasMode,
                           //m_nCh,
                           m_nChType,
                           m_nChType2,
                           m_nChAtten,
                           m_nChAtten2,
                           m_nTaktADC,
                           m_nGenMode,
                           m_nGenFc,
                           m_nAmax,
                           m_nAmin,
                           m_nGenFdev,
                           m_nGenFsym,
                           m_nGenFmod,
                           m_nGenSSBType,
                           m_fGenK,
                           Fs,
                           Fcentr,
                           Band,
                           window,//&fft_window[window][0],
                           m_nChScale1,
                           m_nChScale2,
                           m_nTimeScale,
                           currentMode->getPlot(),
                           m_bCycle);
        renderThread->setProcess(currentMode->getProcess());


        currentMode->getRender()->startProcess2(
                    currentMode->getRenderConfig());
        //startDecoding(); // начинаем запись и чтение
    }
    else
    {
        currentMode->getRender()->stopProcess();
        //stopDecoding();
    }

}

void ADcfDemoMainWindow::configInit(Mode *m) {
    if (m->initDone) return;

//    renderThread->setProcess(m->getProcess());
    m_bCycle=ui->check_Cycle->isChecked();
    m_nChType=ui->combo_Ch1Type->currentIndex();
    m_nChType2=ui->combo_Ch2Type->currentIndex();
    m_nChAtten=ui->combo_Ch1Atten->currentIndex();
    m_nChAtten2=ui->combo_Ch2Atten->currentIndex();
    m_nMeasMode=m->getProcess()->getID();//ui->combo_MeasMode->currentIndex();
//    m_nChScale1=ui->ch1ScaleBox->currentIndex();
//    m_nChScale2=ui->ch2ScaleBox->currentIndex();
//    m_nTimeScale=ui->timescaleBox->currentIndex();
    m_nChScale1=-1;
    m_nChScale2=-1;
    m_nTimeScale=-1;

    m_nTaktADC=ui->combo_taktADC->currentIndex();
    m_nTaktPLL=ui->combo_taktPLL->currentIndex();

    m_nGenMode=ui->combo_Gen->currentIndex();
    m_nGenFsym=ui->edit_GenFsym->text().toLongLong();
    m_nGenFmod=ui->edit_GenFmod->text().toLongLong();
    m_nGenFdev=ui->edit_GenFdev->text().toLongLong();

    m_Ch1 = m->getRenderConfig()->Ch1;
    m_Ch2 = m->getRenderConfig()->Ch2;

    on_button_SpecWrite_clicked();
    makeTestData();

    renderThread->setProcess(m->getProcess());
//    m->getRenderConfig()->plots=
//            m->getPlotList();
    m->renderConfInit(m_Ch1,
                            m_Ch2,
                           m_nMeasMode,
                           //m_nCh,
                           m_nChType,
                           m_nChType2,
                           m_nChAtten,
                           m_nChAtten2,
                           m_nTaktADC,
                           m_nGenMode,
                           m_nGenFc,
                           m_nAmax,
                           m_nAmin,
                           m_nGenFdev,
                           m_nGenFsym,
                           m_nGenFmod,
                           m_nGenSSBType,
                           m_fGenK,
                           Fs,
                           Fcentr,
                           Band,
                           window,//&fft_window[window][0],
                           m_nChScale1,
                           m_nChScale2,
                           m_nTimeScale,
                           m->getPlot(),
                           m_bCycle);
    renderThread->initRender(m->getRenderConfig());
    m->initDone=true;
}

void ADcfDemoMainWindow::makeTestData()
{
    //////////////////////////////////////
    //make test data
    QPointF s;
    for(int j=0; j<RAW_SAMPLES_NUM; j++)
    {
        s.setX(j);

        short val=0;
        short val2=0;

        short y=0;
        short y2=0;

        val=32767*qSin(2*3.14159265*j*Fcentr/Fs);
        val2=32767*qCos(2*3.14159265*j*Fcentr/Fs);

        // АМ модуляция
        float C=16383;
        float m=0.5;
        float A0=1;
        val=C*(1+m*A0*qSin(2*3.14159265*j*Fcentr/(10*Fs)))*qSin(2*3.14159265*j*Fcentr/Fs);
        //QMessageBox::information(0,QString::number(Fcentr),QString::number(Fs),QMessageBox::Ok);


        y=val;
        y2=val2;

        // if(val2>=0) y2=val2;
        //else y2=(~val2+1)|0x8000;

        m_data_test[j*4]=(y2>>8);
        m_data_test[j*4+1]=y2&0xff;

        m_data_test[j*4+2]=(y>>8);
        m_data_test[j*4+3]=y&0xff;

        /*if(y&0x8000)
    {
        y--;
        y=~y;
        val=-1*(int)y;
    }
    else val=(int)y;*/
        short yy2=m_data_test[j*4]<<8;
        yy2|=0xff&m_data_test[j*4+1];

        short yy=m_data_test[j*4+2]<<8;
        yy|=0xff&m_data_test[j*4+3];

        s.setY(val/*yy*/);
        //d_plot[0]->signalData(2).append(s);
    }
}

void ADcfDemoMainWindow::makeTestData2()
{
    //////////////////////////////////////
    //make test data
    QPointF s;
    for(int j=0; j<RAW_SAMPLES_NUM; j++)
    {
        s.setX(j);

        short val=0;
        short val2=0;

        short y=0;
        short y2=0;

        //Re(i) = cos(Fdev*sin(2*pi*Fn*i/Fd)/Fn)
        //Im(i) = sin(Fdev*sin(2*pi*Fn*i/Fd)/Fn)

        val=32767*qSin(2*3.14159265*j*Fcentr/Fs);
        val2=32767*qCos(2*3.14159265*j*Fcentr/Fs);

        // АМ модуляция
        float C=16383;
        float m=0.32;
        float A0=1;
        val=C*(1+m*A0*qSin(2*3.14159265*j*Fcentr/(10*Fs)))*qSin(2*3.14159265*j*Fcentr/Fs);


        y=val;
        y2=val2;

        // if(val2>=0) y2=val2;
        //else y2=(~val2+1)|0x8000;

        m_data_test[j*4]=(y2>>8);
        m_data_test[j*4+1]=y2&0xff;

        m_data_test[j*4+2]=(y>>8);
        m_data_test[j*4+3]=y&0xff;


        short yy2=m_data_test[j*4]<<8;
        yy2|=0xff&m_data_test[j*4+1];

        short yy=m_data_test[j*4+2]<<8;
        yy|=0xff&m_data_test[j*4+3];

        s.setY(val/*yy*/);
        currentMode->getPlot()->signalData(2).append(s);
    }
}

void ADcfDemoMainWindow::makeTestDataFFT()
{
    QStringList files;
    files.append("KNI_data_4096.txt");

    for(int i=0;i<files.size();i++)
    {
        QString dir=QCoreApplication::applicationDirPath ();
        QString fileName = dir+"/"+files.at(i);//QFileDialog::getOpenFileName(this);
        if (fileName.isEmpty())
            return;

        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox::warning(this,
                                 tr("File error"),
                                 tr("Failed to open\n%1").arg(fileName));
            return;
        }
        int j=0;
        while (!file.atEnd())
        {
            QString str = file.readLine().trimmed();
            if (!str.isEmpty())
            {
                unsigned int y=str.toInt();

                m_data_test[j*4]=(y>>24)&0xff; //m_data[jj*4]<<24;
                m_data_test[j*4+1]=(y>>16)&0xff; //y|=(0xff&m_data[jj*4+1])<<16;
                m_data_test[j*4+1]=(y>>8)&0xff; //y|=(0xff&m_data[jj*4+2])<<8;
                m_data_test[j*4+3]=(y)&0xff; //y|=0xff&m_data[jj*4+3];
            }
            j++;
        }
        file.close();
    }

}


double ADcfDemoMainWindow::getGainY(int nAtten)
{
    //return 1.0;
    //перевод в вольты
    //int nAtten=m_nChAtten;//ui->combo_Ch1Atten->currentIndex();
    int N=1;
    switch(nAtten)
    {
    case 0:N=1;
        break;
    case 1:N=10;
        break;
    case 2:N=30;
        break;
    case 3:N=300;
        break;
    }

    double val=(1.25*N)/(m_nMeasMode==3||m_nMeasMode==4||m_nMeasMode==5||m_nMeasMode==7|| m_nMeasMode==8|| m_nMeasMode==10?qPow(2,28):32767); // todo *2 for spectr

    if(m_nChType==0)val=val*7/8; // 50 ohm
    else if(m_nChType==1)val=val*0.5; //600 ohm
    else if(m_nChType==2)val=val*0.5; //1 Mohm

    return val;
}

double ADcfDemoMainWindow::getGainX()
{
    double val=0.005;  // 200 MHz

    //перевод в mks
    switch(m_nMeasMode)
    {
    case 0:// глубина ам
    case 1:// девиация чм  
    case 6:// тлг
    case 9:// чувств тлг
        val=1000000/Fdec;
        break;
    case 2: // вч напряжение
        break;
    case 3: // спектр
    case 4:// частота
    case 5:// кни
    case 7:// псоф
    case 8:// чувствит
    case 10: // ачх
        val=Band/Nband;
        break;
    }

    return val;
}

void ADcfDemoMainWindow::enableControl()
{
    //ui->combo_ChanSelect->setEnabled(!m_bStarted);
    ui->combo_Ch1Type->setEnabled(!m_bStarted);
    ui->combo_Ch1Atten->setEnabled(!m_bStarted);
    ui->combo_Ch2Type->setEnabled(!m_bStarted);
    ui->combo_Ch2Atten->setEnabled(!m_bStarted);


    ui->edit_Band->setEnabled(!m_bStarted);
    ui->edit_Fcentr->setEnabled(!m_bStarted);
    ui->combo_WindowType->setEnabled(!m_bStarted);

    //ui->ch1ScaleBox->setEnabled(!m_bStarted);
    //ui->ch2ScaleBox->setEnabled(!m_bStarted);
    ui->timescaleBox->setEnabled(!m_bStarted);
    ui->coupBox1->setEnabled(!m_bStarted);
    ui->coupBox2->setEnabled(!m_bStarted);

    //ui->combo_MeasMode->setEnabled(!m_bStarted);

    if(m_nMeasMode==8 || m_nMeasMode==9) // чувствит
        ui->groupBox_6->setEnabled(!m_bStarted);
}

void ADcfDemoMainWindow::dataReady(QString str1, QString str2, QString str3)
{
    static int ma=1;
    if(ma==1)ma=2;
    else ma=1;
    //ui->edit_GlubAM->setText(QString::number(ma,'f', 2));
    qDebug("data ready ............ %d ", ma);
    ///if(m_bCycle)startDecoding();
    //else on_pushButton_2_clicked(); // stop meas
    currentMode->getPlot()->replot();

    if(m_nMeasMode==4 || m_nMeasMode==6 || m_nMeasMode==7 || m_nMeasMode==8 || m_nMeasMode==9 || m_nMeasMode==10)
    {
        QString str=str1+QString("|")+str2+QString("|")+str3;
        //ui->edit_SKZ->setText(str);
    }
    else if(m_nMeasMode==5)
    {
        QString str=QString("Kni=")+str1+QString(" | Kgi=")+str2 +QString(" | SINAD=")+str3;
        //ui->edit_SKZ->setText(str);
    }
    else
    {
        //ui->edit_GlubAM->setText(str1);
        //ui->edit_SKZ->setText(str1+QString("|")+str2+QString("|")+str3);
        //ui->edit_Power->setText(str2);
    }
return;

}

void ADcfDemoMainWindow::on_processStarted()
{
    RenderThread *r =(RenderThread*) QObject::sender();
    //QPalette p; p.setColor(QPalette::Window,Qt::green);
    //this->setPalette(p);

    m_bStarted=true;
    ui->pushButton_2->setText(tr("Stop"));
    ui->button_WriteReg->setEnabled(false);
    enableControl();
    //ui->check_Cycle->setEnabled(false);
    if (!r) return;
    QString text = currentMode->getTabName()/*QString(r->modeConf->tabName)*/ + " ***";
    ui->plotTabWidget->setTabText(
        r->modeConf->tabIndex,
        text);
}

void ADcfDemoMainWindow::on_processStopped()
{
    RenderThread *r =(RenderThread*) QObject::sender();
    //if (!r) return;
    m_bStarted=false;
    //QMessageBox::information(0,"","",QMessageBox::Ok);
    ui->pushButton_2->setText(tr("Start"));
    ui->button_WriteReg->setEnabled(true);
    enableControl();
    chThread->modeChecked();
    if (!r) return;
    ui->plotTabWidget->setTabText(
        r->modeConf->tabIndex,
        currentMode->getTabName()/*QString(r->modeConf->tabName)*/);
    //currentMode->getPlot()->replot();
}

void ADcfDemoMainWindow::loadProfiles()
{
    cur_list=0;
    cur_profile=0;

    QStringList files;
    files.append("windower_coef_rectangular_4096.txt");
    files.append("windower_coef_hann_4096.txt");
    files.append("windower_coef_hamming_4096.txt");
    files.append("windower_coef_blackman_4096.txt");
    files.append("windower_coef_blackmanharris_4096.txt");
    files.append("windower_coef_flattopwin_4096.txt");

    memset(renderThread->getWorker()->fft_window /*m_worker->fft_window*/, 0, FFT_WIN_NUM*FFT_WIN_LEN);

    for(int i=0;i<files.size();i++)
    {
        QString dir=QCoreApplication::applicationDirPath ();
        QString fileName = dir+"/"+files.at(i);//QFileDialog::getOpenFileName(this);
        if (fileName.isEmpty())
            return;

        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox::warning(this,
                                 tr("File error"),
                                 tr("Failed to open\n%1").arg(fileName));
            return;
        }
        int j=0;
        while (!file.atEnd())
        {
            QString str = file.readLine().trimmed();
            if (!str.isEmpty())
            {
                QStringList sl=str.split(/*QRegExp("[ ;]")*/QChar(' '), QString::SkipEmptyParts);
                if(sl.size()>1)
                {
                    int addr=sl.at(0).toInt();
                    int coeff=sl.at(1).toInt();
                    //m_worker->fft_window[i][j]=coeff;
                    renderThread->getWorker()->fft_window[i][j] = coeff;
                }
            }
            j++;
        }
        file.close();
    }

}

void ADcfDemoMainWindow::loadConfig()
{
    m_strIp="192.168.100.4";
    m_strOscIp="192.168.100.3";

    /*QString fileName="apak.conf";

    //for(int i=0;i<files.size();i++)
    //{
        QString dir=QCoreApplication::applicationDirPath ();
        QString filePath = dir+"/"+fileName;//QFileDialog::getOpenFileName(this);
        if (filePath.isEmpty())
            return;

        QFile file(filePath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox::warning(this,
                                 tr("File error"),
                                 tr("Failed to open\n%1").arg(filePath));
            return;
        }

        while (!file.atEnd())
        {
            QString str = file.readLine().trimmed();
            if (!str.isEmpty())
            {
                QStringList sl=str.split(QRegExp("[ ;]")QChar(' '), QString::SkipEmptyParts);
                if(sl.size()>1)
                {
                    m_strIp=sl.at(0);
                    m_strOscIp=sl.at(1);
                }

            }

        }
        file.close();
    //}

    QString name="apak.ini";
    QString iniFile=dir+"/"+name;
    QSettings settings(iniFile, QSettings::IniFormat);*/

}

void ADcfDemoMainWindow::on_edit_Fcentr_editingFinished()//textChanged(const QString &arg1)
{
    //on_combo_MeasMode_activated(currentMode->getProcess()->getID());//ui->combo_MeasMode->currentIndex());
}

void ADcfDemoMainWindow::on_edit_Band_editingFinished()//textChanged(const QString &arg1)
{
    //on_combo_MeasMode_activated(currentMode->getProcess()->getID());//ui->combo_MeasMode->currentIndex());
}

void ADcfDemoMainWindow::on_slider_Amax_sliderReleased()
{
    m_nAmax=ui->slider_Amax->value();
    on_combo_Gen_activated(m_nGenMode);
}

void ADcfDemoMainWindow::on_slider_Amax_valueChanged(int value)
{
    m_nAmax=value;
    qreal fU=(m_nAmax*25.0)/16383;
    ui->label_Amax->setText(QString::number(fU, 'f', 2)+QString(tr(" mkV")));
    if(!ui->slider_Amax->isSliderDown())
       on_combo_Gen_activated(m_nGenMode);
}

void ADcfDemoMainWindow::setM_nCh() {
    //if (ui->ChanelBox1->isChecked())
    //    this->m_nCh = 1;
}

void ADcfDemoMainWindow::onColorChanged(colorLabel *lbl) {
    int chanelID = lbl->currentChanel();
    for (int i=0; i<mode.size(); i++) {
        mode.at(i)->setChanelColor(chanelID,
                                   lbl->getLblColor());
//        mode.at(i)->getPlot()->setColor(chanelID,
//                      lbl->getLblColor(),m_bStarted);
    }
    //currentMode->getPlot()->setColor(chanelID,
    //          lbl->getLblColor(),m_bStarted);
    //d_plot[0]->replot();
}

void ADcfDemoMainWindow::chanelSelected() {
    QCheckBox *ch = (QCheckBox*) QObject::sender();
    if (!ch) return;
    if (currentMode->currentMode() == Mode::SPEC_ID ||
            currentMode->currentMode() == Mode::IQAN_ID)
    {
        if (ch->objectName() == "ChanelBox1") {
            chanel1Selected(ch->isChecked());
            chanel2Selected(!ch->isChecked());
            ui->ChanelBox2->setChecked(!ch->isChecked());
        }
        if (ch->objectName() == "ChanelBox2") {
            chanel1Selected(!ch->isChecked());
            chanel2Selected(ch->isChecked());
            ui->ChanelBox1->setChecked(!ch->isChecked());
        }
        return;
    }
    if (ch->objectName() == "ChanelBox1") {
        chanel1Selected(ch->isChecked());
    }

    if (ch->objectName() == "ChanelBox2") {
        chanel2Selected(ch->isChecked());
    }

    //currentMode->setSignalState(0,ui->ChanelBox1->isChecked());
    //currentMode->setSignalState(1,ui->ChanelBox2->isChecked());
}

void ADcfDemoMainWindow::setOscInterval(int val) {
    if (val < 0) return;
    //ui->timescaleBox->setCurrentIndex(val==-1?0:val);
    if (currentMode->getRenderConfig()->tSc ==
            val)
        return;
    m_nTimeScale = val;
    currentMode->getRender()->setTimeScale(val);
    currentMode->getRenderConfig()->tSc=val==-1?0:val;
    currentMode->getPlot()->setXScale(val);
    if (currentMode->getRenderConfig()->subMeasMode == 3) {
        currentMode->getPlotList()->first()->setXScale(val);
    }
    if (ui->ChanelBox1->isChecked())
        currentMode->getProcess()->processData(
                    currentMode->getRender()->getData(),0);
    if (ui->ChanelBox2->isChecked())
        currentMode->getProcess()->processData(
                    currentMode->getRender()->getData2(),1);
}

void ADcfDemoMainWindow::setOscScale1(int val) {
    if (val < 0) return;
    if (currentMode->getRenderConfig()->sC1 ==
            val)
        return;
    QString text = ui->ch1ScaleBox->itemText(val);
    //ui->ch1ScaleBox->setCurrentIndex(val==-1?0:val);
    currentMode->getRenderConfig()->sC1=val==-1?0:val;
    //currentMode->getPlot()->setCurveText(text, 0);
    currentMode->getRender()->setScale(0,val);
    currentMode->getPlot()->setYScale(val,QwtPlot::yLeft);
    currentMode->setLegText(2,0,text);
    //d_plot[0]->setScale(-1*32767*getGainY(m_nChAtten), 32767*getGainY(m_nChAtten));
    //d_plot[0]->setAxisTitle(QwtPlot::yLeft, tr("V"));

}

void ADcfDemoMainWindow::setOscScale2(int val) {
    if (val < 0) return;
    if (currentMode->getRenderConfig()->sC2 ==
            val)
        return;
    QString text = ui->ch2ScaleBox->itemText(val);
    //ui->ch2ScaleBox->setCurrentIndex(val==-1?0:val);
    currentMode->getRenderConfig()->sC2=val==-1?0:val;
    //currentMode->getPlot()->setCurveText(text, 1);
    currentMode->getRender()->setScale(1,val);
    currentMode->getPlot()->setYScale(val,QwtPlot::yRight);
    currentMode->setLegText(2,1,text);
    //d_plot[0]->setScale(-1*32767*getGainY(m_nChAtten), 32767*getGainY(m_nChAtten));
    //d_plot[0]->setAxisTitle(QwtPlot::yLeft, tr("V"));
}

void ADcfDemoMainWindow::setCoupling1(int val) {
    QString text=ui->coupBox1->itemText(val);
    currentMode->setLegText(3,0,text);
}

void ADcfDemoMainWindow::setCoupling2(int val) {
    QString text=ui->coupBox1->itemText(val);
    currentMode->setLegText(3,1,text);
}
void ADcfDemoMainWindow::measModeChange(int x){
    //QMessageBox::information(0,"",QString::number(Mode::modeCounter),QMessageBox::Ok);
//    for (int i=0;i<mode.size();i++)
//        mode.at(i)->renderSuspend();
    if (x==0) {//mainTab
        showCurrentMods();
        ui->mainTab->setVisible(true);
        ui->modeTabWidget->setVisible(false);
        fillToolbar(true);
        return;
    }
    ui->groupBox_4->setVisible(true);
    blockUiElements(false);
    ui->mainTab->setVisible(false);
    ui->modeTabWidget->setVisible(true);
    currentMode = mode[x-1];
    for (int i=0; i<mode.size(); i++) {
        if (QObject::sender())
            mode[i]->getWidget()->getLay()->insertWidget(0,
                     mode[i]->getWidget()->getOutputWidget());
        //mode[i]->getWidget()->setPlot(mode[i]->getPlot());
        //mode[i]->setUsed(false);
        if (mode[i]->getWidget()->getOutputWidget()->isHidden())
            mode[i]->getWidget()->getOutputWidget()->show();
        /*if (mode[i]->getPlot()->isHidden())
            mode[i]->getPlot()->show();*/
    }
    //renderThread=currentMode->getRender();
    if (initDone)
    {
        configInit(currentMode);
    }
    baseConfLoad(currentMode->currentMode());
    fillToolbar(false);

    if (currentMode->currentMode()==Mode::OSCI_ID) {
        on_combo_MeasMode_activated(2);
        //ui->combo_MeasMode->setCurrentIndex(2);
    }
    if (currentMode->currentMode()==Mode::SPEC_ID) {
        on_combo_MeasMode_activated(3);
        //ui->combo_MeasMode->setCurrentIndex(3);
    }
    if (currentMode->currentMode()==Mode::IQAN_ID) {
        on_combo_MeasMode_activated(0);
        //ui->combo_MeasMode->setCurrentIndex(0);
    }
    //if (initDone)
    //    configInit();
    //ui->verticalLayout_7->setEnabled(false);
    //ui->mainTab->setVisible(false);
    //ui->modeTabWidget->setVisible(false);
    if (renderThread->isRunning())
    {
        QWaitCondition wc;
        QMutex m;
        renderThread->suspendProcess();
        while(!renderThread->suspended);
        {
            wc.wait(&m, 500);
        }
        currentMode->initDone = false;
        configInit(currentMode);
        renderThread->setProcess(currentMode->getProcess());
        renderThread->resume();
    }
}

void ADcfDemoMainWindow::renderStarted(Mode *obj) {
  if (m_bStarted) {
    obj->getRender()->startProcess2(
                obj->getRenderConfig());
  }
}

Mode* ADcfDemoMainWindow::addNewTab() {
    ModeDialog d(this);
    Mode *m;
    if (d.exec() == QDialog::Rejected)
        return 0;
    unsigned r=d.getResult();

    if (r==Mode::OSCI_ID) {
        m=modeInit(Mode::OSCI_ID);
        //ui->timescaleBox->setCurrentIndex(0);
        //ui->ch1ScaleBox->setCurrentIndex(0);
        //ui->ch2ScaleBox->setCurrentIndex(0);
    }
    if (r==Mode::SPEC_ID) {;
        m=modeInit(Mode::SPEC_ID);
    }
    if (r==Mode::IQAN_ID) {
        m=modeInit(Mode::IQAN_ID);
    }
    //currentMode=m;
    measModeChange(ui->plotTabWidget->count()-1);
    ui->plotTabWidget->setCurrentIndex(ui->plotTabWidget->count()-1);
    ui->timescaleBox->setCurrentIndex(0);
    ui->ch1ScaleBox->setCurrentIndex(0);
    ui->ch2ScaleBox->setCurrentIndex(0);
    m->setChanelColor(0,mode.at(0)->getPlot()->currentColor);
    m->setChanelColor(1,mode.at(0)->getPlot()->currentColor2);
    on_combo_Ch1Type_activated(0);
    on_combo_Ch2Type_activated(0);
    setCoupling1(0);
    setCoupling2(0);
    m->showPeaks(false);

    m->getWidget()->getOutputLay()->addWidget(
            m->getDeltaTab());
    return m;
}

void ADcfDemoMainWindow::deleteTab(int index) {
    if (index<4)
        return;
    mode.at(index-1)->renderSuspend();
    if (mode.at(index-1)->isUsed())
        mainTabPtr->clearMode(mode.at(index-1)->tabPos());
    if (ui->plotTabWidget->currentIndex()==index) {
        ui->plotTabWidget->setCurrentIndex(0);
        currentMode=mode.at(0);
    }
    ui->plotTabWidget->removeTab(index);
    //currMode[mode[index-1]->currentMode()]--;
    delete mode[index-1];
    //modCnt--;
    mode.removeAt(index-1);
    int x = ui->plotTabWidget->count();
    QString text;
    int osc=1;
    int spec=1;
    int iqan=1;
    for (int i=1;i<x;i++) {
        mode.at(i-1)->setTabIndex(i);
        text = mode.at(i-1)->getName();
        switch (mode.at(i-1)->currentMode()) {
        case Mode::OSCI_ID:
            text = text+QString::number(osc++);
            break;
        case Mode::SPEC_ID:
            text = text+QString::number(spec++);
            break;
        case Mode::IQAN_ID:
            text = text+QString::number(iqan++);
            break;
        }

        if (index==ui->plotTabWidget->currentIndex())
            currentMode=0;
        ui->plotTabWidget->setTabText(i,text);
    }
}

Mode* ADcfDemoMainWindow::modeInit(int id) {
    QString name;
    mode.append(Mode::createMode(id));
    qDebug()<<"mode created"<<id;
    name = mode.last()->getName()+
            QString::number(mode.last()->modeCnt());
    qDebug()<<"plot pre";
    int counter;
    if (id == Mode::OSCI_ID)
        counter = 4;
    else counter=2;
    mode.last()->plotInit(counter);
    qDebug()<<"plot created";
    //mode.last()->setLegElementNum();
    qDebug()<<"something";
    mode.last()->processInit(id);
    qDebug()<<"processObj created";
    //mode.last()->renderInit(m_worker, m_strIp, m_strOscIp, this);
    //mode.last()->getRender()->setMainThread(thread());
    mode.last()->renderInit(renderThread);
    qDebug()<<"render created";
    mode.last()->setTabName(name);
    mode.last()->deltaTabInit();
    mode.last()->peakTabInit();
    mode.last()->getWidget()->setPlot(
                mode.last()->getPlotList());
    mode.last()->getProcess()->processInit(
                mode.last()->getPlot(),
                mode.last()->getRender(),
                mode.last()->getRenderConfig());
    mode.last()->getProcess()->SetPlots(
                mode.last()->getPlotList());
    //QString dawd = tr("wdaw");
    mode.last()->setTabIndex(
                ui->plotTabWidget->addTab(mode.last()->getWidget(),name));

    mode.last()->getProcess()->attachPeakMarkers();
//    connect(mode.last()->getRender(), SIGNAL(started()), this, SLOT(on_processStarted()));
//    connect(mode.last()->getRender(), SIGNAL(finished()), this, SLOT(on_processStopped()));
//    connect(mode.last()->getRender(), SIGNAL(renderStarted()), this ,SLOT(rendStarted()), Qt::BlockingQueuedConnection);
    //connect(m_worker, SIGNAL(recvPacket(const QByteArray& )), mode.last()->getRender(), SLOT(processDatagram(const QByteArray& )), Qt::BlockingQueuedConnection);
    //connect(m_worker, SIGNAL(resumeRead()), mode.last()->getRender(), SLOT(resumeProcess()), Qt::BlockingQueuedConnection);
    //connect(mode.last(), SIGNAL(sendData(QString,QString,QString)), this,SLOT(dataReady(QString,QString,QString)));
//    connect(mode.last(), SIGNAL(renderStarted(Mode*)), this, SLOT(renderStarted(Mode*)));
    mode.last()->getRenderConfig()->fFc = 1600000;//->setFc("1600000");
    mode.last()->getRenderConfig()->uiFCentr=1;
    mode.last()->getRenderConfig()->uiBand=500;



    QList<QAction*> *l = mode.last()->getWidget()->getActions(); //= mode.last()->getWidget()->btnsList();
    for (int i = 0;i<l->size();i++) {
        connect(l->at(i),SIGNAL(triggered()), this, SLOT(subMeasModeChanged()));
    }

    mode.last()->getWidget()->getActions()->at(0)->trigger();
    //mode.last()->getWidget()->btnsList()->at(0)->click();
    //mode.last()->getRenderConfig()->currMode = mode.last();


    currentMode = mode.last();
    Mode::modeCounter++;
    return mode.last();
}


void ADcfDemoMainWindow::mainModeWigdetInit(){
    mainTabPtr = new ModsMainWidget();
    ui->plotTabWidget->addTab(mainTabPtr,tr("Main"));
    QList<QToolButton*> *dialBtns =mainTabPtr->getDialogBtns();
    QList<QToolButton*> *clrBtns =mainTabPtr->getClearBtns();
    QList<QToolButton*> *switchBtns = mainTabPtr->getSwitchBtns();
    for (int i=0;i<dialBtns->size(); i++) {
        connect(dialBtns->at(i), SIGNAL(clicked()),
                this, SLOT(modsBoxUpdate()));
        connect(clrBtns->at(i), SIGNAL(clicked()),
                this, SLOT(clearMode()));
        connect(switchBtns->at(i),SIGNAL(clicked()),
                this,SLOT(switchMode()));
    }
}

void ADcfDemoMainWindow::modsBoxUpdate() {
    QPushButton *t = (QPushButton*)QObject::sender();
    int temp;
    if (!t) return;
    PlotDialog d;
    //QMessageBox::information(0,"",t->text(),QMessageBox::Ok);
    for (int i=1; i<ui->plotTabWidget->count();i++) {
        QString text = ui->plotTabWidget->tabText(i);
        Mode* temp = mode.at(i-1);
        d.fillPlotBox(text,temp);
    }
    Mode *m=0;
    if (!d.isReady()) {
        /*QMessageBox::critical(this,"Attention",
                              "No available mods",
                              QMessageBox::Ok);*/
        m = addNewTab();
        if (!m) return;
        ui->plotTabWidget->setCurrentIndex(0);
        //return;
    }
    if (!m) {
        if (d.exec() == QDialog::Rejected)
            return;
        m=d.getMode();
    }
    if (t->objectName() == "dialog0")
        temp=0;
    if (t->objectName() == "dialog1")
        temp=1;
    if (t->objectName() == "dialog2")
        temp=2;
    if (t->objectName() == "dialog3")
        temp=3;
    insertMode(temp,m);
}

void ADcfDemoMainWindow::insertMode(int x, Mode *m){
    //QPushButton *W = new QPushButton;
    mainTabPtr->clearMode(x);
    if (m->getWidget()->getOutputWidget()->isHidden())
        m->getWidget()->getOutputWidget()->show();
    switch(x) {
    case 0:
        //mainTabPtr->vbLay1()->addWidget(m->getWidget()->getPlot());
        mainTabPtr->vbLay1()->addWidget
                (m->getWidget()->getOutputWidget());
        break;
    case 1:
        mainTabPtr->vbLay2()->addWidget
                (m->getWidget()->getOutputWidget());
        break;
    case 2:
        mainTabPtr->vbLay3()->addWidget
                (m->getWidget()->getOutputWidget());
        break;
    case 3:
        mainTabPtr->vbLay4()->addWidget
                (m->getWidget()->getOutputWidget());
        break;
    }

    mainTabPtr->setModeName(x,m->getTabName());
    mainTabPtr->setMode(x,m);
    m->setTabPos(x);
    m->setUsed(true);
}

void ADcfDemoMainWindow::clearMode() {
    QPushButton *t = (QPushButton*)QObject::sender();
    if (!t) return;
    if (t->objectName()=="clear0"){
        mainTabPtr->clearMode(0);
    }
    if (t->objectName()=="clear1"){
        mainTabPtr->clearMode(1);
    }
    if (t->objectName()=="clear2"){
        mainTabPtr->clearMode(2);
    }
    if (t->objectName()=="clear3"){
        mainTabPtr->clearMode(3);
    }
}

void ADcfDemoMainWindow::showCurrentMods(){
    blockUiElements(true);
    for (int i=0;i<4;i++) {
        Mode *m = mainTabPtr->modeAt(i);
        if (m) {
            insertMode(i,m);
        }
    }
}

void ADcfDemoMainWindow::loadOscConf() {
    //QMessageBox::information(0,"","",QMessageBox::Ok);
    ui->timescaleBox->setCurrentIndex(
                currentMode->getRenderConfig()->tSc);
    ui->ch1ScaleBox->setCurrentIndex(
                currentMode->getRenderConfig()->sC1);
    ui->ch2ScaleBox->setCurrentIndex(
                currentMode->getRenderConfig()->sC2);
    m_nTaktADC=ui->combo_taktADC->currentIndex();
    m_nTaktPLL=ui->combo_taktPLL->currentIndex();
}

void ADcfDemoMainWindow::loadSpecConf() {

}

void ADcfDemoMainWindow::loadIQAnConf() {

}

void ADcfDemoMainWindow::baseConfLoad(int x) {
    //configInit();
    //setZoomerState();
    mrkFreezeChb->setChecked(
                currentMode->getRenderConfig()->markersFreeze);

    ui->markersColorGrp->setVisible(
                currentMode->getPlot()->markersIsHidden());

    showCursors = !currentMode->getRenderConfig()->markersShow;
    setZoomerState();
    /*if (!currentMode->getRenderConfig()->markersShow)
    {
        currentMode->setZoomerState(true);
        ui->cursorBtn->setText("Show Cursors");
    }
    else {
        currentMode->setZoomerState(false);
        ui->cursorBtn->setText("Hide Cursors");
    }*/
    ui->check_Cycle->setChecked(
                currentMode->getRenderConfig()->bCycle);
    lbl1->setCurrentColor(currentMode->getPlot()->currentColor);
    lbl2->setCurrentColor(currentMode->getPlot()->currentColor2);
    ui->combo_Gen->setCurrentIndex(
                currentMode->getRenderConfig()->nGenMode);
    ui->edit_GenFc->setText(QString::number(
    currentMode->getRenderConfig()->fFc));
    ui->edit_Fcentr->setText(
                QString::number(currentMode->getRenderConfig()->uiFCentr));
    ui->edit_Band->setText(
                QString::number(currentMode->getRenderConfig()->uiBand));
    ui->combo_Ch1Type->setCurrentIndex(
                currentMode->getRenderConfig()->nChType);
    ui->combo_Ch2Type->setCurrentIndex(
                currentMode->getRenderConfig()->nChType2);
    ui->combo_Ch1Atten->setCurrentIndex(
                currentMode->getRenderConfig()->nChAtten);
    ui->combo_Ch2Atten->setCurrentIndex(
                currentMode->getRenderConfig()->nChAtten2);
    ui->ChanelBox1->setChecked(
                currentMode->getRenderConfig()->Ch1);
    ui->ChanelBox2->setChecked(
                currentMode->getRenderConfig()->Ch2);
    switch (x) {
    case Mode::OSCI_ID:
        loadOscConf();
        break;
    case Mode::SPEC_ID:
        loadSpecConf();
        break;
    case Mode::IQAN_ID:
        loadIQAnConf();
        break;
    }
}

void ADcfDemoMainWindow::genFcChanged(QString t) {
    if (!ui->plotTabWidget->currentIndex())
        return;
    //currentMode->getRenderConfig()->setFc(t);
    currentMode->getRenderConfig()->fFc = t.toDouble();
}

void ADcfDemoMainWindow::uiFCentrChanged(QString t){
    currentMode->getRenderConfig()->uiFCentr=t.toDouble();
    on_combo_MeasMode_activated(currentMode->getProcess()->getID());
    ui->edit_Fcentr->setFocus();
}

void ADcfDemoMainWindow::uiBandChanged(QString t){
    currentMode->getRenderConfig()->uiBand=t.toDouble();
    on_combo_MeasMode_activated(currentMode->getProcess()->getID());
    ui->edit_Band->setFocus();
}

void ADcfDemoMainWindow:: blockUiElements(bool c){
    ui->groupBox_2->setVisible(!c);
    ui->groupBox_3->setVisible(!c);
    ui->triggerBox->setVisible(!c);
    //ui->groupBox_6->setVisible(!c);
    ui->timescale->setVisible(!c);
    //sideModeTab->setVisible(!c);

    ui->combo_Ch1Type->setEnabled(!c);
    ui->combo_Ch2Type->setEnabled(!c);
    ui->combo_Ch1Atten->setEnabled(!c);
    ui->combo_Ch2Atten->setEnabled(!c);
    ui->timescaleBox->setEnabled(!c);
    //ui->ch1ScaleBox->setEnabled(!c);
    //ui->ch2ScaleBox->setEnabled(!c);
    ui->coupBox1->setEnabled(!c);
    ui->coupBox2->setEnabled(!c);
    ui->combo_GenNegPos->setEnabled(!c);
    ui->edit_GenKmod->setEnabled(!c);
    ui->edit_GenFdev->setEnabled(!c);
    ui->edit_GenFmod->setEnabled(!c);
    ui->edit_GenFsym->setEnabled(!c);
    ui->edit_GenFc->setEnabled(!c);
    ui->edit_Fcentr->setEnabled(!c);
    ui->edit_Band->setEnabled(!c);
    ui->combo_WindowType->setEnabled(!c);
    ui->combo_Gen->setEnabled(!c);
    ui->slider_Amax->setEnabled(!c);
    //ui->combo_MeasMode->setEnabled(!c);
    ui->pushButton_2->setEnabled(!c);
    ui->ChanelBox1->setEnabled(!c);
    ui->ChanelBox2->setEnabled(!c);
}

void ADcfDemoMainWindow::checkingCycle() {
    if (chThread)
        chThread->setCheckingCycle(ui->repeatBtn->isChecked());
}

void ADcfDemoMainWindow::startChecking(){
    QList<Mode *> *l = mainTabPtr->getModes();
    /*for (int i = 0; i<4;i++) {
        if (l->at(i)) {
// did this for correct configs init in chosen mods
            if (!initDone)
                ;measModeChange(l->at(i)->getTabIndex());
        }
    }
    if (!initDone) {
        measModeChange(0);
        initDone=true;
    }*/
    chThread->setFullModes(&mode);
    if (!chThread->isRunning())
    {
        ui->startCheckBtn->setText(tr("StopChecking"));
        ui->checkAllBtn->setEnabled(false);
        //checkingAll->setEnabled(false);
        chThread->startChecking(ui->repeatBtn->isChecked(),
                                 ui->checkAllBtn->isChecked());
    }
    else
    {
        ui->startCheckBtn->setText(tr("StartChecking"));
        chThread->stopProcess();
    }
}

void ADcfDemoMainWindow::setCycle(bool c) {
    currentMode->getRenderConfig()->bCycle=c;
}



void ADcfDemoMainWindow::checkingProcessStoped() {
     ui->checkAllBtn->setEnabled(true);
    ui->startCheckBtn->setText(tr("StartChecking"));
}

void ADcfDemoMainWindow::switchMode() {
    QPushButton *b = (QPushButton*) QObject::sender();
    QList<Mode*> *ml = mainTabPtr->getModes();
    Mode *m;
    if (b->objectName() == "switchBtn")
        m=ml->at(0);
    if (b->objectName() == "switchBtn_2")
        m=ml->at(1);
    if (b->objectName() == "switchBtn_3")
        m=ml->at(2);
    if (b->objectName() == "switchBtn_4")
        m=ml->at(3);
    for (int i = 0; i<mode.size(); i++) {
        if (m==mode.at(i))
            ui->plotTabWidget->setCurrentIndex(
                        m->getTabIndex());
    }
}

void ADcfDemoMainWindow::rendStarted() {
    RenderThread *r =(RenderThread*) QObject::sender();
    qDebug()<<"move to "<<r->currentThreadId()<<" from "<< thread()->currentThreadId()<<" main ";
//    m_worker->moveToThread(r);

}

void ADcfDemoMainWindow::chanel1Selected(bool s) {
    this->m_Ch1 = s;
    currentMode->getRenderConfig()->Ch1 =s;
    //currentMode->getPlot()->showMarkers(0,s);
    currentMode->showPlotMarkers(0,s);
    currentMode->setSignalState(0,s);
}

void ADcfDemoMainWindow::chanel2Selected(bool s) {
    this->m_Ch2 = s;
    currentMode->getRenderConfig()->Ch2 =s;
    currentMode->showPlotMarkers(1,s);
    //currentMode->getPlot()->showMarkers(1,s);
    currentMode->setSignalState(1,s);
}

void ADcfDemoMainWindow::setZoomerState() {
    //bool markersOn;
    if (showCursors == false) {
        currentMode->setZoomerState(false);
        ui->cursorBtn->setText(tr("Hide Cursors"));
        //markersOn = true;
        currentMode->getRenderConfig()->markersShow = true;
        showCursors = true;
        //currentMode->getDeltaTab()->setVisible(true);
    }
    else if (showCursors == true) {
        currentMode->setZoomerState(true);
        ui->cursorBtn->setText(tr("Show Cursors"));
        //markersOn = false;
        currentMode->getRenderConfig()->markersShow = false;
        showCursors = false;
        //currentMode->getDeltaTab()->setVisible(false);
    }
    ui->markersColorGrp->setVisible(showCursors);

}

void ADcfDemoMainWindow::markersColorChanged(colorLabel *lbl) {
    for (int i = 0;i<mode.size();i++)
    mode.at(i)->markerColorChanged(lbl->currentChanel(),
                                    lbl->getLblColor());
}

void ADcfDemoMainWindow::setMarkersFreeze(bool s) {
    currentMode->getRenderConfig()->markersFreeze = s;
    currentMode->setMarkersFreeze(s);
}

void ADcfDemoMainWindow::setDefaultMarkers() {
    currentMode->setMarkersAsDefault();
}

void ADcfDemoMainWindow::subMeasModeChanged()  {
    if (!currentMode) return;
    on_combo_MeasMode_activated(currentMode->getProcess()->getID());
}

void ADcfDemoMainWindow::makeTestData3() {

    int x=-1;
    int temp = RAW_SAMPLES_NUM/4 - (x<0?1024:3074);
    for(int j=0; j<RAW_SAMPLES_NUM; j++)
    {
        if (temp-- == 0) {x = (x<0?1:-1);
            temp=RAW_SAMPLES_NUM/4 - (x<0?1024:3074);

        }
        short val=0;
        short val2=0;

        short y=0;
        short y2=0;

        val=32767/x;

        //val=32767*qSin(2*3.14159265*j*Fcentr/Fs);
        val2=32767*qCos(2*3.14159265*j*Fcentr/Fs);

        y=val;
        y2=val2;

        // if(val2>=0) y2=val2;
        //else y2=(~val2+1)|0x8000;

        m_data_test[j*4]=(y2>>8);
        m_data_test[j*4+1]=y2&0xff;

        m_data_test[j*4+2]=(y>>8);
        m_data_test[j*4+3]=y&0xff;

        /*if(y&0x8000)
    {
        y--;
        y=~y;
        val=-1*(int)y;
    }
    else val=(int)y;*/
        short yy2=m_data_test[j*4]<<8;
        yy2|=0xff&m_data_test[j*4+1];

        short yy=m_data_test[j*4+2]<<8;
        yy|=0xff&m_data_test[j*4+3];
    }
}

void ADcfDemoMainWindow::showPeaksTab() {
    if (currentMode->getPeaksTab()->isHidden())
    {
        ui->peakBtn->setText(tr("Hide peaks"));
    } else
    {
        ui->peakBtn->setText(tr("Show peaks"));
    }
    currentMode->showPeaks(currentMode->getPeaksTab()->isHidden());
}

void ADcfDemoMainWindow::fillToolbar(bool empty)
{
    ui->mainToolBar->clear();
    ui->mainToolBar->addAction(ui->actionQuit);
    ui->mainToolBar->addSeparator();
    if (!empty)
    {
        ui->mainToolBar->addAction(genState);
        ui->mainToolBar->addSeparator();
        QList<QAction*> *list = currentMode->getWidget()->getActions();
        for (int i=0;i<list->size();i++)
            ui->mainToolBar->addAction(list->at(i));
        ui->mainToolBar->addSeparator();
    }
    //ui->mainToolBar->addWidget(ui->edit_IP);
    //ui->mainToolBar->addWidget(ui->edit_Port);
}

void ADcfDemoMainWindow::showGenerator(bool s)
{
    ui->groupBox_6->setVisible(s);
}
