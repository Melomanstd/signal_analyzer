#include <qapplication.h>
#include <qevent.h>
#include <qwhatsthis.h>
#include <qpainter.h>
#include <qwt_plot.h>
#include <qwt_symbol.h>
#include <qwt_scale_map.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_directpainter.h>
#include "canvaspicker.h"

CanvasPicker::CanvasPicker(QwtPlot *plot):
    QObject(plot),
    d_selectedCurve(NULL),
    d_selectedPoint(-1)

{
    QwtPlotCanvas *canvas = plot->canvas();
    deltaTab = 0;
    ch1Marker = 0;
    ch2Marker = 0;
    moveHLine = false;
    moveVLine = false;
    dragged = false;
    cursFreeze = false;
    //Plot *p = (Plot*) plot;
    //deltaTab = p->getDeltaTab();
    canvas->installEventFilter(this);

    // We want the focus, but no focus rect. The
    // selected point will be highlighted instead.

//    canvas->setFocusPolicy(Qt::StrongFocus);
//#ifndef QT_NO_CURSOR
//    canvas->setCursor(Qt::PointingHandCursor);
//#endif
//    canvas->setFocusIndicator(QwtPlotCanvas::ItemFocusIndicator);
//    canvas->setFocus();
    canvas->setMouseTracking(true);
    const char *text =
        "All points can be moved using the left mouse button "
        "or with these keys:\n\n"
        "- Up:\t\tSelect next curve\n"
        "- Down:\t\tSelect previous curve\n"
        "- Left, �-�:\tSelect next point\n"
        "- Right, �+�:\tSelect previous point\n"
        "- 7, 8, 9, 4, 6, 1, 2, 3:\tMove selected point";
    canvas->setWhatsThis(text);

    shiftCurveCursor(true);
}

bool CanvasPicker::event(QEvent *e)
{
    if ( e->type() == QEvent::User )
    {
        //showCursor(true);
        return true;
    }
    return QObject::event(e);
}

bool CanvasPicker::eventFilter(QObject *object, QEvent *e)
{
    if ( object != (QObject *)plot()->canvas() )
        return false;

    switch(e->type())
    {
        case QEvent::FocusIn:
            //showCursor(true);
        case QEvent::FocusOut:
            //showCursor(false);

        case QEvent::Paint:
        {   
            QApplication::postEvent(this, new QEvent(QEvent::User));
            break;
        }
        case QEvent::MouseButtonRelease:
        {
            chMarkersMoved();
            selectedMarker=0;
            dragged = false;
            cursFreeze = false;
            return true;
        }
        case QEvent::MouseButtonPress:
        {
            select(((QMouseEvent *)e)->pos());
            if (selectedMarker != 0)
                dragged = true;
            return true; 
        }
        case QEvent::MouseMove:
        {
            move(((QMouseEvent *)e)->pos());
            return true; 
        }
        /*case QEvent::KeyPress:
        {
            const int delta = 5;
            switch(((const QKeyEvent *)e)->key())
            {
                case Qt::Key_Up:
                    shiftCurveCursor(true);
                    return true;
                    
                case Qt::Key_Down:
                    shiftCurveCursor(false);
                    return true;

                case Qt::Key_Right:
                case Qt::Key_Plus:
                    if ( d_selectedCurve )
                        shiftPointCursor(true);
                    else
                        shiftCurveCursor(true);
                    return true;

                case Qt::Key_Left:
                case Qt::Key_Minus:
                    if ( d_selectedCurve )
                        shiftPointCursor(false);
                    else
                        shiftCurveCursor(true);
                    return true;

                // The following keys represent a direction, they are
                // organized on the keyboard.
 
                case Qt::Key_1: 
                    moveBy(-delta, delta);
                    break;
                case Qt::Key_2:
                    moveBy(0, delta);
                    break;
                case Qt::Key_3: 
                    moveBy(delta, delta);
                    break;
                case Qt::Key_4:
                    moveBy(-delta, 0);
                    break;
                case Qt::Key_6: 
                    moveBy(delta, 0);
                    break;
                case Qt::Key_7:
                    moveBy(-delta, -delta);
                    break;
                case Qt::Key_8:
                    moveBy(0, -delta);
                    break;
                case Qt::Key_9:
                    moveBy(delta, -delta);
                    break;
                default:
                    break;
            }
        }*/
        default:
            break;
    }
    return QObject::eventFilter(object, e);
}

// Select the point at a position. If there is no point
// deselect the selected point

void CanvasPicker::select(const QPoint &pos)
{
//    QwtPlotCurve *curve = NULL;
//    QwtPlotMarker *marker = 0;
//    double dist = 10e10;
//    int index = -1;

    const QwtPlotItemList& itmList = plot()->itemList();
    for ( QwtPlotItemIterator it = itmList.begin();
        it != itmList.end(); ++it )
    {
        if ( (*it)->rtti() == QwtPlotItem::Rtti_PlotMarker )
        {
            PlotMarker *m = (PlotMarker*)(*it);
            //QwtPlotCurve *c = (QwtPlotCurve*)(*it);
            if (m->hitTest(pos, moveHLine, moveVLine)) {
            //if (closestPoint(pos,m)) {
                selectedMarker = m;

                return;
            }
        }
    }

    d_selectedCurve = NULL;
    selectedMarker=0;
    d_selectedPoint = -1;
    //dragged = false;
}

// Move the selected point
void CanvasPicker::moveBy(int dx, int dy)
{
    if ( dx == 0 && dy == 0 )
        return;

    if ( !d_selectedCurve )
        return;

    const QPointF sample = 
        d_selectedCurve->sample(d_selectedPoint);

    const double x = plot()->transform(
        d_selectedCurve->xAxis(), sample.x());
    const double y = plot()->transform(
        d_selectedCurve->yAxis(), sample.y());

    move( QPoint(qRound(x + dx), qRound(y + dy)) );
}

// Move the selected point
void CanvasPicker::move(const QPoint &pos)
{
    QwtPlotMarker *selectedM=selectedMarker;
    bool vline = moveVLine;
    bool hline = moveHLine;
    select(pos);
    if (!cursFreeze)
    {
        if (selectedMarker == 0)
            plot()->canvas()->setCursor(Qt::CrossCursor);
        else if (moveVLine && moveHLine)
            plot()->canvas()->setCursor(Qt::SizeAllCursor);
        else if (moveVLine)
            plot()->canvas()->setCursor(Qt::SizeVerCursor);
        else if (moveHLine)
            plot()->canvas()->setCursor(Qt::SizeHorCursor);
    }
    //plot()->canvas()->setCursor(selectedMarker? Qt::SizeVerCursor : Qt::CrossCursor);
    selectedMarker=selectedM;
    moveVLine=vline;
    moveHLine=hline;
    if ( !selectedMarker )
        return;
    cursFreeze = true;
    //dragged = true;
    //QwtScaleMap yrMap = plot()->canvasMap( QwtPlot::yRight );
    PlotMarker *m = (PlotMarker*) selectedMarker;
    Plot *p = (Plot*)plot();
    QPointF point = pos;
    point = adjustPoint(point);
    bool crosses;
    if(selectedMarker != p->crossMarker1 &&
            selectedMarker != p->crossMarker2)
        crosses = false;
    else crosses = true;

    /*QwtScaleMap yMap = plot()->canvasMap( QwtPlot::yLeft );
    QwtScaleMap xMap = plot()->canvasMap( QwtPlot::xBottom );
    double yVal = yMap.invTransform(point.y());
    double xVal = xMap.invTransform(point.x());*/
    /*if (moveHLine && moveVLine) {
        selectedMarker->setValue(xVal, yVal);
    }
    else if (moveHLine) {
        selectedMarker->setXValue(xVal);
    }
    else if (moveVLine) {
        selectedMarker->setYValue(yVal);
    }*/
    //m->yAxis();
    if (!dragged) return;
    m->moveMarker(point, moveHLine, moveVLine,p);
    if (crosses) p->deltatabUpdate();
    plot()->replot();
}

// Hightlight the selected point
void CanvasPicker::showCursor(bool showIt)
{
    if ( !selectedMarker )
        return;

    QwtSymbol *symbol = const_cast<QwtSymbol *>( selectedMarker->symbol() );

    const QBrush brush = symbol->brush();
    if ( showIt )
        symbol->setBrush(symbol->brush().color().dark(180));

    QwtPlotDirectPainter directPainter;
    directPainter.drawSeries(d_selectedCurve, d_selectedPoint, d_selectedPoint);

    if ( showIt )
        symbol->setBrush(brush); // reset brush
}

// Select the next/previous curve 
void CanvasPicker::shiftCurveCursor(bool up)
{
    QwtPlotItemIterator it;

    const QwtPlotItemList &itemList = plot()->itemList();

    QwtPlotItemList curveList;
    for ( it = itemList.begin(); it != itemList.end(); ++it )
    {
        if ( (*it)->rtti() == QwtPlotItem::Rtti_PlotCurve )
            curveList += *it;
    }
    if ( curveList.isEmpty() )
        return;

    it = curveList.begin();

    if ( d_selectedCurve )
    {
        for ( it = curveList.begin(); it != curveList.end(); ++it )
        {
            if ( d_selectedCurve == *it )
                break;
        }
        if ( it == curveList.end() ) // not found
            it = curveList.begin();

        if ( up )
        {
            ++it;
            if ( it == curveList.end() )
                it = curveList.begin();
        }
        else
        {
            if ( it == curveList.begin() )
                it = curveList.end();
            --it;
        }
    }
        
    //showCursor(false);
    d_selectedPoint = 0;
    d_selectedCurve = (QwtPlotCurve *)*it;
    //showCursor(true);
}

// Select the next/previous neighbour of the selected point
void CanvasPicker::shiftPointCursor(bool up)
{
    if ( !d_selectedCurve )
        return;

    int index = d_selectedPoint + (up ? 1 : -1);
    index = (index + d_selectedCurve->dataSize()) % d_selectedCurve->dataSize();

    if ( index != d_selectedPoint )
    {
        //showCursor(false);
        d_selectedPoint = index;
        //showCursor(true);
    }
}

bool CanvasPicker::closestPoint( const QPoint &pos , QwtPlotMarker *m)
{
    if ( plot() == NULL || !m)
        return false;
    //return true;
    moveHLine=false;
    moveVLine=false;
    QwtScaleMap yMap;
    QwtScaleMap xMap = plot()->canvasMap(QwtPlot ::xBottom);
    if (m == ch1Marker || m == ch2Marker) {
        if (m == ch1Marker) {
            yMap = plot()->canvasMap( QwtPlot::yLeft );
            //t=el1;
        }
        if (m == ch2Marker) {
            yMap = plot()->canvasMap( QwtPlot::yRight );
            //t=el2;
        }

        QPointF sample = m->value();
        //double cx = xMap.transform( sample.x() ) - pos.x();
        double cy = yMap.transform( sample.y() ) - pos.y();

        if (qAbs(cy)<=3 && pos.x()<=17)
            return true;
        return false;
    }
    yMap = plot()->canvasMap( QwtPlot::yLeft );
    QPointF sample = m->value();
    double cx = xMap.transform( sample.x() ) - pos.x();
    double cy = yMap.transform( sample.y() ) - pos.y();
    if (qAbs(cy) <= 3 || qAbs(cx) <= 3){
        if (qAbs(cy) <= 3) {
            moveVLine = true;
        }
        if (qAbs(cx) <= 3) {
            moveHLine = true;
        }
        return true;
    }

    return false;

}

void CanvasPicker::setMarkers(QwtPlotMarker *m1, QwtPlotMarker *m2) {
    ch1Marker = m1;
    ch2Marker = m2;
}

int CanvasPicker::checkHorizontal(const QPoint &pos) {
    QSize s = plot()->canvas()->size();
    if (pos.x() >= s.width())
        return 1;
    if (pos.x()<=0)
        return -1;
    return 0;
    return 0;
}

int CanvasPicker::checkVertical(const QPoint &pos) {
    QSize s = plot()->canvas()->size();
    if (pos.y() >= s.height())
        return 1;
    if (pos.y()<=0)
        return -1;
    return 0;
}

void CanvasPicker::deltaTabUpdate(bool l,
                                 bool r,
                                 bool b,
                                 double val) {
    //l - yLeft scale
    //r - yRight scale
    //b - xBottom scale
    Plot* p =(Plot*) plot();
    if (!selectedMarker) return;
    if (!p->getDeltaTab()) return;
    int m = selectedMarker == p->crossMarker1?0:1;
    if (b) {
        p->getDeltaTab()->topLevelItem(m)->setText(1,QString::number(val));
    }
    if (l) {
        p->getDeltaTab()->topLevelItem(m)->setText(2,QString::number(val));
    }
    if (r) {
        p->getDeltaTab()->topLevelItem(m)->setText(3,QString::number(val));
    }
    //dX
    double temp = p->getDeltaTab()->topLevelItem(0)->text(1).toDouble();
    double temp1 = p->getDeltaTab()->topLevelItem(1)->text(1).toDouble();
    double res = qAbs(temp-temp1);
    p->getDeltaTab()->topLevelItem(0)->setText(4,QString::number(res));

    //dY1
    temp = p->getDeltaTab()->topLevelItem(0)->text(2).toDouble();
    temp1 = p->getDeltaTab()->topLevelItem(1)->text(2).toDouble();
    res = qAbs(temp - temp1);
    p->getDeltaTab()->topLevelItem(0)->setText(5,QString::number(res));
    //dY2
    temp = p->getDeltaTab()->topLevelItem(0)->text(3).toDouble();
    temp1 = p->getDeltaTab()->topLevelItem(1)->text(3).toDouble();
    res = qAbs(temp - temp1);
    p->getDeltaTab()->topLevelItem(0)->setText(6,QString::number(res));
}

void CanvasPicker::chMarkersMoved() {
    Plot *p = (Plot*) plot();
    if (selectedMarker == ch1Marker)
        p->markerMoved(0);
    if (selectedMarker == ch2Marker)
        p->markerMoved(1);
}

QPointF CanvasPicker::adjustPoint(QPointF &pos) {
    QSize s = plot()->canvas()->size();
    QPointF temp = pos;
    if (temp.x() >= s.width())
        temp.setX(s.width()-1);
    if (temp.x() <= 0)
        temp.setX(0);
    if (temp.y() >= s.height())
        temp.setY(s.height()-1);
    if (temp.y() <= 0)
        temp.setY(0);
    return temp;
}
