#include "colorlabel.h"

colorLabel::colorLabel(QWidget *parent)
{
    this->installEventFilter(this);
    this->setAutoFillBackground(true);
    this->chanelID=0;
    this->currentColor=Qt::red;
}

void colorLabel::changeColor() {
    QColorDialog temp;
    temp.setCurrentColor(currentColor);
    if (temp.exec()==QDialog::Rejected)
        return;
    QColor c = temp.currentColor();
    currentColor=c;
    QPalette p; p.setColor(QPalette::Window,c);
    this->setPalette(p);
    emit colorChanged(this);
}

bool colorLabel::eventFilter(QObject *, QEvent *e) {
    if (e->type() == QEvent::MouseButtonDblClick)
        changeColor();
}

QColor colorLabel::getLblColor() {
    return currentColor;
}

int colorLabel::currentChanel() {
    return this->chanelID;
}

void colorLabel::setChanel(int Chn) {
    this->chanelID=Chn;
}

void colorLabel::setCurrentColor(QColor c) {
    QPalette p; p.setColor(QPalette::Window,c);
    this->setPalette(p);
    currentColor=c;
}
