#include "plotcurve.h"
#include <QDebug>


PlotCurve::PlotCurve(const QString &title) :
    QwtPlotCurve(title)
{
}

PlotCurve::~PlotCurve(){}

QWidget *PlotCurve::legendItem() const
{
    QWidget *item = new QWidget;
    //QwtTextLabel *item = new QwtTextLabel(QwtText("Text"));
    //QwtLegendItem *item = new QwtLegendItem;
    if ( /*d_data->plot*/plot() )
    {
        /*QObject::connect( item, SIGNAL( clicked() ),
            plot(), SLOT( legendItemClicked() ) );
        QObject::connect( item, SIGNAL( checked( bool ) ),
            plot(), SLOT( legendItemChecked( bool ) ) );*/
    }
    return item;
}

void PlotCurve::updateLegend( QwtLegend *legend ) const
{
    if ( legend == NULL )
        return;

    QWidget *lgdItem = legend->find( this );
    if ( testItemAttribute( QwtPlotItem::Legend ) )
    {
        if ( lgdItem == NULL )
        {
            lgdItem = legendItem();
            if ( lgdItem )
                legend->insert( this, lgdItem );
        }
        QWidget *w = lgdItem;
        if (!l) return;
        w->setLayout(l);

        for (int i=0; i<labels.size();i++){
            QwtText t = labels.at(i)->text();
            t.setColor(pen().color());
            labels.at(i)->setText(t);
        }
        //QwtTextLabel *label1 = qobject_cast<QwtTextLabel *>( lgdItem );
        //QwtText t("text");
        //t.setColor(pen().color());
        //label1->setText(t);
    //QwtPlotItem::updateLegend( legend );
        }
}

void PlotCurve::setLegendText(QString text, QwtLegend *legend) {
    QWidget *lgdItem = legend->find( this );
    QwtTextLabel *label1 = qobject_cast<QwtTextLabel *>( lgdItem );
    QwtText t(text);
    t.setColor(pen().color());
    label1->setText(t);
}

void PlotCurve::setLinkedLbls(int n) {
    linkedLbls = n;
}

void PlotCurve::createLbls(int n) {
    l = new QHBoxLayout;
    l->setContentsMargins(0,0,0,0);
    qDebug()<<"creating textlabesl";
    for (int i=0;i<n; i++) {
        qDebug("creating %d of %d",i,n);
        labels.append(new QwtTextLabel);
        QwtText t(QString("text"));
        t.setColor(pen().color());
        labels.last()->setText(t);
        l->addWidget(labels.last());

        //l->setSpacing(0);
        //l->setStretch(i, 0);
    }
}

void PlotCurve::setLabelText(int n, QString text) {
    if (labels.isEmpty()) return;
    if (n>=labels.size()) return;
    QwtText t=text;
    t.setColor(pen().color());
    labels.at(n)->setText(t);
}

void PlotCurve::drawCurve( QPainter *painter, int style,
    const QwtScaleMap &xMap, const QwtScaleMap &yMap,
    const QRectF &canvasRect, int from, int to ) const
{
    qDebug()<<"drawing";
    //return;
    switch ( style )
    {
        case Lines:
            if ( testCurveAttribute( Fitted ) )
            {
                // we always need the complete
                // curve for fitting
                from = 0;
                to = dataSize() - 1;
            }
            drawLines( painter, xMap, yMap, canvasRect, from, to );
            break;
        case Sticks:
            drawSticks( painter, xMap, yMap, canvasRect, from, to );
            break;
        case Steps:
            drawSteps( painter, xMap, yMap, canvasRect, from, to );
            break;
        case Dots:
            drawDots( painter, xMap, yMap, canvasRect, from, to );
            break;
        case NoCurve:
        default:
            break;
    }
}
