#include "modsmainwidget.h"
#include "ui_modsmainwidget.h"

ModsMainWidget::ModsMainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ModsMainWidget)
{
    ui->setupUi(this);

    ui->dialog0->setToolTip(tr("Insert mode"));
    ui->dialog1->setToolTip(tr("Insert mode"));
    ui->dialog2->setToolTip(tr("Insert mode"));
    ui->dialog3->setToolTip(tr("Insert mode"));
    ui->clear0->setToolTip(tr("Remove mode"));
    ui->clear1->setToolTip(tr("Remove mode"));
    ui->clear2->setToolTip(tr("Remove mode"));
    ui->clear3->setToolTip(tr("Remove mode"));
    ui->switchBtn->setToolTip(tr("Move to tab"));
    ui->switchBtn_2->setToolTip(tr("Move to tab"));
    ui->switchBtn_3->setToolTip(tr("Move to tab"));
    ui->switchBtn_4->setToolTip(tr("Move to tab"));

    dialBtns.append(ui->dialog0);
    dialBtns.append(ui->dialog1);
    dialBtns.append(ui->dialog2);
    dialBtns.append(ui->dialog3);

    clrBtns.append(ui->clear0);
    clrBtns.append(ui->clear1);
    clrBtns.append(ui->clear2);
    clrBtns.append(ui->clear3);

    switchBtns.append(ui->switchBtn);
    switchBtns.append(ui->switchBtn_2);
    switchBtns.append(ui->switchBtn_3);
    switchBtns.append(ui->switchBtn_4);

    for (int i=0;i<4;i++)
        modeList.append(0);
}

ModsMainWidget::~ModsMainWidget()
{
    delete ui;
    /*for (int i=0;i<modeList.size();i++)
        if (modeList[i])
            delete modeList[i];*/
}


QGridLayout* ModsMainWidget::mainLayout(){
    return ui->gLay;
}

QList<QToolButton *> *ModsMainWidget::getDialogBtns() {
    return &dialBtns;
}

QList<QToolButton *> *ModsMainWidget::getClearBtns() {
    return &clrBtns;
}

QVBoxLayout* ModsMainWidget::vbLay1() {
    return ui->verticalLayout;
}

QVBoxLayout* ModsMainWidget::vbLay2() {
    return ui->verticalLayout_2;
}
QVBoxLayout* ModsMainWidget::vbLay3() {
    return ui->verticalLayout_3;
}
QVBoxLayout* ModsMainWidget::vbLay4() {
    return ui->verticalLayout_4;
}

void ModsMainWidget::setModeName(int n, QString name) {
    //QString temp = "Current mode: "+name;
    switch (n+1){
    case 1:
        ui->switchBtn->setText(name);
        break;
    case 2:
        ui->switchBtn_2->setText(name);
        break;
    case 3:
        ui->switchBtn_3->setText(name);
        break;
    case 4:
        ui->switchBtn_4->setText(name);
        break;
    }
}

void ModsMainWidget::clearMode(int n) {
   //m->getPlot()->hide();
   if (!modeList.at(n))
       return;
   modeList.at(n)->getWidget()->getOutputWidget()->hide();
   modeList.at(n)->setUsed(false);
   switch (n) {
   case 0:
       ui->horizontalLayout->removeWidget(
         modeList.at(n)->getWidget()->getOutputWidget());
       break;
   case 1:
       ui->horizontalLayout_2->removeWidget(
         modeList.at(n)->getWidget()->getOutputWidget());
       break;
   case 2:
       ui->horizontalLayout_3->removeWidget(
         modeList.at(n)->getWidget()->getOutputWidget());
       break;
   case 3:
       ui->horizontalLayout_4->removeWidget(
         modeList.at(n)->getWidget()->getOutputWidget());
       break;
   }
   modeList[n] = 0;
   setModeName(n,"NONE");
}

void ModsMainWidget::setMode(int pos, Mode *m){
    modeList[pos] = m;
}

Mode* ModsMainWidget::modeAt(int n){
    return modeList.at(n);
}

QList<Mode *> *ModsMainWidget::getModes(){
    return &modeList;
}

QList<QToolButton*> *ModsMainWidget::getSwitchBtns() {
    return &switchBtns;
}
