#include "checkingthread.h"

CheckingThread::CheckingThread(ModsMainWidget *tab,
                               QObject *parent) :
    QThread(parent)
{
    cycle = false;
    checkAll =false;
    mainModes=0;
    setModes(tab->getModes());
}

void CheckingThread::run() {
    int c=0;
    bool doneOnce = false;
    do  {
        if (checkAll) {
            for (int i=0;i<allModes->size();i++) {
                checkingMode(allModes->at(i));
                if (!cycle && doneOnce) break;
            }
        }
        else for (int i=0;i<mainModes->size();i++) {
            if (mainModes->at(i))
                checkingMode(mainModes->at(i));
            if (!cycle && doneOnce) break;
        }

        //qDebug("done %i",c++);
        //msleep(500);
        doneOnce = true;
    } while (cycle);
    //btn->setText("StartChecking");
}

void CheckingThread::startChecking(bool c, bool a) {
    cycle = c;
    checkAll = a;
    start();
}

void CheckingThread::setCheckingCycle(bool c) {
    //QMutex m;
    mut.lock();
    cycle = c;
    mut.unlock();
}

void CheckingThread::stopProcess() {
    //QMutex m;
    mut.lock();
    cycle = false;
    mut.unlock();
    //wait();
}

void CheckingThread::modeChecked() {
    pause.wakeAll();
}

void CheckingThread::checkingMode(Mode *m) {
    QMutex q;
    m->getRender()->setProcess(m->getProcess());
    m->getRender()->startProcess2(
                m->getRenderConfig());
    pause.wait(&q,ULONG_LONG_MAX);
}

void CheckingThread::setModes(QList<Mode *> *l) {
    if (!mainModes) {
        mainModes = l;
        return; }
    //QMutex m;
    mut.lock();
    mainModes = l;
    mut.unlock();
}

void CheckingThread::setFullModes(QList<Mode *> *l) {
    //QMutex m;
    mut.lock();
    allModes=l;
    mut.unlock();
}

void CheckingThread::pauseProcess() {
    wait();
}
