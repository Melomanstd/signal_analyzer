#include "modedialog.h"
#include "ui_modedialog.h"

ModeDialog::ModeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ModeDialog)
{
    ui->setupUi(this);
    setWindowTitle(tr("New mode"));
    result=0;
    Mode t;
    for (int i=0; i<Mode::COUNT_ID; i++)
        ui->modeBox->addItem(t.modes[i]);

    connect(ui->modeBox, SIGNAL(currentIndexChanged(int)),this, SLOT(setMode(int)));
}

ModeDialog::~ModeDialog()
{
    delete ui;
}

unsigned ModeDialog::getResult() {
    return result;
}

void ModeDialog::setMode(int m) {
    result = m;
}
