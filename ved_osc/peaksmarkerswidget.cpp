#include "peaksmarkerswidget.h"
#include "ui_peaksmarkerswidget.h"

PeaksMarkersWidget::PeaksMarkersWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PeaksMarkersWidget)
{
    ui->setupUi(this);
    setMaximumHeight(80);
    setVisible(false);
    //setContentsMargins(0,0,0,0);
    //ui->scrollArea->setContentsMargins(0,0,0,0);
}

PeaksMarkersWidget::~PeaksMarkersWidget()
{
    delete ui;
}

void PeaksMarkersWidget::headersInit(int count) {
    QString str;
    for (int i = 1;i<count+1;i++)
        str+="\t\t"+QString::number(i);
    ui->headerLbl->setText(str);
}

void PeaksMarkersWidget::setValues(QPointF *values, int size) const {
    QString xStr="  X:";
    QString yStr="  Y:";
    for (int i = 0;i<size;i++)
    {
        xStr +="\t\t"+QString::number(values[i].x());
        yStr +="\t\t"+QString::number(values[i].y());
    }

    ui->xValueLbl->setText(xStr);
    ui->yValueLbl->setText(yStr);
}

void PeaksMarkersWidget::setValues(int *xArr,
                                   int *yArr,
                                   int size,
                                   double gainY,
                                   double gainX) const
{
    QString xStr="  X:";
    QString yStr="  Y:";
    for (int i = 0;i<size;i++)
    {
        xStr +="\t\t"+QString::number(xArr[i]*gainX);
        yStr +="\t\t"+QString::number(yArr[i]*gainY);
    }

    ui->xValueLbl->setText(xStr);
    ui->yValueLbl->setText(yStr);
}
