#ifndef MODSMAINWIDGET_H
#define MODSMAINWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QGridLayout>
#include <QPushButton>
#include <QVBoxLayout>
#include <QToolButton>
#include "mode.h"
#include "plotdialog.h"

class MeasureConfigItem;
class RenderThread;
namespace Ui {
class ModsMainWidget;
}

class ModsMainWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit ModsMainWidget(QWidget *parent = 0);
    ~ModsMainWidget();

    QGridLayout* mainLayout();
    QList<QToolButton*> *getDialogBtns();
    QList<QToolButton*> *getClearBtns();
    QList<QToolButton*> *getSwitchBtns();
    QVBoxLayout * vbLay1();
    QVBoxLayout * vbLay2();
    QVBoxLayout * vbLay3();
    QVBoxLayout * vbLay4();
    void setModeName (int n, QString name);
    void clearMode(int n);
    void setMode(int pos, Mode* m);
    Mode* modeAt(int n);
    QList <Mode*>* getModes();

    
private:
    Ui::ModsMainWidget *ui;
    QList<QToolButton*> dialBtns;
    QList<QToolButton*> clrBtns;
    QList<QToolButton*> switchBtns;
    QList<Mode*> modeList;

public:

};

#endif // MODSMAINWIDGET_H
