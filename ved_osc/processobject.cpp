#include "processobject.h"

ProcessObject::ProcessObject(QObject *parent) :
    QObject(parent)
{
    posPeak=0; negPeak=0;
    SKZ=0; AVG=0;
    lastGainY = 0;
    bWait = true;
    //statisticData = new double[DATASIZE];
    posPeakData = new double[DATASIZE];
    negPeakData = new double[DATASIZE];
    AVGData = new double[DATASIZE];
    SKZData = new double[DATASIZE];

    //xData = new int[RAW_SAMPLES_NUM];
    yData = new int[RAW_SAMPLES_NUM];

    writeXPtr = xPeakBuff_0;
    writeYPtr = yPeakBuff_0;
    readXPtr=0; readYPtr=0;
    lastGainX = 1; lastGainY = 1;
    peaksCount = 0;

    peaksList = new PeakMarker;
    //peaksList->setText(QString::number(i));
    //peaksList->setYAxis(QwtPlot::yLeft);
    peaksList->setAlignment(Qt::AlignTop);
    peaksList->setValue(0,0);
    peaksList->setLineStyle(QwtPlotMarker::HLine);
    peaksList->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    peaksList->setLinePen(QPen(Qt::black, 0, Qt::DashDotLine));
    peaksList->hide();
//    for(int i=0;i<100; i++) {
//        PeakMarker *m = new PeakMarker;
//        m->setText(QString::number(i));
//        m->setYAxis(QwtPlot::yLeft);
//        m->setAlignment(Qt::AlignTop);
//        m->setValue(0,0);
//        m->setLineStyle(QwtPlotMarker::HLine);
//        m->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
//        m->setLinePen(QPen(Qt::black, 0, Qt::DashDotLine));
//        //m->attach(plots->last());
//        m->hide();
//        markers.append(m);
//    }

    clearStatistic();
}

ProcessObject::~ProcessObject() {
    //delete statisticData;
    delete posPeakData;
    delete negPeakData;
    delete AVGData;
    delete SKZData;
    delete yData;
}

void ProcessObject::processInit(Plot *p,
                                RenderThread *r,
                                MeasureConfigItem *c)
{
    setConfig(c);
    plot=p;
    render=r;
}

void ProcessObject::setConfig(MeasureConfigItem *c) {
    conf=c;
}

MeasureConfigItem* ProcessObject::getConfig() {
    return conf;
}

QString ProcessObject::getProcessName() {
    return processName;
}

void ProcessObject::setProcessName(QString name) {
    processName = name;
}

void ProcessObject::setRender(RenderThread *render) {
    this->render=render;
}

int ProcessObject::getID() {
    return measModeID;
}

void ProcessObject::setID(int id){
    measModeID=id;
}

void ProcessObject::SetPlots(QList<Plot *> *p) {
    plots=p;
}

void ProcessObject::initRenderVar(int x){
    if (x==0) {
        conf->max_y=0;
        conf->max_y2=0;
        conf->min_y=0xffff;
        conf->min_y2=0xffff;

        /*int */conf->nPass=0;
        /*int */conf->nPassNum=1;
    }
    if (x==1) {
        memset(conf->m_Fa, 0, sizeof(conf->m_Fa));
        // sensometr data
        conf->m_nSmax= 16383;/*m_nAmax*/;//
        conf->m_nSmin=0;
        conf->m_nSc=conf->nAmax; //10000; //m_nSmax;
        conf->m_fSINAD_bound = (conf->nGenMode==DDS_F3EA ||
                          conf->nGenMode==DDS_F3EJ) ? 20.0 : 12.0 ;
        conf->m_fKvp_bound=50.0;
    }
}

double ProcessObject::delta(double max, double min) {
    return max-min;
}

double ProcessObject::averageValue(double sum, double count) {
    return sum/count;
}

double ProcessObject::RMS(double sum, double count) {
    return qSqrt(sum/count);
}

double ProcessObject::statistic(double *data, QPair<double *, int> &prop, double &val) {
    *prop.first = val;
    double sum = 0;
    for (int i = 0;i<DATASIZE; i++) {
        if (i>prop.second)
            continue;
        sum += data[i];
    }
    if (prop.second<DATASIZE)
        prop.second++;
    if (prop.first==data+DATASIZE-1)
        prop.first=data;
    else prop.first++;
    return sum/prop.second;
    /**statisticsProp.first = val;
    double sum=0;
    for (int i = 0;i<DATASIZE; i++) {
        if (i>statisticsProp.second)
            continue;
        sum += statisticData[i];
    }
    if (statisticsProp.second<DATASIZE)
        statisticsProp.second++;
    if (statisticsProp.first==statisticData+DATASIZE-1)
        statisticsProp.first=statisticData;
    else statisticsProp.first++;
    return sum/statisticsProp.second;*/
}

void ProcessObject::clearStatistic() {
    qDebug()<<"negPeak \t posPeak \t AVG \t SKZ";
    for (int i=0;i<DATASIZE;i++) {
        qDebug()<<negPeakData[i]<<' '<<
                  posPeakData[i]<<' '<<
                  AVGData[i]<<' '<<SKZData[i];
        //statisticData[i] = 0.0;
        negPeakData[i] = 0.0;
        posPeakData[i] = 0.0;
        AVGData[i] = 0.0;
        SKZData[i] = 0.0;
    }
//    statisticsProp.first=statisticData;
//    statisticsProp.second=0;
    //statisticPtr = statisticData;
    negPeakProp.first=negPeakData;
    negPeakProp.second=0;

    posPeakProp.first=posPeakData;
    posPeakProp.second=0;

    AVGProp.first=AVGData;
    AVGProp.second=0;

    SKZProp.first=SKZData;
    SKZProp.second=0;
}

#define PREC 100
#define RANGE_PIK_MAX 0.1

void ProcessObject::GetSignalParams(QList<double> &ptArray, int nSize)
{
    //CString str;
        int DATA_COUNT=nSize; // кол-во отсчетов для обработки

        qint64 *data, min_data, max_data;
        int  c, n;

        data= new(qint64[DATA_COUNT]);

    //	m_list.ResetContent();
        /******************** ФО� МИ� ОВАНИЕ ИСХОДНЫХ ДАННЫХ *****************/

    //	float f1, f2, f3;

        c=0;
        do
        {
            data[c]=ptArray.at(c)*1000000;
            if (c==0) {min_data = max_data = data[c];}
            else
            {
                if (data[c]<min_data) min_data=data[c];
                if (data[c]>max_data) max_data=data[c];
            }
            c++;
        }while (c<DATA_COUNT);


        /* ДАННЫЕ для обработки: */
        /* int DATA_COUNT -  кол-во отсчетов */
        /* int data[] - массив отсчетов (каждый исходный умножен на 1000000)*/
        /* int max_data -  максимальное значение отсчетов */
        /* int min_data -  минимальное значение отсчетов */

        //str.Format("для рассчета используем %d точек\n", DATA_COUNT);
    //	m_list.AddString(str);
        qDebug("для рассчета используем %d точек\n", DATA_COUNT);
        //str.Format("min=%f \t max=%f \n", min_data/1.0e6, max_data/1.0e6);
    //	m_list.AddString(str);
        qDebug("min=%f \t max=%f \n", min_data/1.0e6, max_data/1.0e6);
        /**************** � АСЧЕТ ***********************/
        int ves[PREC]; // массив весов
        for (c=0; c<PREC; c++)	ves[c]=0; // его инициализация

        qint64 avr_ves, pik_max, pik_range, avr_pik, avr_fon;
        double sum;
        qint64 v;

        // заполняем таблицу весов
        // весь диапазон изменения сигнала разбивается на PREC областей (соотв. ячейкам ves[])
        // в каждой ячейке ves[] хранится число значений сигнала из соотв. области
        for (c=0, pik_max=data[0]; c<DATA_COUNT; c++)
        {
            v=(data[c]-min_data)/double(max_data-min_data)*(PREC-1);
            if (v<0 || v>=PREC) { //qDebug("%d: data= %d   v=%d\n", c, data[c], v);
                /*str.Format("%d: data= %d   v=%d\n", c, data[c], v);*//*m_list.AddString(str);*/}
            else ves[v]++;
        }

        // вычисление среднего значения  таблицы весов
        for (c=0, sum=0; c<PREC; c++)
        {
            sum+=ves[c];

            // вывод заполненной таблицы весов (просто для информации)
            //str.Format("ves[%d, %f]=%d\n", c, (double(c)/PREC*(max_data-min_data)+min_data)/1.0e6,ves[c]);
    //		m_list.AddString(str);
            //qDebug("ves[%d, %f]=%d\n", c, (double(c)/PREC*(max_data-min_data)+min_data)/1.0e6,ves[c]);
        }

        avr_ves=sum/v; // среднее по массиву весов
        //str.Format("avr_ves=%d \n", avr_ves);
    //	m_list.AddString(str);
        qDebug("avr_ves=%d \n", avr_ves);
        // определяем начало диапазона, на RANGE_PIK_MAX ниже максимального значения сигнала (для определения среднего значения пичков).
        pik_range=max_data-max_data*RANGE_PIK_MAX;

        // определяем среднее значение пичков в этом диапазоне
        for (c=0, n=0, sum=0; c<DATA_COUNT; c++)
        {
            if (data[c]>pik_range) {sum+=data[c]; n++;}
        }
        avr_pik=sum/n;
        //str.Format("max_pik=%f\n", max_data/1.0e6); // максимальный пичок
    //	m_list.AddString(str);
        qDebug("max_pik=%f\n", max_data/1.0e6);
        //str.Format("avr_pik(%d)=%f\n", n, avr_pik/1.0e6); // среднее значение величины пичков в диапазоне RANGE_PIK_MAX ниже максимального
    //	m_list.AddString(str);
        qDebug("avr_pik(%d)=%f\n", n, avr_pik/1.0e6);

        // определяем самый большой спад в массиве весов (т.е. примерную границу фона)
        qint64 max_fall, max_fall_point;

        max_fall_point=1;	max_fall=ves[0]-ves[1];
        for (c=1; c<PREC; c++)
        {
            if ((ves[c-1]-ves[c]) > max_fall)
            {
                max_fall=ves[c-1]-ves[c];
                max_fall_point=c;
            }

        }
        // если спад вообще не обнаружен
        if (max_fall<=0) {qDebug("err: max_fall<=0");
            /*str.Format("err: max_fall<=0");*//*m_list.AddString(str);*/ delete data; return;}

        // определяем после максимального спада первую точку ниже среднего значения масиива весов
        for (c=max_fall_point; c<PREC; c++)
        {
            if (ves[c]<avr_ves) break;
        }

        // теоретически такого не может быть, но все-таки
        if (c>=PREC) c=max_fall_point; // {printf("err: c>=PREC"); delete data; return -1;}

        // пороговое значение фона:
        v=double(c)/PREC*(max_data-min_data)+min_data; // пороговое значение для фона  в массиве исходных данных
        //str.Format("maxfall[%d]=%d fon_range[%d]=%f \n", max_fall_point, max_fall, c, v/1.0e6);
    //	m_list.AddString(str);
        qDebug("maxfall[%d]=%d fon_range[%d]=%f \n", max_fall_point, max_fall, c, v/1.0e6);

        // среднее значение порогового значения фона
        int fon1,fon2;// границы поискового диапазона от средней амплитуды фона
        // диапазон значений в районе максимального спада
        fon1=double(c-1)/PREC*(max_data-min_data)+min_data;
        fon2=double(c)/PREC*(max_data-min_data)+min_data;
        if (fon1>fon2) {v=fon1; fon1=fon2; fon2=v;}

        // определяем среднее значение фона в этом диапазоне
        for (c=0, n=0, sum=0; c<DATA_COUNT; c++)
        {
            if (data[c]>fon1 && data[c]<fon2) {sum+=data[c]; n++;}
        }
        avr_fon=sum/n;

        //str.Format("fon_range= %f ... %f \t avr_fon(%d)=%f\n", fon1/1.0e6, fon2/1.0e6, n, avr_fon/1.0e6);
    //	m_list.AddString(str);
        qDebug("fon_range= %f ... %f \t avr_fon(%d)=%f\n", fon1/1.0e6, fon2/1.0e6, n, avr_fon/1.0e6);

        delete data;
}

void ProcessObject::GetSignalParams2(QList <double> &basePtArray, int nSize)
{
    //CString str;
    int DEBUG = 0;
    int DATA_COUNT=nSize; // кол-во отсчетов для обработки

    int  c, n=0;
    qint64 min_data, max_data, avr_data, avr_data1=0;
    double value, Us=0, Usq=0, Umax=-1e23, Umin=1e23;

    qint64 *ptArray = new (qint64[nSize]);
    for (int i=0;i<nSize; i++)
        ptArray[i] = basePtArray.at(i)*1000000;

    qDebug()<<"array size: "<<nSize;
//	m_list.ResetContent();
    /******************** ФО� МИ� ОВАНИЕ ИСХОДНЫХ ДАННЫХ *****************/

//	float f1, f2, f3;

    c=0;
    do
    {
        avr_data1+=ptArray[c];
        if (c==0) {min_data = max_data = ptArray[c];}
        else
        {
            if (ptArray[c]<min_data) min_data=ptArray[c];
            if (ptArray[c]>max_data) max_data=ptArray[c];
        }
        c++;
    }while (c<DATA_COUNT);

    qDebug()<<"min_data: "<<min_data;
    qDebug()<<"max_data: "<<max_data;

    avr_data=min_data+(max_data-min_data)/2;
    avr_data1=avr_data1/DATA_COUNT;

    qDebug()<<"average array data: "<<avr_data1;
    qDebug()<<"average something data: "<<avr_data;


    //str.Format("для рассчета используем %d точек\n", DATA_COUNT);
//	m_list.AddString(str);
    //str.Format("min=%d max=%d avr=%d avr1=%d", min_data, max_data, avr_data, avr_data1);
//	m_list.AddString(str);

    ////////////////

    int p1=0, p2=0;
    int nn=0;
    qint64 crossval = avr_data; //уровень перехода через 0

    qDebug()<<"crossval = average data: "<<crossval;

    int RE_CNT=0;
    int FE_CNT=0;

    int RE_FIRST=0;
    int RE_LAST=0;

    int FE_LAST=0;
    int FE_FIRST=0;


    int T=0;

    int Ti_1=0;
    int Ti_0=0;

    int Ti_1_cnt=0;
    int Ti_0_cnt=0;

    for(nn = 0; nn < DATA_COUNT-1-1; nn++)
    {
        if (ptArray[nn]<=crossval &&
            ptArray[nn+1]>=crossval&&
            ptArray[nn+2]>=crossval)
        {
            RE_CNT++;
            if(RE_CNT==1)RE_FIRST=nn;
            RE_LAST=nn;

            if(RE_CNT>=1&&FE_CNT>=1)
            {	Ti_0+=(RE_LAST-FE_LAST);Ti_0_cnt++;}

            //str.Format("RE=%d Ti_0=%d", nn, Ti_0);
//			m_list.AddString(str);
            //qDebug("RE=%d Ti_0=%d", nn, Ti_0);
        }

        if (ptArray[nn]>=crossval &&
            ptArray[nn+1]<=crossval)
        {
            FE_CNT++;
            if(FE_CNT==1)FE_FIRST=nn;
            FE_LAST=nn;

            if(RE_CNT>=1&&FE_CNT>=1)
            {	Ti_1+=(FE_LAST-RE_LAST);Ti_1_cnt++;}

            //str.Format("FE=%d Ti_1=%d", nn, Ti_1);
//			m_list.AddString(str);
            //qDebug("FE=%d Ti_1=%d", nn, Ti_1);
        }
    }

    qDebug()<<DEBUG++;

    qDebug()<<"re_cnt: "<<RE_CNT;
    qDebug()<<"re_first: "<<RE_FIRST;
    qDebug()<<"fe_cnt: "<<FE_CNT;
    qDebug()<<"fe_first: "<<FE_FIRST;

    qDebug()<<DEBUG++;

    if(RE_CNT>1)
    {
        T=(RE_LAST-RE_FIRST)/(RE_CNT-1);
        //str.Format("Tavr_RE=%d ", T);
//		m_list.AddString(str);
        qDebug("Tavr_RE=%d ", T);
    }

    qDebug()<<DEBUG++;

    if(FE_CNT>1)
    {
        T=(FE_LAST-FE_FIRST)/(FE_CNT-1);
        //str.Format("Tavr_FE=%d ", T);
//		m_list.AddString(str);
        qDebug("Tavr_FE=%d ", T);
    }

    qDebug()<<DEBUG++;

    if( Ti_0_cnt>=1 )
    {
        Ti_0=Ti_0/Ti_0_cnt;
        //str.Format("Ti_0=%d(%d)", Ti_0, Ti_0_cnt);
//		m_list.AddString(str);
        qDebug("Ti_0=%d(%d)", Ti_0, Ti_0_cnt);
    }

    qDebug()<<DEBUG++;

    if( Ti_1_cnt>=1 )
    {
        Ti_1=Ti_1/Ti_1_cnt;
        //str.Format("Ti_1=%d(%d)", Ti_1, Ti_1_cnt);
//		m_list.AddString(str);
        qDebug("Ti_1=%d(%d)", Ti_1, Ti_1_cnt);
    }

    qDebug()<<DEBUG++;

    for(nn = RE_FIRST; nn < RE_LAST; nn++)
    {
        Usq+=ptArray[nn]*ptArray[nn];
    }

    qDebug()<<DEBUG++;

    value=sqrt(fabs(Usq/(RE_LAST-RE_FIRST)));
    //str.Format("SKZ=%f", value);
//		m_list.AddString(str);
    qDebug("SKZ=%f", value);

//////////////////////////////////////////////////////////////////////////

    delete ptArray;
}

#define ACCURACY 50
void ProcessObject::GetSignalParams3(QList<double> &basePtArray, int nSize,
                                     double timebaseSec,
                                     double *fA,
                                     double *fSKZ,
                                     double *fSKZ2,
                                     double *fF,
                                     double *fT,
                                     double *fTi,
                                     double *fTi_1,
                                     double *fTi_2,
                                     double *fAi)
{
    //CString str;
        int DATA_COUNT=nSize; // кол-во отсчетов для обработки

        int c, n=0;
        qint64 min_data, max_data, avr_data, avr_data1=0;

        double value=0, Us=0, Usq=0, Umax=-1e23, Umin=1e23;

        qint64 *ptArray = new(qint64[nSize]);
        for (int i=0;i<nSize; i++)
            ptArray[i] = basePtArray.at(i)*1000000;

    //	if(CalibrWnd)CalibrWnd->m_list.ResetContent();
        //if(ListVal)ListVal->ResetContent();
    //	float f1, f2, f3;

        c=0;
        do
        {
            avr_data1+=ptArray[c];
            if (c==0) {min_data = max_data = ptArray[c];}
            else
            {
                if (ptArray[c]<min_data) min_data=ptArray[c];
                if (ptArray[c]>max_data) max_data=ptArray[c];
            }
            c++;
        }while (c<DATA_COUNT);

        avr_data=min_data+(max_data-min_data)/2;
        avr_data1=avr_data1/DATA_COUNT;

        (*fA)=(max_data-min_data);

        //str.Format("для рассчета используем %d точек\n", DATA_COUNT);
        qDebug("для рассчета используем %d точек\n", DATA_COUNT);
        //if(ListVal)ListVal->AddString(str);
        //str.Format("min=%f max=%f avr=%f avr1=%f", min_data, max_data, avr_data, avr_data1);
        qDebug("min=%d max=%d avr=%d avr1=%d", min_data, max_data, avr_data, avr_data1);
        //if(ListVal)ListVal->AddString(str);

        ////////////////

        int p1=0, p2=0;
        int nn=0;
        qint64 crossval = avr_data; //уровень перехода через 0

        int RE_CNT=0;	// количество восходящих фронтов
        int FE_CNT=0;	// количество спадающих фронтов

        int RE_FIRST=0;
        int RE_LAST=0;

        int FE_LAST=0;
        int FE_FIRST=0;


        int T=0;
        int Tavr_RE=0;
        int Tavr_FE=0;

        double Ti_1=0;
        double Ti_0=0;
        double Ti_1_first=0;//длительность первого импульса
        double Ti_1_other=0;//длительность остальных импульсов, усреднённая

        double Ti_1_cnt=0;
        double Ti_0_cnt=0;

        double Ai_1=0;
        double Ai_0=0;


        for(nn = ACCURACY/*0*/; nn < DATA_COUNT-ACCURACY/*1*/; nn++)
        {
            if(FE_LAST>=RE_LAST)
            {
                if (ptArray[nn]<=crossval &&
                    ptArray[nn+1]>=crossval )
                if(Sred(ptArray,nn,ACCURACY,crossval,1)!=-1)
                {
                    RE_CNT++;
                    if(RE_CNT==1)RE_FIRST=nn;
                    RE_LAST=nn;

                    if(RE_CNT>=1&&FE_CNT>=1)
                    {
                        Ti_0+=(RE_LAST-FE_LAST);
                        Ti_0_cnt++;
                        value=0;
                        for(long j = FE_LAST; j < RE_LAST; j++)
                            value+=ptArray[j];
                        value=value/(RE_LAST-FE_LAST);
                        Ai_0+=value;
                    }

                    //str.Format("RE=%d Ti_0=%f", nn, Ti_0);
                    //if(ListVal)ListVal->AddString(str);
                    //qDebug("RE=%d Ti_0=%f", nn, Ti_0);

                    continue;
                }
            }

            if(RE_LAST>=FE_LAST)
            {
                if (ptArray[nn]>=crossval &&
                    ptArray[nn+1]<=crossval )
                if(Sred(ptArray,nn,ACCURACY,crossval,0)!=-1)
                {
                    FE_CNT++;
                    if(FE_CNT==1)FE_FIRST=nn;
                    FE_LAST=nn;

                    if(RE_CNT>=1&&FE_CNT>=1)
                    {
                        Ti_1+=(FE_LAST-RE_LAST);
                        Ti_1_cnt++;
                        if(Ti_1_cnt==1)Ti_1_first=(FE_LAST-RE_LAST);// запоминаем длительность первого импульса
                        value=0;
                        for(long j = RE_LAST; j < FE_LAST; j++)
                            value+=ptArray[j];
                        value=value/(FE_LAST-RE_LAST);
                        Ai_1+=value;
                    }

                    //str.Format("FE=%d Ti_1=%f", nn, Ti_1);
                    //if(ListVal)ListVal->AddString(str);
                    //qDebug("FE=%d Ti_1=%f", nn, Ti_1);
                    continue;
                }
            }
        }

        // амплитуда импульсов с усреднением на верхних полках
        if(Ti_0_cnt!=0)Ai_0=Ai_0/Ti_0_cnt;
        if(Ti_1_cnt!=0)Ai_1=Ai_1/Ti_1_cnt;

        (*fAi)=(Ai_1-Ai_0);

        //str.Format("Ai=%f,  Ai_0=%f (%f),  Ai_1=%f (%f)", (*fAi), Ai_0, Ti_0_cnt, Ai_1, Ti_1_cnt);
            //if(ListVal)ListVal->AddString(str);
        qDebug("Ai=%f,  Ai_0=%f (%f),  Ai_1=%f (%f)", (*fAi), Ai_0, Ti_0_cnt, Ai_1, Ti_1_cnt);

        // вычисляем длительность периода
        if(RE_CNT>1)
        {
            Tavr_RE=(RE_LAST-RE_FIRST)/(RE_CNT-1);
            //str.Format("Tavr_RE=%d ", Tavr_RE);
            //if(ListVal)ListVal->AddString(str);
            qDebug("Tavr_RE=%d ", Tavr_RE);
        }

        if(FE_CNT>1)
        {
            Tavr_FE=(FE_LAST-FE_FIRST)/(FE_CNT-1);
            //str.Format("Tavr_FE=%d ", Tavr_FE);
            //if(ListVal)ListVal->AddString(str);
            qDebug("Tavr_FE=%d ", Tavr_FE);
        }

        (*fT)=Tavr_RE*timebaseSec;
        if(Tavr_RE!=0)(*fF)=1/(*fT);


        // вычисляем длительности импульсов
        (*fTi_1)=Ti_1_first*timebaseSec;
        (*fTi_2)=((Ti_1-Ti_1_first)/(Ti_1_cnt-1))*timebaseSec;

        if( Ti_0_cnt>=1 )
        {
            Ti_0=Ti_0/Ti_0_cnt;
            //str.Format("Ti_0=%f(%f)", Ti_0, Ti_0_cnt);
            //if(ListVal)ListVal->AddString(str);
            qDebug("Ti_0=%f(%f)", Ti_0, Ti_0_cnt);
        }

        if( Ti_1_cnt>=1 )
        {
            Ti_1=Ti_1/Ti_1_cnt;
            //str.Format("Ti_1=%f(%f)", Ti_1, Ti_1_cnt);
            //if(ListVal)ListVal->AddString(str);
            qDebug("Ti_1=%f(%f)", Ti_1, Ti_1_cnt);
        }

        (*fTi)=Ti_1*timebaseSec;


        // вычисляем постоянную составляющую
        avr_data=0.0;
        for(nn = RE_FIRST; nn < RE_LAST; nn++)
            avr_data+=ptArray[nn];
        if((RE_LAST-RE_FIRST)!=0)
            avr_data=avr_data/(RE_LAST-RE_FIRST);
        //str.Format("Пост.Сост=%f", avr_data);
            //if(ListVal)ListVal->AddString(str);
        qDebug("Пост.Сост=%d", avr_data);

        // вычисляем С� З с вычетом постоянной составляющей
        value=0.0;
        for(nn = RE_FIRST; nn < RE_LAST; nn++)
            Usq+=pow(fabs(ptArray[nn]-avr_data),2);
        if((RE_LAST-RE_FIRST)!=0)
        value=sqrt(fabs(Usq/(RE_LAST-RE_FIRST)));
        //str.Format("SKZ=%f", value);
            //if(ListVal)ListVal->AddString(str);
        qDebug("SKZ=%f", value);

        (*fSKZ)=value;


        // вариант без периодов - по всем выборкам
        // вычисляем постоянную составляющую
        avr_data1=0.0;
        for(nn = 0; nn < DATA_COUNT; nn++)
            avr_data1+=ptArray[nn];
        if(DATA_COUNT!=0)
            avr_data1=avr_data1/DATA_COUNT;
        //str.Format("Пост.Сост2=%f", avr_data1);
            //if(ListVal)ListVal->AddString(str);
        qDebug("Пост.Сост2=%d", avr_data1);

        // вычисляем С� З с вычетом постоянной составляющей
        value=0.0;
        for(nn = 0; nn < DATA_COUNT; nn++)
            Usq+=pow(fabs(ptArray[nn]-avr_data1),2);
        if(DATA_COUNT!=0)
        value=sqrt(fabs(Usq/DATA_COUNT));
        //str.Format("SKZ2=%f", value);
            //if(ListVal)ListVal->AddString(str);
        qDebug("SKZ2=%f", value);

        (*fSKZ2)=value;


    //////////////////////////////////////////////////////////////////////////

        //if(ListVal)ListVal->AddString("________________________");

        //str.Format("A=%f ", (*fA));
            //if(ListVal)ListVal->AddString(str);
        qDebug("A=%f ", (*fA));

        //str.Format("F=%f ", (*fF));
            //if(ListVal)ListVal->AddString(str);
        qDebug("F=%f ", (*fF));

        //str.Format("T=%f ", (*fT));
            //if(ListVal)ListVal->AddString(str);
        qDebug("T=%f ", (*fT));

        //str.Format("Ti_1=%f(%f)", (*fTi), Ti_1_cnt);
            //if(ListVal)ListVal->AddString(str);
        qDebug("Ti_1=%f(%f)", (*fTi), Ti_1_cnt);

        //str.Format("Ti_0=%f(%f)", Ti_0*timebaseSec, Ti_0_cnt);
            //if(ListVal)ListVal->AddString(str);
        qDebug("Ti_0=%f(%f)", Ti_0*timebaseSec, Ti_0_cnt);

        //str.Format("Пост.Сост=%f", avr_data);
            //if(ListVal)ListVal->AddString(str);
        qDebug("Пост.Сост=%d", avr_data);

        //str.Format("С� З=%f", (*fSKZ));
            //if(ListVal)ListVal->AddString(str);
        qDebug("С� З=%f", (*fSKZ));

        //str.Format("Пост.Сост2=%f", avr_data1);
            //if(ListVal)ListVal->AddString(str);
        qDebug("Пост.Сост2=%d", avr_data1);

        //str.Format("С� З2=%f", (*fSKZ2));
            //if(ListVal)ListVal->AddString(str);
        qDebug("С� З2=%f", (*fSKZ2));

        //if(ListVal)ListVal->AddString("________________________");

    //////////////////////////////////////////////////////////////////////////
        c=0;
        do
        {
            //str.Format("%05d %f ",c, ptArray[c]);
            //if(ListVal)ListVal->AddString(str);
            //qDebug("%05d %d ",c, ptArray[c]);
            c++;
        }while (c<DATA_COUNT);
    //////////////////////////////////////////////////////////////////////////
    delete ptArray;
}

long ProcessObject::Sred(qint64 *buf, long n,
                         int toch, double crossval,
                         bool bRE)
{
    double x=0;
    qint64 sum1=0, sum2=0, rez=0;
    long i;

    for(i=1;i<=toch;i++)
    {
        sum1 = sum1+buf[n-toch+i];
        sum2 = sum2+buf[n+i];
    }

    sum1=sum1/toch;
    sum2=sum2/toch;

    if(sum1>crossval && sum2>crossval)
        return -1;
    if(sum1<crossval && sum2<crossval)
        return -1;

    if(sum1<crossval && sum2>crossval && !bRE)
        return -1;
    if(sum1>crossval && sum2<crossval && bRE)
        return -1;

    if(sum1!=crossval && sum2!=crossval)	x=-(sum1/sum2);

    rez=100*(1.0-sqrt(x))/(1.0+sqrt(x));

    return 1;/*n-rez*/;
}

long ProcessObject::Sred(int *buf, long n,
                         int toch, double crossval,
                         bool bRE)
{
    double x=0;
    qint64 sum1=0, sum2=0, rez=0;
    long i;

    for(i=1;i<=toch;i++)
    {
        sum1 = sum1+buf[n-toch+i];
        sum2 = sum2+buf[n+i];
    }

    sum1=sum1/toch;
    sum2=sum2/toch;

    if(sum1>crossval && sum2>crossval)
        return -1;
    if(sum1<crossval && sum2<crossval)
        return -1;

    if(sum1<crossval && sum2>crossval && !bRE)
        return -1;
    if(sum1>crossval && sum2<crossval && bRE)
        return -1;

    if(sum1!=crossval && sum2!=crossval)	x=-(sum1/sum2);

    rez=100*(1.0-sqrt(x))/(1.0+sqrt(x));

    return 1;/*n-rez*/;
}

void ProcessObject::peaks(int *ptArray1 ,int nSize, int nCh, bool max) {
    int DATA_COUNT=nSize; // кол-во отсчетов для обработки

    int  c, n=0;
    qint64 min_data, max_data, avr_data, avr_data1=0;
    double value, Us=0, Usq=0, Umax=-1e23, Umin=1e23;

    int DEBUG=0;

    //qint64 *ptArray = new qint64[nSize];
//    for (int i=0;i<nSize; i++)
//        ptArray[i] = ptBaseArray.at(i)*1000000;
    //int *ptArray = yData;
    int *ptArray = new int[nSize];
    mutex.lock();
    for (int i=0;i<nSize;i++)
        ptArray[i]=ptArray1[i];
    mutex.unlock();

    int minPeakPos = 0, maxPeakPos = 0;

    c=0;
    do
    {
        avr_data1+=ptArray[c];
        if (c==0) {min_data = max_data = ptArray[c];}
        else
        {
            if (ptArray[c]<min_data)
            {
                min_data=ptArray[c];
                minPeakPos = c;
            }
            if (ptArray[c]>max_data)
            {
                max_data=ptArray[c];
                maxPeakPos = c;
            }
        }
        c++;
    }while (c<DATA_COUNT);
    avr_data=min_data+(max_data-min_data)/2;
    avr_data1=avr_data1/DATA_COUNT;

    int nn=0;
    //qint64 crossval = avr_data; //уровень перехода через 0

    //QwtScaleMap map = plots->last()->canvasMap(QwtPlot::yLeft);

    qint64 crossval = plots->last()->getRightTriggerMarker()->yValue()/lastGainY;//*1000000;

    //double ff = max_data;
    //ff = ff*lastGainY;
    //ff = map.invTransform(ff);

    //crossval = crossval/lastGainY;

    qDebug()<<"crossval = average data: "<<crossval;

    int RE_CNT=0;
    int FE_CNT=0;

    int RE_FIRST=0;
    int RE_LAST=0;

    int FE_LAST=0;
    int FE_FIRST=0;


    int T=0;

    int Ti_1=0;
    int Ti_0=0;

    int Ti_1_cnt=0;
    int Ti_0_cnt=0;

    int re[100];
    int reCnt=0;
    int fe[100];
    int feCnt=0;

    int maxPeak = crossval;
    int minPeak = crossval;
    int peakPosMax = 0;
    int peakPosMin = 0;

    for(nn = 0; nn < DATA_COUNT-1-1; nn++)
    {
        if (ptArray[nn]>maxPeak) {
            maxPeak=ptArray[nn];
            peakPosMax = nn;
            //maxPeakPos=nn;
        }
        if (ptArray[nn]<minPeak) {
            minPeak=ptArray[nn];
            peakPosMin = nn;
        }

        if (ptArray[nn]<=crossval &&
            ptArray[nn+1]>=crossval&&
            ptArray[nn+2]>=crossval)
        {
        if (nn-1 != RE_LAST)
            {
                fe[feCnt]=peakPosMin;
                minPeak = crossval;
                feCnt++;
            }

            RE_CNT++;
            if(RE_CNT==1)RE_FIRST=nn;
            RE_LAST=nn;

            //re.append(nn);

            if(RE_CNT>=1&&FE_CNT>=1)
            {	Ti_0+=(RE_LAST-FE_LAST);Ti_0_cnt++;}

            //str.Format("RE=%d Ti_0=%d", nn, Ti_0);
//			m_list.AddString(str);
            //qDebug("RE=%d Ti_0=%d", nn, Ti_0);
        }

        if (ptArray[nn]>=crossval &&
            ptArray[nn+1]<=crossval)
        {
            if (nn-1 != FE_LAST)
            {
                re[reCnt]=peakPosMax;
                maxPeak = crossval;
                reCnt++;
            }

            FE_CNT++;
            if(FE_CNT==1)FE_FIRST=nn;
            FE_LAST=nn;

            //fe.append(nn);


            if(RE_CNT>=1&&FE_CNT>=1)
            {	Ti_1+=(FE_LAST-RE_LAST);Ti_1_cnt++;}

            //str.Format("FE=%d Ti_1=%d", nn, Ti_1);
//			m_list.AddString(str);
            //qDebug("FE=%d Ti_1=%d", nn, Ti_1);
        }
    }

    int *arr;
    int size;
    if (max) {arr = re; size = reCnt;}
    else {arr = fe; size = feCnt;}
    mutex.lock();
    //peaksBuffer.clear();
    for(c=0;c<size; c++) {
        if (c>=100) break;
        //peaksBuffer[c]=plots->last()->signalData(nCh).value(arr[c]);
        writeXPtr[c]=arr[c];
        writeYPtr[c]=ptArray[arr[c]];
        //double ww=writeXPtr[c]*lastGainX;
        //double www=writeYPtr[c]*lastGainY;
    }
    peaksCount = c;
    mutex.unlock();
    //updatePeaksMarkers(peaksBuffer, nCh, c);
    qDebug()<<"entering updatepeaksmarkers";
    updatePeaksMarkers(writeXPtr, writeYPtr,
                       nCh, c,
                       lastGainY, lastGainX);
    qDebug()<<"re_cnt: "<<RE_CNT;
    qDebug()<<"re_first: "<<RE_FIRST;
    qDebug()<<"fe_cnt: "<<FE_CNT;
    qDebug()<<"fe_first: "<<FE_FIRST<<endl;
    delete ptArray;
    //qDebug()<<re<<endl<<fe;
}

void ProcessObject::attachPeakMarkers() {
    peaksList->attach(plots->last());
    /*for (int i=0;i<markers.size();i++)
        markers.at(i)->attach(plots->last());*/
}

void ProcessObject::updatePeaksMarkers(QPointF *list, int nCh, int pCount) {
    qDebug()<<"in updatepeaksmarkers";
    int tempAxis;
    if (nCh == 0) tempAxis = QwtPlot::yLeft;
    else if (nCh == 1) tempAxis = QwtPlot::yRight;
    peaksList->setYAxis(tempAxis);
    peaksList->setPeaks(list);
    peaksList->setPeaksCount(pCount);
    //peaksList->itemChanged();

    //list++;
    peaksList->show();
    qDebug()<<"leaving updatepeaksmarkers";
}

void ProcessObject::updatePeaksMarkers(int *xArr, int *yArr,
                                       int nCh, int pCount,
                                       double yGain, double xGain)
{
    qDebug()<<"in updatepeaksmarkers int";
    int tempAxis;
    if (nCh == 0) tempAxis = QwtPlot::yLeft;
    else if (nCh == 1) tempAxis = QwtPlot::yRight;
    peaksList->setYAxis(tempAxis);
    mutex.lock();
    peaksList->setPeaks(xArr, yArr, yGain, xGain);
    peaksList->setPeaksCount(pCount);
    mutex.unlock();
    //peaksList->itemChanged();
    peaksList->show();
    qDebug()<<"leaving updatepeaksmarkers int";
}

void ProcessObject::GetSignalParamsInt(int *ptArray, int nSize)
{
    int DATA_COUNT=nSize; // кол-во отсчетов для обработки

    int *data, min_data, max_data;
    int  c, n;

    data= new(int[DATA_COUNT]);

//	m_list.ResetContent();
    /******************** ФО� МИ� ОВАНИЕ ИСХОДНЫХ ДАННЫХ *****************/

//	float f1, f2, f3;

    c=0;
    do
    {
        data[c]=ptArray[c];
        if (c==0) {min_data = max_data = data[c];}
        else
        {
            if (data[c]<min_data) min_data=data[c];
            if (data[c]>max_data) max_data=data[c];
        }
        c++;
    }while (c<DATA_COUNT);


    /* ДАННЫЕ для обработки: */
    /* int DATA_COUNT -  кол-во отсчетов */
    /* int data[] - массив отсчетов (каждый исходный умножен на 1000000)*/
    /* int max_data -  максимальное значение отсчетов */
    /* int min_data -  минимальное значение отсчетов */

    //str.Format("для рассчета используем %d точек\n", DATA_COUNT);
//	m_list.AddString(str);
    qDebug("для рассчета используем %d точек\n", DATA_COUNT);
    //str.Format("min=%f \t max=%f \n", min_data/1.0e6, max_data/1.0e6);
//	m_list.AddString(str);
    qDebug("min=%f \t max=%f \n", min_data/1.0e6, max_data/1.0e6);
    /**************** � АСЧЕТ ***********************/
    int ves[PREC]; // массив весов
    for (c=0; c<PREC; c++)	ves[c]=0; // его инициализация

    qint64 avr_ves, pik_max, pik_range, avr_pik, avr_fon;
    double sum;
    qint64 v;

    // заполняем таблицу весов
    // весь диапазон изменения сигнала разбивается на PREC областей (соотв. ячейкам ves[])
    // в каждой ячейке ves[] хранится число значений сигнала из соотв. области
    for (c=0, pik_max=data[0]; c<DATA_COUNT; c++)
    {
        v=(data[c]-min_data)/double(max_data-min_data)*(PREC-1);
        if (v<0 || v>=PREC) { //qDebug("%d: data= %d   v=%d\n", c, data[c], v);
            /*str.Format("%d: data= %d   v=%d\n", c, data[c], v);*//*m_list.AddString(str);*/}
        else ves[v]++;
    }

    // вычисление среднего значения  таблицы весов
    for (c=0, sum=0; c<PREC; c++)
    {
        sum+=ves[c];

        // вывод заполненной таблицы весов (просто для информации)
        //str.Format("ves[%d, %f]=%d\n", c, (double(c)/PREC*(max_data-min_data)+min_data)/1.0e6,ves[c]);
//		m_list.AddString(str);
        //qDebug("ves[%d, %f]=%d\n", c, (double(c)/PREC*(max_data-min_data)+min_data)/1.0e6,ves[c]);
    }

    avr_ves=sum/v; // среднее по массиву весов
    //str.Format("avr_ves=%d \n", avr_ves);
//	m_list.AddString(str);
    qDebug("avr_ves=%d \n", avr_ves);
    // определяем начало диапазона, на RANGE_PIK_MAX ниже максимального значения сигнала (для определения среднего значения пичков).
    pik_range=max_data-max_data*RANGE_PIK_MAX;

    // определяем среднее значение пичков в этом диапазоне
    for (c=0, n=0, sum=0; c<DATA_COUNT; c++)
    {
        if (data[c]>pik_range) {sum+=data[c]; n++;}
    }
    avr_pik=sum/n;
    //str.Format("max_pik=%f\n", max_data/1.0e6); // максимальный пичок
//	m_list.AddString(str);
    qDebug("max_pik=%f\n", max_data/1.0e6);
    //str.Format("avr_pik(%d)=%f\n", n, avr_pik/1.0e6); // среднее значение величины пичков в диапазоне RANGE_PIK_MAX ниже максимального
//	m_list.AddString(str);
    qDebug("avr_pik(%d)=%f\n", n, avr_pik/1.0e6);

    // определяем самый большой спад в массиве весов (т.е. примерную границу фона)
    qint64 max_fall, max_fall_point;

    max_fall_point=1;	max_fall=ves[0]-ves[1];
    for (c=1; c<PREC; c++)
    {
        if ((ves[c-1]-ves[c]) > max_fall)
        {
            max_fall=ves[c-1]-ves[c];
            max_fall_point=c;
        }

    }
    // если спад вообще не обнаружен
    if (max_fall<=0) {qDebug("err: max_fall<=0");
        /*str.Format("err: max_fall<=0");*//*m_list.AddString(str);*/ delete data; return;}

    // определяем после максимального спада первую точку ниже среднего значения масиива весов
    for (c=max_fall_point; c<PREC; c++)
    {
        if (ves[c]<avr_ves) break;
    }

    // теоретически такого не может быть, но все-таки
    if (c>=PREC) c=max_fall_point; // {printf("err: c>=PREC"); delete data; return -1;}

    // пороговое значение фона:
    v=double(c)/PREC*(max_data-min_data)+min_data; // пороговое значение для фона  в массиве исходных данных
    //str.Format("maxfall[%d]=%d fon_range[%d]=%f \n", max_fall_point, max_fall, c, v/1.0e6);
//	m_list.AddString(str);
    qDebug("maxfall[%d]=%d fon_range[%d]=%f \n", max_fall_point, max_fall, c, v/1.0e6);

    // среднее значение порогового значения фона
    int fon1,fon2;// границы поискового диапазона от средней амплитуды фона
    // диапазон значений в районе максимального спада
    fon1=double(c-1)/PREC*(max_data-min_data)+min_data;
    fon2=double(c)/PREC*(max_data-min_data)+min_data;
    if (fon1>fon2) {v=fon1; fon1=fon2; fon2=v;}

    // определяем среднее значение фона в этом диапазоне
    for (c=0, n=0, sum=0; c<DATA_COUNT; c++)
    {
        if (data[c]>fon1 && data[c]<fon2) {sum+=data[c]; n++;}
    }
    avr_fon=sum/n;

    //str.Format("fon_range= %f ... %f \t avr_fon(%d)=%f\n", fon1/1.0e6, fon2/1.0e6, n, avr_fon/1.0e6);
//	m_list.AddString(str);
    qDebug("fon_range= %f ... %f \t avr_fon(%d)=%f\n", fon1/1.0e6, fon2/1.0e6, n, avr_fon/1.0e6);

    delete data;
}

void ProcessObject::GetSignalParams2Int(int *basePtArray, int nSize)
{
    int DATA_COUNT=nSize; // кол-во отсчетов для обработки

    int  c, n=0;
    qint64 min_data, max_data, avr_data, avr_data1=0;
    double value, Us=0, Usq=0, Umax=-1e23, Umin=1e23;

    qint64 *ptArray = new (qint64[nSize]);
    for (int i=0;i<nSize; i++)
        ptArray[i] = basePtArray[i];

    qDebug()<<"array size: "<<nSize;
//	m_list.ResetContent();
    /******************** ФО� МИ� ОВАНИЕ ИСХОДНЫХ ДАННЫХ *****************/

//	float f1, f2, f3;

    c=0;
    do
    {
        avr_data1+=ptArray[c];
        if (c==0) {min_data = max_data = ptArray[c];}
        else
        {
            if (ptArray[c]<min_data) min_data=ptArray[c];
            if (ptArray[c]>max_data) max_data=ptArray[c];
        }
        c++;
    }while (c<DATA_COUNT);

    qDebug()<<"min_data: "<<min_data;
    qDebug()<<"max_data: "<<max_data;

    avr_data=min_data+(max_data-min_data)/2;
    avr_data1=avr_data1/DATA_COUNT;

    qDebug()<<"average array data: "<<avr_data1;
    qDebug()<<"average something data: "<<avr_data;


    //str.Format("для рассчета используем %d точек\n", DATA_COUNT);
//	m_list.AddString(str);
    //str.Format("min=%d max=%d avr=%d avr1=%d", min_data, max_data, avr_data, avr_data1);
//	m_list.AddString(str);

    ////////////////

    int p1=0, p2=0;
    int nn=0;
    qint64 crossval = avr_data; //уровень перехода через 0

    qDebug()<<"crossval = average data: "<<crossval;

    int RE_CNT=0;
    int FE_CNT=0;

    int RE_FIRST=0;
    int RE_LAST=0;

    int FE_LAST=0;
    int FE_FIRST=0;


    int T=0;

    int Ti_1=0;
    int Ti_0=0;

    int Ti_1_cnt=0;
    int Ti_0_cnt=0;

    for(nn = 0; nn < DATA_COUNT-1-1; nn++)
    {
        if (ptArray[nn]<=crossval &&
            ptArray[nn+1]>=crossval&&
            ptArray[nn+2]>=crossval)
        {
            RE_CNT++;
            if(RE_CNT==1)RE_FIRST=nn;
            RE_LAST=nn;

            if(RE_CNT>=1&&FE_CNT>=1)
            {	Ti_0+=(RE_LAST-FE_LAST);Ti_0_cnt++;}

            //str.Format("RE=%d Ti_0=%d", nn, Ti_0);
//			m_list.AddString(str);
            //qDebug("RE=%d Ti_0=%d", nn, Ti_0);
        }

        if (ptArray[nn]>=crossval &&
            ptArray[nn+1]<=crossval)
        {
            FE_CNT++;
            if(FE_CNT==1)FE_FIRST=nn;
            FE_LAST=nn;

            if(RE_CNT>=1&&FE_CNT>=1)
            {	Ti_1+=(FE_LAST-RE_LAST);Ti_1_cnt++;}

            //str.Format("FE=%d Ti_1=%d", nn, Ti_1);
//			m_list.AddString(str);
            //qDebug("FE=%d Ti_1=%d", nn, Ti_1);
        }
    }

    qDebug()<<"re_cnt: "<<RE_CNT;
    qDebug()<<"re_first: "<<RE_FIRST;
    qDebug()<<"fe_cnt: "<<FE_CNT;
    qDebug()<<"fe_first: "<<FE_FIRST;

    if(RE_CNT>1)
    {
        T=(RE_LAST-RE_FIRST)/(RE_CNT-1);
        //str.Format("Tavr_RE=%d ", T);
//		m_list.AddString(str);
        qDebug("Tavr_RE=%d ", T);
    }

    if(FE_CNT>1)
    {
        T=(FE_LAST-FE_FIRST)/(FE_CNT-1);
        //str.Format("Tavr_FE=%d ", T);
//		m_list.AddString(str);
        qDebug("Tavr_FE=%d ", T);
    }

    if( Ti_0_cnt>=1 )
    {
        Ti_0=Ti_0/Ti_0_cnt;
        //str.Format("Ti_0=%d(%d)", Ti_0, Ti_0_cnt);
//		m_list.AddString(str);
        qDebug("Ti_0=%d(%d)", Ti_0, Ti_0_cnt);
    }

    if( Ti_1_cnt>=1 )
    {
        Ti_1=Ti_1/Ti_1_cnt;
        //str.Format("Ti_1=%d(%d)", Ti_1, Ti_1_cnt);
//		m_list.AddString(str);
        qDebug("Ti_1=%d(%d)", Ti_1, Ti_1_cnt);
    }

    for(nn = RE_FIRST; nn < RE_LAST; nn++)
    {
        Usq+=ptArray[nn]*ptArray[nn];
    }

    value=sqrt(fabs(Usq/(RE_LAST-RE_FIRST)));
    //str.Format("SKZ=%f", value);
//		m_list.AddString(str);
    qDebug("SKZ=%f", value);

//////////////////////////////////////////////////////////////////////////

    delete ptArray;
}

void ProcessObject::GetSignalParams3Int(int *basePtArray, int nSize,
                                        double timebaseSec,
                                        double *fA,
                                        double *fSKZ,
                                        double *fSKZ2,
                                        double *fF,
                                        double *fT,
                                        double *fTi,
                                        double *fTi_1,
                                        double *fTi_2,
                                        double *fAi)
{
    int DATA_COUNT=nSize; // кол-во отсчетов для обработки

    int c, n=0;
    qint64 min_data, max_data, avr_data, avr_data1=0;

    double value=0, Us=0, Usq=0, Umax=-1e23, Umin=1e23;

    int *ptArray = new int[nSize];
    for (int i=0;i<nSize; i++)
        ptArray[i] = basePtArray[i];

//	if(CalibrWnd)CalibrWnd->m_list.ResetContent();
    //if(ListVal)ListVal->ResetContent();
//	float f1, f2, f3;

    c=0;
    do
    {
        avr_data1+=ptArray[c];
        if (c==0) {min_data = max_data = ptArray[c];}
        else
        {
            if (ptArray[c]<min_data) min_data=ptArray[c];
            if (ptArray[c]>max_data) max_data=ptArray[c];
        }
        c++;
    }while (c<DATA_COUNT);

    avr_data=min_data+(max_data-min_data)/2;
    avr_data1=avr_data1/DATA_COUNT;

    (*fA)=(max_data-min_data);

    //str.Format("для рассчета используем %d точек\n", DATA_COUNT);
    qDebug("для рассчета используем %d точек\n", DATA_COUNT);
    //if(ListVal)ListVal->AddString(str);
    //str.Format("min=%f max=%f avr=%f avr1=%f", min_data, max_data, avr_data, avr_data1);
    qDebug("min=%d max=%d avr=%d avr1=%d", min_data, max_data, avr_data, avr_data1);
    //if(ListVal)ListVal->AddString(str);

    ////////////////

    int p1=0, p2=0;
    int nn=0;
    qint64 crossval = avr_data; //уровень перехода через 0

    int RE_CNT=0;	// количество восходящих фронтов
    int FE_CNT=0;	// количество спадающих фронтов

    int RE_FIRST=0;
    int RE_LAST=0;

    int FE_LAST=0;
    int FE_FIRST=0;


    int T=0;
    int Tavr_RE=0;
    int Tavr_FE=0;

    double Ti_1=0;
    double Ti_0=0;
    double Ti_1_first=0;//длительность первого импульса
    double Ti_1_other=0;//длительность остальных импульсов, усреднённая

    double Ti_1_cnt=0;
    double Ti_0_cnt=0;

    double Ai_1=0;
    double Ai_0=0;


    for(nn = ACCURACY/*0*/; nn < DATA_COUNT-ACCURACY/*1*/; nn++)
    {
        if(FE_LAST>=RE_LAST)
        {
            if (ptArray[nn]<=crossval &&
                ptArray[nn+1]>=crossval )
            if(Sred(ptArray,nn,ACCURACY,crossval,1)!=-1)
            {
                RE_CNT++;
                if(RE_CNT==1)RE_FIRST=nn;
                RE_LAST=nn;

                if(RE_CNT>=1&&FE_CNT>=1)
                {
                    Ti_0+=(RE_LAST-FE_LAST);
                    Ti_0_cnt++;
                    value=0;
                    for(long j = FE_LAST; j < RE_LAST; j++)
                        value+=ptArray[j];
                    value=value/(RE_LAST-FE_LAST);
                    Ai_0+=value;
                }

                //str.Format("RE=%d Ti_0=%f", nn, Ti_0);
                //if(ListVal)ListVal->AddString(str);
                //qDebug("RE=%d Ti_0=%f", nn, Ti_0);

                continue;
            }
        }

        if(RE_LAST>=FE_LAST)
        {
            if (ptArray[nn]>=crossval &&
                ptArray[nn+1]<=crossval )
            if(Sred(ptArray,nn,ACCURACY,crossval,0)!=-1)
            {
                FE_CNT++;
                if(FE_CNT==1)FE_FIRST=nn;
                FE_LAST=nn;

                if(RE_CNT>=1&&FE_CNT>=1)
                {
                    Ti_1+=(FE_LAST-RE_LAST);
                    Ti_1_cnt++;
                    if(Ti_1_cnt==1)Ti_1_first=(FE_LAST-RE_LAST);// запоминаем длительность первого импульса
                    value=0;
                    for(long j = RE_LAST; j < FE_LAST; j++)
                        value+=ptArray[j];
                    value=value/(FE_LAST-RE_LAST);
                    Ai_1+=value;
                }

                //str.Format("FE=%d Ti_1=%f", nn, Ti_1);
                //if(ListVal)ListVal->AddString(str);
                //qDebug("FE=%d Ti_1=%f", nn, Ti_1);
                continue;
            }
        }
    }

    // амплитуда импульсов с усреднением на верхних полках
    if(Ti_0_cnt!=0)Ai_0=Ai_0/Ti_0_cnt;
    if(Ti_1_cnt!=0)Ai_1=Ai_1/Ti_1_cnt;

    (*fAi)=(Ai_1-Ai_0);

    //str.Format("Ai=%f,  Ai_0=%f (%f),  Ai_1=%f (%f)", (*fAi), Ai_0, Ti_0_cnt, Ai_1, Ti_1_cnt);
        //if(ListVal)ListVal->AddString(str);
    qDebug("Ai=%f,  Ai_0=%f (%f),  Ai_1=%f (%f)", (*fAi), Ai_0, Ti_0_cnt, Ai_1, Ti_1_cnt);

    // вычисляем длительность периода
    if(RE_CNT>1)
    {
        Tavr_RE=(RE_LAST-RE_FIRST)/(RE_CNT-1);
        //str.Format("Tavr_RE=%d ", Tavr_RE);
        //if(ListVal)ListVal->AddString(str);
        qDebug("Tavr_RE=%d ", Tavr_RE);
    }

    if(FE_CNT>1)
    {
        Tavr_FE=(FE_LAST-FE_FIRST)/(FE_CNT-1);
        //str.Format("Tavr_FE=%d ", Tavr_FE);
        //if(ListVal)ListVal->AddString(str);
        qDebug("Tavr_FE=%d ", Tavr_FE);
    }

    (*fT)=Tavr_RE*timebaseSec;
    if(Tavr_RE!=0)(*fF)=1/(*fT);


    // вычисляем длительности импульсов
    (*fTi_1)=Ti_1_first*timebaseSec;
    (*fTi_2)=((Ti_1-Ti_1_first)/(Ti_1_cnt-1))*timebaseSec;

    if( Ti_0_cnt>=1 )
    {
        Ti_0=Ti_0/Ti_0_cnt;
        //str.Format("Ti_0=%f(%f)", Ti_0, Ti_0_cnt);
        //if(ListVal)ListVal->AddString(str);
        qDebug("Ti_0=%f(%f)", Ti_0, Ti_0_cnt);
    }

    if( Ti_1_cnt>=1 )
    {
        Ti_1=Ti_1/Ti_1_cnt;
        //str.Format("Ti_1=%f(%f)", Ti_1, Ti_1_cnt);
        //if(ListVal)ListVal->AddString(str);
        qDebug("Ti_1=%f(%f)", Ti_1, Ti_1_cnt);
    }

    (*fTi)=Ti_1*timebaseSec;


    // вычисляем постоянную составляющую
    avr_data=0.0;
    for(nn = RE_FIRST; nn < RE_LAST; nn++)
        avr_data+=ptArray[nn];
    if((RE_LAST-RE_FIRST)!=0)
        avr_data=avr_data/(RE_LAST-RE_FIRST);
    //str.Format("Пост.Сост=%f", avr_data);
        //if(ListVal)ListVal->AddString(str);
    qDebug("Пост.Сост=%d", avr_data);

    // вычисляем С� З с вычетом постоянной составляющей
    value=0.0;
    for(nn = RE_FIRST; nn < RE_LAST; nn++)
        Usq+=pow(fabs(ptArray[nn]-avr_data),2);
    if((RE_LAST-RE_FIRST)!=0)
    value=sqrt(fabs(Usq/(RE_LAST-RE_FIRST)));
    //str.Format("SKZ=%f", value);
        //if(ListVal)ListVal->AddString(str);
    qDebug("SKZ=%f", value);

    (*fSKZ)=value;


    // вариант без периодов - по всем выборкам
    // вычисляем постоянную составляющую
    avr_data1=0.0;
    for(nn = 0; nn < DATA_COUNT; nn++)
        avr_data1+=ptArray[nn];
    if(DATA_COUNT!=0)
        avr_data1=avr_data1/DATA_COUNT;
    //str.Format("Пост.Сост2=%f", avr_data1);
        //if(ListVal)ListVal->AddString(str);
    qDebug("Пост.Сост2=%d", avr_data1);

    // вычисляем С� З с вычетом постоянной составляющей
    value=0.0;
    for(nn = 0; nn < DATA_COUNT; nn++)
        Usq+=pow(fabs(ptArray[nn]-avr_data1),2);
    if(DATA_COUNT!=0)
    value=sqrt(fabs(Usq/DATA_COUNT));
    //str.Format("SKZ2=%f", value);
        //if(ListVal)ListVal->AddString(str);
    qDebug("SKZ2=%f", value);

    (*fSKZ2)=value;


//////////////////////////////////////////////////////////////////////////

    //if(ListVal)ListVal->AddString("________________________");

    //str.Format("A=%f ", (*fA));
        //if(ListVal)ListVal->AddString(str);
    qDebug("A=%f ", (*fA));

    //str.Format("F=%f ", (*fF));
        //if(ListVal)ListVal->AddString(str);
    qDebug("F=%f ", (*fF));

    //str.Format("T=%f ", (*fT));
        //if(ListVal)ListVal->AddString(str);
    qDebug("T=%f ", (*fT));

    //str.Format("Ti_1=%f(%f)", (*fTi), Ti_1_cnt);
        //if(ListVal)ListVal->AddString(str);
    qDebug("Ti_1=%f(%f)", (*fTi), Ti_1_cnt);

    //str.Format("Ti_0=%f(%f)", Ti_0*timebaseSec, Ti_0_cnt);
        //if(ListVal)ListVal->AddString(str);
    qDebug("Ti_0=%f(%f)", Ti_0*timebaseSec, Ti_0_cnt);

    //str.Format("Пост.Сост=%f", avr_data);
        //if(ListVal)ListVal->AddString(str);
    qDebug("Пост.Сост=%d", avr_data);

    //str.Format("С� З=%f", (*fSKZ));
        //if(ListVal)ListVal->AddString(str);
    qDebug("С� З=%f", (*fSKZ));

    //str.Format("Пост.Сост2=%f", avr_data1);
        //if(ListVal)ListVal->AddString(str);
    qDebug("Пост.Сост2=%d", avr_data1);

    //str.Format("С� З2=%f", (*fSKZ2));
        //if(ListVal)ListVal->AddString(str);
    qDebug("С� З2=%f", (*fSKZ2));

    //if(ListVal)ListVal->AddString("________________________");

//////////////////////////////////////////////////////////////////////////
    c=0;
    do
    {
        //str.Format("%05d %f ",c, ptArray[c]);
        //if(ListVal)ListVal->AddString(str);
        //qDebug("%05d %d ",c, ptArray[c]);
        c++;
    }while (c<DATA_COUNT);
//////////////////////////////////////////////////////////////////////////
delete ptArray;
}

void ProcessObject::setPeaksVisible(bool s)
{
    showPeaks = s;
    if (peaksList)
        peaksList->setVisible(s);
}

// OSC PROCESS OBJECT

OscProcess::OscProcess(QObject *parent) :
    ProcessObject(parent) {
    setProcessName("Osci");
}

bool OscProcess::processData(QByteArray *data, int nCh) {
    bool bRes=false;
    switch (conf->subMeasMode) {
    case 0:
        bRes=calculatePower(data,nCh);
        break;
    }
    if (bRes)
    {//bwait = true
        bWait = true;
        emit dataReady(str1,str2,str3);
    }
    /////////////////////////////////////
    return bRes;
}

void OscProcess::processDataDDC(int &len) {
    render->modeConf->fft_window = render->fft_window;
    if (render->modeConf->nPass==0) {
        len=RAW_SAMPLES_NUM;
        render->m_worker->initDDC(false, render->modeConf);
    }
}

bool OscProcess::calculatePower(QByteArray *data, int nCh) {
    if (!render->modeConf)
        return false;
    double gain=render->getGainY(nCh);//1.0;//
    lastGainY=gain;
    double gain_mks=render->getGainX();
    lastGainX=gain_mks;
    QList<double> yList;
    QList<double> yList1;
    peaksList->hide();
//    for (int i=0;i<markers.size(); i++)
//        markers.at(i)->hide();
#ifdef unused
    if(plot){
        plot->signalData(nCh).clearValues();
    }
#endif
    QPointF s;
    QPointF s2;
    double deviation;
    deviation = plot->markerYValue(nCh);
    int deviationInt = deviation/gain;

    double ss=0.0;
    double ss2=0.0;

    double avgVal1 = 0;
    double avgVal2 = 0;
    for(int i=/*5*/0; i<RAW_SAMPLES_NUM; i++)
    {
        s.setX(gain_mks*i);
        s2.setX(gain_mks*i);

        short y2=data->at(i*4)<<8;
        y2|=0xff&data->at(i*4+1);
        short y=data->at(i*4+2)<<8;
        y|=0xff&data->at(i*4+3);

        yData[i] = (nCh==0?y:y2)+deviationInt;

        if(y>render->modeConf->max_y)
            render->modeConf->max_y=y;
        if(y2>render->modeConf->max_y2)
            render->modeConf->max_y2=y2;

        if(y<render->modeConf->min_y
                )render->modeConf->min_y=y;
        if(y2<render->modeConf->min_y2)
            render->modeConf->min_y2=y2;
        //перевод в вольты

        double val=gain*y/**7/8*/;
        //yList.append(val);
        avgVal1+=val;

        double val2=gain*y2/**7/8*/;
        //yList1.append(val);
        avgVal2+=val2;

        s.setY(gain*y+deviation/**7/8/**ma*/);
        s2.setY((gain*y2)+deviation/**7/8*/);

#ifdef unused
        if(plot) {
            //plot->signalData(1).value()
            plot->signalData(nCh).append((nCh==0?s:s2));
        }
#endif
        ss+=val*val;
        ss2+=val2*val2;
    }
    avgVal1 = averageValue(avgVal1, RAW_SAMPLES_NUM);
    avgVal2 = averageValue(avgVal2, RAW_SAMPLES_NUM);

    double delta1 = delta(render->modeConf->max_y,
            render->modeConf->min_y)*gain;
    double delta2 = delta(render->modeConf->max_y2,
            render->modeConf->min_y2)*gain;

    double myMax1 = render->modeConf->max_y*gain;
    double myMax2 = render->modeConf->max_y2*gain;
    double myMin1 = render->modeConf->min_y*gain;
    double myMin2 = render->modeConf->min_y2*gain;

    double max=gain*render->modeConf->max_y;
    double max2=gain*render->modeConf->max_y2;

    double SKZ=RMS((nCh==1?ss2:ss),RAW_SAMPLES_NUM);//qSqrt((nCh==1?ss2:ss)/RAW_SAMPLES_NUM);
    double P=10*(qLn(1000*qPow(SKZ,2)/50)/qLn(10));


//    str1=QString::number(SKZ,'f', 3);
//    str2=QString::number(P,'f', 2);
//    str3=QString::number(nCh==1?max2:max,'f', 3);

    str1="average1 "+QString::number(avgVal1)+", average2: "+QString::number(avgVal2);
    str2="min1 "+QString::number(myMin1)+", max1: "+QString::number(myMax1)+", delta1: "+QString::number(delta1);
    str3="min2 "+QString::number(myMin2)+", max2: "+QString::number(myMax2)+", delta2: "+QString::number(delta2);;

    //
    qDebug() << "Напряжение ВЧ " <<  SKZ << endl;
    qDebug()<<"Average value 1: "<<avgVal1<<" average value 2: "<<avgVal2;
    qDebug()<<"min1: "<<myMin1<<" max1: "<<myMax1<<" delta1: "<<delta1;
    qDebug()<<"min2: "<<myMin2<<" max2: "<<myMax2<<" delta2: "<<delta2<<endl;

    //GetSignalParams2(yList, yList.size());
    //GetSignalParams2Int(yData, RAW_SAMPLES_NUM);
    //GetSignalParams(yList, yList.size());
    double t[9];
//    GetSignalParams3(yList, yList.size(),1,
//            &t[0],&t[1],&t[2],&t[3],&t[4],
//            &t[5],&t[6],&t[7],&t[8]);
    //QList<int> maxPeaks;
    if (showPeaks)
        peaks(yData ,RAW_SAMPLES_NUM, nCh, true);

    qDebug()<<endl;
    this->negPeak = statistic(negPeakData, negPeakProp, myMin1);
    this->posPeak = statistic(posPeakData, posPeakProp, myMax1);
    this->AVG = statistic(AVGData, AVGProp, avgVal1);
    this->SKZ = statistic(SKZData, SKZProp, SKZ);
    return true;
}

// SPEC PROCESS OBJECT

SpecProcess::SpecProcess(QObject *parent) :
    ProcessObject(parent) {
    setProcessName("Spec");
}

bool SpecProcess::processData(QByteArray *data, int nCh)
{
    bool bRes=false;
    switch (conf->subMeasMode) {
    case 0:
        bRes=calculateSpectre(data,nCh);
        break;
    case 1:
        bRes=calculateFrequency(data,nCh);
        break;
    case 2:
        bRes=calculateKNI(data,nCh);
        break;
    case 3:
        bRes=calculatePHOSO(data,nCh);
        break;
    }
    if (bRes)
        emit dataReady(str1,str2,str3);
    /////////////////////////////////////
    return bRes;
}

void SpecProcess::processDataDDC(int &len) {
    render->modeConf->fft_window = render->fft_window;
    if (render->modeConf->nPass==0) {
        len=FFT_SAMPLES_NUM;
        switch (conf->subMeasMode) {
        case 0:
            render->m_worker->initDDC(true,render->modeConf);
            render->modeConf->nPass=1;//
            break;
        case 1:
            render->modeConf->nPassNum=3;
            break;
        case 2:
            render->m_worker->initDDC(true, render->modeConf);
            break;
        case 3:
            render->modeConf->nPassNum=16;
            render->m_worker->initFFT_PHOSO(render->modeConf);
            break;
        }
    }
    if (conf->subMeasMode==1)
        render->m_worker->initFFT_FREQ(render->modeConf);
}

bool SpecProcess::calculateSpectre(QByteArray *data, int nCh) {
    qDebug("fft data ready ............");

    quint32 y_max=0;
    quint32 x_max=0;
    double f=0.0;
    // todo switch by data type/mode
    if(render->m_nReadPtr>=FFT_SAMPLES_NUM)
    {
        // calculate
         // todo
        // перестновка
        str1="";
        str2="";
        str3="";
#ifdef unused
        if(plot) {
            plot->signalData(nCh).clearValues();
        }
#endif
        double deviation;
        deviation = plot->markerYValue(nCh);
        double gain=render->getGainY(nCh);//1.25*1/qPow(2,28);// // todo...
        double gain_mks=render->getGainX();
        double gain_x=render->modeConf->fBand/render->modeConf->Nband;
        QPointF s;
        qDebug()<<"gain_x="<<gain_x<<" Band="<<render->modeConf->fBand<<" NBand="<<render->modeConf->Nband<<endl;
        int x=0;
        for(int j=FFT_SAMPLES_NUM-render->modeConf->Nband/2; j<FFT_SAMPLES_NUM; j++)
        {
            s.setX((render->modeConf->Fleft+gain_x*x)/1000000);
            x++;
            unsigned int y=data->at(j*4)<<24;
            y|=(0xff&data->at(j*4+1))<<16;
            y|=(0xff&data->at(j*4+2))<<8;
            y|=0xff&data->at(j*4+3);

            if(y>y_max)
            {
                y_max=y;
                x_max=j;  // or x
                f=((render->modeConf->Fdec*j)/FFT_SAMPLES_NUM)-render->modeConf->Fdec;//(Fleft+gain_x*x);//
            }

            double dB=(y==0)?-170:20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=(y==0)?-170:10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????
            s.setY(dBV+deviation/*gain*y*/);
#ifdef unused
            if(plot) {
                plot->signalData(nCh).append(s);
            }
#endif
        }
        for(int i=0; i<render->modeConf->Nband/2; i++)
        {
            s.setX((render->modeConf->Fleft+gain_x*x)/1000000);
            x++;
            unsigned int y=data->at(i*4)<<24;
            y|=(0xff&data->at(i*4+1))<<16;
            y|=(0xff&data->at(i*4+2))<<8;
            y|=0xff&data->at(i*4+3);

            if(y>y_max)
            {
                y_max=y;
                x_max=i;  // or x
                f=((render->modeConf->Fdec*i)/FFT_SAMPLES_NUM)-render->modeConf->Fdec;//(Fleft+gain_x*x);//
            }

            double dB=(y==0)?-170:20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=(y==0)?-170:10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????
            s.setY(dBV+deviation/*gain*y*/);
#ifdef unused
            if(plot) {
                plot->signalData(nCh).append(s);
            }
#endif
        }
        qDebug()<<"FFT x_max="<<x_max<< " y_max=" << y_max<< ":f="<< f<<  endl;
    }

    return true;
}

bool SpecProcess::calculateFrequency(QByteArray *data, int nCh){
    quint32 y_max=0;
    quint32 x_max=0;
    double f=0.0;
    // todo switch by data type/mode
    if(render->m_nReadPtr>=FFT_SAMPLES_NUM)
    {
        // calculate
         // todo
        // перестновка
        str1="";
        str2="";
        str3="";
#ifdef unused
        if(plot) {
            plot->signalData(nCh).clearValues();
        }
#endif
        double deviation;
        deviation = plot->markerYValue(nCh);
        double gain=render->getGainY(nCh);//1.25*1/qPow(2,28);// // todo...
        double gain_mks=render->getGainX();
        double gain_x=/*m_mw->*/render->modeConf->fBand/render->modeConf->Nband;
        QPointF s;
        int x=0;
        for(int j=FFT_SAMPLES_NUM-render->modeConf->Nband/2; j<FFT_SAMPLES_NUM; j++)
        {
            s.setX(x);
            unsigned int y=data->at(j*4)<<24;
            y|=(0xff&data->at(j*4+1))<<16;
            y|=(0xff&data->at(j*4+2))<<8;
            y|=0xff&data->at(j*4+3);

            if(y>y_max)
            {
                y_max=y;
                x_max=j;  // or x
                f=((render->modeConf->Fdec*j)/FFT_SAMPLES_NUM)-render->modeConf->Fdec;//(Fleft+gain_x*x);//
            }
            x++;

            double dB=20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????
            s.setY(dBV+deviation/*gain*y*/);
#ifdef unused
            if(plot) {
                plot->signalData(nCh).append(s);
            }
#endif
        }

        for(int i=0; i<render->modeConf->Nband/2; i++)
        {
            s.setX(x);
            unsigned int y=data->at(i*4)<<24;
            y|=(0xff&data->at(i*4+1))<<16;
            y|=(0xff&data->at(i*4+2))<<8;
            y|=0xff&data->at(i*4+3);

            if(y>y_max)
            {
                y_max=y;
                x_max=i;  // or x
                f=(render->modeConf->Fdec*i)/FFT_SAMPLES_NUM;//(Fleft+gain_x*x);//
            }
            x++;
            double dB=20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????
            s.setY(dBV+deviation/*gain*y*/);
#ifdef unused
            if(plot){
                plot->signalData(nCh).append(s);
            }
#endif
        }
    }

    if(render->modeConf->nPass==0)
    {
        render->modeConf->dFt1=f;
        render->modeConf->Ft1=render->modeConf->Ft0+render->modeConf->dFt1;//f;//
        str1=QString::number(render->modeConf->Ft1,'f', 2);
        qDebug("pass 1 dFt1=%f Ft0=%f Ft1=%f x_max=%d",
               render->modeConf->dFt1 , render->modeConf->Ft0 ,render->modeConf->Ft1, x_max);
    }
    else if(render->modeConf->nPass==1)
    {
        render->modeConf->dFt2=f;
        render->modeConf->Ft2=render->modeConf->Ft1+render->modeConf->dFt2;//f;//
        str2=QString::number(render->modeConf->Ft2,'f', 2);
        qDebug("pass 2 dFt2=%f Ft1=%f Ft2=%f x_max=%d",
               render->modeConf->dFt2 , render->modeConf->Ft1 ,render->modeConf->Ft2, x_max);
    }
    else if(render->modeConf->nPass==2)
    {
        render->modeConf->dFt3=f;
        render->modeConf->Ft3=render->modeConf->Ft2+render->modeConf->dFt3;//f;//
        str3=QString::number(render->modeConf->Ft3,'f', 2);
        qDebug("pass 3 dFt3=%f Ft2=%f Ft3=%f x_max=%d y_max=%d",
               render->modeConf->dFt3 , render->modeConf->Ft2 ,render->modeConf->Ft3, x_max, y_max);
    }

    return render->modeConf->nPass==2? true: false;
}

bool SpecProcess::calculateKNI(QByteArray *data, int nCh) {
    qDebug("fft kni data ready ............");
    unsigned int F[FFT_SAMPLES_NUM]={0};

    quint32 y_max=0;
    quint32 g=0;
    double f=0.0;
    const quint8 dF=8;
    int N=32;
    quint64 U[32]={0};
    quint64 S0=0;
    quint64 S1=0;
    int a_left=qRound((render->modeConf->Fleft*render->modeConf->Nfft)/render->modeConf->Fdec);//10;//
    int a_right=qRound((render->modeConf->Fright*render->modeConf->Nfft)/render->modeConf->Fdec);//3000;//
    if(a_left==0)a_left=1;
    // todo switch by data type/mode
    if(render->m_nReadPtr>=FFT_SAMPLES_NUM)
    {
        // calculate
         // todo
        // перестновка
        str1=str2=str3="";

#ifdef unused
        if(plot) {
            plot->signalData(nCh).clearValues();
        }
#endif
        double deviation;
        deviation = plot->markerYValue(nCh);
        double gain=render->getGainY(nCh);//1.25*1/qPow(2,28);// // todo...
        double gain_mks=render->getGainX();
        double gain_x=render->modeConf->fBand/render->modeConf->Nband;
        QPointF s;
        int m=0;
        for(int jj=FFT_SAMPLES_NUM-render->modeConf->Nband/2; jj<FFT_SAMPLES_NUM; jj++)
        {
            unsigned int y=data->at(jj*4)<<24;
            y|=(0xff&data->at(jj*4+1))<<16;
            y|=(0xff&data->at(jj*4+2))<<8;
            y|=0xff&data->at(jj*4+3);
            F[m]=y;
            m++;
            double f=render->modeConf->fFcentr+((render->modeConf->Fdec*jj)/FFT_SAMPLES_NUM)-render->modeConf->Fdec;
            double dB=(y==0)?-170:20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=(y==0)?-170:10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????
            s.setX(f/1000000);
            s.setY(dBV+deviation/*gain*y*/);
#ifdef unused
            if(plot)
                plot->signalData(nCh).append(s);
#endif
        }
        for(int ii=0; ii<render->modeConf->Nband/2; ii++)
        {
            unsigned int y=data->at(ii*4)<<24;
            y|=(0xff&data->at(ii*4+1))<<16;
            y|=(0xff&data->at(ii*4+2))<<8;
            y|=0xff&data->at(ii*4+3);
            F[m]=y;
            m++;
            double f=render->modeConf->fFcentr+(render->modeConf->Fdec*ii)/FFT_SAMPLES_NUM;
            double dB=(y==0)?-170:20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
            double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
            double dBm=(y==0)?-170:10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????

            s.setX(f/1000000);
            s.setY(dBV+deviation/*gain*y*/);
            //qDebug()<<"FFT x="<<s.x()<< " y=" << s.y()<< ":"<< m<< endl;
#ifdef unused
            if(plot)
                plot->signalData(nCh).append(s);
#endif
        }
/////////////////
//        makeTestDataFFT(F);
//        m=4096;
///////////////
        a_left=1;
        a_right=m-1;
        for(int n=0; n<N; n++)
        {
            y_max=0;
            g=a_left;
            for(int j=a_left; j<=a_right; j++)
            {
                if(F[j]>y_max)
                {
                    y_max=F[j];
                    g=j;
                }
            }
            int g_left=g-dF/2;
            int g_right=g+dF/2;
            if(g_left<a_left)g_left=a_left;
            if(g_right>a_right)g_right=a_right;

            for(int x=g_left; x<=g_right; x++)
            {
                U[n]+=F[x];
                F[x]=0;
            }
            qDebug()<<n<<" ymax="<<y_max<<" g="<< g << " "<< g_left << " " << g_right;

            S0+=qPow(U[n],2);
            if(n>0)S1+=qPow(U[n],2);
        }

        qreal Kgi=qSqrt(S1)/U[0];
        qreal Kni=qSqrt(S1)/qSqrt(S0);
        qreal SINAD=20*(qLn(U[0]/qSqrt(S1)/*Kgi*/)/qLn(10));

        str1=QString::number(SINAD,'f', 3);
        str2=QString::number(100*Kgi,'f', 3);
        str3=QString::number(100*Kni,'f', 3);
        qDebug()<< "SINAD=" << SINAD <<" Kgi=" << Kgi << " Kni=" << Kni;
    }

    return true;
}

bool SpecProcess::calculatePHOSO(QByteArray *data, int nCh) {
    qDebug("fft psopho data ready ............");

    const double Ppsoph[32]={
    /*300*/     0.295,
    /*400*/     0.484,
    /*500*/     0.661,
    /*600*/     0.794,
    /*700*/     0.902,
    /*800*/     1.0,
    /*900*/     1.072,
    /*1000*/	1.22,
    /*1100*/	1.072,
    /*1200*/	1.0,
    /*1300*/	0.955,
    /*1400*/	0.905,
    /*1500*/	0.861,
    /*1600*/	0.824,
    /*1700*/	0.791,
    /*1800*/	0.76,
    /*1900*/	0.723,
    /*2000*/	0.708,
    /*2100*/	0.689,
    /*2200*/	0.67,
    /*2300*/	0.652,
    /*2400*/	0.634,
    /*2500*/	0.617,
    /*2600*/	0.598,
    /*2700*/	0.58,
    /*2800*/	0.562,
    /*2900*/	0.543,
    /*3000*/	0.525,
    /*3100*/	0.501,
    /*3200*/	0.473,
    /*3300*/	0.444,
    /*3400*/	0.412};

    unsigned int F[FFT_SAMPLES_NUM]={0};

    int N=32;
    double U[32]={0};
    double S=0.0;
    // calculate
    str1=str2=str3="";

#ifdef unused
    if(plot) {
        plot->signalData(nCh).clearValues();
    }
#endif
    double deviation;
    deviation = plot->markerYValue(nCh);
    double gain=render->getGainY(nCh);
    double gain_mks=render->getGainX();
    double gain_x=render->modeConf->fBand/render->modeConf->Nband;

    QPointF s;

    int m=0;
    for(int jj=FFT_SAMPLES_NUM-render->modeConf->Nband/2; jj<FFT_SAMPLES_NUM; jj++)
    {
        s.setX(m);
        unsigned int y=data->at(jj*4)<<24;
        y|=(0xff&data->at(jj*4+1))<<16;
        y|=(0xff&data->at(jj*4+2))<<8;
        y|=0xff&data->at(jj*4+3);

        F[m]=y;
        m++;

        double dB=20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
        double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
        double dBm=10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????

        s.setY(dBV+deviation);

#ifdef unused
        if(plot) {
            plot->signalData(nCh).append(s);
        }
#endif
    }
    for(int ii=0; ii<render->modeConf->Nband/2; ii++)
    {
        s.setX(m);
        unsigned int y=data->at(ii*4)<<24;
        y|=(0xff&data->at(ii*4+1))<<16;
        y|=(0xff&data->at(ii*4+2))<<8;
        y|=0xff&data->at(ii*4+3);

        F[m]=y;
        m++;

        double dB=20*qLn(/*g**/y/(qPow(2,28)-1))/qLn(10); // ???????
        double dBV=(y==0)?-170:20*qLn(/*g**/gain*y)/qLn(10); // ???????
        double dBm=10*qLn(1000*qPow(gain*y/**14/8*/,2)/50)/qLn(10); // ???????

        s.setY(dBV+deviation);

#ifdef unused
        if(plot) {
            plot->signalData(nCh).append(s);
        }
#endif
    }
    for(int n=0; n<N; n++)
    {
        int a=qRound(render->modeConf->Nfft*(300+100*n)/render->modeConf->Fdec);
        if(a>=0 && a<FFT_SAMPLES_NUM)
            render->modeConf->m_Fa[n]+=F[a];
        if(render->modeConf->nPass==16-1)
        {
            render->modeConf->m_Fa[n]=render->modeConf->m_Fa[n]/16;
            U[n]=gain*render->modeConf->m_Fa[n];          // ????? перевод в вольты
            S+=qPow(Ppsoph[n]*U[n],2);
        }
    }
    if(render->modeConf->nPass==16-1)
    {
        qreal Upsoph_800=qSqrt(S);
        str3=QString::number(Upsoph_800,'f', 3);
    }

    return render->modeConf->nPass==16-1? true: false;
}

// IQANALYZER PROCESS OBJECT
IqAnProcess::IqAnProcess (QObject *parent) :
    ProcessObject(parent) {
    setProcessName("IqAn");
}


bool IqAnProcess::processData(QByteArray *data, int nCh)
{
    bool bRes=false;
    switch (conf->subMeasMode) {
    case 0:
        bRes=calculateDeep(data,nCh);
        break;
    case 1:
        bRes=calculateDeviation(data,nCh);
        break;
    case 2:
        bRes=calcutateTLGData(data,nCh);
        break;
    case 3:
        bRes=calculateTest(data,nCh);
        break;
    case 4:
        bRes=calculateVector(data, nCh);
        break;
    }

    if (bRes)
        emit dataReady(str1,str2,str3);
    return bRes;
}

void IqAnProcess::processDataDDC(int &len) {
    render->modeConf->fft_window = render->fft_window;
    if (render->modeConf->nPass==0) {
        len=RAW_SAMPLES_NUM;
        switch (conf->subMeasMode) {
        case 0:
            render->m_worker->initDDC(false, render->modeConf);
            break;
        case 1:
            render->m_worker->initDDC(false, render->modeConf);
            break;
        case 2:
            render->m_worker->initDDC_TLG(render->modeConf);
            break;
        case 3:
        case 4:
            render->m_worker->initDDC(false, render->modeConf);
            break;
        }
    }
}

bool IqAnProcess::calculateVector(QByteArray *data, int nCh) {
    double gain=render->getGainY(nCh);
    double gain_mks=render->getGainX();
    double Amin=65767.0;
    double Amax=-65767.0;
    double ss=0.0;
    //plots->at(0)->signalData(nCh).clearValues();
    plots->last()->signalData(nCh).clearValues();
    double deviation;
    double deviation2;
    deviation = plots->at(0)->markerYValue(nCh);
    deviation2 = plots->at(1)->markerYValue(nCh);
    QPointF s;
    QPointF s2;
    for(int i=0; i<RAW_SAMPLES_NUM; i++)
    {
        s.setX(gain_mks*i);
        s2.setX(gain_mks*i);

        short im=data->at(i*4)<<8;
        im |= 0xff&data->at(i*4+1);
        short re=data->at(i*4+2)<<8;
        re |= 0xff&data->at(i*4+3);

        double A=qSqrt(qPow(re,2)+qPow(im,2));

        if(A<Amin)Amin=A;
        if(A>Amax)Amax=A;

        s.setY((gain*re)+deviation/*val*/);
        s2.setY((gain*im)+deviation2);

        s2.setX(gain*re);
        s2.setY(gain*im);

        plots->last()->signalData(nCh).append(s2);
        double val=gain*re;
        ss+=val*val;
    }
    double m=100*(Amax-Amin)/(Amax+Amin);
    qDebug() << "Глубина модуляции " << m << endl;
    double SKZ=qSqrt(ss/RAW_SAMPLES_NUM);
    double Am=gain*Amax;
    str1=QString::number((int)m);
    str2=QString::number(SKZ,'f', 2);
    str3=QString::number(Am,'f', 2);

    return true;
}

bool IqAnProcess::calculateTest(QByteArray *data, int nCh) {
    double gain=render->getGainY(nCh);
    double gain_mks=render->getGainX();
    double Amin=65767.0;
    double Amax=-65767.0;
    double ss=0.0;
    plots->at(0)->signalData(nCh).clearValues();
    plots->at(1)->signalData(nCh).clearValues();
    double deviation;
    double deviation2;
    deviation = plots->at(0)->markerYValue(nCh);
    deviation2 = plots->at(1)->markerYValue(nCh);
    QPointF s;
    QPointF s2;
    for(int i=0; i<RAW_SAMPLES_NUM; i++)
    {
        s.setX(gain_mks*i);
        s2.setX(gain_mks*i);

        short im=data->at(i*4)<<8;
        im |= 0xff&data->at(i*4+1);
        short re=data->at(i*4+2)<<8;
        re |= 0xff&data->at(i*4+3);

        double A=qSqrt(qPow(re,2)+qPow(im,2));

        if(A<Amin)Amin=A;
        if(A>Amax)Amax=A;

        s.setY((gain*re)+deviation/*val*/);
        s2.setY((gain*im)+deviation2);

        plots->at(0)->signalData(nCh).append(s);
        plots->at(1)->signalData(nCh).append(s2);
        double val=gain*re;
        ss+=val*val;
    }
    double m=100*(Amax-Amin)/(Amax+Amin);
    qDebug() << "Глубина модуляции " << m << endl;
    double SKZ=qSqrt(ss/RAW_SAMPLES_NUM);
    double Am=gain*Amax;
    str1=QString::number((int)m);
    str2=QString::number(SKZ,'f', 2);
    str3=QString::number(Am,'f', 2);

    return true;
}


bool IqAnProcess::calculateDeep(QByteArray *data, int nCh) {
    double gain=render->getGainY(nCh);
    double gain_mks=render->getGainX();
    double Amin=65767.0;
    double Amax=-65767.0;
    double ss=0.0;
    plot->signalData(nCh).clearValues();
    double deviation;
    deviation = plot->markerYValue(nCh);
    QPointF s;
    for(int i=0; i<RAW_SAMPLES_NUM; i++)
    {
        s.setX(gain_mks*i);
        short im=data->at(i*4)<<8;
        im |= 0xff&data->at(i*4+1);
        short re=data->at(i*4+2)<<8;
        re |= 0xff&data->at(i*4+3);
        double A=qSqrt(qPow(re,2)+qPow(im,2));
        if(A<Amin)Amin=A;
        if(A>Amax)Amax=A;
        s.setY((gain*re)+deviation/*val*/);
        if(plot)
        {
            plot->signalData(nCh).append(s);
        }
        double val=gain*re;
        ss+=val*val;
    }
    double m=100*(Amax-Amin)/(Amax+Amin);
    qDebug() << "Глубина модуляции " << m << endl;
    double SKZ=qSqrt(ss/RAW_SAMPLES_NUM);
    double Am=gain*Amax;
    str1=QString::number((int)m);
    str2=QString::number(SKZ,'f', 2);
    str3=QString::number(Am,'f', 2);

    return true;
}

bool IqAnProcess::calculateDeviation(QByteArray *data, int nCh) {
    double gain=render->getGainY(nCh);
    double gain_mks=render->getGainX();

    double Amin=65767.0;
    double Amax=-65767.0;

    short re_1[RAW_SAMPLES_NUM]={0};
    short im_1[RAW_SAMPLES_NUM]={0};

    short re_prev=0;
    short im_prev=0;
    double W[RAW_SAMPLES_NUM]={0.0};

    QPointF s2;

#ifdef unused
    if(plot) {
        plot->signalData(nCh).clearValues();
    }
#endif
    double deviation;
    deviation = plot->markerYValue(nCh);

    for(int i=0; i<RAW_SAMPLES_NUM; i++)
    {
        short im=data->at(i*4)<<8;
        im |= 0xff&data->at(i*4+1);
        short re=data->at(i*4+2)<<8;
        re |= 0xff&data->at(i*4+3);

        re_1[i]=(i==0?0:re-re_prev);
        im_1[i]=(i==0?0:im-im_prev);

        W[i]=(re==0&&im==0)?0:((re*im_1[i])-(im*re_1[i]))/(qPow(re,2)+qPow(im,2));
        if(W[i]<Amin)Amin=W[i];
        if(W[i]>Amax)Amax=W[i];

        re_prev=re;
        im_prev=im;
    }

    double K=render->modeConf->Fdec/(2.0*3.14159265358);

    double w=((Amax-Amin)*K)/2.0;
    qDebug() << "Девиация " << w << endl;

    str1=QString::number(w,'f', 2);
    str2="";
    str3="";
    /////////////////////////////////////

    for(int j=0; j<RAW_SAMPLES_NUM; j++)
    {
        s2.setX(gain_mks*j);


        double koef=render->modeConf->Fdec/(2.0*3.14159265358*w);
        s2.setY((koef*W[j])+deviation/*val2*/);
#ifdef unused
        if(plot) {
            plot->signalData(nCh).append(s2);
        }
#endif
    }

    return true;
}

bool IqAnProcess::calcutateTLGData(QByteArray *data, int nCh){
    double gain=render->getGainY(nCh);
    double gain_mks=render->getGainX();
    double Rmin=65767.0;
    double Rmax=-65767.0;
    double M=0.0; // avg
    double Mmin=0.0;
    double Mmax=0.0;

    short re_a[RAW_SAMPLES_NUM]={0};
    int   l[RAW_SAMPLES_NUM]={0};
    int Lc=0;

#ifdef unused
    if(plot) {
        //plots->first()->signalData(nCh).clearValues();
        plot->signalData(nCh).clearValues();
    }
#endif
    double deviation;
    deviation = plot->markerYValue(nCh);
    QPointF s;
    QPointF s1;
    //QPointF s3;
    for(int i=0; i<RAW_SAMPLES_NUM; i++)
    {
        s.setX(gain_mks*i);
        s1.setX(gain_mks*i);
        //s3.setX(gain_mks*i);
        short im=data->at(i*4)<<8;
        im |= 0xff&data->at(i*4+1);
        short re=data->at(i*4+2)<<8;
        re |= 0xff&data->at(i*4+3);
        re_a[i]=re;
        M+=re;
        double A=qSqrt(qPow(re,2)+qPow(im,2));
        s.setY((gain*re)+deviation/*val*/);
        //s3.setY(gain*im);
        s1.setY(gain*A/*re*//*val2*/);
#ifdef unused
        if(plot) {
            //plots->first()->signalData(nCh).append(s1);
            plot->signalData(nCh).append(s);
            //plot->signalData(nCh-1).append(s3);
            //plot->showCurve(0,true);
        }
#endif
    }
    M=M/RAW_SAMPLES_NUM;
    for(int i1=0; i1<RAW_SAMPLES_NUM; i1++)
    {
        re_a[i1]=re_a[i1]-M;
    }
    // определяем количество переходов через 0
    for(int i2=0; i2<RAW_SAMPLES_NUM-1; i2++)
    {
        if(re_a[i2]<=0 && re_a[i2+1]>0)
        {
            l[Lc]=i2;
            Lc++;
        }
    }
    for(int k=0; k<Lc; k++)
    {
        Rmin=65767.0;
        Rmax=-65767.0;

        for(int m=l[k]; m<l[k+1]; m++)
        {
            if(re_a[m]<Rmin)Rmin=re_a[m];
            if(re_a[m]>Rmax)Rmax=re_a[m];
        }
        Mmin+=Rmin;
        Mmax+=Rmax;
    }
    Mmin=Mmin/Lc;
    Mmax=Mmax/Lc;

    quint8 Mode=3;//0;//   // 0:0 1:-1 2:+1	3:x
    double Bound=0.0;

    if(Mode==0) {Bound=0.0;}
    else if(Mode==1) {Bound=Mmin/2;}
    else if(Mode==2) {Bound=Mmax/2;}
    else if(Mode==3)
    {
        if(Mmin>-Mmax/2) Bound=Mmax/2;
        if(Mmax<-Mmin/2) Bound=Mmin/2;
    }
    double T1=0.0;
    double T0=0.0;
    for(int m=l[0]; m<l[Lc-1]; m++)
    {
        if(re_a[m]<Bound)T0++;
        if(re_a[m]>=Bound)T1++;
    }
    double mk=qAbs(100*(T1-T0)/(T1+T0));
    qDebug() << "� ВП " << mk << endl;

    str1=QString::number(mk,'f', 2);
    str2="";
    str3="";
    return true;
}
