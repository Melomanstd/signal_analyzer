/****************************************************************************
**ggg
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef RENDERTHREAD_H
#define RENDERTHREAD_H

#include <QImage>
#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <QtCore>
#include <QApplication>
#include <QUdpSocket>
#include <QMessageBox>

#include "workerobject.h"
#include "measconf.h"
#include "processobject.h"
//#include "mode.h"


#define unused

#ifdef unused
#include "plot.h"
#endif
//#include <qwt_scale_engine.h>
//#include "signaldata.h"

#define RAW_SAMPLES_NUM 16384
#define FFT_SAMPLES_NUM 4096


//#define F1B 0
//#define G1B 1
//#define J3E 2


//class  ADcfDemoMainWindow;

class ProcessObject;
class WorkerObject;
class MeasureConfigItem;
//class Mode;

//! [RenderThread class definition]
class RenderThread : public QThread
{
    Q_OBJECT

public:
    RenderThread(ProcessObject *p,
                 WorkerObject* w,
                 QString strIp,
                 QString strOscIp,
                 QObject *parent = 0);
    RenderThread(WorkerObject* w,
                 QString strIp,
                 QString strOscIp,
                 QObject *parent = 0);
    ~RenderThread();

    void setMainThread(QThread *thr);
    void setProcess(ProcessObject *p);
    void suspendProcess();
    void resume();
    void stop();
    void initRender(MeasureConfigItem *conf);
    void setUdpPropetries(QString oscIp, QString ip, quint32 port);
    WorkerObject* getWorker();

//    void processDatagram(const char* datagram);

signals:
    void sendBlock(QString str1, QString str2, QString str3);
    void writeLog(QString str);
    void renderStarted();

public slots:
    void startProcess(quint8 nMeasMode,
                      quint8 nCh,
                      quint8 nChType,
                      quint8 nChAtten,
                      quint8 nTaktADC,
                      quint8 nGenMode,
                      double fFc,
                      quint32 nAmax,
                      quint32 nAmin,
                      double fFdev,
                      double fFsym,
                      double fFmod,
                      quint8 nSSBType,
                      qreal fK,
                      double fFs,
                      double fFcentr,
                      double fBand,
                      int nFftWindow,
#ifdef unused
                      Plot *pPlot,
#endif
                      bool bCycle=false);
    //COPY OF STARTPROCESSING
    void startProcess2(
            MeasureConfigItem *conf);
    //||||||||||||||||
    void stopProcess();
    void setScale(int ch, int sc);
    void setTimeScale(int val);
    void processDatagram(const QByteArray& datagram);


private slots:
    void initDevice();
    void initChannel(int nCh, int nChType, int nChAtten);
    void initTakt();
    void initDDC(bool bFft);
    void enableGEN(bool bEnable);
    void initGEN(quint8 nMode);
    void initGEN();
    void initFFT_Freq(int nPass);
    void initFFT_KNI();
    void initDDC_TLG();
    void initFFT_PSOPHO();
    void initFFT_AFC();
    void initPLL();
    void startDecoding(int nCh);
    void readData();
    bool processData(int nCh); //INT
    bool processDdcData(int nCh);  //INT
    bool processDdcData2(int nCh);     //INT
    bool processRawData(int nCh);  //INT
    bool processFftData(int nCh);  //INT
    bool processFftDataFreq(int nCh);  //INT
    bool processFftDataKNI(int nCh);  //INT
    bool processDdcDataTLG(int nCh);  //INT
    bool processFftDataPSOPHO(int nCh);  //INT
    bool processFftDataSens(int nCh);  //INT
    bool processFftDataAFC(int nCh);

    //void processDatagram(const QByteArray& datagram);
    void processOscDatagram(QByteArray datagram);
    void processDdcDatagram(QByteArray datagram);
    void processFftDatagram(QByteArray datagram);
    void processRawDatagram(QByteArray datagram);

    void calculateDeviation(int nCh);  //INT
    void calculateDeep(int nCh);  //INT
    void calculatePower(int nCh); //INT

    void WriteLog(quint16 LDATA, quint8 CMD, quint8 BADDR, const QList<QPoint> &WDATA);
    void WriteLog(QByteArray datagram);

    void makeTestDataFFT(unsigned int *F);

public slots:
    void WriteReg(quint8 BADDR, const QList<QPoint> &WDATA);

    double getGainY(int ch);
    double getGainX();

    void resumeProcess();

    QByteArray *getData();
    QByteArray *getData2();
    QByteArray *getCurrentData();

protected:
    void run();

private:
    ProcessObject *process;
    QString m_strIp;
    QString m_strOscIp;

    QByteArray m_data;
    QByteArray m_data2;
    QByteArray *currentData;
    int m_nFftGain;
    QThread *mainThread;

#ifdef unused
    Plot *d_plot;
#endif
public:
    int m_nReadPtr;
    int *fft_window;
    quint8 m_nMeasMode;
    bool suspended;

    MeasureConfigItem *modeConf;
    WorkerObject* m_worker;


private:
    //bool m_bCycle;
    bool m_abort;
    bool m_bDelWorker;
    bool stopThreadF;
    bool suspF;
    bool exitThreadF;

    QWaitCondition m_codition;
    QWaitCondition m_codition_run;
    QMutex mutex;

//    QString m_strIp;
//    QString m_strOscIp;
    quint32 port;

    //ADcfDemoMainWindow *m_mw;

    QString str1, str2, str3;
    QUdpSocket *udp_client;

};
//! [RenderThread class definition]

#endif
