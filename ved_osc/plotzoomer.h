#ifndef PLOTZOOMER_H
#define PLOTZOOMER_H
#include <qwt_plot_zoomer.h>
#include <qwt_plot.h>
#include <qwt_scale_div.h>
#include "plot.h"

class Plot;

class PlotZoomer : public QwtPlotZoomer
{
    Q_OBJECT
public:
    explicit PlotZoomer(QwtPlotCanvas *canvas, bool doReplot = true);
    QPointF getZoomRectBase();
    PlotZoomer *getLinkedZoomer();

protected:
    void rescale();
    virtual void begin();
    virtual void widgetMousePressEvent( QMouseEvent * );
    virtual void move( const QPoint & );
    virtual void widgetMouseReleaseEvent( QMouseEvent * );

    virtual bool accept( QPolygon & ) const;

    QPointF rectBasePoint;
    bool readyToZoom; //if we on markers position = false and ignoring zoomers methods
    PlotZoomer *linkedZoomer;


signals:
    void zoomedOut();
    void zoomRectMoving(QPointF pos);
    
public Q_SLOTS:
    virtual void zoom( const QRectF & );
    virtual void zoom( int up );

public slots:
    void secondaryAppend(QPointF p);
    void secondaryActivated(bool s);
    void attachZoomer(PlotZoomer *z);
    void secondaryMove(QPointF p);
    void secondarySelected(QRectF p);
    void zoomerZoomedOut();

public:
    bool b;
    bool plotZoomed;
    bool primaryZoomer;
    //bool alreadyZoomed;
    
};

#endif // PLOTZOOMER_H
