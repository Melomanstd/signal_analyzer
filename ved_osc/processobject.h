#ifndef PROCESSOBJECT_H
#define PROCESSOBJECT_H

#define DATASIZE 20

#include <QObject>
#include "renderthread.h"
#include "measconf.h"
#include "plot.h"

class RenderThread;
class MeasureConfigItem;

class ProcessObject : public QObject
{
    Q_OBJECT
public:
    explicit ProcessObject(QObject *parent = 0);
    ~ProcessObject();
    virtual bool processData(QByteArray *data, int nCh) = 0;
    virtual void processDataDDC(int &len)=0;
    virtual void processInit(Plot *p,
                             RenderThread *r,
                             MeasureConfigItem *c);
    void setConfig(MeasureConfigItem *c);
    MeasureConfigItem* getConfig();
    void setProcessName(QString name);
    void setRender(RenderThread *render);
    void SetPlots(QList<Plot*> *p);
    QString getProcessName();
    int getID();
    void setID(int id);
    void GetSignalParams(QList<double> &ptArray, int nSize);
    void GetSignalParams2(QList<double> &basePtArray, int nSize);
    void GetSignalParams3(QList<double> &basePtArray, int nSize, double timebaseSec,
                          double* fA,
                          double* fSKZ,
                          double* fSKZ2,
                          double* fF,
                          double* fT,
                          double* fTi,
                          double* fTi_1,
                          double* fTi_2,
                          double* fAi);

    void GetSignalParamsInt(int *ptArray, int nSize);
    void GetSignalParams2Int(int *basePtArray, int nSize);
    void GetSignalParams3Int(int *basePtArray, int nSize, double timebaseSec,
                          double* fA,
                          double* fSKZ,
                          double* fSKZ2,
                          double* fF,
                          double* fT,
                          double* fTi,
                          double* fTi_1,
                          double* fTi_2,
                          double* fAi);

    long Sred(qint64 *buf, long n, int toch, double crossval, bool bRE);
    long Sred(int *buf, long n, int toch, double crossval, bool bRE);
    void initRenderVar(int x);

    double delta(double max, double min);
    double averageValue(double sum, double count);
    double RMS(double sum, double count);
    double statistic(double* data, QPair<double *, int> &prop, double &val);
    void clearStatistic();

    void peaks(int *ptArray1 , int nSize, int nCh, bool max);
    virtual void attachPeakMarkers();
    void updatePeaksMarkers(QPointF *list, int nCh, int pCount);
    void updatePeaksMarkers(int *xArr, int *yArr,
                            int nCh, int pCount,
                            double yGain, double xGain);


    void setPeaksVisible(bool s);
signals:
    void dataReady(QString str1, QString str2, QString str3);
    
public slots:

public:
    //QList<QPointF> peaksBuffer;
    //QPointF peaksBuffer[100];
    int xPeakBuff_0[100];
    int yPeakBuff_0[100];

    int xPeakBuff_1[100];
    int yPeakBuff_1[100];

    int *readXPtr;
    int *readYPtr;
    int *writeXPtr;
    int *writeYPtr;

    bool bWait;

    double lastGainY;
    double lastGainX;
    int peaksCount;

    MeasureConfigItem *conf;
    Plot* plot;
    QList<Plot*> *plots;
    RenderThread *render;
    QByteArray data;
    QString processName;
    enum measID {
        DEEP,
        DEV,
        OSCI,
        SPEC,
        FREQ,
        KNI,
        TKG,
        PHOSO
    };

    QMutex mutex;
protected:
    bool showPeaks;
    //int *xData
    int *yData;
    int measModeID;
    QString str1;
    QString str2;
    QString str3;
    //QList<PeakMarker*> markers;
    PeakMarker *peaksList;

    //QPair<double *, int> statisticsProp;
        //double* - current element pointer in statisticData array,
        //int - added elements counter(DATASIZE if array is full)
    //double *statisticPtr;
    double *statisticData;
    double posPeak, negPeak, AVG, SKZ;
    double *posPeakData, *negPeakData, *AVGData, *SKZData;
    QPair<double *, int> posPeakProp;
    QPair<double *, int> negPeakProp;
    QPair<double *, int> AVGProp;
    QPair<double *, int> SKZProp;
};


class OscProcess: public ProcessObject
{
    Q_OBJECT
public:
    OscProcess(QObject *parent = 0);
    bool processData(QByteArray *data, int nCh);
    bool calculatePower(QByteArray *data, int nCh);
    void processDataDDC(int &len);
};


class SpecProcess: public ProcessObject
{
    Q_OBJECT
public:
    SpecProcess(QObject *parent = 0);
    bool processData(QByteArray *data, int nCh);
    bool calculateSpectre(QByteArray *data, int nCh);
    bool calculateFrequency(QByteArray *data, int nCh);
    bool calculateKNI(QByteArray *data, int nCh);
    bool calculatePHOSO(QByteArray *data, int nCh);
    void processDataDDC(int &len);
};


class IqAnProcess: public ProcessObject
{
    Q_OBJECT
public:
    IqAnProcess(QObject *parent = 0);
    bool processData(QByteArray *data, int nCh);
    bool calculateDeep(QByteArray *data, int nCh);
    bool calculateDeviation(QByteArray *data ,int nCh);
    bool calcutateTLGData(QByteArray *data ,int nCh);
    bool calculateTest(QByteArray *data ,int nCh);
    bool calculateVector(QByteArray *data ,int nCh);
    void processDataDDC(int &len);
};

#endif // PROCESSOBJECT_H
