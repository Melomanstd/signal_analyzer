#ifndef PLOTCURVE_H
#define PLOTCURVE_H
#include <qwt_plot_curve.h>
#include <qwt_legend_item.h>
#include <qwt_plot.h>
#include <qwt_symbol.h>
#include <QLabel>
#include <qwt_text_label.h>
#include <QPainter>
#include <QHBoxLayout>

class PlotCurve : public QwtPlotCurve
{
public:
    PlotCurve(const QString &title = QString::null);
    ~PlotCurve();
    virtual void updateLegend( QwtLegend * ) const;
    virtual QWidget *legendItem() const;
    void setLegendText(QString text, QwtLegend *legend);
    void setLinkedLbls(int n);
    void createLbls(int n);
    void setLabelText(int n, QString text);

protected:
    virtual void drawCurve( QPainter *p, int style,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &canvasRect, int from, int to ) const;

    QList<QwtTextLabel*> labels;
    QHBoxLayout *l;
    int linkedLbls;
};

#endif // PLOTCURVE_H
