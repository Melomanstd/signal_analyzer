#include "mode.h"
#include <QAction>


unsigned Mode::modeCounter=0;
unsigned OscMode::oscCounter=0;
unsigned SpecMode::SpecCounter=0;
unsigned IqAnMode::iqAnCounter=0;

Mode::Mode(QObject *parent) : QObject(parent)
{
    //this->OSCI_ID=0;
    //this->SPEC_CD=1;
    //this->IQANALYZER=2;
    plot=0; render=0;
    initDone = false;
    //osci=0; spectre=0;
    tabWidget=0; //iqAn=0;
    peaksWidget = 0;
    used=false; mainTabPos=0;
    renderConf = new MeasureConfigItem;
    process = 0; measMode=-1;
    deltaTab = 0; peaksTab = 0;
    modes[OSCI_ID] = tr("Oscillograph");
    modes[SPEC_ID] = tr("Spectre");
    modes[IQAN_ID] = tr("IqAnalyzer");
    //generatorState = false;
    //showGen = new QAction("Show gen",0);
    //showGen->setCheckable(true);
    //peaksTimer = new QTimer(this);
    //peaksTimer->start(100);
    //connect(peaksTimer, SIGNAL(timeout()), this, SLOT(updatePeaksTab()));
}

Mode::~Mode(){
    Mode::modeCounter--;
    delete renderConf;
    //delete plot;
    //delete render;
    delete process;
    //delete deltaTab;
    //delete tabWidget;
}

baseTabWidget* Mode::createWidget() {
    return 0;
}

bool Mode::isUsed() {
    return used;
}

void Mode::setUsed(bool b) {
    used = b;
}

void Mode::plotInit(int legLblsCounter, QWidget *parent) {
    qDebug()<<"plot";
    plot = new Plot(legLblsCounter,parent);
    qDebug()<<"plot49";
    plot->setInterval(0.0,82.0);
    qDebug()<<"plot51";
    plot->setMinimumHeight(100);
    qDebug()<<"plot53";
    plot->setMinimumWidth(100);
    qDebug()<<"plot55";
    plots.append(plot);
    connect(plot, SIGNAL(chMarkerMoved(int)), this, SLOT(curveUpdate(int)));
    qDebug()<<"plot57";
    //plot->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::Expanding);
}

void Mode::renderInit(WorkerObject *w,
                      QString strIp,
                      QString strOscIp,
                      QObject *parent) {
    render=new RenderThread(process,
                            w,
                            strIp,
                            strOscIp,
                            parent);
    connect(render, SIGNAL(sendBlock(QString,QString,QString))
            ,this, SLOT(readyData(QString,QString,QString)));

}

void Mode::renderInit(RenderThread *r)
{
    render = r;
}

void Mode::renderStart() {
    if (!render) return;
    //if (render->isRunning())
    emit renderStarted(this);
}

void Mode::renderSuspend() {
    if (!render) return;
    if (render->isRunning()) {
        render->stopProcess();
        render->wait(5000);
    //QMessageBox::information(0,this->getName(),"Clossed",QMessageBox::Ok);
    }
}

Mode* Mode::createMode(int mode)
{
    switch(mode) {
    case OSCI_ID:
        return new OscMode();
    case SPEC_ID:
        return new SpecMode();
    case IQAN_ID:
        return new IqAnMode();
    }
    return 0;
}

Plot *Mode::getPlot() {
    return plots.last();
}

RenderThread* Mode::getRender() {
    return render;
}

baseTabWidget* Mode::getWidget() {
    //if (osci) return osci;
    //if (spectre) return spectre;
    //if (iqAn) return iqAn;
    return tabWidget;
}

int Mode::currentMode() {
    return currMode;
}

unsigned Mode::modeCnt() {
    return 0;
}

QString Mode::getName() {
    return modeName;
}

void Mode::setTabName(QString name) {
    tabName=name;
    QByteArray tName = name.toAscii();
    //char * t= tName.data();
    //renderConf->tabName=tName.data();
}

QString Mode::getTabName() {
    return tabName;
}

void Mode::readyData(QString str1, QString str2, QString str3){
    emit sendData(str1,str2,str3);
}

int Mode::tabPos() {
    return mainTabPos;
}

void Mode::setTabPos(int x) {
    mainTabPos=x;
}

MeasureConfigItem* Mode::getRenderConfig() {
    return renderConf;
}

ProcessObject* Mode::getProcess(){
    return process;
}

void Mode::processInit(int i) {
    switch(i) {
    case Mode::OSCI_ID:
        process = new OscProcess;
        break;
    case Mode::SPEC_ID:
        process = new SpecProcess;
        break;
    case Mode::IQAN_ID:
        process = new IqAnProcess;
        break;
    }
    process->setRender(render);
    process->setConfig(renderConf);
    connect(process, SIGNAL(dataReady(QString,QString,QString)),
            this, SLOT(displayData(QString,QString,QString)));


}

void Mode::measModeChanged() {}
int Mode::getMeasMode() {
    return measMode;
}

void Mode::bntDown(int i) {
    //QList<QPushButton*> *l=getWidget()->btnsList();
    QList<QAction*> *l = tabWidget->getActions();
    for (int x=0; x<l->size();x++) {
        l->at(x)->setChecked(x==i);
    }
}

void Mode::displayData(QString str1, QString str2, QString str3){
    static int ma=1;
    if(ma==1)ma=2;
    else ma=1;
    qDebug("data ready ............ %d ", ma);
    for (int i=0;i<plots.size();i++)
        plots.at(i)->replot();
    //getPlot()->replot();
    updatePeaksTab();
    displayMode(str1,str2,str3);
    if (render)
        render->resumeProcess();
    //renderresume...
    //QLabel *l =(QLabel*)getWidget()->footer();
    //l->setText(str1+"|"+str2+"|"+str3+"|");
}

QList<Plot*>* Mode::getPlotList() {
    return &plots;
}

void Mode ::displayMode(QString str1, QString str2, QString str3)
{}

void Mode::setTabIndex(int i) {
    tabIndex = i;
    renderConf->tabIndex = i;
}

int Mode::getTabIndex() {
    return tabIndex;
}

void Mode::setSignalState(int i, bool s) {
    for (int j=0; j<plots.size(); j++) {
        plots.at(j)->showCurve(i,s);
        //plots.at(j)->signalData(i).clearValues();
    }
}

void Mode::setChanelColor(int nCh, QColor c) {
    for (int i = 0; i<plots.size(); i++)
        plots.at(i)->setColor(nCh, c, render->isRunning());
}

void Mode::setLegElementNum() {}

void Mode::setLegText(int pos, int nCh, QString text) {
    for (int i=0;i<plots.size();i++) {
        plots.at(i)->setCurveText(text,nCh,pos);
    }
}

void Mode::showPlotMarkers(int nCh, bool s) {
    for (int i=0;i<plots.size();i++) {
        plots.at(i)->showMarkers(nCh,s);
    }
}

void Mode::setZoomerState(bool b) {
    for (int i = 0;i<plots.size(); i++) {
        plots.at(i)->setZoomerState(b);
    }
}

void Mode::markerColorChanged(int nMrk, QColor c) {
    for (int i = 0; i<plots.size();i++) {
        plots.at(i)->setMarkersColor(nMrk,c);
    }
}

QTreeWidget* Mode::getDeltaTab() {
    return deltaTab;
}

void Mode::deltaTabInit() {
    QStringList l;
    l.append("");
    l.append("X");
    l.append("Y1");
    l.append("Y2");
    l.append("dX");
    l.append("dY1");
    l.append("dY2");

    deltaTab = new QTreeWidget();
    deltaTab->setMaximumHeight(80);
    deltaTab->setRootIsDecorated(false);
    deltaTab->setVisible(false);
    deltaTab->setDragEnabled(false);
    deltaTab->setColumnCount(7);
    deltaTab->setHeaderLabels(l);
    QList<QTreeWidgetItem *> items;
    items.append(new QTreeWidgetItem(QStringList(QString(tr("Cursor: %1")).arg(1))));
    items.append(new QTreeWidgetItem(QStringList(QString(tr("Cursor: %1")).arg(2))));
    for (int j=0;j<2;j++)
    for (int i=1;i<7;i++)
        items.at(j)->setText(i,"0");

    deltaTab->insertTopLevelItems(0,items);
    getPlot()->setDeltaTab(deltaTab);
    tabWidget->getOutputLay()->addWidget(deltaTab);
}

void Mode::setMarkersFreeze(bool s) {
    for (int i=0;i<plots.size();i++)
        plots.at(i)->setMarkersFreeze(s);
}

void Mode::setMarkersAsDefault() {
    for (int i=0;i<plots.size();i++)
        plots.at(i)->setDefaultMarkersPos();
}

void Mode::curveUpdate(int nCh) {
    if (nCh == 0 && getRenderConfig()->Ch1 == false ||
        nCh == 1 && getRenderConfig()->Ch2 == false)
        return;
    QByteArray *data;
    if (nCh == 0)
        data = getRender()->getData();
    else data = getRender()->getData2();
    getProcess()->processData(data, nCh);
    if (!getRender()->isRunning())
        for (int i = 0;i<plots.size();i++)
           plots.at(i)->replot();
}

void Mode::setZoomerEnable(bool s) {
    for (int i=0; i<plots.size();i++) {
        plots.at(i)->setZoomerEnable(s);
    }
}

void Mode::peakTabInit() {
    /*QStringList l;
    for (int i=0;i<100; i++)
        l.append(QString::number(i));

    peaksTab = new QTreeWidget();
    peaksTab->setMaximumHeight(80);
    peaksTab->setRootIsDecorated(false);
    peaksTab->setVisible(false);
    peaksTab->setDragEnabled(false);
    peaksTab->setColumnCount(100);
    peaksTab->setHeaderLabels(l);
    QList<QTreeWidgetItem *> items;
    items.append(new QTreeWidgetItem(QStringList(QString("X"))));
    items.append(new QTreeWidgetItem(QStringList(QString("Y"))));
    for (int j=0;j<2;j++)
    for (int i=1;i<100;i++)
        items.at(j)->setText(i,"NULL");

    peaksTab->insertTopLevelItems(0,items);*/
    peaksWidget = new PeaksMarkersWidget;
    peaksWidget->headersInit(100);
    tabWidget->getOutputLay()->addWidget(peaksWidget);
}

QWidget* Mode::getPeaksTab() {
    return peaksWidget;
}

void Mode::updatePeaksTab() {
    //return;
    if (!process || !peaksWidget)
        return;
    if (!process->mutex.tryLock()) return;
    if (process->writeXPtr == process->xPeakBuff_0)
    {
        process->writeXPtr = process->xPeakBuff_1;
        process->writeYPtr = process->yPeakBuff_1;
        process->readXPtr = process->xPeakBuff_0;
        process->readYPtr = process->yPeakBuff_0;
    } else {
        process->writeXPtr = process->xPeakBuff_0;
        process->writeYPtr = process->yPeakBuff_0;
        process->readXPtr = process->xPeakBuff_1;
        process->readYPtr = process->yPeakBuff_1;
    }

    int pCount = process->peaksCount;
    double gainX=process->lastGainX;
    double gainY=process->lastGainY;
    //QPointF temp[100];
    //int xArr[pCount];
    //int yArr[pCount];
    //for (int i =0;i<100;i++)
        //temp[i] = process->peaksBuffer[i];
    //temp = getProcess()->peaksBuffer;
    process->mutex.unlock();
    peaksWidget->headersInit(pCount);
    //peaksWidget->setValues(temp,pCount);
    peaksWidget->setValues(process->readXPtr,
                           process->readYPtr,
                           pCount, gainY, gainX);
}

void Mode::showPeaks(bool s)
{
    if (!peaksWidget || !process)
        return;
    if (s)
    {
        peaksWidget->show();
        process->setPeaksVisible(true);
    }
    else
    {
        peaksWidget->hide();
        process->setPeaksVisible(false);
    }
    //peaksWidget->setVisible(s);
    //process->setPeaksVisible(s);
}

void Mode::clearPlots(int c)
{   //if c=0 - clear all signaldata
    for (int i=0; i<plots.size();i++)
    {
        if (c==1)
            plots.at(i)->signalData(0).clearValues();
        else if (c==2)
            plots.at(i)->signalData(1).clearValues();
        else {
            plots.at(i)->signalData(0).clearValues();
            plots.at(i)->signalData(1).clearValues();
        }
    }
}

void Mode::renderConfInit(bool Ch1,
                          bool Ch2,
                          quint8 nMeasMode,
                          quint8 nChType,
                          quint8 nChType2,
                          quint8 nChAtten,
                          quint8 m_nChAtten2,
                          quint8 nTaktADC,
                          quint8 nGenMode,
                          double fFc,
                          quint32 nAmax,
                          quint32 nAmin,
                          double fFdev,
                          double fFsym,
                          double fFmod,
                          quint8 nSSBType,
                          qreal fK,
                          double fFs,
                          double fFcentr,
                          double fBand,
                          int nFftWindow,
                          quint8 sC1,
                          quint8 sC2,
                          quint8 tSc,
                          Plot *pPlot,
                          bool bCycle)
{
    renderConf->Ch1 = Ch1;
    renderConf->Ch2 = Ch2;
    renderConf->nMeasMode = nMeasMode;
    renderConf->nChType = nChType;
    renderConf->nChType2 = nChType2;
    renderConf->nChAtten = nChAtten;
    renderConf->nChAtten2 = m_nChAtten2;
    renderConf->nTaktADC = nTaktADC;
    renderConf->nGenMode = nGenMode;
    renderConf->fFc =fFc;
    renderConf->nAmax = nAmax;
    renderConf->nAmin = nAmin;
    renderConf->fFdev = fFdev;
    renderConf->fFsym = fFsym;
    renderConf->fFmod = fFmod;
    renderConf->nSSBType = nSSBType;
    renderConf->fK =fK;
    renderConf->fFs = fFs;
    renderConf->fFcentr = fFcentr;
    renderConf->fBand = fBand;
    renderConf->nFftWindow = nFftWindow;
    renderConf->sC1 =sC1;
    renderConf->sC2 = sC2;
    renderConf->tSc = tSc;
    renderConf->bCycle = bCycle;
//    renderConf->dataInit(
//                Ch1,    Ch2,
//                nMeasMode,
//                nChType,    nChType2,
//                nChAtten,   m_nChAtten2,
//                nTaktADC,   nGenMode,
//                fFc,    nAmax,
//                nAmin,  fFdev,
//                fFsym,  fFmod,
//                nSSBType,   fK,
//                fFs,    fFcentr,
//                fBand,  nFftWindow,
//                sC1,    sC2,
//                tSc,    /*pPlot,*/
//                bCycle);
}


//------------------
OscMode::OscMode(QObject *parent):
    Mode(parent)
{
    /*osci = new OsciWidget();
    currMode = OSCI_ID;
    //osci->setPlot(plot);
    modeName = osci->getName();
    createWidget();*/
    tabWidget = createWidget();
    OscMode::oscCounter++;
    //getPlot()->setTextCount(4);
    //osci->createSideBar(btns);
    //if (header != "") osci->createHeader(header);
    //if (footer != "") osci->createFooter(footer);
}

OscMode::~OscMode() {
    OscMode::oscCounter--;
}

unsigned OscMode::modeCnt() {
    return OscMode::oscCounter;
}

baseTabWidget* OscMode::createWidget() {
    QStringList text;
    QStringList names;

    OsciWidget *t=new OsciWidget();
    //osci = new OsciWidget();
    currMode = OSCI_ID;
    //osci->setPlot(plot);
    modeName = tr("Oscillograph");//t->getName();

    text.append(tr("Oscillograph"));
    names.append("OsciBtn");
    //t->createSideBar(text,names);
    t->createToolBarActions(text);
    t->createHeader(tr("Oscillograph"));
    t->createFooter("");
    for(int i=0;i<t->getActions()->size();i++)
    {
        QAction *act = t->getActions()->at(i);
        connect(act, SIGNAL(triggered()), this, SLOT(measModeChanged()));
    }
    /*for (int i=0;i<t->btnsList()->size();i++)
        connect(t->btnsList()->at(i),SIGNAL(clicked()),
                this,SLOT(measModeChanged()));*/
    return t;
}

void OscMode::measModeChanged() {
    QAction *act = dynamic_cast<QAction*>(QObject::sender());
    if (!act) return;
    clearPlots();
    switch(tabWidget->getActions()->indexOf(act))
    {
    case 0:
        getRenderConfig()->subMeasMode=0;
        process->setID(2);
        break;
    }
    this->bntDown(getRenderConfig()->subMeasMode);
    /*QPushButton* b=(QPushButton*) QObject::sender();
    if (!b) return;
    //QMessageBox::information(0,"",b->objectName(),QMessageBox::Ok);
    if (b->objectName() == "OsciBtn") {
        getRenderConfig()->subMeasMode=0;
        process->setID(2);
    }
    this->bntDown(getRenderConfig()->subMeasMode);*/
}

void OscMode::displayMode(QString str1, QString str2, QString str3){
    QLabel *l =(QLabel*)getWidget()->footer();
    QString text;
    text="/ "+str1+" | / "+str2+" | / "+str3+" |";
    l->setText(text);
}

void OscMode::setLegElementNum() {
    for (int i=0;i<plots.size(); i++)
        plots.at(i)->setTextCount(4);
}

//----------------------
SpecMode::SpecMode(QObject *parent) :
    Mode(parent)
{
    /*spectre = new SpectreWidget();
    currMode = SPEC_ID;
    //spectre->setPlot(plot);
    modeName = spectre->getName();*/
    tabWidget = createWidget();
    SpecMode::SpecCounter++;
    //getPlot()->setTextCount(2);
    //spectre->createSideBar(btns);
    //if (header != "") spectre->createHeader(header);
    //if (footer != "") spectre->createFooter(footer);
}

SpecMode::~SpecMode() {
    SpecMode::SpecCounter--;
}

unsigned SpecMode::modeCnt() {
    return SpecMode::SpecCounter;
}

baseTabWidget* SpecMode::createWidget() {
    QStringList text;
    QStringList names;

    SpectreWidget *t = new SpectreWidget();
    currMode = SPEC_ID;
    //spectre->setPlot(plot);
    modeName = tr("Spectre");//t->getName();

    text.append(tr("Spectre"));
    text.append(tr("Frequency"));
    text.append(tr("KNI"));
    text.append(tr("PHOSO"));
    names.append("SpecBtn");
    names.append("FreqBtn");
    names.append("KNIBtn");
    names.append("PHOSOBtn");
    //t->createSideBar(text,names);
    t->createToolBarActions(text);
    t->createHeader(tr("SPECTRE"));
    t->createFooter("");
    for(int i=0;i<t->getActions()->size();i++)
    {
        QAction *act = t->getActions()->at(i);
        connect(act, SIGNAL(triggered()), this, SLOT(measModeChanged()));
    }
    /*for (int i=0;i<t->btnsList()->size();i++)
        connect(t->btnsList()->at(i),SIGNAL(clicked()),
                this,SLOT(measModeChanged()));*/
    return t;
}

void SpecMode::measModeChanged() {
    QAction *act = dynamic_cast<QAction*>(QObject::sender());
    if (!act) return;
    clearPlots();
    switch(tabWidget->getActions()->indexOf(act))
    {
    case 0:
        getRenderConfig()->subMeasMode=0;
        process->setID(3);
        break;
    case 1:
        getRenderConfig()->subMeasMode=1;
        process->setID(4);
        break;
    case 2:
        getRenderConfig()->subMeasMode=2;
        process->setID(5);
        break;
    case 3:
        getRenderConfig()->subMeasMode=3;
        process->setID(7);
        break;
    }
    this->bntDown(getRenderConfig()->subMeasMode);
    /*QPushButton* b=(QPushButton*) QObject::sender();
    if (!b) return;
    //QMessageBox::information(0,"",b->objectName(),QMessageBox::Ok);
    if (b->objectName() == "SpecBtn") {
        getRenderConfig()->subMeasMode=0;
        process->setID(3);
    }
    if (b->objectName() == "FreqBtn") {
        getRenderConfig()->subMeasMode=1;
        process->setID(4);
    }
    if (b->objectName() == "KNIBtn") {
        getRenderConfig()->subMeasMode=2;
        process->setID(5);
    }
    if (b->objectName() == "PHOSOBtn") {
        getRenderConfig()->subMeasMode=3;
        process->setID(7);
    }
    this->bntDown(getRenderConfig()->subMeasMode);*/
}

void SpecMode::displayMode(QString str1, QString str2, QString str3){
    QLabel *l =(QLabel*)getWidget()->footer();
    QString text;
    switch (renderConf->subMeasMode) {
    case 0:
        text="FFT "+str1+" | FFT "+str2+" | FFT "+str3+" |";
        break;
    case 1:
        text="FRQ "+str1+" | FRQ "+str2+" | FRQ "+str3+" |";
        break;
    case 2:
        text="KNI "+str1+" | KNI "+str2+" | KNI "+str3+" |";
        break;
    case 3:
        text="PHS "+str1+" | PHS "+str2+" | PHS "+str3+" |";
        break;
    }

    l->setText(text);
}

void SpecMode::setLegElementNum() {
    for (int i=0;i<plots.size(); i++)
        plots.at(i)->setTextCount(2);
}

//------------------
IqAnMode::IqAnMode(QObject *parent) :
    Mode(parent)
{
    /*iqAn = new IqAnWidget();
    currMode = IQAN_ID;
    //iqAn->setPlot(plot);
    modeName = iqAn->getName();*/
    tabWidget = createWidget();
    IqAnMode::iqAnCounter++;
    //getPlot()->setTextCount(2);
    //iqAn->createSideBar(btns);
    //if (header != "") iqAn->createHeader(header);
    //if (footer != "") iqAn->createFooter(footer);
}

IqAnMode::~IqAnMode(){
    IqAnMode::iqAnCounter--;
}

unsigned IqAnMode::modeCnt() {
    return IqAnMode::iqAnCounter;
}

baseTabWidget* IqAnMode::createWidget() {
    QStringList text;
    QStringList name;

    IqAnWidget *t = new IqAnWidget();
    currMode = IQAN_ID;
    //iqAn->setPlot(plot);
    modeName = tr("IQ Analayzer");//t->getName();

    text.append(tr("Deep"));
    text.append(tr("Deviaton"));
    text.append(tr("Deformation TLG"));
    text.append(tr("IQANTEST"));
    text.append(tr("IQ-Vector"));
    name.append("DeepBtn");
    name.append("DevBtn");
    name.append("TLGBtn");
    name.append("TESTBTN");
    name.append("vectorBtn");
    //t->createSideBar(text,name);
    t->createToolBarActions(text);
    t->createHeader(tr("IQANALYZER"));
    t->createFooter("");
    for(int i=0;i<t->getActions()->size();i++)
    {
        QAction *act = t->getActions()->at(i);
        connect(act, SIGNAL(triggered()), this, SLOT(measModeChanged()));
    }
    /*for (int i=0;i<t->btnsList()->size();i++)
        connect(t->btnsList()->at(i),SIGNAL(clicked()),
                this,SLOT(measModeChanged()));*/
    return t;
}

void IqAnMode::measModeChanged() {
    QAction *act = dynamic_cast<QAction*>(QObject::sender());
    if (!act) return;
    clearPlots();
    switch(tabWidget->getActions()->indexOf(act))
    {
    case 0:
        getRenderConfig()->subMeasMode=0;
        process->setID(0);
        break;
    case 1:
        getRenderConfig()->subMeasMode=1;
        process->setID(1);
        break;
    case 2:
        getRenderConfig()->subMeasMode=2;
        process->setID(6);
        break;
    case 3:
        getRenderConfig()->subMeasMode=3;
        process->setID(0);
        break;
    case 4:
        getRenderConfig()->subMeasMode=4;
        process->setID(0);
        break;

    }
    this->bntDown(getRenderConfig()->subMeasMode);
    /*QPushButton* b=(QPushButton*) QObject::sender();
    if (!b) return;
    //QMessageBox::information(0,"",b->objectName(),QMessageBox::Ok);
    if (b->objectName() == "vectorBtn") {
        getRenderConfig()->subMeasMode=4;
        process->setID(0);
    }
    if (b->objectName() == "TESTBTN") {
        getRenderConfig()->subMeasMode=3;
        process->setID(0);
    }

    if (b->objectName() == "DeepBtn") {
        getRenderConfig()->subMeasMode=0;
        process->setID(0);
    }

    if (b->objectName() == "DevBtn") {
        getRenderConfig()->subMeasMode=1;
        process->setID(1);
    }

    if (b->objectName() == "TLGBtn") {
        getRenderConfig()->subMeasMode=2;
        process->setID(6);
    }*/

    /*else  {
        if (plots.first()->isHidden())
            plots.first()->show();
        for (int i=0;i<plots.size();i++) {
            plots->at(i)->setInterval(0, RAW_SAMPLES_NUM/(getRenderConfig()->Fdec/1000000));
            plots->at(i)->setScale(-1*32767*getGainY(m_nChAtten), 32767*getGainY(m_nChAtten));
        }
    }*/
    //this->bntDown(getRenderConfig()->subMeasMode);
}

void IqAnMode::displayMode(QString str1, QString str2, QString str3) {
    QLabel *l =(QLabel*)getWidget()->footer();
    QString text;
    switch (renderConf->subMeasMode){
    case 0:
        text="AM "+str1+" | AM "+str2+" | AM "+str3+" |";
        break;
    case 1:
        text="DEV "+str1+" | DEV "+str2+" | DEV "+str3+" |";
        break;
    case 2:
        text="TLG "+str1+" | TLG "+str2+" | TLG "+str3+" |";
        break;
    }

    l->setText(text);
}

void IqAnMode::plotInit(int legLblsCounter, QWidget *parent) {
    for (int i=0;i<2;i++){
        plots.append(new Plot(legLblsCounter,parent));
        plots.last()->setMinimumHeight(50);
        plots.last()->setMinimumWidth(100);
        //connect(plots.last(), SIGNAL(zoomed(QRectF)), this, SLOT(plotZoomed(QRectF)));
        connect(plots.last(), SIGNAL(chMarkerMoved(int)), this,SLOT(curveUpdate(int)));
        connect(plots.last(), SIGNAL(zoomerCreated()), this, SLOT(zoomerCreated()));
        //connect(plots.last(), SIGNAL(plotZoomedOut()), this, SLOT(plotZoomedOut()));
    }

    //connect(plots.first(), SIGNAL(zoomed(QRectF)), this, SLOT(plotZoomed(QRectF)));

    /*Plot* p = new Plot(legLblsCounter,parent);
    p->setInterval(0.0,82.0);
    p->setMinimumHeight(50);
    p->setMinimumWidth(100);

    Plot* p1 = new Plot(legLblsCounter,parent);
    p1->setInterval(0.0,82.0);
    p1->setMinimumHeight(50);
    p1->setMinimumWidth(100);

    plots.append(p);
    plots.append(p1);

    connect(p, SIGNAL(zoomerCreated()), this, SLOT(zoomerCreated()));

    connect(p, SIGNAL(zoomed(QRectF)), this, SLOT(plotZoomed(QRectF)));
    connect(p1, SIGNAL(zoomed(QRectF)), this, SLOT(plotZoomed(QRectF)));

    connect(p, SIGNAL(chMarkerMoved(int)), this,SLOT(curveUpdate(int)));
    connect(p1, SIGNAL(chMarkerMoved(int)), this, SLOT(curveUpdate(int)));
    */
}

void IqAnMode::setLegElementNum() {
    for (int i=0;i<plots.size(); i++)
        plots.at(i)->setTextCount(2);
}

void IqAnMode::zoomerCreated() {
    Plot *p = (Plot*) QObject::sender();
    //connect(p->getZoomer(), SIGNAL(activated(bool)), this, SLOT(zoomRectMoving(bool)));
    Plot *p2;
    if (p==plots.first())
        p2 = plots.last();
    if (p==plots.last())
    {
        p2=plots.first();
        p->getZoomer()->attachZoomer(p2->getZoomer());
        p2->getZoomer()->attachZoomer(p->getZoomer());
    }
}
