/******************************************************************************
 * Copyright (c) Aaronia AG, http://www.aaronia.de
 * This source is subject to the Microsoft Reference Source License (Ms-RSL).
 * Please see the license.txt file for more information.
 * All other rights reserved.
 ******************************************************************************/

#include <QtGui/QApplication>
#include <QTranslator>
#include "adcfdemomainwindow.h"
//#include "samplingthread.h"

int main(int argc, char *argv[])
{
    // create QT application object
    QApplication a(argc, argv);

    QTranslator translator;
            translator.load("project_ru");
            a.installTranslator(&translator);

    // create and display main window to handle UI logic
    ADcfDemoMainWindow window;
/*
    SamplingThread samplingThread;
    samplingThread.setFrequency(window.frequency());
    samplingThread.setAmplitude(window.amplitude());
    samplingThread.setInterval(window.signalInterval());

    window.connect(&window, SIGNAL(frequencyChanged(double)),
        &samplingThread, SLOT(setFrequency(double)));
    window.connect(&window, SIGNAL(amplitudeChanged(double)),
        &samplingThread, SLOT(setAmplitude(double)));
    window.connect(&window, SIGNAL(signalIntervalChanged(double)),
        &samplingThread, SLOT(setInterval(double)));
*/
    //window.show();
    //window.showFullScreen();
    window.showMaximized();


   // samplingThread.start();
   // window.start();

    // execute application object to start event loop
    bool ok = a.exec();
/*
    samplingThread.stop();
    samplingThread.wait(1000);
*/
    return  ok;
}
