#ifndef OSCIWIDGET_H
#define OSCIWIDGET_H
#include "basetabwidget.h"

class OsciWidget : public baseTabWidget
{
public:
    OsciWidget(QWidget *parent=0);
    QString getName();

protected:
};


#endif // OSCIWIDGET_H
