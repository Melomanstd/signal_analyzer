#include "plot.h"
#include "curvedata.h"
#include "signaldata.h"
#include <QMessageBox>
#include <qwt_plot_grid.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_directpainter.h>
#include <qwt_plot_zoomer.h>
#include <qwt_curve_fitter.h>
#include <qwt_legend.h>
#include <qwt_painter.h>
#include <qwt_picker_machine.h>
#include <qwt_scale_widget.h>
#include <qevent.h>
#include <QLabel>
#include <QDebug>

Plot::Plot(int legLabelsCounter, QWidget *parent):
    QwtPlot(parent),
    //d_paintedPoints(0),
    d_interval(0.0, 30000.0),
    d_timerId(-1)
{
    rightTrigger = 0;
    markersFreeze = false;
    deltaTab = 0;
    zoomerState=true;
    curYLSc=0;
    leg=0;
    textCount = legLabelsCounter;
    currentColor=Qt::red;
    currentColor2=Qt::yellow;
    textBackgrColor=Qt::black;
    yRightScale=true;
    yLeftScale=true;
    xBotScale=true;
    mark1State = true;
    mark2State = false;

    qDebug()<<"Plot constr 1";

    //------
    /*vMarker = new PlotMarker();
    vMarker->setChNumber(1);
    vMarker->setYAxis(QwtPlot::xBottom);
    vMarker->setValue(5.0, 0.0);
    vMarker->setLineStyle(QwtPlotMarker::VLine);
    vMarker->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    vMarker->setLinePen(QPen(currentColor, 0, Qt::DashDotLine));
    vMarker->attach(this);*/

    d_marker1 = new PlotMarker(false);
    d_marker1->setText("1");
    d_marker1->setAlignment(Qt::AlignLeft);
    d_marker1->setYAxis(QwtPlot::yLeft);
    d_marker1->setValue(0.0, 0.0);
    d_marker1->setLineStyle(QwtPlotMarker::HLine);
    d_marker1->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    d_marker1->setLinePen(QPen(currentColor, 0, Qt::DashDotLine));
    d_marker1->attach(this);

    d_marker2 = new PlotMarker(false);
    d_marker2->setText("2");
    d_marker2->setAlignment(Qt::AlignLeft);
    d_marker2->setYAxis(QwtPlot::yRight);
    d_marker2->setValue(0.0, 0.0);
    d_marker2->setLineStyle(QwtPlotMarker::HLine);
    d_marker2->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    d_marker2->setLinePen(QPen(currentColor2, 0, Qt::DashDotLine));
    d_marker2->attach(this);

    crossMarker1 = new PlotMarker(true);
    crossMarker1->setYAxis(QwtPlot::yLeft);
    crossMarker1->setValue(2.0, 5.0);
    crossMarker1->setLineStyle(QwtPlotMarker::Cross);
    crossMarker1->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    crossMarker1->setLinePen(QPen(Qt::white, 0, Qt::DashDotLine));
    crossMarker1->attach(this);

    crossMarker2 = new PlotMarker(true);
    crossMarker2->setYAxis(QwtPlot::yLeft);
    crossMarker2->setValue(5.0, 0.0);
    crossMarker2->setLineStyle(QwtPlotMarker::Cross);
    crossMarker2->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    crossMarker2->setLinePen(QPen(Qt::green, 0, Qt::DashDotLine));
    crossMarker2->attach(this);
    /*

    d_picker = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft,
        QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOff,
        this->canvas());
    d_picker->setStateMachine(new QwtPickerClickPointMachine());
    d_picker->setRubberBandPen(QColor(Qt::green));
    d_picker->setRubberBand(QwtPicker::CrossRubberBand);
    d_picker->setTrackerPen(QColor(Qt::white));
    //d_picker->setMousePattern(QwtEventPattern::MouseSelect1,Qt::RightButton);
    connect(d_picker, SIGNAL(selected(const QPolygon &)),
            SLOT(selected(const QPolygon &)));
    connect(d_picker, SIGNAL(selected(QPointF)),this, SLOT(setMarker(QPointF)));*/

        qDebug()<<"Plot constr 2";
    //--------------------------
    d_paintedPoints[0]=0;
    d_paintedPoints[1]=0;
    d_paintedPoints[2]=0;

    d_directPainter = new QwtPlotDirectPainter();

    zoomer=NULL;
    zoom=NULL;
    zoom1=NULL;
    setAutoReplot(false);

    // The backing store is important, when working with widget
    // overlays ( f.e rubberbands for zooming ). 
    // Here we don't have them and the internal 
    // backing store of QWidget is good enough.

    canvas()->setPaintAttribute(QwtPlotCanvas::BackingStore, false);


#if defined(Q_WS_X11)
    // Even if not recommended by TrollTech, Qt::WA_PaintOutsidePaintEvent
    // works on X11. This has a nice effect on the performance.
    
    canvas()->setAttribute(Qt::WA_PaintOutsidePaintEvent, true);

    // Disabling the backing store of Qt improves the performance
    // for the direct painter even more, but the canvas becomes
    // a native window of the window system, receiving paint events
    // for resize and expose operations. Those might be expensive
    // when there are many points and the backing store of
    // the canvas is disabled. So in this application
    // we better don't both backing stores.

    if ( canvas()->testPaintAttribute( QwtPlotCanvas::BackingStore ) )
    {
        canvas()->setAttribute(Qt::WA_PaintOnScreen, true);
        canvas()->setAttribute(Qt::WA_NoSystemBackground, true);
    }

#endif
qDebug()<<"Plot constr 3";
    initGradient();

    plotLayout()->setAlignCanvasToScales(true);
    canvas()->setFrameStyle(0);

    setAxisTitle(QwtPlot::xBottom, "mks");
    setAxisScale(QwtPlot::xBottom, d_interval.minValue(), d_interval.maxValue());
    setAxisTitle(QwtPlot::yLeft, "V");
    //setAxisScale(QwtPlot::yLeft, -120.0, 0.0);
    setAxisScale(QwtPlot::yLeft, -65535.0, 65535.0);
    setAxisScale(QwtPlot::yRight, -65535.0, 65535.0);

    enableAxis(QwtPlot::yLeft, false);
    enableAxis(QwtPlot::yRight, false);
    enableAxis(QwtPlot::xTop, false);
    enableAxis(QwtPlot::xBottom, false);

qDebug()<<"Plot constr 4";
    /*QwtLegend *legend = new QwtLegend;
    legend->setItemMode(QwtLegend::CheckableItem);
    insertLegend(legend, QwtPlot::RightLegend);*/



/*
    d_origin = new QwtPlotMarker();
    d_origin->setLineStyle(QwtPlotMarker::Cross);
    d_origin->setValue(d_interval.minValue() + d_interval.width() / 2.0, 0.0);
    d_origin->setLinePen(QPen(Qt::gray, 0.0, Qt::DashLine));
    d_origin->attach(this);*/

    for(int i=0;i<3;i++)
    {
        d_curve[i] = new PlotCurve();
        d_curve[i]->setStyle(QwtPlotCurve::Lines/*Sticks*/);
        d_curve[i]->setPen(QPen(Qt::green));
        //d_curve[i]->setSymbol(new QwtSymbol(QwtSymbol::XCross, Qt::NoBrush, QPen(Qt::black), QSize(5, 5) ) );
        d_curve[i]->setBaseline(-200.0);
#ifdef Q_OS_WIN
        d_curve[i]->setRenderHint(QwtPlotItem::RenderAntialiased, true);
#else
        d_curve[i]->setRenderHint(QwtPlotItem::RenderAntialiased, false);
#endif
#if 1
        d_curve[i]->setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
#endif
        d_curve[i]->setData(new CurveData());
        d_curve[i]->attach(this);
    }
    qDebug()<<"Plot constr 5";
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    d_curve[0]->setPen(QPen(currentColor));
    d_curve[1]->setPen(QPen(currentColor2));
    //this->enableAxis(QwtPlot::yRight);
    //this->enableAxis(QwtPlot::xTop);
    //setAxisScale(QwtPlot::yRight, 0, 65535.0);
    //setAxisScale(QwtPlot::xTop, 0, 333.0);
    d_curve[0]->setAxes(QwtPlot::xBottom,QwtPlot::yLeft);
    d_curve[1]->setAxes(QwtPlot::xBottom,QwtPlot::yRight);

    /*Qwt*/grid = new /*Qwt*/PlotGrid();

    //grid->setAxes(QwtPlot::xBottom, QwtPlot::yRight);
    grid->setPen(QPen(Qt::gray, 0.0, Qt::DotLine));
    //grid->setMinPen(QPen(Qt::white, 0.0, Qt::DotLine));
    //grid->setYAxis(QwtPlot::yLeft);
    grid->enableX(true);
    grid->enableXMin(false);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);
    d_curve[2]->hide();
    d_curve[2]->detach();


    rightTrigger = new PlotMarker(false);
    rightTrigger->setText("T");
    rightTrigger->setAlignment(Qt::AlignRight);
    rightTrigger->setYAxis(QwtPlot::yLeft);
    rightTrigger->setValue(0.0, 0.0);
    rightTrigger->setLineStyle(QwtPlotMarker::HLine);
    rightTrigger->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    rightTrigger->setLinePen(QPen(Qt::green, 0, Qt::DashDotLine));
    rightTrigger->attach(this);

    PlotMarker *testVMarker = new PlotMarker(false);
    testVMarker->setText("T");
    testVMarker->setAlignment(Qt::AlignTop);
    testVMarker->setYAxis(QwtPlot::yLeft);
    testVMarker->setValue(5.0, 0.0);
    testVMarker->setLineStyle(QwtPlotMarker::HLine);
    testVMarker->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    testVMarker->setLinePen(QPen(Qt::white, 0, Qt::DashDotLine));
    testVMarker->attach(this);

    PlotMarker *testVMarker2 = new PlotMarker(false);
    testVMarker2->setText("T");
    testVMarker2->setAlignment(Qt::AlignBottom);
    testVMarker2->setYAxis(QwtPlot::yLeft);
    testVMarker2->setValue(5.0, 0.0);
    testVMarker2->setLineStyle(QwtPlotMarker::HLine);
    testVMarker2->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    testVMarker2->setLinePen(QPen(Qt::white, 0, Qt::DashDotLine));
    testVMarker2->attach(this);

    c = new CanvasPicker(this);

    c->setMarkers(d_marker1, d_marker2);
    //c->setElementsRect(d_marker1->elementRect, d_marker2->elementRect);
    qDebug()<<"Plot constr 5/2";
    setZoomerState(true);
    qDebug()<<"Plot constr 6";
}

Plot::~Plot()
{
    delete d_directPainter;
}

void Plot::initGradient()
{
    QPalette pal = canvas()->palette();

#if QT_VERSION >= 0x040400
    QLinearGradient gradient( 0.0, 0.0, 1.0, 0.0 );
    gradient.setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient.setColorAt(0.0, QColor( 0, 49, 110 ) );
    gradient.setColorAt(1.0, QColor( 0, 87, 174 ) );

    pal.setBrush(QPalette::Window, QBrush(gradient));
#else
    pal.setBrush(QPalette::Window, QBrush( color ));
#endif

    canvas()->setPalette(pal);
}

void Plot::start()
{
    d_clock.start();
    d_timerId = startTimer(10);
}

void Plot::replot()
{
    for(int i=0;i<3;i++){
    CurveData *data = (CurveData *)d_curve[i]->data();
    data->values().lock();
    QwtPlot::replot();
    d_paintedPoints[i] = data->size();

    data->values().unlock();
    }
}

void Plot::setIntervalLength(double interval)
{
    if ( interval > 0.0 && interval != d_interval.width() )
    {
        d_interval.setMaxValue(d_interval.minValue() + interval);
        setAxisScale(QwtPlot::xBottom, 
            d_interval.minValue(), d_interval.maxValue());

        replot();
    }
}

void Plot::setInterval(double minValue, double maxValue)
{
    //if ( interval > 0.0 && interval != d_interval.width() )
    {
        d_interval.setMinValue(minValue);
        d_interval.setMaxValue(maxValue);
        setAxisScale(QwtPlot::xBottom,
            d_interval.minValue(), d_interval.maxValue());
        //this->setAxisMaxMajor(QwtPlot::xBottom,5);

        replot();
    }
}

void Plot::setScale(double minValue, double maxValue)
{

    //if ( interval > 0.0 && interval != d_interval.width() )
    {
        /*QwtScaleDiv *obj;
        QwtScaleDiv *obj1;
        double temp;
        QList<double> list[QwtScaleDiv::NTickTypes];
        temp=qAbs((qAbs(maxValue)+qAbs(minValue)));
        list[2].append(minValue);
        temp=temp/100;
        for (int i=1;i<10;i++)
            list[2].append(maxValue-(temp*i*10));
        list[2].append(maxValue);
        obj1 = new QwtScaleDiv(minValue,maxValue,list);
        setAxisScaleDiv(QwtPlot::yLeft,*obj1);
        setAxisScaleDiv(QwtPlot::yRight, *obj1);
        grid->setRYDiv(*obj1);*/

        setZoomScale(minValue, maxValue, yLeft);
        setZoomScale(minValue, maxValue, yRight);

        /*
        list[2].clear();
        temp=qAbs(qAbs(d_interval.maxValue())-
                         qAbs(d_interval.minValue()));
        temp=temp/100;
        list[2].append(d_interval.minValue());
        for (int i=1;i<10;i++)
            list[2].append(d_interval.maxValue()-(temp*i*10));
        list[2].append(d_interval.maxValue());
        obj = new QwtScaleDiv(d_interval.minValue(),
                              d_interval.maxValue(),
                              list);
        setAxisScaleDiv(QwtPlot::xBottom,*obj);
        */
        setZoomScale(d_interval.minValue(), d_interval.maxValue(), xBottom);

        setScale2();
        replot();
    }
}

void Plot::updateCurve()
{
    for(int i=0;i<3;i++){
        if(!d_curve[i]->isVisible()) continue;
    CurveData *data = (CurveData *)d_curve[i]->data();
    data->values().lock();

    const int numPoints = data->size();
    if ( numPoints > d_paintedPoints[i] )
    {
        const bool doClip = !canvas()->testAttribute( Qt::WA_PaintOnScreen );
        if ( doClip )
        {
            /*
                Depending on the platform setting a clip might be an important
                performance issue. F.e. for Qt Embedded this reduces the
                part of the backing store that has to be copied out - maybe
                to an unaccelerated frame buffer device.
            */

            const QwtScaleMap xMap = canvasMap( d_curve[i]->xAxis() );
            const QwtScaleMap yMap = canvasMap( d_curve[i]->yAxis() );

            QRectF br = qwtBoundingRect( *data, 
                                         d_paintedPoints[i] - 1, numPoints - 1 );

            const QRect clipRect = QwtScaleMap::transform( xMap, yMap, br ).toRect();
            // by AAA: d_directPainter->setClipRegion( clipRect );
        }

        d_directPainter->drawSeries(d_curve[i],
                                    d_paintedPoints[i] - 1, numPoints - 1);
        d_paintedPoints[i] = numPoints;
    }

    data->values().unlock();
    }
}

void Plot::showCurve(int curve, bool show)
{
    if(curve>=0 && curve<3)
    d_curve[curve]->setVisible(show);

    replot();
}

void Plot::incrementInterval(int i)
{
    //for(int i=0;i<3;i++)
    {
    CurveData *data1 = (CurveData *)d_curve[i]->data();
    data1->values().clearValues();

    d_paintedPoints[i] = 0;
}
    replot();

    return;

}

void Plot::incrementInterval()
{
    for(int i=0;i<3;i++)
    {
    CurveData *data1 = (CurveData *)d_curve[i]->data();
    data1->values().clearValues();

    d_paintedPoints[i] = 0;
}
    replot();

    return;

}

void Plot::timerEvent(QTimerEvent *event)
{
    if ( event->timerId() == d_timerId )
    {
        updateCurve();

        const double elapsed = d_clock.elapsed() / 1000.0;
        if ( elapsed > d_interval.maxValue() )
            incrementInterval();

        return;
    }

    QwtPlot::timerEvent(event);
}

void Plot::resizeEvent(QResizeEvent *event)
{
    d_directPainter->reset();
    QwtPlot::resizeEvent(event);
}

void Plot::showEvent( QShowEvent * )
{
    replot();
}

SignalData& Plot::signalData(int i)
{
    CurveData *data = (CurveData *)d_curve[i]->data();
    return data->values();
}

void Plot::setColor(int chanel, QColor color, bool isStarded) {
    if (chanel==0) {
        currentColor=color;
        d_marker1->setLinePen(QPen(currentColor, 0, Qt::DashDotLine));
    }
    if (chanel==1) {
        currentColor2=color;
        d_marker2->setLinePen(QPen(currentColor2, 0, Qt::DashDotLine));
    }
    d_curve[chanel]->setPen(QPen(color));
    if (leg)
        d_curve[chanel]->updateLegend(leg);
    replot();
    //updateCurve();
}

void Plot::setCurveText(QString text, int cId, int pos) {
    d_curve[cId]->setLabelText(pos,text);
    if (leg)
        d_curve[cId]->updateLegend(leg);
    replot();
    //d_curve[cId]->setTitle(text);
    //replot();

}

void Plot::setScale2() {
    qDebug()<<"setscale2 1";
    if (!leg)
    {
        d_curve[0]->createLbls(textCount);
        qDebug()<<"setscale2 1/2";
        d_curve[1]->createLbls(textCount);
        qDebug()<<"setscale2 2";
        leg = new PlotLegend();
        leg->setItemMode(QwtLegend::ReadOnlyItem);
        leg->setAutoFillBackground(true);
        plotLayout()->setSpacing(0);
        leg->setPalette(canvas()->palette());
        insertLegend(leg,QwtPlot::BottomLegend);
    }
//        QHBoxLayout b; b.set;
        //leg->insert(d_curve[0], new QLabel("text"));
    //leg->insert(grid,new QwtTextLabel(QString("text")));

    //insertEmptyLegend(leg,QwtPlot::BottomLegend);}
    /*if (zoom) {
        QStack<QRectF> s = zoom->zoomStack();
        zoom->zoom(s.at(0));
        delete zoom;
        zoom = 0;
    }
    qDebug()<<"setscale2 3";
    if (zoom1) {
        QStack<QRectF> s = zoom1->zoomStack();
        zoom1->zoom(s.at(0));
        delete zoom1;
        zoom1 = 0;
    }
    qDebug()<<"setscale2 4";
    if (!zoomerState) {
        return;
    }*/
    /*QwtPlotZoomer* */
    if (!zoom) {

    zoom = new PlotZoomer( canvas() );
    zoom->setAxis(QwtPlot::xBottom,QwtPlot::yLeft);
    zoom->b=true;
    //zoom->setTrackerMode(QwtPicker::AlwaysOff);
    //zoom->
    zoom->setRubberBandPen( QColor( Qt::black ) );
    zoom->setTrackerPen( QColor( Qt::black ) );
    zoom->setMousePattern( QwtEventPattern::MouseSelect2, Qt::RightButton, Qt::ControlModifier );
    zoom->setMousePattern( QwtEventPattern::MouseSelect3, Qt::RightButton );
    zoom->setZoomBase(true);//*/

    connect(zoom, SIGNAL(zoomed(QRectF)), this, SLOT(plotZoomed(QRectF)));
    connect(zoom, SIGNAL(zoomedOut()), this, SIGNAL(plotZoomedOut()));
    emit zoomerCreated();
    }

    /*if (zoom1) {

    zoom1 = new PlotZoomer( canvas() );
    zoom1->setAxis(QwtPlot::xBottom,QwtPlot::yRight);
    zoom1->b=false;
    zoom1->setRubberBandPen( QColor( Qt::black ) );
    zoom1->setTrackerPen( QColor( Qt::black ) );
    zoom1->setTrackerMode(QwtPicker::AlwaysOff);
    zoom1->setMousePattern( QwtEventPattern::MouseSelect2, Qt::RightButton, Qt::ControlModifier );
    zoom1->setMousePattern( QwtEventPattern::MouseSelect3, Qt::RightButton );
    zoom1->setZoomBase(true);

    connect(zoom1, SIGNAL(zoomed(QRectF)), this, SLOT(plotZoomed(QRectF)));
    }*/
    //zoom->setAxis(QwtPlot::xBottom,QwtPlot::yLeft);
    QStack<QRectF> s = zoom->zoomStack();
    zoom->zoom(s.at(0));
    //s=zoom1->zoomStack();
    //zoom1->zoom(s.at(0));
    zoom->setZoomBase(true);
    //zoom1->setZoomBase(true);

//    if (!zoomerState) {

//    } else {
//        //zoom->setAxis(QwtPlot::xBottom,QwtPlot::yLeft);
//        //zoom->setAxis(QwtPlot::xBottom,QwtPlot::yRight);
//        //zoom->setEnabled(true);
//        //zoom1->setEnabled(true);
    replot();
}

void Plot::setXScale(int val) {
    int x=(val+1)%3;
    if (!x) x=5;    // x = 1||2||5

    switch(static_cast<int>(val/3)) {
    case 2:
    case 5: //x100 per section
        x*=1000;
        break;
    case 0:
    case 3:  //x per section
    case 6:
        x*=10;
        break;
    case 1: //x10 per section
    case 4:
        x*=100;
        break;
    }

    QwtScaleMap s = canvasMap(xBottom);
    setZoomScale(0,x,xBottom);
    if (markersFreeze)
        xMarkersUpdate(s);
    //deltatabUpdate();
    /*QwtScaleMap bot = canvasMap(xBottom);
    double xVal = s.transform(crossMarker1->xValue());
    double xVal2 = s.transform(crossMarker2->xValue());

    if (markersFreeze) {
        crossMarker1->setXValue(bot.invTransform(xVal));
        crossMarker2->setXValue(bot.invTransform(xVal2));
        deltatabUpdate();
    }*/


    setScale2();
    replot();
}

void Plot::setYScale(int val, Axis ax) {
    setScale2();
    int x=(val+1)%3;
    if (!x) x=5;    //x= 1||2||5
    switch (val/3) {
    case 0:
    //case 3:
        x*=10; break;
    case 1:
    //case 4:
        x*=100; break;
    case 2:
        x*=1000; break;
    case 3:
        x*=10000; break;
    case 4:
        x*=100000; break;
    }
    double x1=x;
    x1/=1000;
    QwtScaleMap s = canvasMap(yLeft);
    setZoomScale(-x1/2,x1/2,ax);
    if (markersFreeze)
        yMarkersUpdate(s);
    //deltatabUpdate();
    setScale2();
    replot();
}

void Plot::setMarker(QPointF f) {
    d_marker1->setValue(f);
}

void Plot::setZoomScale(double y1, double y2, int ax) {
    QwtScaleDiv *obj1;
    double temp;  //y1 min \ y2 max
                  //x min  \ y max
    QList<double> list[QwtScaleDiv::NTickTypes];
    temp=(y2)-(y1);
    temp=temp/10;
    list[2].append(y1);
    for (int i=1;i<10;i++)
        list[2].append(y1+(temp*i));
    list[2].append(y2);
    if (qAbs(y1)==qAbs(y2))
        list[2][5]=0;
    //list[2].reserve(list[2].size());
    obj1 = new QwtScaleDiv(y1,y2,list);
    setAxisScaleDiv(ax,*obj1);
    if (ax == QwtPlot::yRight) {
        grid->setRYDiv(*obj1);
    }
    //grid->itemChanged();
    delete obj1;
    replot();
}

void Plot::showGridScale(bool s, int ax) {
    switch (ax) {
    case QwtPlot::yLeft:
        yLeftScale=s;
        break;
    case QwtPlot::yRight:
        yRightScale=s;
        break;
    case QwtPlot::xBottom:
        xBotScale=s;
        break;
    }
}

void Plot::setBackgrColor(QColor c) {
    textBackgrColor = c;
}

void Plot::showMarkers(int nCh, bool s) {
    switch (nCh) {
    case 0:
        mark1State=s;
        break;
    case 1:
        mark2State=s;
        break;
    }
    replot();
}

void Plot::setLegendText(int nCh, QString text) {
    if (!leg) return;
    d_curve[nCh]->setLegendText(text, leg);
}

void Plot::setTextCount(int c) {
    textCount=c;
}

void Plot::setZoomerState(bool s) {
    qDebug()<<"zoomer 1";
    zoomerState = s;
    if (s) {
        crossMarker1->detach();
        crossMarker1->hide();
        crossMarker2->detach();
        crossMarker2->hide();
        //crossMarker2->detach();
    }

    qDebug()<<"zoomer 2";

    if (!s) {
        crossMarker1->attach(this);
        crossMarker1->show();
        crossMarker2->attach(this);
        crossMarker2->show();
        //crossMarker2->detach();
    }
    qDebug()<<"zoomer 3";
    if (deltaTab) {
        deltaTab->setVisible(!s);
        deltatabUpdate();
    }
    qDebug()<<"zoomer 4";
    //setScale2();
    qDebug()<<"zoomer 5";

    if (!zoom/*||!zoom1*/) return;

    //zoom->setEnabled(s);
    //zoom1->setEnabled(s);

    setDefaultMarkersPos();

    replot();
    qDebug()<<"zoomer 6";


}

void Plot::setMarkersColor(int nMrk, QColor c) {
    QPen p = crossMarker1->linePen();
    p.setColor(c);
    if (nMrk == 0) {
        crossMarker1->setLinePen(p);
    }
    if (nMrk == 1) {
        crossMarker2->setLinePen(p);
    }
    replot();
}

void Plot::setDeltaTab(QTreeWidget *t) {
    deltaTab = t;
}

QTreeWidget* Plot::getDeltaTab() {
    return deltaTab;
}

void Plot::deltatabUpdate() {
    QwtScaleMap yMap = canvasMap( QwtPlot::yLeft );
    //QwtScaleMap xMap = canvasMap( QwtPlot::xBottom );
    QwtScaleMap yrMap = canvasMap( QwtPlot::yRight );

    if (!deltaTab)
        return;

    deltaTab->topLevelItem(0)->setText(1,QString::number(crossMarker1->xValue()));
    deltaTab->topLevelItem(1)->setText(1,QString::number(crossMarker2->xValue()));

    deltaTab->topLevelItem(0)->setText(2,QString::number(crossMarker1->yValue()));
    deltaTab->topLevelItem(1)->setText(2,QString::number(crossMarker2->yValue()));

    double temp = yMap.transform(crossMarker1->yValue());
    double temp2 = yMap.transform(crossMarker2->yValue());
    temp = yrMap.invTransform(temp);
    temp2 = yrMap.invTransform(temp2);
    deltaTab->topLevelItem(0)->setText(3,QString::number(temp));
    deltaTab->topLevelItem(1)->setText(3,QString::number(temp2));

    deltaTab->topLevelItem(0)->setText(4,QString::number(qAbs(
                     crossMarker1->xValue()-crossMarker2->xValue())));
    deltaTab->topLevelItem(0)->setText(5,QString::number(qAbs(
                     crossMarker1->yValue()-crossMarker2->yValue())));
    deltaTab->topLevelItem(0)->setText(6,QString::number(qAbs(
                     temp-temp2)));

}

void Plot::yMarkersUpdate(QwtScaleMap yl) {
    QwtScaleMap left = canvasMap(yLeft);

    double y = yl.transform(crossMarker1->yValue());
    double y2 = yl.transform(crossMarker2->yValue());

    crossMarker1->setYValue(left.invTransform(y));
    crossMarker2->setYValue(left.invTransform(y2));
    deltatabUpdate();
}

void Plot::xMarkersUpdate(QwtScaleMap xb) {
    QwtScaleMap bot = canvasMap(xBottom);

    double x = xb.transform(crossMarker1->xValue());
    double x2 = xb.transform(crossMarker2->xValue());

    crossMarker1->setXValue(bot.invTransform(x));
    crossMarker2->setXValue(bot.invTransform(x2));
    deltatabUpdate();
}

void Plot::setMarkersFreeze(bool s) {
    markersFreeze = s;
}

void Plot::setDefaultMarkersPos() {
    QwtScaleMap yMap = canvasMap( QwtPlot::yLeft );
    QwtScaleMap xMap = canvasMap( QwtPlot::xBottom );

    /*temp=(y2)-(y1);
    temp=temp/10;
    (y1+(temp*i));*/

    double tempx=(xMap.s2()-xMap.s1())/100;
    tempx=xMap.s1()+tempx*10;
    double tempx2=(xMap.s2()-xMap.s1())/100;
    tempx2=xMap.s1()+tempx2*90;
    double tempy=(yMap.s2()-yMap.s1())/100;
    //tempy=tempy/100;
    tempy=yMap.s1()+tempy*90;
    double tempy2=(yMap.s2()-yMap.s1())/100;
    //tempy2=tempy2/100;
    tempy2=yMap.s1()+tempy2*10;

    /*
    double tempx=xMap.s2()/100*10;
    double tempx2=xMap.s2()/100*90;
    double tempy=yMap.s2()/100*80;
    double tempy2=yMap.s1()/100*80;
    */
    crossMarker1->setValue(tempx,
                           tempy);
    crossMarker2->setValue(tempx2,
                           tempy2);
    deltatabUpdate();
    replot();
}

bool Plot::markersIsHidden() {
    return !zoomerState;
}

double Plot::markerYValue(int nMrk) {
    if (nMrk == 0)
        return d_marker1->yValue();
    if (nMrk == 1)
        return d_marker2->yValue();
    return 0;
}

void Plot::markerMoved(int nMrk) {
    emit chMarkerMoved(nMrk);
}

bool Plot::cursorsIsFrozen() {
    return markersFreeze;
}

void Plot::setZoomerEnable(bool s) {
    if (!zoom/*||!zoom1*/) return;

    zoom->setEnabled(s);
    //zoom1->setEnabled(s);
}

void Plot::plotZoomed(QRectF r) {
    emit zoomed(r);
}

void Plot::zoomPlot(QRectF r) {
    if (!zoom /*|| !zoom1*/) return;
    static bool zoomed = false;
    if (zoomed) {
        zoomed = false;
        return;
    }
    zoomed = true;
    int x = zoom->zoomStack().indexOf(r);
    //QStack<QRectF> q = zoom->zoomStack();
    if (x>=0) {
        zoom->zoom(-1);
    }
//    else {
//        zoom->zoom(r);
//    }
    //zoom1->zoom(r);
}

PlotZoomer* Plot::getZoomer() {
    return zoom;
}

PlotMarker* Plot::getRightTriggerMarker() {
    return rightTrigger;
}
