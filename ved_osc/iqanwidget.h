#ifndef IQANWIDGET_H
#define IQANWIDGET_H
#include "basetabwidget.h"

class IqAnWidget : public baseTabWidget
{
public:
    IqAnWidget(QWidget *parent=0);
    QString getName();

};

#endif // IQANWIDGET_H
