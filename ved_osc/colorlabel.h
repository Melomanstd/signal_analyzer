#ifndef COLORLABEL_H
#define COLORLABEL_H

#include <QColorDialog>
#include <QLabel>
#include <QEvent>

class colorLabel: public QLabel
{
    Q_OBJECT

public:
    colorLabel(QWidget *parent = 0);
    bool eventFilter(QObject *, QEvent *e);
    void changeColor();
    QColor getLblColor();
    int currentChanel();
    void setChanel(int Chn);
    void setCurrentColor(QColor c);

signals:
    void colorChanged(colorLabel* lbl);

private:
    QColor currentColor;
    int chanelID;
};

#endif // COLORLABEL_H
