#include "curvedata.h"
#include "signaldata.h"

const SignalData &CurveData::values() const
{
    return sig_data;//SignalData::instance();
}

SignalData &CurveData::values() 
{
    return sig_data;//SignalData::instance();
}

QPointF CurveData::sample(size_t i) const
{
    return sig_data.value(i);//SignalData::instance().value(i);
}

size_t CurveData::size() const
{
    return sig_data.size();//SignalData::instance().size();
}

QRectF CurveData::boundingRect() const
{
    return sig_data.boundingRect();//SignalData::instance().boundingRect();
}
