#ifndef _SIGNAL_DATA_H_
#define _SIGNAL_DATA_H_ 1

#include <qrect.h>
#include <QVector>

class SignalData
{
public:
    static SignalData &instance();

    void append(const QPointF &pos);
    void clearStaleValues(double min);
    void clearValues();

    const QVector<QPointF> &minValues() const;

    int size() const;
    QPointF value(int index) const;

    QRectF boundingRect() const;

    void lock();
    void unlock();
    

    SignalData();
    SignalData(const SignalData &);
    SignalData &operator=( const SignalData & );

    virtual ~SignalData();

private:
    class PrivateData;
    PrivateData *d_data;
};

#endif
