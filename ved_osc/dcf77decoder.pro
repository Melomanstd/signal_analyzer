#-------------------------------------------------
#
# Project created by QtCreator 2011-08-19T12:09:15
#
#-------------------------------------------------


# This is a GUI application
QT       += core gui network svg

TARGET = dcf77decoder
TEMPLATE = app

# Include aspectran module from ASDK
CONFIG += warn_on aspectran
DESTDIR = build/
DLLDESTDIR = build/
OBJECTS_DIR = build/obj
MOC_DIR = build/moc
UI_DIR = build/ui
RCC_DIR = build/rcc

# The project itself just contains the user interface, the actual signal processing is done by the decoder in the ASDK
SOURCES += main.cpp\
        adcfdemomainwindow.cpp \
    signaldata.cpp \
    samplingthread.cpp \
    plot.cpp \
    curvedata.cpp \
    wheelbox.cpp \
    knob.cpp \
    renderthread.cpp \
    fortunethread.cpp \
    fortuneserver.cpp \
    workerobject.cpp \
    thread.cpp \
    measconf.cpp \
    colorlabel.cpp \
    basetabwidget.cpp \
    osciwidget.cpp \
    spectrewidget.cpp \
    iqanwidget.cpp \
    mode.cpp \
    modedialog.cpp \
    modsmainwidget.cpp \
    plotdialog.cpp \
    processobject.cpp \
    plotzoomer.cpp \
    checkingthread.cpp \
    plotgrid.cpp \
    plotmarker.cpp \
    plotcurve.cpp \
    plotlegend.cpp \
    canvaspicker.cpp \
    peaksmarkerswidget.cpp \
    timercombobox.cpp

HEADERS  += adcfdemomainwindow.h \
    signaldata.h \
    samplingthread.h \
    plot.h \
    curvedata.h \
    wheelbox.h \
    knob.h \
    renderthread.h \
    fortuneserver.h \
    fortunethread.h \
    thread.h \
    workerobject.h \
    measconf.h \
    colorlabel.h \
    basetabwidget.h \
    osciwidget.h \
    spectrewidget.h \
    iqanwidget.h \
    mode.h \
    modedialog.h \
    modsmainwidget.h \
    plotdialog.h \
    processobject.h \
    plotzoomer.h \
    checkingthread.h \
    plotgrid.h \
    plotmarker.h \
    plotcurve.h \
    plotlegend.h \
    canvaspicker.h \
    peaksmarkerswidget.h \
    timercombobox.h

FORMS    += adcfdemomainwindow.ui \
    table1.ui \
    basetabwidget.ui \
    modedialog.ui \
    modsmainwidget.ui \
    plotdialog.ui \
    peaksmarkerswidget.ui

RESOURCES += \
    res.qrc

# ASDK
#include($$quote($$(ASDK)/asdk.pri))

# Qwt
include( $$quote($$(QWT_ROOT)/qwtconfig.pri) )
#include( $$quote($$(QWT_ROOT)/qwtbuild.pri) )

INCLUDEPATH += $$(QWT_ROOT)/src
DEPENDPATH  += $$(QWT_ROOT)/src
QMAKE_RPATHDIR *= $$(QWT_ROOT)/lib
#LIBS      += -L$$(QWT_ROOT)/lib -lqwtd
CONFIG(debug, debug|release) {
        LIBS      += -L$$(QWT_ROOT)/lib -lqwtd
        #DEFINES += QT_NO_DEBUG_OUTPUT
}else{
        LIBS      += -L$$(QWT_ROOT)/lib -lqwt
        #DEFINES += QT_NO_DEBUG_OUTPUT
}
#qtAddLibrary(qwt)
DEFINES += QT_DLL QWT_DLL

TRANSLATIONS = project_ru.ts
