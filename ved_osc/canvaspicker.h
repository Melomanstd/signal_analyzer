#ifndef CANVASPICER_H
#define CANVASPICER_H

#include <qobject.h>
#include <qwt_plot_marker.h>
#include "plot.h"


class QPoint;
class Plot;
class QCustomEvent;
class QwtPlot;
class QwtPlotCurve;


class CanvasPicker: public QObject
{
    Q_OBJECT
public:
    CanvasPicker(QwtPlot *plot);
    virtual bool eventFilter(QObject *, QEvent *);

    virtual bool event(QEvent *);
    void setMarkers(QwtPlotMarker *m1, QwtPlotMarker *m2);
    int checkVertical(const QPoint &);
    int checkHorizontal(const QPoint &);

    void deltaTabUpdate(bool l,
                        bool r,
                        bool b,
                        double val);

private:
    void select(const QPoint &);
    void move(const QPoint &);
    void moveBy(int dx, int dy);

    void release();

    void showCursor(bool enable);
    void shiftPointCursor(bool up);
    void shiftCurveCursor(bool up);

    void chMarkersMoved();
    QPointF adjustPoint(QPointF &pos);

    bool closestPoint(const QPoint &pos, QwtPlotMarker *m);

    QwtPlot *plot() { return (QwtPlot *)parent(); }
    const QwtPlot *plot() const { return (QwtPlot *)parent(); }

    QwtPlotCurve *d_selectedCurve;
    QwtPlotMarker *selectedMarker;
    QwtPlotMarker *ch1Marker;
    QwtPlotMarker *ch2Marker;
    bool moveHLine;
    bool moveVLine;
    int d_selectedPoint;
    bool dragged;
    bool cursFreeze;
    QTreeWidget *deltaTab;
};

#endif //CANVASPICER_H
