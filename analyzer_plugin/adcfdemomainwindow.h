/******************************************************************************
 * Copyright (c) Aaronia AG, http://www.aaronia.de
 * This source is subject to the Microsoft Reference Source License (Ms-RSL).
 * Please see the license.txt file for more information.
 * All other rights reserved.
 ******************************************************************************/

#ifndef ADCFDEMOMAINWINDOW_H
#define ADCFDEMOMAINWINDOW_H

#include <QMainWindow>
#include <QStateMachine>
#include <QFinalState>
#include <QTimer>
#include <QGraphicsPixmapItem>
#include <QUdpSocket>
#include <QColorDialog>
#include <QInputDialog>
#include <QCheckBox>
#include <QSettings>
#include <QLinearGradient>

//#include "adecoder.h"
//#include "aspectranfactory.h"
//#include "asweepprocessor.h"

#include "plot.h"

#include "renderthread.h"
#include "samplingthread.h"
#include "osciwidget.h"
#include "spectrewidget.h"
#include "iqanwidget.h"
#include "mode.h"
#include "modedialog.h"
#include "modsmainwidget.h"
#include "plotdialog.h"
#include "processobject.h"
#include "checkingthread.h"
//#define MAXMODES Mode::COUNT_ID //current numbers of mods

#include "fortuneserver.h"
#include "timercombobox.h"

// platform includes for setting system time
//#ifdef Q_OS_WIN32
//#include <Windows.h>
//#endif

#include "thread.h"
#include "workerobject.h"

#include "colorlabel.h"

#include "interfaces2.h"

class colorLabel;
class Plot;
class Knob;
class WheelBox;
class MeasureConfigItem;


class RenderThread;

namespace Ui {
    class ADcfDemoMainWindow;
}

class ADcfDemoMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ADcfDemoMainWindow(RenderThread *core=0, bool limFunc=false, QWidget *parent = 0);
    ~ADcfDemoMainWindow();

    void loadSettings();
    void saveSettings();
    void loadDefaultSettings();
    void blockGUI(bool s);
    int loadPlugin();

Q_SIGNALS:
    void setError(QString);
    //void deviceConnected(ASpectranDevice *dev);
    //void deviceInitialized(ASpectranDevice *dev);
    void decodingStarted();
    void stopClicked();
protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *e);

private Q_SLOTS:
    void connectDevice();
    void disconnectDevice();
    void setM_nCh();

public Q_SLOTS:
    void changeBackground();
    void changeGridColor();

    void dataReady(QString str1, QString str2, QString str3);
    void WriteLog(QString str);
    void onColorChanged(colorLabel *lbl);
    void chanelSelected();
    void chanel1Selected(bool s);
    void chanel2Selected(bool s);
    void setOscInterval(int val);
    void setOscScale1(int val);
    void setOscScale2(int val);
    void setCoupling1(int val);
    void setCoupling2(int val);
    void renderStarted(Mode* obj);
    Mode *addNewTab();
    void deleteTab(int index);
    void modsBoxUpdate();
    void insertMode(int x, Mode* m);
    void clearMode();
    void showCurrentMods();
    void loadOscConf();
    void loadSpecConf();
    void loadIQAnConf();
    void baseConfLoad(int x);
    void genFcChanged(QString t);
    void uiFCentrChanged(QString t);
    void uiBandChanged(QString t);
    void blockUiElements(bool c);
    void setCycle(bool c);
    void switchMode();
    void rendStarted();
    void setZoomerState();
    void markersColorChanged(colorLabel *lbl);
    void setMarkersFreeze(bool s);
    void setDefaultMarkers();
    void subMeasModeChanged();
    void showPeaksTab();

    void fillToolbar(bool empty);
    void showGenerator(bool s);
    void addProcessingSymbols(bool s, int tab);
    void resumeMeasuring();

private Q_SLOTS:
    void on_pushButton_clicked();

    void readPendingDatagramsSrv();
    void processDatagramSrv(QByteArray datagram, QHostAddress sender, quint16 senderPort);

    void makeTestData();
    void makeTestData2();
    void makeTestDataFFT();
    void enableControl();

    void makeTestData3();

    void on_hard_gain_slider_valueChanged(int val);
    void on_accur_slider_valueChanged(int val);
    void on_hard_gain_slider_2_valueChanged(int val);

    void on_processStarted();
    void on_processStopped();

    void on_button_WriteReg_clicked();

    void on_combo_Ch1Type_activated(int index);

    void on_combo_Ch2Type_activated(int index);

    void on_button_SpecWrite_clicked();

    void on_pushButton_2_clicked();

    void on_combo_MeasMode_activated(int index);

    void on_combo_Ch1Atten_activated(int index);
    void on_combo_Ch2Atten_activated(int index);
    void on_combo_Gen_activated(int index);

    void on_edit_Fcentr_editingFinished();//(const QString &arg1);

    void on_edit_Band_editingFinished();//(const QString &arg1);

    void on_slider_Amax_sliderReleased();

    void on_slider_Amax_valueChanged(int value);

    //void on_button_SwitchMode_clicked();

    void measModeChange(int x);
    void checkingCycle();
    void startChecking();
    void configInit(Mode *m);
    void checkingProcessStoped();

public:
    Ui::ADcfDemoMainWindow *ui;

    QTranslator translator;

    int cur_list;
    int cur_profile;
    double m_last_captured_f;

    QStringList titles;

    QString lastError;

    RenderThread *renderThread;
    Mode* currentMode;
    // oscilloscope
    SamplingThread samplingThread;

    FortuneServer server;
    QUdpSocket* udp_client;
    QUdpSocket* udp_server;

protected:
    void initOscope();

    void mainModeWigdetInit();

    void initSocket();
    void closeSocket();

    void loadProfiles();
    void loadConfig();

    void WriteLog(quint16 LDATA, quint8 CMD, quint8 BADDR, const QList<QPoint> &WDATA);
    void WriteLog(QByteArray datagram);

    Mode *modeInit(int id);

public:
    double getGainY(int nAtten);
    double getGainX();
    void start();

    double amplitude() const;
    double frequency() const;
    double signalInterval() const;

Q_SIGNALS:
    void amplitudeChanged(double);
    void frequencyChanged(double);
    void signalIntervalChanged(double);

public:
    ProcessObject *process;
    QPushButton *tabBtn;

    ModsMainWidget *mainTabPtr;

    QList<Mode*> mode;

    int fft_window[6][FFT_SAMPLES_NUM];

private:
    Thread* m_thread;
    WorkerObject *m_worker;
    CheckingThread *chThread;

    QString m_strIp;
    QString m_strOscIp;

    bool m_bWork;
    bool m_bManual; // true - manual mode, false - auto mode(remote ctrl)
    bool m_bStarted;
    bool measuring;

    QCheckBox *mrkFreezeChb;
    QAction *genState;
    bool limFunc;

    DriverInterface *dconvInterface;
    //AutoWindow* auto_window;

public:
    bool showCursors;
    bool m_bCycle;
    bool m_Ch1;
    bool m_Ch2;
    bool win_creadted;
    bool initDone;
    quint8 m_nMeasMode;
    //quint8 m_nCh;
    quint8 m_nChType;
    quint8 m_nChType2;
    quint8 m_nChAtten;
    quint8 m_nChAtten2;
    quint8 m_nChScale1;
    quint8 m_nChScale2;
    quint8 m_nTimeScale;

    quint8 m_nTaktADC;
    quint8 m_nTaktPLL;

    quint8 m_nGenMode;
    quint64 m_nGenFc;
    quint32 m_nAmax;
    quint32 m_nAmin;
    quint64 m_nGenFsym;
    quint64 m_nGenFmod;
    quint64 m_nGenFdev;
    quint8 m_nGenSSBType;
    qreal m_fGenK;
//private:
    //QByteArray m_data;
    QByteArray m_data_test;

    double Fs;
    double Fleft;
    double Fright;
    int window;
    int Nfft;

    double Fcentr;
    double Band;

    double Dec_ideal;
    int Dec_3;
    int Dec_1;
    int Dec_2;

    unsigned long Dec_sum;

    double Fdec;

    int Nband;

    colorLabel *lbl1;
    colorLabel *lbl2;
    colorLabel *markColor;
    colorLabel *markColor2;
    QWidget *sideModeTab;
    QWidget *sideMainTab;
    QPushButton *startCheckingButton;
    QCheckBox *checkingCycleBox;
    QCheckBox *checkingAll;

};



#endif // ADCFDEMOMAINWINDOW_H

