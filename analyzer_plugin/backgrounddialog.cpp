#include "backgrounddialog.h"
#include "ui_backgrounddialog.h"
#include <QDebug>
#include <QSettings>

BackgroundDialog::BackgroundDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BackgroundDialog)
{
    ui->setupUi(this);
    this->gradient = new QLinearGradient(0.0, 0.0, 1.0, 0.0);
    ui->color_lbl->setVisible(false);
    ui->color_lbl2->setVisible(false);
    QSettings s("analyzerconf.ini", QSettings::IniFormat);
    QColor c1 = s.value("analyzer/background_color1").value<QColor>();
    QColor c2 = s.value("analyzer/background_color2").value<QColor>();
    ui->color_lbl->setPalette(QPalette( c1 ));
    ui->color_lbl2->setPalette(QPalette( c2 ));
    gradient->setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient->setColorAt(0.0, c1 );
    gradient->setColorAt(1.0, c2 );
    connect(ui->color1_btn, SIGNAL(clicked()), this, SLOT(set1stColor()));
    connect(ui->color2_btn, SIGNAL(clicked()), this, SLOT(set2ndColor()));
    connect(ui->gr_chbox, SIGNAL(clicked()), this, SLOT(setSignleColor()));

//    QSettings s("analyzerconf.ini", QSettings::IniFormat);
    bool g = s.value("analyzer/gradient").toBool();
    if (!g) ui->gr_chbox->click();
}

BackgroundDialog::~BackgroundDialog()
{
    delete ui;
}

void BackgroundDialog::set1stColor()
{
    QSettings s("analyzerconf.ini", QSettings::IniFormat);
    QColor c = s.value("analyzer/background_color1").value<QColor>();
    QPalette pal = ui->color_lbl2->palette();//canvas()->palette();

    QColorDialog d; d.setCurrentColor(c);//ui->color_lbl->palette().color(QPalette::Button));
    if (d.exec() == QDialog::Rejected) return;
    QColor color = d.currentColor();
    ui->color_lbl->setPalette(QPalette(color));

    if (!ui->gr_chbox->isChecked())
        pal = ui->color_lbl->palette();
//    return;

    QColor color2 = ui->color_lbl2->palette().color(QPalette::Button);

#if QT_VERSION >= 0x040400
//    gradient( 0.0, 0.0, 1.0, 0.0 );
    gradient->setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient->setColorAt(0.0, color );
    gradient->setColorAt(1.0, pal.color(QPalette::Button));

    pal.setBrush(QPalette::Window, QBrush(*gradient));
#else
    pal.setBrush(QPalette::Window, QBrush( color ));
#endif

    ui->color_lbl3->setPalette(pal);
//    canvas()->setPalette(pal);
}

void BackgroundDialog::set2ndColor()
{
    QSettings s("analyzerconf.ini", QSettings::IniFormat);
    QColor c = s.value("analyzer/background_color2").value<QColor>();
    QPalette pal = ui->color_lbl->palette();//canvas()->palette();
    QColorDialog d; d.setCurrentColor(c);//ui->color_lbl2->palette().color(QPalette::Button));
    if (d.exec() == QDialog::Rejected) return;
    QColor color = d.currentColor();//QColorDialog::getColor(ui->color_lbl2->palette().color(QPalette::Button));
    ui->color_lbl2->setPalette(color);
//    return;

#if QT_VERSION >= 0x040400
//    gradient( 0.0, 0.0, 1.0, 0.0 );
    gradient->setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient->setColorAt(0.0, pal.color(QPalette::Button) );
    gradient->setColorAt(1.0, color );

    pal.setBrush(QPalette::Window, QBrush(*gradient));
#else
    pal.setBrush(QPalette::Window, QBrush( color ));
#endif

    ui->color_lbl3->setPalette(pal);
//    canvas()->setPalette(pal);
}

QLinearGradient BackgroundDialog::getBackground()
{
    return *gradient;
}

void BackgroundDialog::setCurrentColor(QPalette p)
{
    ui->color_lbl3->setPalette(p);

//    QGradientStops v = gradient->stops();
//    ui->color_lbl->setPalette(p);
//    ui->color_lbl2->setPalette(QPalette(v.at(1).second));
    qDebug()<<"";
}

void BackgroundDialog::setSignleColor()
{
    QSettings s("analyzerconf.ini", QSettings::IniFormat);
    s.setValue("analyzer/gradient", ui->gr_chbox->isChecked());
    if (ui->gr_chbox->isChecked())
        return;
    QPalette pal = ui->color_lbl->palette();
#if QT_VERSION >= 0x040400
//    gradient( 0.0, 0.0, 1.0, 0.0 );
//    gradient->setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient->setColorAt(0.0, pal.color(QPalette::Button) );
    gradient->setColorAt(1.0, pal.color(QPalette::Button) );

    pal.setBrush(QPalette::Window, QBrush(*gradient));
#else
    pal.setBrush(QPalette::Window, QBrush( color ));
#endif
    ui->color_lbl2->setPalette(ui->color_lbl->palette());
    ui->color_lbl3->setPalette(ui->color_lbl->palette());
}

QColor BackgroundDialog::get1stColor()
{
    return ui->color_lbl->palette().color(QPalette::Button);
}

QColor BackgroundDialog::get2ndColor()
{
    return ui->color_lbl2->palette().color(QPalette::Button);
}
