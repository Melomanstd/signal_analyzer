#ifndef TIMERTABWIDGET_H
#define TIMERTABWIDGET_H

//#include <QtGui>
#include <QObject>
#include <QTabWidget>
#include <QTimer>

class TimerTabWidget : public QTabWidget
{
    Q_OBJECT
public:
    explicit TimerTabWidget(QWidget *parent = 0);
    
signals:
    void currentTabChanged(int tab);
    
public slots:
    void tabChanged();
    void updateTimer();

protected:
    QTimer timer;
    
};

#endif // TIMERTABWIDGET_H
