#include "timertabwidget.h"

TimerTabWidget::TimerTabWidget(QWidget *parent) :
    QTabWidget(parent)
{
    timer.setInterval(300);
    connect(this, SIGNAL(currentChanged(int)), this, SLOT(updateTimer()));
    connect(&timer, SIGNAL(timeout()), this, SLOT(tabChanged()));
}

void TimerTabWidget::updateTimer()
{
    timer.start();
}

void TimerTabWidget::tabChanged()
{
    timer.stop();
    emit currentTabChanged(this->currentIndex());
}
