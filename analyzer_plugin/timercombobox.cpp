#include "timercombobox.h"

TimerComboBox::TimerComboBox(QWidget *parent,int msec ) :
    QComboBox(parent)
{
    timer.setInterval(msec);
    connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(setTimer(int)));
    connect(&timer, SIGNAL(timeout()), this ,SLOT(changeIndex()));
}

void TimerComboBox::setTimer(int i)
{
    timer.start();
}

void TimerComboBox::changeIndex()
{
    timer.stop();
    emit itemChanged(currentIndex());
}
