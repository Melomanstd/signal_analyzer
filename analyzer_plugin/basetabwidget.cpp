#include "basetabwidget.h"
#include "ui_basetabwidget.h"
//wdawdawd

baseTabWidget::baseTabWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::baseTabWidget)
{
    headerPtr=0;
    footerPtr=0;
    sideBarPtr=0;
    ui->setupUi(this);
    ui->bntGroup->setVisible(false);
    //outputWidget=new QWidget(this);
    //ui->horizontalLayout->insertWidget(0,outputWidget);
    //outputLay=new QVBoxLayout(outputWidget);
    //QHBoxLayout d;
    //outputLay->setContentsMargins(0,0,5,0);

    //outputWidget->setLayout(outputLay);
    ui->verticalLayout->setParent(0);
    ui->widget->setLayout(ui->verticalLayout);
    ui->verticalLayout->setContentsMargins(2,2,2,2);
    //outputWidget->setLayout(ui->verticalLayout);
    //this->setLayout(ui->mainLayout);
}

baseTabWidget::~baseTabWidget()
{
    for (int i=0;i<btnList.size();i++)
        delete btnList[i];
    for (int i=0;i<actionList.size();i++)
        delete actionList[i];
    if (headerPtr) delete headerPtr;
    if (footerPtr) delete footerPtr;
    //delete outputLay;
    //delete outputWidget;
    delete ui;
}

QString baseTabWidget::getName() {
    return name;
}

void baseTabWidget::setName(QString name) {
    this->name = name;
}

void baseTabWidget::setPlot(QList<Plot *> *plots) {
    //this->plot=plots;
    for (int i=0;i<plots->size();i++)
        ui->verticalLayout->insertWidget(i+1,plots->at(i));
}

Plot *baseTabWidget::getPlot() {
    return this->plot;
}

QWidget* baseTabWidget::createHeader(QString text){
    if (text=="") return 0;
    QLabel *lbl = new QLabel;
    lbl->setText(text);
    headerPtr=lbl;
    insertHead(lbl);
    return lbl;
}

QWidget* baseTabWidget::createFooter(QString text) {
    //if (text=="") return 0;
    QLabel *lbl = new QLabel;
    lbl->setMaximumWidth(400);
    lbl->setText("");
    footerPtr=lbl;
    insertFoot(lbl);
    return lbl;
}

QWidget* baseTabWidget::createSideBar(QStringList names,
                                      QStringList btnsNames) {
    if (names.isEmpty()) return 0;
    for (int i=0; i<names.size();i++) {
        QPushButton* btn = new QPushButton;
        btn->setText(names.at(i));
        btn->setObjectName(btnsNames.at(i));
        btn->setCheckable(true);
        btnList.append(btn);
    }
    insertSideBar();
    return btnGroup();
}

QWidget* baseTabWidget::header() {
    return headerPtr;
}
QWidget* baseTabWidget::footer() {
    return footerPtr;
}
QWidget* baseTabWidget::sideBar() {
    return sideBarPtr;
}

void baseTabWidget::insertHead(QWidget *head) {
    ui->verticalLayout->insertWidget(0,head);
}

void baseTabWidget::insertFoot(QWidget *head) {
    ui->verticalLayout->insertWidget(3,head);
}

void baseTabWidget::insertSideBar(){
    for (int i=0;i<btnList.size(); i++)
        ui->btnLayout->addWidget(btnList.at(i));
    ui->bntGroup->setTitle("Meas Mode");
    ui->bntGroup->setVisible(true);
}

QWidget* baseTabWidget::btnGroup(){
    return ui->bntGroup;
}

QList<QPushButton *> *baseTabWidget::btnsList() {
    return &btnList;
}

QWidget* baseTabWidget::getOutputWidget() {
    return ui->widget;
}

QHBoxLayout* baseTabWidget::getLay() {
    return ui->horizontalLayout;
}

void baseTabWidget::addWidget(QWidget *w) {
    ui->verticalLayout->insertWidget(0,w);
}

QVBoxLayout* baseTabWidget::getOutputLay() {
    return ui->verticalLayout;
}

void baseTabWidget::createToolBarActions(QStringList &names)
{
    if (names.isEmpty())
        return;
    for (int i=0;i<names.size(); i++)
    {
        QAction *act = new QAction(names.at(i), 0);
        act->setCheckable(true);
        actionList.append(act);
    }
}

QList<QAction*>* baseTabWidget::getActions()
{
    return &actionList;
}
