#ifndef MODE_H
#define MODE_H
#include "renderthread.h"
#include "osciwidget.h"
#include "spectrewidget.h"
#include "iqanwidget.h"
#include "measconf.h"
#include "processobject.h"
#include <QTableWidget>
#include <QTreeWidget>
#include "peaksmarkerswidget.h"
#include "backgrounddialog.h"
//#include <QObject>

class OscMode;
class SpecMode;
class IqAnMode;
class ProcessObject;
class WorkerObject;
class RenderThread;
class MeasureConfigItem;


class Mode :public QObject
{

   Q_OBJECT

public:
    Mode(QObject *parent =0 );
    virtual ~Mode();
    virtual void plotInit(int legLblsCounter, QWidget *parent=0);
    virtual void renderInit(WorkerObject* w,
                            QString strIp,
                            QString strOscIp,
                            QObject *parent = 0);
    virtual void renderInit(RenderThread *r);
    virtual void renderSuspend();
    virtual void renderStart();
    virtual baseTabWidget* createWidget();
    virtual unsigned modeCnt();
    virtual void setLegElementNum();
    static Mode* createMode(int mode);
    Plot* getPlot();
    RenderThread* getRender();
    baseTabWidget* getWidget();
    int currentMode();
    QString getName();
    bool isUsed();
    void setUsed(bool b);
    void setTabPos(int x);
    int tabPos();
    void setTabName(QString name);
    QString getTabName();
    void setSignalState(int i, bool s);
    void renderConfInit(bool Ch1,
                        bool Ch2,
                        quint8 nMeasMode,
                        quint8 nChType,
                        quint8 nChType2,
                        quint8 nChAtten,
                        quint8 m_nChAtten2,
                        quint8 nTaktADC,
                        quint8 nGenMode,
                        double fFc,
                        quint32 nAmax,
                        quint32 nAmin,
                        double fFdev,
                        double fFsym,
                        double fFmod,
                        quint8 nSSBType,
                        qreal fK,
                        double fFs,
                        double fFcentr,
                        double fBand,
                        int nFftWindow,
                        quint8 sC1,
                        quint8 sC2,
                        quint8 tSc,
  #ifdef unused
                        Plot *pPlot,
  #endif
                        bool bCycle=false);
    MeasureConfigItem *getRenderConfig();
    ProcessObject* getProcess();
    void processInit(int i);
    int getMeasMode();
    void bntDown(int i);
    QList<Plot*>* getPlotList();
    void setTabIndex(int i);
    int getTabIndex();
    void setChanelColor(int nCh, QColor c);
    void setLegText(int pos, int nCh, QString text);
    void showPlotMarkers(int nCh, bool s);
    void setZoomerState(bool b);
    void markerColorChanged(int nMrk, QColor c);
    void deltaTabInit();
    QTreeWidget *getDeltaTab();
    void setMarkersFreeze(bool s);
    void setMarkersAsDefault();
    void setZoomerEnable(bool s);
    void peakTabInit();
    QWidget *getPeaksTab();
    void showPeaks(bool s);
    void clearPlots(int c = 0);
    static void clearConfig(MeasureConfigItem *conf);
    void changePlotsBackground(QGradient g);
    void changePlotsBackground(QPalette p);
    void changePlotGridColor(QColor c);
    void setLimFunc(bool s);
    bool getLimFunc();
    void updatePlotZoomer();


signals:
    void sendData(QString str1, QString str2, QString str3);
    void renderStarted(Mode *obj);

public slots:
    void readyData(QString str1, QString str2, QString str3);
    virtual void measModeChanged();
    void displayData(QString str1, QString str2, QString str3);
    virtual void displayMode(QString str1, QString str2, QString str3);
    void curveUpdate(int nCh);
    void updatePeaksTab();

protected:
    bool limFunc;
    //bool generatorState; //hidden/showed
    QMutex mutex;
    int measMode;
    Plot *plot;
    RenderThread *render;
    //OsciWidget *osci;
    //SpectreWidget *spectre;
    //IqAnWidget *iqAn;
    baseTabWidget *tabWidget;
    int currMode;
    QString modeName;
    QString tabName;
    bool used;
    int mainTabPos;
    int tabIndex;
    MeasureConfigItem *renderConf;
    ProcessObject *process;
    QList<Plot *> plots;
    QTreeWidget *deltaTab;
    QTreeWidget *peaksTab;
    PeaksMarkersWidget *peaksWidget;

    QTimer *peaksTimer;

public:
    bool initDone;
    //QAction *showGen;
    static unsigned modeCounter;
    enum ModeID {
    OSCI_ID,
    SPEC_ID,
    IQAN_ID,
    COUNT_ID};

    QString modes[COUNT_ID];
};


//Mode`s child classes
class OscMode:public Mode {
    Q_OBJECT
public:
    OscMode(QObject *parent=0);
    ~OscMode();
    baseTabWidget* createWidget();
    virtual unsigned modeCnt();
    virtual void setLegElementNum();

public slots:
    void measModeChanged();
    void displayMode(QString str1, QString str2, QString str3);

public:
    static unsigned oscCounter;
};

class SpecMode:public Mode {
    Q_OBJECT
public:
    SpecMode(QObject *parent=0);
    ~SpecMode();
    baseTabWidget* createWidget();
    virtual unsigned modeCnt();
    virtual void setLegElementNum();

public slots:
    void measModeChanged();
    void displayMode(QString str1, QString str2, QString str3);

public:
    static unsigned SpecCounter;
};

class IqAnMode:public Mode {
    Q_OBJECT
public:
    IqAnMode(QObject *parent=0);
    ~IqAnMode();
    baseTabWidget* createWidget();
    virtual unsigned modeCnt();
    void plotInit(int legLblsCounter, QWidget *parent);
    virtual void setLegElementNum();


public slots:
    void measModeChanged();
    void displayMode(QString str1, QString str2, QString str3);
    void zoomerCreated();

public:
    static unsigned iqAnCounter;
};

#endif // MODE_H
