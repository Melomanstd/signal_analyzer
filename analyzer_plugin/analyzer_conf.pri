#-------------------------------------------------
#
# Project created by QtCreator 2011-08-19T12:09:15
#
#-------------------------------------------------


# This is a GUI application
QT       += core gui network svg

#TARGET = analyzerLib
#TEMPLATE = lib#app

#LIBS += C:/Projects/pluginsProject/Release/libAnalyzerWorkerLib.a#C:\Projects\analyzerTest\AnalyzerWorkerLib-build-Desktop_Qt_4_8_4-_______\debug\libAnalyzerWorkerLib.a


# Include aspectran module from ASDK
CONFIG += warn_on aspectran
#DESTDIR = C:/Projects/pluginsProject/Release
DLLDESTDIR = build/
OBJECTS_DIR = build/obj
MOC_DIR = build/moc
UI_DIR = build/ui
RCC_DIR = build/rcc

# The project itself just contains the user interface, the actual signal processing is done by the decoder in the ASDK
SOURCES += main.cpp\
        adcfdemomainwindow.cpp \
    signaldata.cpp \
    samplingthread.cpp \
    plot.cpp \
    curvedata.cpp \
    wheelbox.cpp \
    knob.cpp \
    renderthread.cpp \
    fortunethread.cpp \
    fortuneserver.cpp \
    workerobject.cpp \
    thread.cpp \
    measconf.cpp \
    colorlabel.cpp \
    basetabwidget.cpp \
    osciwidget.cpp \
    spectrewidget.cpp \
    iqanwidget.cpp \
    mode.cpp \
    modedialog.cpp \
    modsmainwidget.cpp \
    plotdialog.cpp \
    processobject.cpp \
    plotzoomer.cpp \
    checkingthread.cpp \
    plotgrid.cpp \
    plotmarker.cpp \
    plotcurve.cpp \
    plotlegend.cpp \
    canvaspicker.cpp \
    peaksmarkerswidget.cpp \
    timercombobox.cpp \
    analyzerlib.cpp \
    timertabwidget.cpp \
    backgrounddialog.cpp

HEADERS  += adcfdemomainwindow.h \
    signaldata.h \
    samplingthread.h \
    plot.h \
    curvedata.h \
    wheelbox.h \
    knob.h \
    renderthread.h \
    fortuneserver.h \
    fortunethread.h \
    thread.h \
    workerobject.h \
    measconf.h \
    colorlabel.h \
    basetabwidget.h \
    osciwidget.h \
    spectrewidget.h \
    iqanwidget.h \
    mode.h \
    modedialog.h \
    modsmainwidget.h \
    plotdialog.h \
    processobject.h \
    plotzoomer.h \
    checkingthread.h \
    plotgrid.h \
    plotmarker.h \
    plotcurve.h \
    plotlegend.h \
    canvaspicker.h \
    peaksmarkerswidget.h \
    timercombobox.h \
    analyzerlib_global.h \
    analyzerlib.h \
    timertabwidget.h \
    backgrounddialog.h

FORMS    += adcfdemomainwindow.ui \
    table1.ui \
    basetabwidget.ui \
    modedialog.ui \
    modsmainwidget.ui \
    plotdialog.ui \
    peaksmarkerswidget.ui \
    backgrounddialog.ui

RESOURCES += \
    res.qrc

QWT_ROOT=../qwt-6.0.1
# ASDK
#include($$quote($$(ASDK)/asdk.pri))

# Qwt
include( $$quote($${QWT_ROOT}/qwtconfig.pri) )
#include( $$quote($$(QWT_ROOT)/qwtbuild.pri) )

INCLUDEPATH += $${QWT_ROOT}/src
DEPENDPATH  += $${QWT_ROOT}/src
QMAKE_RPATHDIR *= $${QWT_ROOT}/lib
#LIBS      += -L$$(QWT_ROOT)/lib -lqwtd

PROJECT_PATH=\"$$PWD\"

INCLUDEPATH += ../analyzer_drv \
    ../../plugintest/TestPlugin

CONFIG(debug, debug|release) {
        LIBS      += -L$${QWT_ROOT}/lib -lqwtd
        #DESTDIR = $$quote($${PROJECT_PATH}/../../pluginsProject)
        #LIBS += $$quote($${PROJECT_PATH}/../../pluginsProject/liblibsadrv.a)
        #DEFINES += QT_NO_DEBUG_OUTPUT
        DESTDIR = ../../bin
        LIBS += -L ../../bin/ -lsa_drv#../../bin/libsa_drv.a
}else{
        LIBS      += -L$${QWT_ROOT}/lib -lqwt
        #DESTDIR = $$quote($${PROJECT_PATH}/../../pluginsProject/Release)
        #LIBS += $$quote($${PROJECT_PATH}/../../pluginsProject/Release/liblibsadrv.a)
        #DEFINES += QT_NO_DEBUG_OUTPUT
        DESTDIR = ../../bin/Release
        LIBS += -L ../../bin/Release -lsa_drv#../../bin/Release/libsa_drv.a
}
#qtAddLibrary(qwt)
DEFINES += QT_DLL QWT_DLL \
    ANALYZERLIB_LIBRARY

TRANSLATIONS = project_ru_an.ts
