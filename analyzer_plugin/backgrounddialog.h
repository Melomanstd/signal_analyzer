#ifndef BACKGROUNDDIALOG_H
#define BACKGROUNDDIALOG_H

#include <QDialog>
#include <QColorDialog>

namespace Ui {
class BackgroundDialog;
}

class BackgroundDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit BackgroundDialog(QWidget *parent = 0);
    ~BackgroundDialog();
    QLinearGradient getBackground();
    void setCurrentColor(QPalette p);
    QColor get1stColor();
    QColor get2ndColor();

public slots:
    void set1stColor();
    void set2ndColor();
    void setSignleColor();
    
private:
    Ui::BackgroundDialog *ui;

    QLinearGradient *gradient;
};

#endif // BACKGROUNDDIALOG_H
