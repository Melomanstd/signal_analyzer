<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>ADcfDemoMainWindow</name>
    <message>
        <location filename="adcfdemomainwindow.ui" line="26"/>
        <source>ADcfDemoMainWindow</source>
        <translation type="unfinished">АПАК КД</translation>
    </message>
    <message>
        <source>Decoder State:</source>
        <translation type="obsolete">Состояние:</translation>
    </message>
    <message>
        <source>not connected</source>
        <translation type="obsolete">не подключен</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2118"/>
        <location filename="adcfdemomainwindow.cpp" line="1944"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <source>Scan</source>
        <translation type="obsolete">Сканировать</translation>
    </message>
    <message>
        <source>US &quot;Proliv&quot; frequencies</source>
        <translation type="obsolete">Частоты УС &quot;Пролив&quot;</translation>
    </message>
    <message>
        <source>DPC Limit:</source>
        <translation type="obsolete">Ограничения ДПЧ:</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>Prediction</source>
        <translation type="obsolete">Прогноз</translation>
    </message>
    <message>
        <source>KMS</source>
        <translation type="obsolete">КМС</translation>
    </message>
    <message>
        <source>Net &quot;Vyuga&quot;, &quot;Monolit&quot;, &quot;Proliv&quot;</source>
        <translation type="obsolete">Р/с &quot;Вьюга&quot;, &quot;Монолит&quot;, ОБУ УС &quot;Пролив&quot;</translation>
    </message>
    <message>
        <source>&quot;Vyuga&quot;</source>
        <translation type="obsolete">&quot;Вьюга&quot;</translation>
    </message>
    <message>
        <source>&quot;Monolit&quot;</source>
        <translation type="obsolete">&quot;Монолит&quot;</translation>
    </message>
    <message>
        <source>OBU</source>
        <translation type="obsolete">ОБУ</translation>
    </message>
    <message>
        <source>US &quot;Tribuna&quot; frequencies</source>
        <translation type="obsolete">Частоты УС &quot;Трибуна&quot;</translation>
    </message>
    <message>
        <source>Net &quot;Tribuna&quot;</source>
        <translation type="obsolete">Р/с ОБУ УС &quot;Трибуна&quot;</translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="199"/>
        <source>Записать</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="219"/>
        <source>Параметры тактирования</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="255"/>
        <source>Тактирование АЦП:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="278"/>
        <source>Тактирование PLL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="301"/>
        <source>Выход «CLK OUT»:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>APAK KD - auto mode</source>
        <translation type="obsolete">АПАК КД - автоматический режим</translation>
    </message>
    <message>
        <source>Switch to manual mode</source>
        <translation type="obsolete">Перейти в ручной режим</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Подключение</translation>
    </message>
    <message>
        <source>IP address:</source>
        <translation type="obsolete">IP адрес:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation type="obsolete">Порт:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="388"/>
        <location filename="adcfdemomainwindow.cpp" line="1268"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="589"/>
        <source>Channel settings</source>
        <translation>Параметры канала</translation>
    </message>
    <message>
        <source>Channel:</source>
        <translation type="obsolete">Канал:</translation>
    </message>
    <message>
        <source>Input:</source>
        <translation type="obsolete">Вход:</translation>
    </message>
    <message>
        <source>Attenuator:</source>
        <translation type="obsolete">Аттенюатор:</translation>
    </message>
    <message>
        <source>Spectr analyze</source>
        <translation type="obsolete">Анализ спектра</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1171"/>
        <source>Fs, MHz:</source>
        <translation type="unfinished">Дискретизация, MHz:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1204"/>
        <source>Fcentr, MHz:</source>
        <translation type="unfinished">Центр. частота, MHz:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1237"/>
        <source>Bandwidth, kHz:</source>
        <translation type="unfinished">Ширина полосы, KHz:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1276"/>
        <source>FFT window:</source>
        <translation>Тип окна:</translation>
    </message>
    <message>
        <source>Generatot</source>
        <translation type="obsolete">Генератор</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="118"/>
        <location filename="adcfdemomainwindow.cpp" line="1190"/>
        <source>Osci</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="123"/>
        <location filename="adcfdemomainwindow.cpp" line="1191"/>
        <source>Spec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="438"/>
        <source>Repeat</source>
        <translation>Цикл</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="451"/>
        <source>Check All</source>
        <translation>Опрашивать все</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="464"/>
        <location filename="adcfdemomainwindow.cpp" line="2761"/>
        <location filename="adcfdemomainwindow.cpp" line="2776"/>
        <source>StartChecking</source>
        <translation>Начать опрос</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="668"/>
        <source>Chanel1</source>
        <translation>Канал 1</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="685"/>
        <source>Chanel2</source>
        <translation>Канал 2</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="819"/>
        <source>Timescale</source>
        <translation>Делений на секцию:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="917"/>
        <source>Trigger</source>
        <translation>Триггер</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="962"/>
        <location filename="adcfdemomainwindow.ui" line="1537"/>
        <source>Mode:</source>
        <translation type="unfinished">Режим:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="992"/>
        <source>Slope:</source>
        <translation>Наклон</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1022"/>
        <source>Source: </source>
        <translation>Источник</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1052"/>
        <source>Level:</source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1145"/>
        <source>Spectre analyze</source>
        <translation>Анализ спектра</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1371"/>
        <source>Gain</source>
        <translation>Усиление</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1394"/>
        <source>Hard gain value(1):</source>
        <translation>Грубое усиление 1Ф</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1431"/>
        <source>Hard gain value(2):</source>
        <translation>Грубое усиление 2Ф</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1468"/>
        <source>Accur gain value(1):</source>
        <translation>Точное усиление</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1506"/>
        <source>Generator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1579"/>
        <source>n/d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1609"/>
        <source>Fmod:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1641"/>
        <source>Fsym:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1670"/>
        <source>Fdev:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1716"/>
        <source>Fc:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1745"/>
        <source>K:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1774"/>
        <source>Neg/Pos:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1885"/>
        <source>Cursors</source>
        <translation>Курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1965"/>
        <source>Measure</source>
        <translation>Измерение</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2007"/>
        <source>Background Color</source>
        <translation>Цвет фона</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2020"/>
        <location filename="adcfdemomainwindow.ui" line="2066"/>
        <source>Change</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2053"/>
        <source>Grid Color</source>
        <translation>Цвет сетки</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2102"/>
        <source>Cursor Show</source>
        <translation>Вкл. курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2089"/>
        <source>Peaks</source>
        <translation>Вкл. пики</translation>
    </message>
    <message>
        <source>Mode: </source>
        <translation type="obsolete">Режим:</translation>
    </message>
    <message>
        <source>Result2:</source>
        <translation type="obsolete">Мощность, дБм:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2131"/>
        <source>Cycle</source>
        <translation>Цикл</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2178"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2183"/>
        <source>Set System Time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="2188"/>
        <source>Reset Decoder</source>
        <translation></translation>
    </message>
    <message>
        <source>connecting ...</source>
        <translation type="obsolete">подключение...</translation>
    </message>
    <message>
        <source>connected to %1</source>
        <translation type="obsolete">подключен к %1</translation>
    </message>
    <message>
        <source>no HF device found</source>
        <translation type="obsolete">нет доступных устройств</translation>
    </message>
    <message>
        <source>decoding data</source>
        <translation type="obsolete">измерение</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1169"/>
        <source>f1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1170"/>
        <source>f2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1171"/>
        <source>f3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1172"/>
        <source>f4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1175"/>
        <location filename="adcfdemomainwindow.cpp" line="1181"/>
        <source>Number</source>
        <translation type="unfinished">Номер</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1176"/>
        <location filename="adcfdemomainwindow.cpp" line="1179"/>
        <location filename="adcfdemomainwindow.cpp" line="1182"/>
        <location filename="adcfdemomainwindow.cpp" line="1185"/>
        <source>Frequency</source>
        <translation type="unfinished">Частота</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1177"/>
        <location filename="adcfdemomainwindow.cpp" line="1180"/>
        <location filename="adcfdemomainwindow.cpp" line="1183"/>
        <location filename="adcfdemomainwindow.cpp" line="1186"/>
        <source>dBm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1178"/>
        <location filename="adcfdemomainwindow.cpp" line="1184"/>
        <source>Net</source>
        <translation type="unfinished">Р/с</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1925"/>
        <source>Stop</source>
        <translation type="unfinished">Стоп</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1782"/>
        <location filename="adcfdemomainwindow.cpp" line="1985"/>
        <location filename="adcfdemomainwindow.cpp" line="2035"/>
        <source>File error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Channel 1</source>
        <translation type="obsolete">Канал 1</translation>
    </message>
    <message>
        <source>Channel 2</source>
        <translation type="obsolete">Канал 2</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="88"/>
        <source>Show Gen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="146"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="147"/>
        <source>Normal</source>
        <translation>Нормальный</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="150"/>
        <source>Chanel 1</source>
        <translation>Канал 1</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="151"/>
        <source>Chanel 2</source>
        <translation>Канал 2</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="152"/>
        <source>External</source>
        <translation>Внешний</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="175"/>
        <source>Freeze Cursors</source>
        <translation>Блок. курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="180"/>
        <source>Default</source>
        <translation>По умолч.</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="185"/>
        <source>Cursor 1</source>
        <translation>Курсор 1</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="187"/>
        <source>Cursor 2</source>
        <translation>Курсор 2</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="332"/>
        <location filename="adcfdemomainwindow.cpp" line="337"/>
        <source>50 Ohm</source>
        <translation>50 Ом</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="333"/>
        <location filename="adcfdemomainwindow.cpp" line="338"/>
        <source>600 Ohm Diff.</source>
        <translation>600 Ом</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="334"/>
        <location filename="adcfdemomainwindow.cpp" line="339"/>
        <source>1 MOhm/20pF</source>
        <translation>1 МОм</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="417"/>
        <source>1 Mks</source>
        <translation>1 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="418"/>
        <source>2 Mks</source>
        <translation>2 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="419"/>
        <source>5 Mks</source>
        <translation>5 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="420"/>
        <source>10 Mks</source>
        <translation>10 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="421"/>
        <source>20 Mks</source>
        <translation>20 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="422"/>
        <source>50 Mks</source>
        <translation>50 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="423"/>
        <source>100 Mks</source>
        <translation>100 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="424"/>
        <source>200 Mks</source>
        <translation>200 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="425"/>
        <source>500 Mks</source>
        <translation>500 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="426"/>
        <source>1 Ms</source>
        <translation>1 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="427"/>
        <source>2 Ms</source>
        <translation>2 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="428"/>
        <source>5 Ms</source>
        <translation>5 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="429"/>
        <source>10 Ms</source>
        <translation>10 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="430"/>
        <source>20 Ms</source>
        <translation>20 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="431"/>
        <source>50 Ms</source>
        <translation>50 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="432"/>
        <source>100 Ms</source>
        <translation>100 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="433"/>
        <source>200 Ms</source>
        <translation>200 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="434"/>
        <source>500 Ms</source>
        <translation>500 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="435"/>
        <source>1 Sec</source>
        <translation>1 Сек</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="436"/>
        <source>2 Sec</source>
        <translation>2 Сек</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="437"/>
        <source>5 Sec</source>
        <translation>5 Сек</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="444"/>
        <source>1 mV/d</source>
        <translation>1 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="445"/>
        <source>2 mV/d</source>
        <translation>2 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="446"/>
        <source>5 mV/d</source>
        <translation>5 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="447"/>
        <source>10 mV/d</source>
        <translation>10 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="448"/>
        <source>20 mV/d</source>
        <translation>20 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="449"/>
        <source>50 mV/d</source>
        <translation>50 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="450"/>
        <source>100 mV/d</source>
        <translation>100 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="451"/>
        <source>200 mV/d</source>
        <translation>200 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="452"/>
        <source>500 mV/d</source>
        <translation>500 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="453"/>
        <source>1 V/d</source>
        <translation>1 В/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="454"/>
        <source>2 V/d</source>
        <translation>2 В/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="455"/>
        <source>5 V/d</source>
        <translation>5 В/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="456"/>
        <source>10 V/d</source>
        <translation>10 В/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="470"/>
        <source>DC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="471"/>
        <source>AC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="472"/>
        <source>GND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="482"/>
        <source>rectangular</source>
        <translation type="unfinished">Отсутствует</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="483"/>
        <source>hann</source>
        <translation type="unfinished">Ханна</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="484"/>
        <source>hamming</source>
        <translation type="unfinished">Хэминга</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="485"/>
        <source>blackman</source>
        <translation type="unfinished">Блэкмана</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="486"/>
        <source>blackmanharris</source>
        <translation type="unfinished">Блэкмена-Харриса</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="487"/>
        <source>flattopwin</source>
        <translation type="unfinished">С плоской вершиной</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="528"/>
        <source>Negative</source>
        <translation type="unfinished">Негатив</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="529"/>
        <source>Positive</source>
        <translation type="unfinished">Позитив</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1189"/>
        <source>MAIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1192"/>
        <source>IQ Analyzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1195"/>
        <source>Add Tab</source>
        <translation>Добавить вкладку</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2476"/>
        <source>Main</source>
        <translation type="unfinished">Мульти окно</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2753"/>
        <source>StopChecking</source>
        <translation>Завершить опрос</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2825"/>
        <source>Hide Cursors</source>
        <translation>Выкл. курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2833"/>
        <source>Show Cursors</source>
        <translation>Вкл. курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2917"/>
        <source>Hide peaks</source>
        <translation>Выкл. пики</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2920"/>
        <source>Show peaks</source>
        <translation>Вкл. пики</translation>
    </message>
    <message>
        <source>Am</source>
        <translation type="obsolete">Глубина АМ</translation>
    </message>
    <message>
        <source>Fm</source>
        <translation type="obsolete">Девиация ЧМ</translation>
    </message>
    <message>
        <source>Osc</source>
        <translation type="obsolete">Осциллограмма</translation>
    </message>
    <message>
        <source>Fft</source>
        <translation type="obsolete">Спектр</translation>
    </message>
    <message>
        <source>Freq</source>
        <translation type="obsolete">Частотомер</translation>
    </message>
    <message>
        <source>SINAD</source>
        <translation type="obsolete">КНИ/SINAD</translation>
    </message>
    <message>
        <source>TLG</source>
        <translation type="obsolete">Искажения ТЛГ</translation>
    </message>
    <message>
        <source>Psopho</source>
        <translation type="obsolete">Псофометр</translation>
    </message>
    <message>
        <source>Sens SINAD</source>
        <translation type="obsolete">Чувствительность SINAD</translation>
    </message>
    <message>
        <source>Sens TLG</source>
        <translation type="obsolete">Чувствительность ТЛГ</translation>
    </message>
    <message>
        <source>AFC</source>
        <translation type="obsolete">АЧХ</translation>
    </message>
    <message>
        <source>Deep AM, %:</source>
        <translation type="obsolete">Глубина АМ, %:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="858"/>
        <location filename="adcfdemomainwindow.cpp" line="974"/>
        <location filename="adcfdemomainwindow.cpp" line="1033"/>
        <source>V</source>
        <translation type="unfinished">В</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="859"/>
        <location filename="adcfdemomainwindow.cpp" line="975"/>
        <location filename="adcfdemomainwindow.cpp" line="1034"/>
        <source>mks</source>
        <translation type="unfinished">мкс</translation>
    </message>
    <message>
        <source>Deviation FM, Hz:</source>
        <translation type="obsolete">Девиация ЧМ, Гц:</translation>
    </message>
    <message>
        <source>SKZ, V:</source>
        <translation type="obsolete">СКЗ, В:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="903"/>
        <location filename="adcfdemomainwindow.cpp" line="931"/>
        <location filename="adcfdemomainwindow.cpp" line="953"/>
        <location filename="adcfdemomainwindow.cpp" line="992"/>
        <location filename="adcfdemomainwindow.cpp" line="1015"/>
        <location filename="adcfdemomainwindow.cpp" line="1052"/>
        <source>dBV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="904"/>
        <location filename="adcfdemomainwindow.cpp" line="954"/>
        <location filename="adcfdemomainwindow.cpp" line="1016"/>
        <location filename="adcfdemomainwindow.cpp" line="1053"/>
        <source>MHz</source>
        <translation type="unfinished">МГц</translation>
    </message>
    <message>
        <source>Freq, Hz:</source>
        <translation type="obsolete">Частота, Гц:</translation>
    </message>
    <message>
        <source>KNI, %:</source>
        <translation type="obsolete">КНИ/КГИ, %:</translation>
    </message>
    <message>
        <source>KVP, %:</source>
        <translation type="obsolete">КВП, %:</translation>
    </message>
    <message>
        <source>Upsoph, V:</source>
        <translation type="obsolete">Uпсоф, В:</translation>
    </message>
    <message>
        <source>U, mkV:</source>
        <translation type="obsolete">U, мкВ:</translation>
    </message>
    <message>
        <source>Nonliner, dB:</source>
        <translation type="obsolete">Нелинейность, dB:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1268"/>
        <source>Close</source>
        <translation type="unfinished">Закрыть</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1783"/>
        <location filename="adcfdemomainwindow.cpp" line="1986"/>
        <location filename="adcfdemomainwindow.cpp" line="2036"/>
        <source>Failed to open
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2085"/>
        <source> mkV</source>
        <translation type="unfinished">мкВ</translation>
    </message>
    <message>
        <source>Switch to auto mode</source>
        <translation type="obsolete">Перейти в автоматический режим</translation>
    </message>
    <message>
        <source>APAK KD - manual mode</source>
        <translation type="obsolete">АПАК КД - ручной режим</translation>
    </message>
</context>
<context>
    <name>AnalyzerPlugin</name>
    <message>
        <location filename="analyzerlib.cpp" line="94"/>
        <source>Analyzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="109"/>
        <source>Configure Channel</source>
        <translation>Задать параметры канала</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="114"/>
        <source>Configure Timescale</source>
        <translation>Задать разверстку</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="119"/>
        <source>Configure Spectre</source>
        <translation>Задать параметры спекта</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="124"/>
        <source>Measure</source>
        <translation>Измерение</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="142"/>
        <location filename="analyzerlib.cpp" line="208"/>
        <source>Channel</source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="143"/>
        <source>1
2
</source>
        <translation>1
2
</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="149"/>
        <source>Input</source>
        <translation>Вход</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="150"/>
        <source>50Om
600Om
1MOm
</source>
        <translation>50Ом
600Ом
1MОм
</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="156"/>
        <source>Attenuator</source>
        <translation>Аттенюатор</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="157"/>
        <source>1/1
1/10
1/30
1/300
</source>
        <translation>1/1
1/10
1/30
1/300
</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="163"/>
        <source>Range</source>
        <translation>Диапазон</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="164"/>
        <source>1mV/s
2mV/s
5mV/s
10mV/s
20mV/s
50mV/s
100mV/s
200mV/s
500mV/s
1V/s
2V/s
5V/s
10V/s
</source>
        <translation>1мВ/д
2мВ/д
5мВ/д
10мВ/д
20мВ/д
50мВ/д
100мВ/д
200мВ/д
500мВ/д
1В/д
2В/д
5В/д
10В/д
</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="172"/>
        <source>Timescale</source>
        <translation>Делений на секцию:</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="173"/>
        <source>1mkS
2mkS
5mkS
10mkS
20mkS
50mkS
100mkS
200mkS
500mkS
1mS
2mS
5mS
10mS
20mS
50mS
100mS
200mS
500mS
1Sec
2Sec
5Sec</source>
        <translation>1мкС
2мкС
5мкС
10мкС
20мкС
50мкС
100мкС
200мкС
500мкС
1мС
2мС
5мС
10мС
20мС
50мС
100мС
200мС
500мС
1Сек
2Сек
5Сек
</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="182"/>
        <source>Central Frequency</source>
        <translation>Центр Частота</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="189"/>
        <source>Bandwidth</source>
        <translation>Ширина Полосы</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="196"/>
        <source>Window</source>
        <translation>Окно</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="197"/>
        <source>ÐÑÑÑÑÑÑÐ²ÑÐµÑ
Ð¥Ð°Ð½Ð½Ð°
Ð¥ÑÐ¼Ð¸Ð½Ð³Ð°
ÐÐ»ÑÐºÐ¼Ð°Ð½Ð°
ÐÐ»ÑÐºÐ¼Ð°Ð½Ð°-Ð¥Ð°ÑÑÐ¸ÑÐ°
Ð¡ Ð¿Ð»Ð¾ÑÐºÐ¾Ð¹ Ð²ÐµÑÑÐ¸Ð½Ð¾Ð¹
</source>
        <translation>Отсутствует\nХанна\nХэминга\nБлэкмана\nБлэкмана-Харриса\nС плоской вершиной\n</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="209"/>
        <source>1 Channel
2 Channel
</source>
        <translation>1 Канал\n2 Канал\n</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="215"/>
        <source>Measuring Mode</source>
        <translation>Режим Измерения</translation>
    </message>
    <message>
        <location filename="analyzerlib.cpp" line="216"/>
        <source>Deep
Deviation
Oscilograph, SKZ, Amplitude, Ti, T
Spectre Analyzer
Frequency
KNI
Deformation TLG
PHOSO
</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AutoWindow</name>
    <message>
        <source>Listen port:</source>
        <translation type="obsolete">Порт:</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="obsolete">Сервер</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="obsolete">Старт</translation>
    </message>
    <message>
        <source>Server IP:</source>
        <translation type="obsolete">IP адрес:</translation>
    </message>
    <message>
        <source>ARM ARK</source>
        <translation type="obsolete">АРМ АРК</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Дата</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Время</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="obsolete">Сообщение</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="obsolete">Стоп</translation>
    </message>
    <message>
        <source>Server stopped</source>
        <translation type="obsolete">Остановка сервера</translation>
    </message>
    <message>
        <source>Server started</source>
        <translation type="obsolete">Запуск сервера</translation>
    </message>
</context>
<context>
    <name>BackgroundDialog</name>
    <message>
        <location filename="backgrounddialog.ui" line="14"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="backgrounddialog.ui" line="22"/>
        <source>Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="backgrounddialog.ui" line="32"/>
        <source>Color 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="backgrounddialog.ui" line="39"/>
        <source>Color 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="backgrounddialog.ui" line="53"/>
        <location filename="backgrounddialog.ui" line="63"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="table1.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="32"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="39"/>
        <source>Listen port:</source>
        <translation type="unfinished">Порт:</translation>
    </message>
    <message>
        <location filename="table1.ui" line="59"/>
        <location filename="table1.ui" line="127"/>
        <source>Start</source>
        <translation type="unfinished">Старт</translation>
    </message>
    <message>
        <location filename="table1.ui" line="72"/>
        <source>Server</source>
        <translation type="unfinished">Сервер</translation>
    </message>
    <message>
        <location filename="table1.ui" line="108"/>
        <source>Mulfunction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="134"/>
        <source>Server port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="160"/>
        <source>Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="180"/>
        <source>Server IP:</source>
        <translation type="unfinished">IP адрес:</translation>
    </message>
    <message>
        <location filename="table1.ui" line="187"/>
        <source>Archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="194"/>
        <source>Calibrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="201"/>
        <source>Measure</source>
        <translation type="unfinished">Измерение</translation>
    </message>
</context>
<context>
    <name>FortuneServer</name>
    <message>
        <location filename="fortuneserver.cpp" line="50"/>
        <source>You&apos;ve been leading a dog&apos;s life. Stay off the furniture.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="51"/>
        <source>You&apos;ve got to think about tomorrow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="52"/>
        <source>You will be surprised by a loud noise.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="53"/>
        <source>You will feel hungry again in another hour.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="54"/>
        <source>You might have mail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="55"/>
        <source>You cannot kill time without injuring eternity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="56"/>
        <source>Computers are not intelligent. They only think they are.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IqAnMode</name>
    <message>
        <location filename="mode.cpp" line="851"/>
        <source>IQ Analayzer</source>
        <translation>IQ анализатор</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="853"/>
        <source>Deep</source>
        <translation>Глубина</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="854"/>
        <source>Deviaton</source>
        <translation>Девиация</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="855"/>
        <source>Deformation TLG</source>
        <translation>Деформация TLG</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="856"/>
        <source>IQANTEST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mode.cpp" line="857"/>
        <source>IQ-Vector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mode.cpp" line="865"/>
        <source>IQANALYZER</source>
        <translation>IQ анализатор</translation>
    </message>
</context>
<context>
    <name>IqAnWidget</name>
    <message>
        <location filename="iqanwidget.cpp" line="6"/>
        <source>IQ Analayzer</source>
        <translation>IQ анализатор</translation>
    </message>
</context>
<context>
    <name>Mode</name>
    <message>
        <location filename="mode.cpp" line="26"/>
        <source>Oscillograph</source>
        <translation>Осциллограф</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="27"/>
        <source>Spectre</source>
        <translation>Анализатор спектра</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="28"/>
        <source>IqAnalyzer</source>
        <translation>IQ анализатор</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="316"/>
        <location filename="mode.cpp" line="317"/>
        <source>Cursor: %1</source>
        <translation>Курсор: %1</translation>
    </message>
</context>
<context>
    <name>ModeDialog</name>
    <message>
        <location filename="modedialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modedialog.ui" line="49"/>
        <source>Ok</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="modedialog.ui" line="56"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="modedialog.cpp" line="9"/>
        <source>New mode</source>
        <translation>Новый режим</translation>
    </message>
</context>
<context>
    <name>ModsMainWidget</name>
    <message>
        <location filename="modsmainwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modsmainwidget.ui" line="77"/>
        <location filename="modsmainwidget.ui" line="148"/>
        <location filename="modsmainwidget.ui" line="213"/>
        <location filename="modsmainwidget.ui" line="278"/>
        <source>NONE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modsmainwidget.ui" line="97"/>
        <source>Dialog1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modsmainwidget.cpp" line="10"/>
        <location filename="modsmainwidget.cpp" line="11"/>
        <location filename="modsmainwidget.cpp" line="12"/>
        <location filename="modsmainwidget.cpp" line="13"/>
        <source>Insert mode</source>
        <translation>Добавить наблюдаемый режим</translation>
    </message>
    <message>
        <location filename="modsmainwidget.cpp" line="14"/>
        <location filename="modsmainwidget.cpp" line="15"/>
        <location filename="modsmainwidget.cpp" line="16"/>
        <location filename="modsmainwidget.cpp" line="17"/>
        <source>Remove mode</source>
        <translation>Удалить наблюдаемый режим</translation>
    </message>
    <message>
        <location filename="modsmainwidget.cpp" line="18"/>
        <location filename="modsmainwidget.cpp" line="19"/>
        <location filename="modsmainwidget.cpp" line="20"/>
        <location filename="modsmainwidget.cpp" line="21"/>
        <source>Move to tab</source>
        <translation>Перейти на вкладку</translation>
    </message>
</context>
<context>
    <name>OscMode</name>
    <message>
        <location filename="mode.cpp" line="639"/>
        <location filename="mode.cpp" line="641"/>
        <location filename="mode.cpp" line="645"/>
        <source>Oscillograph</source>
        <translation>Осциллограф</translation>
    </message>
</context>
<context>
    <name>OsciWidget</name>
    <message>
        <location filename="osciwidget.cpp" line="6"/>
        <source>Oscillograph</source>
        <oldsource>Oscillographrewtre</oldsource>
        <translation>Осциллограф</translation>
    </message>
</context>
<context>
    <name>PeaksMarkersWidget</name>
    <message>
        <location filename="peaksmarkerswidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlotDialog</name>
    <message>
        <location filename="plotdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Server</name>
    <message>
        <source>Server: Incoming connection</source>
        <translation type="obsolete">Сервер: новое подключение</translation>
    </message>
    <message>
        <source>Server: Reject incoming connection</source>
        <translation type="obsolete">Сервер: новое подключение игнорировано</translation>
    </message>
    <message>
        <source>Server: Close connection</source>
        <translation type="obsolete">Сервер: закрытие соединения</translation>
    </message>
</context>
<context>
    <name>ServerWorker</name>
    <message>
        <source> Server: Connect received</source>
        <translation type="obsolete">Сервер: открытие сеанса связи</translation>
    </message>
    <message>
        <source> Server: Disconnect received</source>
        <translation type="obsolete">Сервер: закрытие еанса связи</translation>
    </message>
    <message>
        <source> Server: Request received</source>
        <translation type="obsolete">Сервер: новый запрос</translation>
    </message>
    <message>
        <source> Server: Request finished</source>
        <translation type="obsolete">Сервер: запрос выполнен</translation>
    </message>
</context>
<context>
    <name>SpecMode</name>
    <message>
        <location filename="mode.cpp" line="723"/>
        <location filename="mode.cpp" line="725"/>
        <source>Spectre</source>
        <translation>Анализатор спектра</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="726"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="727"/>
        <source>KNI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mode.cpp" line="728"/>
        <source>PHOSO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mode.cpp" line="735"/>
        <source>SPECTRE</source>
        <translation>Анализатор спектра</translation>
    </message>
</context>
<context>
    <name>SpectreWidget</name>
    <message>
        <location filename="spectrewidget.cpp" line="6"/>
        <source>Spectre</source>
        <translation>Анализатор спектра</translation>
    </message>
</context>
<context>
    <name>baseTabWidget</name>
    <message>
        <location filename="basetabwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="basetabwidget.ui" line="31"/>
        <source>GroupBox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
