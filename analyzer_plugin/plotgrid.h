/* -*- mode: C++ ; c-file-style: "stroustrup" -*- *****************************
 * Qwt Widget Library
 * Copyright (C) 1997   Josef Wilgen
 * Copyright (C) 2002   Uwe Rathmann
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Qwt License, Version 1.0
 *****************************************************************************/

#ifndef PLOT_GRID_H
#define PLOT_GRID_H

#include "qwt_plot_grid.h"
#include "plot.h"

//#include "qwt_global.h"
//#include "qwt_plot_item.h"
//#include "qwt_scale_div.h"

//class QPainter;
//class QPen;
//class QwtScaleMap;
//class QwtScaleDiv;

/*!
  \brief A class which draws a coordinate grid

  The QwtPlotGrid class can be used to draw a coordinate grid.
  A coordinate grid consists of major and minor vertical
  and horizontal gridlines. The locations of the gridlines
  are determined by the X and Y scale divisions which can
  be assigned with setXDiv() and setYDiv().
  The draw() member draws the grid within a bounding
  rectangle.
*/
class Plot;

class PlotGrid: public QwtPlotGrid
{
public:
    explicit PlotGrid();
    virtual ~PlotGrid();

    virtual void setRYDiv(const QwtScaleDiv &ry);

    virtual void draw( QPainter *p,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &rect ) const;

//    virtual void updateScaleDiv(
//        const QwtScaleDiv &xMap, const QwtScaleDiv &yMap );

private:
    void drawLines( QPainter *painter, const QRectF &,
        Qt::Orientation orientation, const QwtScaleMap &,
        const QList<double> & ) const;


    QwtScaleDiv *yRightDiv;

};

#endif
