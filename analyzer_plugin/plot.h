#ifndef _PLOT_H_
#define _PLOT_H_

#include <qwt_plot.h>
#include <qwt_interval.h>
#include <qwt_system_clock.h>
#include <qwt_dyngrid_layout.h>
#include <qwt_symbol.h>
#include <qwt_plot_picker.h>
#include <QTreeWidget>
#include "plotzoomer.h"
#include "plotgrid.h"
#include "plotmarker.h"
#include "plotcurve.h"
#include "plotlegend.h"
#include "canvaspicker.h"
//#include "peakmarker.h"

#include "signaldata.h"

#include "qwt_legend_item.h"

class QwtPlotCurve;
class QwtPlotMarker;
class QwtPlotDirectPainter;
class QwtPlotZoomer;
class PlotZoomer;
class PlotGrid;
class PlotMarker;
//class CanvasPicker;


class Plot: public QwtPlot
{
    Q_OBJECT
public:
    Plot(int legLabelsCounter ,QWidget * = NULL);
    virtual ~Plot();

    void start();
    virtual void replot();

    void incrementInterval();
    void incrementInterval(int i);
    SignalData& signalData(int i);

    void showCurve(int curve, bool show);

    void setColor(int chanel, QColor color,bool isStarded);
    void setCurveText(QString text, int cId, int pos);
    void setScale2(); //copy
    void setXScale(int val);
    void setYScale(int val, Axis ax);
    void showGridScale(bool s, int ax);
    void setBackgrColor(QColor c);
    void showMarkers(int nCh, bool s);
    void setTextCount(int c);
    void setZoomerState(bool s);
    void setMarkersColor(int nMrk, QColor c);
    void setDeltaTab(QTreeWidget *t);
    void deltatabUpdate();
    QTreeWidget* getDeltaTab();
    void setDefaultMarkersPos();
    bool markersIsHidden();
    bool cursorsIsFrozen();
    double markerYValue(int nMrk);
    void setZoomerEnable(bool s);
    void zoomPlot(QRectF r);
    PlotZoomer *getZoomer();
    PlotMarker* getRightTriggerMarker();
    QColor gridColor();
    QPalette backgroundPalette();
    void setZoomerColor(QColor c);
    void updateZoomerColor();

public Q_SLOTS:
    void setIntervalLength(double);
    void setInterval(double, double);
    void setScale(double, double);
    void setMarker(QPointF f);
    void setZoomScale(double y1, double y2, int ax);
    void setLegendText(int nCh, QString text);
    void yMarkersUpdate(QwtScaleMap yl);
    void xMarkersUpdate(QwtScaleMap xb);
    void setMarkersFreeze(bool s);
    void markerMoved(int nMrk);
    void plotZoomed(QRectF r);
    void updateLegend();
    void setGridColor(QColor c);
    //void hideCurve(int i);

signals:
    void chMarkerMoved(int nMrk);
    void zoomed(QRectF r);
    void zoomerCreated();
    void plotZoomedOut();

protected:
    virtual void showEvent(QShowEvent *);
    virtual void resizeEvent(QResizeEvent *);
    virtual void timerEvent(QTimerEvent *);

private:
    void initGradient();

    void updateCurve();

    QTreeWidget *deltaTab;
    PlotLegend *leg;
    QwtPlotPicker *d_picker;
    QwtPlotZoomer* zoomer;
    PlotMarker *d_marker1;
    PlotMarker *d_marker2;
    PlotMarker *rightTrigger;
    PlotCurve *d_curve[3];
    int d_paintedPoints[3];
    PlotZoomer *zoom;
    PlotZoomer *zoom1;
    PlotGrid *grid;
    int curYLSc;
    int textCount;
    bool zoomerState;
    bool markersFreeze;

    QwtPlotDirectPainter *d_directPainter;

    QwtInterval d_interval;
    int d_timerId;

    QwtSystemClock d_clock;
    CanvasPicker *c;


public:
    QColor currentColor;   //1st and 2nd chanels;
    QColor currentColor2;  //Curves colors
    QColor textBackgrColor;
    bool yLeftScale;
    bool yRightScale;
    bool xBotScale;
    bool mark1State;
    bool mark2State;
    PlotMarker *crossMarker1;
    PlotMarker *crossMarker2;
};
#endif
