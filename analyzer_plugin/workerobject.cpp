/****************************************************************************

****************************************************************************/
#include <QtCore>
#include "workerobject.h"

#include <QMessageBox>

#define LOCAL_DEBUG 0//0//
/*
 * represents an object that lives in another thread where it polls a resource
 * and communicates with the gui thread
 */



#ifdef asd
//---------------------------------------------------------------
WorkerObject::WorkerObject(QString strIp, QString strOscIp, QObject *parent)
    : QObject(parent)
{
    m_strIp=strIp;
    m_strOscIp=strOscIp;
    pllInitSteps = 4;

    m_udpSocket=NULL;
#ifdef sadad
    m_udpSocket=new QUdpSocket(this);
    //m_udpSocket->bind(QHostAddress(/*"192.168.100.1"*/m_strIp), 50001);
    m_udpSocket->bind(LOCAL_DEBUG? QHostAddress::LocalHost: QHostAddress(m_strIp), 50001);

    connect(m_udpSocket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
#endif

    open();
//    mainThread = 0;

//    m_thread = 0;// new Thread;
    //m_thread->launchWorker(this);


}

//---------------------------------------------------------------
WorkerObject::~WorkerObject()
{
    qDebug() << " destruction WorkerObject in thread " << thread()->currentThreadId();

//    if(m_thread)
//    {
//        m_thread->stop();
//        if(!m_thread->wait(5000))
//            qDebug() << "Osc: can't stop thread!";

//        delete m_thread;
//        m_thread=NULL;
//    }
/*
    if(m_udpSocket)
    {
        m_udpSocket->close();
        delete m_udpSocket;
    }
    */
    close();
}

//---------------------------------------------------------------
void WorkerObject::open()
{
    qDebug() << "open in thread " << thread()->currentThreadId() ;

    if(!m_udpSocket)
    {
        m_udpSocket=new QUdpSocket(this);
        //m_udpSocket->bind(QHostAddress(/*"192.168.100.1"*/m_strIp), 50001);
        m_udpSocket->bind(LOCAL_DEBUG? QHostAddress::LocalHost: QHostAddress(m_strIp), 50001);

        //connect(m_udpSocket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
    }
}

//---------------------------------------------------------------
void WorkerObject::close()
{
    qDebug() << "close in thread " << thread()->currentThreadId() ;
    if(m_udpSocket)
    {
        m_udpSocket->close();
        delete m_udpSocket;
        m_udpSocket=NULL;
    }
}

//---------------------------------------------------------------
void WorkerObject::doWork()
{
    qDebug() << "doing work in thread " << thread()->currentThreadId() ;

}

//---------------------------------------------------------------
void WorkerObject::readPendingDatagrams()
{
    //qDebug() << "read data >>>>>>> " << thread()->currentThreadId() ;
    while (m_udpSocket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(m_udpSocket->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        m_udpSocket->readDatagram(datagram.data(), datagram.size(),
                                &sender, &senderPort);

        emit recvPacket(datagram);
    }

    emit resumeRead();
    //mutex.lock();
    //m_codition.wakeAll();
    //mutex.unlock();
}

//---------------------------------------------------------------
void WorkerObject::writeData(const QByteArray &datagram)
{
    //qDebug() << "write data >>>>>>> " << thread()->currentThreadId() ;
    if(m_udpSocket)
        m_udpSocket->writeDatagram(datagram, LOCAL_DEBUG? QHostAddress::LocalHost: QHostAddress(m_strOscIp), 50000);
    //m_udpSocket->writeDatagram((const char*)data, len, QHostAddress(/*"192.168.100.2"*/m_strOscIp), 50000);
}

//---------------------------------------------------------------
void WorkerObject::WriteReg(quint8 BADDR, const QList<QPoint> &WDATA)
{
    //qDebug() << "write reg >>>>>>> " << thread()->currentThreadId() ;
    //QMutex m;
    //qDebug("in writeReg");
    m.lock();
    if (0/*thread() != mainThread && thr==mainThread*/) {
        //qDebug() << "write reg push to quue";
        WriteRegRequest *w = new WriteRegRequest(BADDR, WDATA);
        m_queue.push_back(w);
        m.unlock();
        return;
    }
    //qDebug() << "write reg perform write";
    //if (thr != thread())
    {
        //m_udpSocket->moveToThread(thr);
        //moveToThread(thr);

    }
    //return;
    bool b2byte=false;
    quint16 LDATA=0;
    quint8 CMD=1;

    //if(BADDR>3 && BADDR<8) b2byte=true; // упр АЦП 2байт адрес

    LDATA=b2byte ? WDATA.size()*3 : WDATA.size()*2;

    //QByteArray msg;
    //msg.resize(LDATA+4);
    //unsigned char* msg=(uchar*)datagram.data();


    unsigned char msg[1024]={0};

    //qToBigEndian(LDATA, msg);
    //memcpy(msg, &LDATA, 2);
    msg[0]=LDATA>>8;
    msg[1]=LDATA&0x00ff;
    msg[2]=CMD;
    msg[3]=BADDR;

    for(int i=0;i<WDATA.size();i++)
    {
        QPoint item=WDATA.at(i);

        quint16 a=item.x();
        quint8 d=item.y();

        if(b2byte)
        {
            msg[4+i*3]=a>>8;
            msg[4+i*3+1]=a&0x00FF;
            msg[4+i*3+2]=d;
        }
        else
        {
            msg[4+i*2]=a&0x00FF;
            msg[4+i*2+1]=d;
        }
    }
// todo if !release
    //WriteLog(LDATA, CMD, BADDR, WDATA);
    QByteArray datagram((const char*)msg, LDATA+4);
    writeData(datagram);

    //if(m_udpSocket)
    //    m_udpSocket->writeDatagram((const char*)msg, LDATA+4, QHostAddress::LocalHost, 50000);
    //m_udpSocket->writeDatagram((const char*)msg, LDATA+4, QHostAddress(/*"192.168.100.2"*/m_strOscIp), 50000);

    //m_udpSocket->moveToThread(mainThread);
    //moveToThread(mainThread);
    m.unlock();
}

//---------------------------------------------------------------
void WorkerObject::WriteLog(const QList<QPoint>& args)
{
    QByteArray frame;
    frame.resize(args.size());
    int j=0;
    for(int i=args.size()-1; i>=0; i--)
    {
        frame [j]= (quint8)args[i].y();
        j++;
    }

    QString str;
    QTextStream stream(&str);
    stream << hex << qSetFieldWidth(2) << qSetPadChar('0') << right;
    for(int i=0; i<frame.size(); i++)
    {
        stream << qSetFieldWidth(2) << static_cast<unsigned char>(frame[i]) << qSetFieldWidth(0);
        //stream << static_cast<unsigned char>(datagram[i]) <<" ";
    }

    //str.prepend(bIn? ": IN  : " : ": OUT : ");
    //str.prepend(m_strName);

    qDebug("%s", str.toAscii().constData());
}

void WorkerObject::WriteLog(QByteArray frame, bool bIn)
{
    QString str;
    QTextStream stream(&str);
    stream << hex << qSetFieldWidth(2) << qSetPadChar('0') << right;
    for(int i=0; i<frame.size(); i++)
    {
        stream << qSetFieldWidth(2) << static_cast<unsigned char>(frame[i]) << qSetFieldWidth(0) <<" ";
        //stream << static_cast<unsigned char>(datagram[i]) <<" ";
    }

    str.prepend(bIn? ": IN  : " : ": OUT : ");
    //str.prepend(m_strName);

    qDebug("%s", str.toAscii().constData());
}

//---------------------------------------------------------------
void WorkerObject::initGEN(quint8 nMode,
                           quint64 nFc,
                           quint32 nAmax,
                           quint32 nAmin,
                           quint64 nFmod,
                           quint64 nFsym,
                           quint64 nFdev,
                           quint8 nSSBType,
                           qreal fK)
{
    qDebug("Init GEN Mode=%d Amax=%d", nMode, nAmax);

    initDDS();

    switch(nMode)
    {
    case DDS_OFF: initDDS_XXX(100000000, nFc, 0); break;
    case DDS_XXX: initDDS_XXX(100000000, nFc, nAmax); break;
    case DDS_F1B: initDDS_F1B(100000000, nFc, nAmax, nFdev, nFsym); break;
    case DDS_G1B: initDDS_G1B(100000000, nFc, nAmax, nFsym); break;
    case DDS_J3E: initDDS_J3E(100000000 ,nFc, nAmax, nFmod, nSSBType); break;
    case DDS_R3E: initDDS_J3E(100000000 ,nFc, nAmax, nFmod, nSSBType); break; // todo
    case DDS_A3E: initDDS_A3E(100000000, nFc, nAmax, nFmod, fK, 32); break;
    case DDS_H3E: initDDS_H3E(100000000, nFc, nAmax, nFmod, nSSBType, 32); break;
    case DDS_A1A: initDDS_A1A(100000000, nFc, nAmax, nAmin, nFsym); break;
    case DDS_F3EA: initDDS_F3EA(100000000, nFc, nAmax, nFdev, nFmod); break;
    case DDS_F3EJ: initDDS_F3EA(100000000, nFc, nAmax, nFdev, nFmod); break; // todo
    }
}

//---------------------------------------------------------------
// стандартная инициализация
void WorkerObject::initDDS()
{
    qDebug("Init DDS");

    // Инициализация контроллера
    cmd_29(1, 0, 0);
    // Инициализация AD9954
    cmd_30(4, 0, 0x0000020000);
    cmd_30(3, 1, 0x0000000000);
    cmd_30(2, 2, 0x3fff000000LL);
    cmd_30(1, 3, 0x0000000000);
}

//---------------------------------------------------------------
void WorkerObject::initDDS_J3E(double Fs,
                               double Fc,
                               quint64 Amax,
                               double Fb,
                               quint8 SSBType)
{
    qDebug("Init DDS_J3E");

    double dF= SSBType==1 ? Fb : -Fb;
    quint64 Step = qRound64(((Fc+dF)/Fs)*qPow(2,32));

    // Инициализация AD9954
    cmd_30(4, 0, 0x0200020000LL);
    cmd_30(4, 4, (Step<<8)&0xffffffffffLL);
    cmd_30(2, 2, (Amax<<24)&0xffffffffffLL);
}

//---------------------------------------------------------------
void WorkerObject::initDDS_R3E(double Fs, double Fc, quint64 Amax, double Fb, quint8 SSBType)
{
    qDebug("Init DDS_R3E");

    initDDS_J3E(Fs, Fc, Amax, Fb, SSBType);
}

//---------------------------------------------------------------
void WorkerObject::initDDS_XXX(double Fs,
                               double Fc,
                               quint64 Amax)
{
    qDebug("Init DDS_XXX");

    quint64 Step = qRound64((Fc/Fs)*qPow(2,32));

    // Инициализация AD9954
    cmd_30(4, 0, 0x0200020000LL);
    cmd_30(3, 1, 0x0000000000);
    cmd_30(2, 2, (Amax<<24)&0xffffffffffLL);
//    cmd_30(1, 3, 0x0000000000);
    cmd_30(4, 4, (Step<<8)&0xffffffffffLL);
}

//---------------------------------------------------------------
void WorkerObject::initDDS_F1B(double Fs,
                               double Fc,
                               quint64 Amax,
                               double Fdev,
                               quint32 Fsym)
{
    qDebug("Init DDS_F1B");

    quint64 Step_Low = qRound64(((Fc-Fdev)/Fs)*qPow(2,32));
    quint64 Step_High = qRound64(((Fc+Fdev)/Fs)*qPow(2,32));

    quint64 K1=0;
    quint64 K2=0;
/*
    switch(Fsym)
    {
    case 50: K1=32;     K2= 62500; break;
    case 75: K1=23;     K2= 57971; break;
    case 100: K1=16;	K2=	62500; break;
    case 150: K1=11;	K2=	60606; break;
    case 200: K1=8;     K2=	62500; break;
    case 300: K1=7;     K2=	47619; break;
    case 500: K1=4;     K2=	50000; break;
    case 1200: K1=499;	K2=	167; break;
    case 2400: K1=1;	K2=	41667; break;
    case 4800: K1=83;	K2=	251; break;
    case 9600: K1=11;	K2=	947; break;
    case 16000: K1=1;	K2=	6250; break;
    }
*/
    // Вычисляем коэффициенты прореживания
    quint64 K[512] = {0};
    quint64 E[512] = {0};

    // Границы цикла определяются границами K1
    for(int i = 1; i<=512; i++)
    {
        K[i-1] = qRound( Fs / Fsym / i);

        // Ограничиваем K2
        if(K[i-1]>65535)  K[i-1] = 65535;
        if(K[i-1]<1)      K[i-1] = 1;
    }
    quint64 minE=0xffffffffffffffffLL;
    int Ind=1;
    for(int i = 1; i<=512; i++)
    {
        // неоднозначность в доках и скрипте !!!!!!!
        E[i-1] = qAbs( i * K[i-1] - Fs / Fsym );//qAbs( Fsym - Fs / i / K[i-1] );
        if(i==1) minE=E[i-1];
        else if(E[i-1]<minE){ minE=E[i-1]; Ind=i;}
    }

    // Вычисления организованы так, чтобы при равных ошибках выбирать
    // минимальный K1

    K1 = Ind; // Число повторений в блоке памяти
    K2 = qRound( Fs / Fsym / Ind); // Rate timer для AD9954


    // Инициализация AD9954
    cmd_30(4, 0, 0x8200020000LL);
    cmd_30(2, 2, (Amax<<24)&0xffffffffffLL);

    // Step_Low
    for(quint64 i=0; i<=K1-1; i++)
    {
        quint64 addr=(quint64)i&0x3ff;
        quint64 data=0x00;
        data|=((addr&0xff)<<16); //A(7..0)
        data|=((addr&0x3f)<<10); //A(5..0)
        data|=(addr&0x300);      //A(9..8)
        data|=0x90;                 // 100 1
        data|=((addr&0x3C0)>>6);         // A(9..6)

        cmd_30(5, 7, data&0xffffffffffLL);
        cmd_30(4, 11, (Step_Low<<8)&0xffffffffffLL);
    }
    // Step_High
    for(quint64 i=K1; i<=(2*K1-1); i++)
    {
        quint64 addr=(quint64)i&0x3ff;
        quint64 data=0x00;
        data|=((addr&0xff)<<16); //A(7..0)
        data|=((addr&0x3f)<<10); //A(5..0)
        data|=(addr&0x300);      //A(9..8)
        data|=0x90;                 // 100 1
        data|=((addr&0x3C0)>>6);         // A(9..6)

        cmd_30(5, 7, data&0xffffffffffLL);
        cmd_30(4, 11, (Step_High<<8)&0xffffffffffLL);
    }

    quint64 Kbeg=0;
    quint64 Kfin=2*K1-1;

    quint64 data=0x00;
    data|=((K2&0xff)<<32); //K2(7..0)
    data|=((K2&0xff00)<<16); //K2(15..8)
    data|=((Kfin&0xff)<<16); //Kfin(7..0)
    data|=((Kbeg&0x3f)<<10); //Kbeg(5..0)
    data|=(Kfin&0x300);      //Kfin(9..8)
    data|=0x90;                 // 100 1
    data|=((Kbeg&0x3C0)>>6);         // Kbeg(9..6)

    cmd_30(5, 7, data&0xffffffffffLL);
}

//---------------------------------------------------------------
void WorkerObject::initDDS_G1B(double Fs,
                               double Fc,
                               quint64 Amax,
                               quint32 Fsym)
{
    qDebug("Init DDS_G1B");

    quint64 Step_Low = qRound64((1-Fc/Fs)*qPow(2,32));
    quint64 Step_High = qRound64((Fc/Fs)*qPow(2,32));

    quint64 K1=0;
    quint64 K2=0;

    switch(Fsym)
    {
    case 50: K1=32;	K2=62500; break;
    case 75: K1=23;	K2=57971; break;
    case 100: K1=16;	K2=	62500; break;
    case 150: K1=11;	K2=	60606; break;
    case 200: K1=8;	K2=	62500; break;
    case 300: K1=7;	K2=	47619; break;
    case 500: K1=4;	K2=	50000; break;
    case 1200: K1=499;	K2=	167; break;
    case 2400: K1=1;	K2=	41667; break;
    case 4800: K1=83;	K2=	251; break;
    case 9600: K1=11;	K2=	947; break;
    case 16000: K1=1;	K2=	6250; break;
    }

    // Инициализация AD9954
    cmd_30(4, 0, 0x8200020000LL);
    cmd_30(2, 2, (Amax<<24)&0xffffffffffLL);

    // Step_Low
    for(quint64 i=0; i<=K1-1; i++)
    {
        quint64 addr=(quint64)i&0x3ff;
        quint64 data=0x00;
        data|=((addr&0xff)<<16); //A(7..0)
        data|=((addr&0x3f)<<10); //A(5..0)
        data|=(addr&0x300);      //A(9..8)
        data|=0x90;                 // 100 1
        data|=((addr&0x3C0)>>6);         // A(9..6)

        cmd_30(5, 7, data&0xffffffffffLL);
        cmd_30(4, 11, (Step_Low<<8)&0xffffffffffLL);
    }
    // Step_High
    for(quint64 i=K1; i<=(2*K1-1); i++)
    {
        quint64 addr=(quint64)i&0x3ff;
        quint64 data=0x00;
        data|=((addr&0xff)<<16); //A(7..0)
        data|=((addr&0x3f)<<10); //A(5..0)
        data|=(addr&0x300);      //A(9..8)
        data|=0x90;                 // 100 1
        data|=((addr&0x3C0)>>6);         // A(9..6)

        cmd_30(5, 7, data&0xffffffffffLL);
        cmd_30(4, 11, (Step_High<<8)&0xffffffffffLL);
    }

    quint64 Kbeg=0;
    quint64 Kfin=2*K1-1;

    quint64 data=0x00;
    data|=((K2&0xff)<<32); //K2(7..0)
    data|=((K2&0xff00)<<16); //K2(15..8)
    data|=((Kfin&0xff)<<16); //Kfin(7..0)
    data|=((Kbeg&0x3f)<<10); //Kbeg(5..0)
    data|=(Kfin&0x300);      //Kfin(9..8)
    data|=0x90;                 // 100 1
    data|=((Kbeg&0x3C0)>>6);         // Kbeg(9..6)

    cmd_30(5, 7, data&0xffffffffffLL);
}

//---------------------------------------------------------------
void WorkerObject::initDDS_A3E(double Fs,
                               double Fc,
                               quint64 Amax,
                               double Fmod,
                               double K,
                               quint8 N)
{
    qDebug("Init DDS_A3E");

    quint64 Step = qRound64((Fc/Fs)*qPow(2,32));
    quint64 *Am = new quint64[N];

    quint64 Real_Delay = qRound64(Fs/(N*Fmod))-6;
    quint64 A=(qreal)Amax/(qreal)(1+K); // ограничение

    //cmd_30(2, 2, 0x0000000000);

    // Инициализация AD9954
    cmd_30(4, 0, 0x0200020000LL);
    cmd_30(4, 4, (Step<<8)&0xffffffffffLL);

    for(int i=0; i<N; i++)
    {
        Am[i]=qRound64(A*(1+K*qCos(2*PI*i/N)));
        cmd_29(0, 2, 2*i);
        cmd_30(2, 2, (Am[i]<<24)&0xffffffffffLL);
        cmd_29(0, 2, 2*i+1);
        cmd_30(15, 0, Real_Delay&0xffffffff);
    }

    cmd_29(0, 3, 2*N-1);

    delete [] Am;
}

//---------------------------------------------------------------
void WorkerObject::initDDS_H3E(double Fs, double Fc, quint64 Amax, double Fmod, quint8 SSBType, quint8 N)
{
    qDebug("Init DDS_H3E");

    double dF= SSBType==1 ? 0.5*Fmod : -0.5*Fmod;
    quint64 Step = qRound64(((Fc+dF)/Fs)*qPow(2,32));

    qint64 Am_f[100];// = new qint64[N];
    quint64 Am[100];// = new quint64[N];
    quint64 P[100];// = new quint32[N];

    quint64 Real_Delay = qRound64((2*Fs)/(N*Fmod))-9;



    // Инициализация AD9954
    cmd_30(4, 0, 0x0200020000LL);
    cmd_30(2, 2, 0x0000000000);
    cmd_30(4, 4, (Step<<8)&0xffffffffffLL);

    for(int i=0; i<N; i++)
    {
        Am_f[i]=qRound64(Amax*qCos(2*PI*i/N));
        P[i]=(Am_f[i]<0)?8192:0;
        Am[i]=(quint64)qAbs(Am_f[i]);
        cmd_29(0, 2, 3*i);
        cmd_30(2, 2, (Am[i]<<24)&0xffffffffffLL);
        cmd_29(0, 2, 3*i+1);
        cmd_30(2, 5, (P[i]<<24)&0xffffffffffLL);
        cmd_29(0, 2, 3*i+2);
        cmd_30(15, 0, Real_Delay&0xffffffff);
    }

    cmd_29(0, 3, 3*N-1);

    //delete [] Am;
    //delete [] P;
}

//---------------------------------------------------------------
void WorkerObject::initDDS_F3EA(double Fs,
                                double Fc,
                                quint64 Amax,
                                double Fdev,
                                double Fmod,
                                quint32 N)
{
    qDebug("Init DDS_F3EA");

    // Инициализация AD9954
    cmd_30(4, 0, 0x8200020000LL);

    for(quint64 i=0; i<N; i++)
    {
        quint64 Step=qRound64(((Fc+Fdev*qCos(2*PI*i/N))/Fs)*qPow(2,32));

        quint64 addr=(quint64)i&0x3ff;
        quint64 data=0x00;
        data|=((addr&0xff)<<16); //A(7..0)
        data|=((addr&0x3f)<<10); //A(5..0)
        data|=(addr&0x300);      //A(9..8)
        data|=0x90;                 // 100 1
        data|=((addr&0x3C0)>>6);         // A(9..6)

        cmd_30(5, 7, data&0xffffffffffLL);
        cmd_30(4, 11, (Step<<8)&0xffffffffffLL);
    }

    quint64 Delay = qRound64(Fs/(N*Fmod));
    quint32 Abeg=0;
    quint32 Afin=N-1;

    quint64 data=0x00;
    data|=((Delay&0xff)<<32); //K2(7..0)
    data|=((Delay&0xff00)<<16); //K2(15..8)
    data|=((Afin&0xff)<<16); //Kfin(7..0)
    data|=((Abeg&0x3f)<<10); //Kbeg(5..0)
    data|=(Afin&0x300);      //Kfin(9..8)
    data|=0x90;                 // 100 1
    data|=((Abeg&0x3C0)>>6);         // Kbeg(9..6)

    cmd_30(5, 7, data&0xffffffffffLL);
    cmd_30(2, 2, (Amax<<24)&0xffffffffffLL);
}

//---------------------------------------------------------------
void WorkerObject::initDDS_F3EJ()
{
    qDebug("Init DDS_F3EJ");


}

//---------------------------------------------------------------
void WorkerObject::initDDS_A1A(double Fs,
                               double Fc,
                               quint64 Amax,
                               quint64 Amin,
                               double Fsym)
{
    qDebug("Init DDS_A1A");

    quint64 Step = qRound64((Fc/Fs)*qPow(2,32));
    quint64 Real_Delay = qRound64(Fs/Fsym)-6;

    // Инициализация AD9954
    cmd_30(4, 0, 0x0200020000LL);
    cmd_30(4, 4, (Step<<8)&0xffffffffffLL);
    cmd_29(0, 2, 0);
    cmd_30(2, 2, (Amax<<24)&0xffffffffffLL);
    cmd_29(0, 2, 1);
    cmd_30(15, 0, Real_Delay&0xffffffff);
    cmd_29(0, 2, 2);
    cmd_30(2, 2, (Amin<<24)&0xffffffffffLL);
    cmd_29(0, 2, 3);
    cmd_30(15, 0, Real_Delay&0xffffffff);

    cmd_29(0, 3, 3);
}

//---------------------------------------------------------------
void WorkerObject::cmd_29(quint8 AD_9954_RESET, quint8 MODE, quint8 RAM_ADDR)
{
    quint8 nBADDR=0x24;

    //////////
    // формирование команд
    QList<QPoint> args;

    args.clear();
    args.append(QPoint(0,0));    //PARAM0
    args.append(QPoint(1,0));    //PARAM1
    args.append(QPoint(2,0));    //PARAM2
    args.append(QPoint(3,0));    //PARAM3
    args.append(QPoint(4,RAM_ADDR&0xff));   //MAIN_PARAM младш
    args.append(QPoint(5,(AD_9954_RESET<<7)|((MODE&0x03)<<4)|((RAM_ADDR>>8)&0x0f)));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,29));  //CMD_NUM
    WriteReg(nBADDR, args);

    WriteLog(args);

    //m_udpSocket->waitForReadyRead(50); // delay for gen
}

//---------------------------------------------------------------
void WorkerObject::cmd_30(quint8 DATA_LENGTH, quint8 INSTRUCT_CODE, quint64 DATA)
{
    quint8 nBADDR=0x24;

    //////////
    // формирование команд
    QList<QPoint> args;

    args.clear();
    args.append(QPoint(0,DATA&0xff));    //PARAM0
    args.append(QPoint(1,(DATA>>8)&0xff));    //PARAM1
    args.append(QPoint(2,(DATA>>16)&0xff));    //PARAM2
    args.append(QPoint(3,(DATA>>24)&0xff));    //PARAM3
    args.append(QPoint(4,(DATA>>32)&0xff));   //MAIN_PARAM младш
    args.append(QPoint(5,(DATA_LENGTH<<4)|(INSTRUCT_CODE&0x0f)));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,30));  //CMD_NUM
    WriteReg(nBADDR, args);

    WriteLog(args);

    //m_udpSocket->waitForReadyRead(50); // delay for gen
}

void WorkerObject::initTakt(int nTaktADC) {
    qDebug()<< "takting ADC " << nTaktADC;
    quint8 type[2]={0x05, 0x15};

    quint8 b=0x00;
    b|=type[nTaktADC];

    QList<QPoint> args;
    args.append(QPoint(3, 0));
    args.append(QPoint(1, b));
    WriteReg(3, args); //
}

void WorkerObject::initDevice(){
    qDebug("initDevice");

    QList<QPoint> args;

    // установка регистра направления
    args.append(QPoint(3,0));
    WriteReg(1, args);
    WriteReg(2, args);
    //WriteReg(3, args);

    // todo...
    // параметры adc
    args.clear();
    args.append(QPoint(0x14,0x09));
    args.append(QPoint(0xff,0x01));
    WriteReg(4, args);
    WriteReg(6, args);
    WriteReg(8, args);
    WriteReg(10, args);
}

void WorkerObject::initDDC (bool bFft, MeasureConfigItem *modeConf){
    /*double */modeConf->Dec_ideal=modeConf->fFs/
            (2*modeConf->fBand);
    /*int */modeConf->Dec_3=(modeConf->Dec_ideal>=2)? 2 : 1;

    /*int */modeConf->Dec_1=qFloor(modeConf->Dec_ideal/modeConf->Dec_3); //41.2 -> 41
    if(modeConf->Dec_1>=256)modeConf->Dec_1=256;

    /*int */modeConf->Dec_2=qFloor(modeConf->Dec_ideal/(modeConf->Dec_1*modeConf->Dec_3));
    if(modeConf->Dec_2>=256)modeConf->Dec_2=256;

    /*unsigned long */modeConf->Dec_sum=modeConf->Dec_1*modeConf->Dec_2*modeConf->Dec_3;

    /*double */modeConf->Fdec=qRound64(modeConf->fFs/modeConf->Dec_sum);

    /*int */modeConf->Nband=(modeConf->fBand*modeConf->Nfft)/modeConf->Fdec;

    quint8 nBADDR=bFft?0x22:0x21;

    qDebug("Init DDC Fdec=%f Fcentr=%f Nband=%d Band=%f", modeConf->Fdec ,
           modeConf->fFcentr,
           modeConf->Nband,
           modeConf->fBand);
    //////////
    // формирование команд
    QList<QPoint> args;

    // 1
    {
    qint64 Step=qRound64((1-modeConf->fFcentr/
                          modeConf->fFs)*qPow(2,32));
    int Mode=1;

    args.clear();
    args.append(QPoint(0,Step&0xff));    //PARAM0
    args.append(QPoint(1,(Step>>8)&0xff));    //PARAM1
    args.append(QPoint(2,(Step>>16)&0xff));    //PARAM2
    args.append(QPoint(3,(Step>>24)&0xff));    //PARAM3
    args.append(QPoint(4,Mode));   //MAIN_PARAM младш
    args.append(QPoint(5,0));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,17));  //CMD_NUM
    WriteReg(nBADDR, args);
    }
    //////////////
    {
    // 2
    int Mode=modeConf->Dec_1>1 ? 1 : 0;
    int Atten=48-qCeil(6*(qLn(modeConf->Dec_1)/qLn(2)));
    quint8 Last_Dcm_Val=modeConf->Dec_1-1;

    args.clear();
    args.append(QPoint(0,Last_Dcm_Val));    //PARAM0
    args.append(QPoint(1,0));    //PARAM1
    args.append(QPoint(2,Atten&0x3f));    //PARAM2
    args.append(QPoint(3,0));    //PARAM3
    args.append(QPoint(4,Mode));   //MAIN_PARAM младш
    args.append(QPoint(5,0));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,18));  //CMD_NUM
    WriteReg(nBADDR, args);

    // 3
    if(modeConf->Dec_1!=1)
    {
        double Ap=(6*(qLn(modeConf->Dec_1)/qLn(2)));
        //int Ap=qCeil(6*(qLn(Dec_1)/qLn(2)))-1;
        qreal S1=qPow(2,qCeil(Ap)-Ap-1);
        //qreal S1=qPow(2,-Ap);

        double K[6]={9152.01418415251,
                     -51560.9092863082,
                     107960.6904140670,
                     107960.6904140670,
                     -51560.9092863082,
                     9152.01418415251};

        for(int i=0;i<6;i++)
        {
            int K1=qRound(S1*K[i]);
            args.clear();
            args.append(QPoint(0,K1&0xff));    //PARAM0
            args.append(QPoint(1,(K1>>8)&0xff));    //PARAM1
            args.append(QPoint(2,(K1>>16)&0x03));    //PARAM2
            args.append(QPoint(3,0));    //PARAM3
            args.append(QPoint(4,i&0x0f));   //MAIN_PARAM младш
            args.append(QPoint(5,0));   //MAIN_PARAM старш
            args.append(QPoint(6,0));   //BLOCK_NUM
            args.append(QPoint(7,19));  //CMD_NUM
            WriteReg(nBADDR, args);
        }
    }
    }

    //////////////
    {
    // 4

    int Mode=modeConf->Dec_2>1 ? 1 : 0;
    int Atten=48-qCeil(6*(qLn(modeConf->Dec_2)/qLn(2)));
    quint8 Last_Dcm_Val=modeConf->Dec_2-1;
    quint8 Last_Coef_Addr=5;

    args.clear();
    args.append(QPoint(0,Last_Dcm_Val));    //PARAM0
    args.append(QPoint(1,0));    //PARAM1
    args.append(QPoint(2,Atten&0x3f));    //PARAM2
    args.append(QPoint(3,Last_Coef_Addr));    //PARAM3
    args.append(QPoint(4,Mode));   //MAIN_PARAM младш
    args.append(QPoint(5,0));   //MAIN_PARAM старш
    args.append(QPoint(6,0));   //BLOCK_NUM
    args.append(QPoint(7,20));  //CMD_NUM
    WriteReg(nBADDR, args);

    // 5
    if(modeConf->Dec_2!=1)
    {
        //int Ap=qCeil(6*(qLn(Dec_2)/qLn(2)))-1;
        double Ap=(6*(qLn(modeConf->Dec_2)/qLn(2)));
        qreal S2=qPow(2,qCeil(Ap)-Ap-1);
        //qreal S2=qPow(2,-Ap);

        double K[6]={9152.01418415251,
                     -51560.9092863082,
                     107960.6904140670,
                     107960.6904140670,
                     -51560.9092863082,
                     9152.01418415251};

        for(int i=0;i<6;i++)
        {
            int K2=qRound(S2*K[i]);
            args.clear();
            args.append(QPoint(0,K2&0xff));    //PARAM0
            args.append(QPoint(1,(K2>>8)&0xff));    //PARAM1
            args.append(QPoint(2,(K2>>16)&0x03));    //PARAM2
            args.append(QPoint(3,0));    //PARAM3
            args.append(QPoint(4,i&0x0f));   //MAIN_PARAM младш
            args.append(QPoint(5,0));   //MAIN_PARAM старш
            args.append(QPoint(6,0));   //BLOCK_NUM
            args.append(QPoint(7,21));  //CMD_NUM
            WriteReg(nBADDR, args);
        }
    }

    }

    // 6
    {
        int Mode=modeConf->Dec_3>1 ? 1 : 0;

        args.clear();
        args.append(QPoint(0,0));    //PARAM0
        args.append(QPoint(1,0));    //PARAM1
        args.append(QPoint(2,0));    //PARAM2
        args.append(QPoint(3,0));    //PARAM3
        args.append(QPoint(4,Mode));   //MAIN_PARAM младш
        args.append(QPoint(5,0));   //MAIN_PARAM старш
        args.append(QPoint(6,0));   //BLOCK_NUM
        args.append(QPoint(7,22));  //CMD_NUM
        WriteReg(nBADDR, args);

    }

    if(!bFft) return; // ddc only
    ///////////////////////////////////////////////////
    // 7
    if(fft_window)
    {
        for(int i=0;i<FFT_SAMPLES_NUM;i++)
        {
            short K=modeConf->fft_window[i];
            args.clear();
            args.append(QPoint(0,K&0xff));    //PARAM0
            args.append(QPoint(1,(K>>8)&0xff));    //PARAM1
            args.append(QPoint(2,0));    //PARAM2
            args.append(QPoint(3,0));    //PARAM3
            args.append(QPoint(4,i&0xff));   //MAIN_PARAM младш
            args.append(QPoint(5,(i>>8)&0x0f));   //MAIN_PARAM старш
            args.append(QPoint(6,0));   //BLOCK_NUM
            args.append(QPoint(7,24));  //CMD_NUM
            WriteReg(nBADDR, args);
        }
    }

    // 8
    {
        int Mode=2;
        //int Delay=0;

        args.clear();
        args.append(QPoint(0,0));    //PARAM0
        args.append(QPoint(1,0));    //PARAM1
        args.append(QPoint(2,0));    //PARAM2
        args.append(QPoint(3,0));    //PARAM3
        args.append(QPoint(4,Mode));   //MAIN_PARAM младш
        args.append(QPoint(5,0));   //MAIN_PARAM старш
        args.append(QPoint(6,0));   //BLOCK_NUM
        args.append(QPoint(7,25));  //CMD_NUM
        WriteReg(nBADDR, args);

    }

    // 9
    {
        int Mode=0;
        int Order=0;

        args.clear();
        args.append(QPoint(0,0));    //PARAM0
        args.append(QPoint(1,0));    //PARAM1
        args.append(QPoint(2,0));    //PARAM2
        args.append(QPoint(3,0));    //PARAM3
        args.append(QPoint(4,Order));   //MAIN_PARAM младш
        args.append(QPoint(5,Mode));   //MAIN_PARAM старш
        args.append(QPoint(6,0));   //BLOCK_NUM
        args.append(QPoint(7,26));  //CMD_NUM
        WriteReg(nBADDR, args);

    }
}

void WorkerObject::initDDC_TLG(MeasureConfigItem *modeConf) {
    quint32 Fsym=100; //todo 	//– символьная скорость (50, 100, 150, 300, 500, 1200, 2400, 4800, 9600, 16000 бод);
    quint8 Nsym=16; 	//– число отсчётов на символ модуляции = 16.
    quint8 Mode=3;   // 0:0 1:-1 2:+1	3:x

    //Band=Fsym*Nsym/2;
    //Fcentr=0.0;
    //Fleft=0.0;
    //Fright=Band;

    initDDC(false,modeConf);
}

void WorkerObject::initFFT_PHOSO(MeasureConfigItem *modeConf) {
    modeConf->fFcentr=0.0;
    modeConf->Fleft=300.0;
    modeConf->Fright=3400.0;
    modeConf->fBand=2*modeConf->Fright;
    //window=4;

    initDDC(true,modeConf);
}

void WorkerObject::initFFT_FREQ(MeasureConfigItem *modeConf) {
    modeConf->Ft0=modeConf->fFs/4.0;
    // вынести в инициализацию
    switch(modeConf->nPass)
    {
    case 0://1-е приближение частоты
        modeConf->fFcentr=modeConf->Ft0;
        modeConf->fBand=modeConf->fFs/2;
        break;
    case 1://2-е приближение частоты
        modeConf->fFcentr=qRound64(modeConf->Ft1);
        modeConf->fBand=qRound64(modeConf->fFs/1024);
        break;
    case 2://3-е приближение частоты
        modeConf->Ft2=qRound64((modeConf->Ft2*qPow(2,32))/modeConf->fFs)*(modeConf->fFs/qPow(2,32));
        modeConf->fFcentr=qRound64(modeConf->Ft2);//qRound((Ft2*qPow(2,32))/Fs)*(Fs/qPow(2,32));
        modeConf->fBand=qRound64(modeConf->fFs/262144);
        break;
    case 3:
        break;
    }

    modeConf->Fleft=modeConf->fFcentr-
            modeConf->fBand/2;
    modeConf->Fright=modeConf->fFcentr+
            modeConf->fBand/2;

    initDDC(true,modeConf);
}

void WorkerObject::startDecoding(int nCh, int m_nMeasMode)
{
    QList<QPoint> args;
    args.clear();
    args.append(QPoint(7, 0x80/*|m_mw->m_nCh*/)); // выбор канала
    args.append(QPoint(m_nMeasMode==3||m_nMeasMode==4||m_nMeasMode==5||m_nMeasMode==7||m_nMeasMode==8||m_nMeasMode==10?9:8, nCh)); // выбор канала
    args.append(QPoint(1, 0));           // сброс бита записи
    quint8 start=1;
    switch(m_nMeasMode)
    {
    case 0:
    case 1:
    case 6:
    case 9:
        start=1;
        break;
    case 2:
        start=2;
        break;
    case 3:
    case 4:
    case 5:
    case 7:
    case 8:
    case 10:
        start=4;
        break;
    }
    args.append(QPoint(1,start/*m_mw->m_nMeasMode==0?1:1<<(m_mw->m_nMeasMode-1)*/));           // установка бита записи
    WriteReg(16, args);
}

void WorkerObject::readData(int &m_nReadPtr,
                            int m_nMeasMode,
                            char *currentData)
{
    QList<QPoint> args;
    args.clear();
    args.append(QPoint(3,m_nReadPtr&0xff)); // адрес0
    args.append(QPoint(4,(m_nReadPtr>>8)&0xff)); // адрес1
    args.append(QPoint(5,0x80)); // количество0
    args.append(QPoint(6,0x00)); // количество1
    args.append(QPoint(2,0));           // сброс бита чтения
    quint8 start=1;
    switch(m_nMeasMode)
    {
    case 0:
    case 1:
    case 6:
    case 9:
        start=1;
        break;
    case 2:
        start=2;
        break;
    case 3:
    case 4:
    case 5:
    case 7:
    case 8:
    case 10:
        start=3;
        break;
    }
    args.append(QPoint(2, start/*m_mw->m_nMeasMode==0?1:m_mw->m_nMeasMode*/));           // установка бита чтения
    WriteReg(16, args);

    m.lock();
    if (m_udpSocket->waitForReadyRead(/*-1*/1000))
    {
        while(m_udpSocket->hasPendingDatagrams()) {
            QByteArray datagram;
            datagram.resize(m_udpSocket->pendingDatagramSize());
            QHostAddress sender;
            quint16 senderPort;

            m_udpSocket->readDatagram(datagram.data(), datagram.size(),
                                     &sender, &senderPort);
            //qout << "datagram received from " << sender.toString() << endl;
            processDatagram(datagram, m_nReadPtr, currentData);
        }
    }
    m.unlock();
}

//void WorkerObject::readData(RenderThread *r) {
//    //QMutex m;
//    //qDebug("in readData");
//    m.lock();
//    //if (thr!=thread())
//    {
//        //m_udpSocket->moveToThread(thr);
//        //moveToThread(thr);
//    }
//    if (m_udpSocket->waitForReadyRead(/*-1*/1000))
//    {
//        while(m_udpSocket->hasPendingDatagrams()) {
//            QByteArray datagram;
//            datagram.resize(m_udpSocket->pendingDatagramSize());
//            QHostAddress sender;
//            quint16 senderPort;

//            m_udpSocket->readDatagram(datagram.data(), datagram.size(),
//                                     &sender, &senderPort);
//            //qout << "datagram received from " << sender.toString() << endl;
//            r->processDatagram(datagram);
//        }
//    }

//    //moveToThread(mainThread);
//    //m_udpSocket->moveToThread(mainThread);
//    m.unlock();
//}

void WorkerObject::processDatagram(const QByteArray &datagram,
                                   int &m_nReadPtr,
                                   char* currentData)
{
//    if(!this->isRunning())return;

    //mutex.lock();

    //return;
    quint8 FRAMEID=datagram[0];

    switch(FRAMEID)
    {
    case 1: // register data
        break;
    case 2: // osc
//        processOscDatagram(datagram);
        break;
    case 3: // ddc
        processDdcDatagram(datagram,m_nReadPtr, currentData);
        break;
    case 4: // spect
         processFftDatagram(datagram,m_nReadPtr, currentData);
        break;
    case 5: // raw adc
        processRawDatagram(datagram,m_nReadPtr, currentData);
        break;
    }
    // todo if !release
    //WriteLog(datagram);

     //mutex.unlock();
}

void WorkerObject::processDdcDatagram(QByteArray datagram,
                                      int &m_nReadPtr,
                                      char* currentData)
{
    quint16 addr=datagram[1]<<8;
    addr |= 0xff&datagram[2];

    quint16 len=datagram[3]<<8;
    len |= 0xff&datagram[4];

    QByteArray *temp = (QByteArray*) currentData;

    qDebug() << "DDC1 data recieved: ADDR=" << addr << " LEN=" << len << " READ_PTR=" << m_nReadPtr << endl;

    if(len>0)
    {
        if(m_nReadPtr!=addr)
            qDebug() << "DDC1 data recieved: error addr\n";
        else
        {
            temp->replace(m_nReadPtr*4, datagram.size()-5, datagram.right(datagram.size()-5));
            m_nReadPtr+=len;
        }
    }
}

void WorkerObject::processFftDatagram(QByteArray datagram,
                                      int &m_nReadPtr,
                                      char* currentData)
{
    quint16 addr=datagram[1]<<8;
    addr |= 0xff&datagram[2];

    quint16 len=datagram[3]<<8;
    len |= 0xff&datagram[4];

    //m_nFftGain=datagram[5];

    qDebug() << "FFT1 data recieved: ADDR=" << addr << " LEN=" << len << " READ_PTR=" << m_nReadPtr << endl;

    QByteArray *temp = (QByteArray*) currentData;

    if(len>0)
    {
        if(m_nReadPtr!=addr)
            qDebug() << "FFT1 data recieved: error addr\n";
        else
        {
            temp->replace(m_nReadPtr*4, datagram.size()-5, datagram.right(datagram.size()-5));
            m_nReadPtr+=len;
        }
    }
}

void WorkerObject::processRawDatagram(QByteArray datagram,
                                      int &m_nReadPtr,
                                      char* currentData)
{
    quint16 addr=datagram[1]<<8;
    addr |= 0xff&datagram[2];

    quint16 len=datagram[3]<<8;
    len |= 0xff&datagram[4];

    qDebug() << "RAW data recieved: ADDR=" << addr << " LEN=" << len << " READ_PTR=" << m_nReadPtr <<endl;

//    QByteArray temp(currentData);
    QByteArray *temp = (QByteArray*) currentData;

    if(len>0)
    {
        if(m_nReadPtr!=addr)
            qDebug() << "RAW data recieved: error addr\n";
        else
        {
            temp->replace(m_nReadPtr*4, datagram.size()-5, datagram.right(datagram.size()-5));
            m_nReadPtr+=len;
        }
    }
//    for (unsigned int i = 0; i<temp.size(); i++)
//        currentData[i] = temp[i];
}

void WorkerObject::initChannel(int nCh, int nChType, int nChAtten) {
    qDebug("initChannel");

    quint8 type[3]={0x08, 0x00, 0x01};
    quint8 atten_600[2]={0x06, 0x00};
    quint8 atten_1M[4]={0x30, 0x10, 0x20, 0x00};

    quint8 b=0x00;
    b|=type[nChType];

    switch(nChType)
    {
    case 0: // 50 Ohm
        break;
    case 1: // 600 Ohm
        b|=atten_600[nChAtten];
        break;
    case 2: // 1 MOhm
        b|=atten_1M[nChAtten];
        break;
    }

    QList<QPoint> args;
    args.append(QPoint(1, b));
    WriteReg(nCh+1, args); // коммутация канала
}

void WorkerObject::processQueue() {
    while (!m_queue.isEmpty()) {
        WriteRegRequest *w = m_queue.first();
        WriteReg(w->BADDR, w->WDATA);
        delete w;
        m_queue.pop_front();
    }
}

//QThread* WorkerObject::getMainThread() {
//    return 0;//mainThread;
//}

//void WorkerObject::setMainThread(QThread *thr) {
////    mainThread = thr;
//}

void WorkerObject::initPLL(int step)
{
    qDebug()<<"initPLL, step: "<<step;

    quint8 nBADDR=0x0C;

    //////////
    // формирование команд
    QList<QPoint> args;
    if (step == 0)
    {
        args.clear();
        args.append(QPoint(3, 0));
        args.append(QPoint(1, 0x05));
        WriteReg(3, args); //
    }

    //- пауза 0.5 сек
//    this->msleep(500);
    //- 0 4 1 0x0C 0x20 0x03 0x80 0xEB
    if (step == 1)
    {
        args.clear();
        args.append(QPoint(0x20, 0x03));
        args.append(QPoint(0x80, 0xEB));
        WriteReg(nBADDR, args);

        //- 0 4 1 0x0C 0x21 0x00 0x40 0xEA
        args.clear();
        args.append(QPoint(0x21, 0x00));
        args.append(QPoint(0x40, 0xEA));
        WriteReg(nBADDR, args);

        //- 0 4 1 0x0C 0x02 0x00 0x40 0xEA
        args.clear();
        args.append(QPoint(0x02, 0x00));
        args.append(QPoint(0x40, 0xEA));
        WriteReg(nBADDR, args);

        //- 0 4 1 0x0C 0x03 0x03 0x02 0xEA
        args.clear();
        args.append(QPoint(0x03, 0x03));
        args.append(QPoint(0x02, 0xEA));
        WriteReg(nBADDR, args);
        /*
    //- 0 4 1 0x0C 0x14 0x01 0x40 0xEB
    args.clear();
    args.append(QPoint(0x14, 0x01));
    args.append(QPoint(0x40, 0xEB));
    WriteReg(nBADDR, args);
*/
        //- 0 4 1 0x0C 0x14 0x03 0x1E 0xEB
        args.clear();
        args.append(QPoint(0x14, 0x03));
        args.append(QPoint(0x1E, 0xEB));
        WriteReg(nBADDR, args);

        //- 0 4 1 0x0C 0xB5 0x0E 0x0C 0x90
        args.clear();
        args.append(QPoint(0xB5, 0x0E));
        args.append(QPoint(0x0C, 0x90));
        WriteReg(nBADDR, args);

        //- 0 4 1 0x0C 0x06 0x01 0x8E 0xA4
        args.clear();
        args.append(QPoint(0x06, 0x01));
        args.append(QPoint(0x8E, 0xA4));
        WriteReg(nBADDR, args);

        //- 0 4 1 0x0C 0xF7 0x3D 0x93 0x0x95
        args.clear();
        args.append(QPoint(0xF7, 0x3D));
        args.append(QPoint(0x93, 0x95));
        WriteReg(nBADDR, args);

        //- 0 4 1 0x0C 0xD8 0x9C 0x00 0x20
        args.clear();
        args.append(QPoint(0xD8, 0x9C));
        args.append(QPoint(0x00, 0x20));
        WriteReg(nBADDR, args);
    }

    //- пауза 0.5 сек
//    this->msleep(500);
    //- 0 4 1 0x0C 0x06 0x01 0x8E 0xA0
    if (step == 2)
    {
        args.clear();
        args.append(QPoint(0x06, 0x01));
        args.append(QPoint(0x8E, 0xA0));
        WriteReg(nBADDR, args);
    }

    //- пауза 0.5 сек
//    this->msleep(500);
    //- 0 4 1 0x0C 0x06 0x01 0x8E 0xA4
    if (step == 3)
    {
        args.clear();
        args.append(QPoint(0x06, 0x01));
        args.append(QPoint(0x8E, 0xA4));
        WriteReg(nBADDR, args);
    }
    //m_worker->processQueue();
    qDebug("initPLL <<<<<<<<<<<<<<<<<<<<<<");
}

//----------------------
WriteRegRequest::WriteRegRequest(qint8 b,
                                 QList<QPoint> data,
                                 QObject *parent) :
    QObject(parent) {
    BADDR=b;
    WDATA=data;
}

WriteRegRequest::~WriteRegRequest() {

}
#endif
