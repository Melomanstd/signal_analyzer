#include "plotzoomer.h"
#include <QMessageBox>
#include <QEvent>
#include <QMouseEvent>
#include <qwt_picker.h>

PlotZoomer::PlotZoomer(QwtPlotCanvas *canvas, bool doReplot) :
    QwtPlotZoomer(canvas)
{
    readyToZoom = true;
    plotZoomed = false;
    primaryZoomer = false;
    linkedZoomer = 0;
    //alreadyZoomed = false;
}

void PlotZoomer::rescale() {
    /*if (plotZoomed) {
        plotZoomed = false;
        return;
    }*/
    QList<double> list[QwtScaleDiv::NTickTypes];
    list[0].append(0);
    list[1].append(0);

    Plot *plt =(Plot*) plot();
    if ( !plt )
        return;
    QwtScaleMap yCurs = plt->canvasMap(QwtPlot::yLeft);
    QwtScaleMap xCurs = plt->canvasMap(QwtPlot::xBottom);
    const QRectF &rect = zoomRect();

    //setAxis(QwtPlot::xBottom,QwtPlot::yRight);
    //d_data->zoomStack[d_data->zoomRectIndex];
    if ( rect != scaleRect() )
    {
        const bool doReplot = plt->autoReplot();
        plt->setAutoReplot( false );

        double x1 = rect.left();
        double x2 = rect.right();
        if ( plt->axisScaleDiv( xAxis() )->lowerBound() >
            plt->axisScaleDiv( xAxis() )->upperBound() )
        {
            qSwap( x1, x2 );
        } //x1 min / x2 max
        if (b) {
            plt->setZoomScale(x1,x2,xAxis());
            //b=false;
        } //else b=true;
        if (plt->cursorsIsFrozen())
            if (b) plt->xMarkersUpdate(xCurs);
        double y1 = rect.top();
        double y2 = rect.bottom();
        if ( plt->axisScaleDiv( yAxis() )->lowerBound() >
            plt->axisScaleDiv( yAxis() )->upperBound() )
        {
            qSwap( y1, y2 );
        }

        //QwtScaleMap left = plt->canvasMap(QwtPlot::yLeft);
        QwtScaleMap right = plt->canvasMap(QwtPlot::yRight);
        double r = yCurs.transform(y1);
        double r2 = yCurs.transform(y2);
        r = right.invTransform(r);
        r2 = right.invTransform(r2);
        //crossMarker1->setYValue(left.invTransform(y));

        plt->setZoomScale(y1,y2,QwtPlot::yLeft);
        plt->setZoomScale(r,r2,QwtPlot::yRight);
        if (plt->cursorsIsFrozen())
            plt->yMarkersUpdate(yCurs);
        plt->setAutoReplot( doReplot );
        plt->replot();
    }
    plotZoomed = true;
}

void PlotZoomer::begin()
{
    if (readyToZoom == false)
        return;
    if (maxStackDepth() >= 0)//d_data->maxStackDepth >= 0 )
    {
        if ( zoomRectIndex()/*d_data->zoomRectIndex*/ >= uint(maxStackDepth() /*d_data->maxStackDepth*/ ) )
            return;
    }

    const QSizeF minSize = minZoomSize();
    if ( minSize.isValid() )
    {
        QStack<QRectF>  st = zoomStack();
        const QSizeF sz =st[zoomRectIndex()].size();
         //d_data->zoomStack[d_data->zoomRectIndex].size() * 0.9999;

        if ( minSize.width() >= sz.width() &&
            minSize.height() >= sz.height() )
        {
            return;
        }
    }

    QwtPlotPicker::begin();
}

void PlotZoomer::widgetMousePressEvent( QMouseEvent *mouseEvent )
{
    QPoint pos = mouseEvent->pos();

    if ( mouseMatch( MouseSelect3, mouseEvent ) )
        return;

    primaryZoomer = true;

    bool t1,t2;
    const QwtPlotItemList& itmList = plot()->itemList();
    for ( QwtPlotItemIterator it = itmList.begin();
        it != itmList.end(); ++it )
    {
        if ( (*it)->rtti() == QwtPlotItem::Rtti_PlotMarker )
        {
            PlotMarker *m = (PlotMarker*)(*it);
            if (m->hitTest(pos, t1, t2))
            {
                readyToZoom = false;
                QwtPicker::transition((QEvent*)mouseEvent);
                return;
            }
        }
    }
    readyToZoom = true;
    rectBasePoint =  invTransform(pos);
    QwtPicker::transition((QEvent*)mouseEvent);
}

void PlotZoomer::widgetMouseReleaseEvent( QMouseEvent *me )
{
    if ( mouseMatch( MouseSelect2, me ) )
        zoom( 0 );
    else if ( mouseMatch( MouseSelect3, me ) ) {
        zoom( -1 );
        emit zoomedOut();
    }
    else if ( mouseMatch( MouseSelect6, me ) )
        zoom( +1 );
    else
        QwtPlotPicker::widgetMouseReleaseEvent( me );
}

void PlotZoomer::move( const QPoint &pos )
{
    QwtPicker::move( pos );
    //if (readyToZoom)
    //    emit zoomRectMoving(invTransform(pos));
    Q_EMIT moved( invTransform( pos ) );
}

void PlotZoomer::secondaryAppend(QPointF p) {
    QwtPicker::append(transform(p));
}

void PlotZoomer::secondaryActivated(bool s) {
    //QRectF r(rectBasePoint, p);
    //PlotZoomer *p = dynamic_cast<PlotZoomer*>(QObject::sender());
    if (s)
    {
        setTrackerMode(QwtPicker::AlwaysOff);
        begin();
    }
    else
    {
        setTrackerMode(QwtPicker::ActiveOnly);
        QwtPicker::end();
    }
}

QPointF PlotZoomer::getZoomRectBase() {
    return rectBasePoint;
}

void PlotZoomer::zoom( const QRectF &rect )
{
    QwtPlotZoomer::zoom(rect);
}
void PlotZoomer::zoom( int offset )
{
    QwtPlotZoomer::zoom(offset);
}

bool PlotZoomer::accept( QPolygon &pa ) const
{
    if ( pa.count() < 2 )
        return false;

    QRect rect = QRect( pa[0], pa[int( pa.count() ) - 1] );
    rect = rect.normalized();

    const int minSize = 2;
    if ( rect.width() < minSize && rect.height() < minSize )
        return false;

    //if (!primaryZoomer) return false;
    //primaryZoomer = false;

    const int minZoomSize = 11;

    const QPoint center = rect.center();
    rect.setSize( rect.size().expandedTo( QSize( minZoomSize, minZoomSize ) ) );
    rect.moveCenter( center );

    pa.resize( 2 );
    pa[0] = rect.topLeft();
    pa[1] = rect.bottomRight();

    return true;
}


void PlotZoomer::attachZoomer(PlotZoomer *z) {
    linkedZoomer = z;
    setTrackerMode(QwtPicker::AlwaysOff);
    connect(z, SIGNAL(activated(bool)), this, SLOT(secondaryActivated(bool)));
    connect(z, SIGNAL(appended(QPointF)), this, SLOT(secondaryAppend(QPointF)));
    connect(z, SIGNAL(moved(QPointF)), this, SLOT(secondaryMove(QPointF)));
    connect(z, SIGNAL(selected(QRectF)), this, SLOT(secondarySelected(QRectF)));
    connect(z, SIGNAL(zoomedOut()), this, SLOT(zoomerZoomedOut()));
}

PlotZoomer* PlotZoomer::getLinkedZoomer() {
    return linkedZoomer;
}

void PlotZoomer::secondaryMove(QPointF p) {
    //if (!primaryZoomer)
    QwtPicker::move(transform(p));
}

void PlotZoomer::secondarySelected(QRectF p) {
    //if (primaryZoomer)
    zoom(p);
    //linkedZoomer->zoom(p);
}

void PlotZoomer::zoomerZoomedOut() {
    //if (!primaryZoomer)
    zoom(-1);
}
