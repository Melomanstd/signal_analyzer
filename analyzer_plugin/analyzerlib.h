#ifndef ANALYZERLIB_H
#define ANALYZERLIB_H

#include "analyzerlib_global.h"
//#include "libs/libgen.h"
//#include "mainwindow.h"
#include "interfaces.h"
#include "interfaces2.h"
#include "adcfdemomainwindow.h"

//class MainWindow;

extern "C"
{
    ANALYZERLIBSHARED_EXPORT QWidget* createMainWindow();
}

class ANALYZERLIBSHARED_EXPORT AnalyzerWindow: public QObject, public GUIInterface
{
    Q_OBJECT
    Q_INTERFACES(GUIInterface)

    ADcfDemoMainWindow *anWin;
public:
    AnalyzerWindow();
    void createWindow();
    void destroyWindow();
    QWidget* getMainWindow();
    QTranslator* getTranslator();
};

class ANALYZERLIBSHARED_EXPORT AnalyzerPlugin:public QObject, public DriverInterface
{
    Q_OBJECT
    Q_INTERFACES(DriverInterface)

    ADcfDemoMainWindow *anWin;
    RenderThread *core;
    Mode *mode;
    ProcessObject *process;
    QList <ProcessObject*> processes;
    MeasureConfigItem *conf;
public:
    AnalyzerPlugin();
    ~AnalyzerPlugin();

    int getInfo(DrvInfo *pDrvInfo);
        int getWndInfo(int nWnd, DrvWndInfo *pWndInfo);
        int getCommandInfo(int nCommand, DrvCommandInfo *pCommandInfo);
        int getCommandParamInfo(int nCommand, int nParam, DrvCommandParamInfo *pCommandParamInfo);
        int drvOpen(QString strAddr);
        int drvClose();
        QStringList commands() const;
        QStringList variables() const;
        int getDOInfo(int nVar, DrvVariableInfo *pDataVariable);
        int getDRInfo(int nVar, DrvVariableInfo *pDataVariable);
        int makeCommand(int nCommand, QByteArray &pData, int nActiveObject, PasportSaveValMgr *pArrayPasportSaveVal);
        int drvInit(DrvInitInfo *pDrvInitInfo);
        int drvSuspend();
        QString getErrorStr(int nErrorCode);
        int createWnd(int nWnd);
        int getCoreStatus();

        int someInitsFromAnalyzer();
        int configureChannel(quint8 ch, quint8 entry, quint8 atten, quint8 range);
        int configTimescale(quint8 tsc);
        int configureSpecOptions(double fc, double band, quint8 win);
        int measuring(quint8 ch, quint8 mode);

protected:
    int classNum;
    int drvNum;
    QString DrvName;
    QString ClassName;
    QString DrvAnnotation;
    QString DrvVersion;
    int CommandsCount;
    int WndCount;
    int varCount;

    QMutex mutex;
    QWaitCondition m_wait;
};

//class GENERATORLIBSHARED_EXPORT GeneratorLib
//{
//public:
//    GeneratorLib();
//};
//MainWindow *window = 0;

#endif // ANALYZERLIB_H
