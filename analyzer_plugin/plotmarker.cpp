#include "plotmarker.h"
#include <QPainter>
#include "qwt_painter.h"
#include <QDebug>

PlotMarker::PlotMarker(bool mode) :
    QwtPlotMarker()
{
    classicMarker = mode;
}

PlotMarker::~PlotMarker() {

}

/*!
  Draw the marker

  \param painter Painter
  \param xMap x Scale Map
  \param yMap y Scale Map
  \param canvasRect Contents rect of the canvas in painter coordinates
*/
void PlotMarker::draw( QPainter *painter,
    const QwtScaleMap &xMap, const QwtScaleMap &yMap,
    const QRectF &canvasRect ) const
{
    const QPointF pos( xMap.transform(xValue()), //d_data->xValue ),
        yMap.transform(yValue()));// d_data->yValue ) );

    // draw lines

    drawLines( painter, canvasRect, pos );

    // draw symbol
    if (symbol() /* d_data->symbol*/ &&
        ( /*d_data->symbol*/symbol()->style() != QwtSymbol::NoSymbol ) )
    {
        /*d_data->symbol*/symbol()->drawSymbol( painter, pos );
    }

    drawLabel( painter, canvasRect, pos );
}

/*!
  Draw the lines marker

  \param painter Painter
  \param canvasRect Contents rect of the canvas in painter coordinates
  \param pos Position of the marker, translated into widget coordinates

  \sa drawLabel(), QwtSymbol::drawSymbol()
*/
void PlotMarker::drawLines( QPainter *painter,
    const QRectF &canvasRect, const QPointF &pos ) const
{
    if ( /*d_data->style*/lineStyle() == NoLine )
        return;

    Plot *plt = (Plot*) plot();
    const bool doAlign = QwtPainter::roundingAlignment( painter );
    painter->setPen( /*d_data->pen*/linePen() );
    if ( /*d_data->style*/lineStyle() == QwtPlotMarker::HLine ||
        /*d_data->style*/lineStyle() == QwtPlotMarker::Cross )
    {
        int h = 5;//element height
        int w = 10; //element
        double y = pos.y();
        double x = pos.x();
        if ( doAlign )
            y = qRound( y );


        QPainterPath pth;
        QPolygonF p;
        if (alignment == Qt::AlignLeft) {
            qreal t = canvasRect.left();
            p.append(QPointF(t, y+h));
            p.append(QPointF(t+w, y+h));
            p.append(QPointF(t+w+h, y));
            p.append(QPointF(t+w, y-h));
            p.append(QPointF(t, y-h));
        }
        if (alignment == Qt::AlignRight) {
            qreal t = canvasRect.right()-1;
            p.append(QPointF(t, y+h));
            p.append(QPointF(t-w, y+h));
            p.append(QPointF(t-w-h, y));
            p.append(QPointF(t-w, y-h));
            p.append(QPointF(t, y-h));
        }
        if (alignment == Qt::AlignTop) {
            qreal t = canvasRect.top();
            p.append(QPointF(x+h, t));
            p.append(QPointF(x+h, t+w));
            p.append(QPointF(x, t+w+h));
            p.append(QPointF(x-h, t+w));
            p.append(QPointF(x-h, t));
        }
        if (alignment == Qt::AlignBottom) {
            qreal t = canvasRect.bottom()-1;
            p.append(QPointF(x+h, t));
            p.append(QPointF(x+h, t-w));
            p.append(QPointF(x, t-w-h));
            p.append(QPointF(x-h, t-w));
            p.append(QPointF(x-h, t));
        }
        pth.addPolygon(p);

        painter->save();
        //painter->setOpacity(0.8);
        painter->setPen(QPen(Qt::SolidLine));
        painter->setPen(linePen().color());

        if (!classicMarker) {
            painter->fillPath(pth, QBrush(Qt::darkBlue)); // todo... setBkColor(...)
            painter->drawPolygon(p);
            int textalign = 0;
            int textXDev = 0;
            int textYDev = 0;
            if (alignment == Qt::AlignLeft ||
                     alignment == Qt::AlignRight) {
                if (alignment == Qt::AlignLeft) textXDev+=2;
                if (alignment == Qt::AlignRight) textXDev-=2;
                textalign = alignment | Qt::AlignVCenter;
            }
            if (alignment == Qt::AlignTop ||
                     alignment == Qt::AlignBottom) {
                if (alignment == Qt::AlignTop) textYDev-=2;
                if (alignment == Qt::AlignBottom) textYDev+=2;
                textalign = alignment | Qt::AlignHCenter;
            }
//            QwtPainter::drawText(painter,
//                                 p.boundingRect(),
//                                 textalign,
//                                 textLabel);
            QwtPainter::drawText(painter,
                                 p.boundingRect().x()+textXDev,
                                 p.boundingRect().y()+textYDev,
                                 p.boundingRect().width(),
                                 p.boundingRect().height(),
                                 textalign,
                                 textLabel);
        }
        painter->restore();
        if (classicMarker)
            QwtPainter::drawLine( painter, canvasRect.left(),
                y, canvasRect.right() - 1.0, y );

    }
    if ( /*d_data->style*/lineStyle() == QwtPlotMarker::VLine ||
        /*d_data->style*/lineStyle() == QwtPlotMarker::Cross )
    {
        double x = pos.x();
        if ( doAlign )
            x = qRound( x );

        QwtPainter::drawLine( painter, x,
            canvasRect.top(), x, canvasRect.bottom() - 1.0 );
    }
}

void PlotMarker::setText(QString text) {
    if (textLabel != text) {
        textLabel = text;
        itemChanged();
    }
}

void PlotMarker::setAlignment(int a) {
    alignment = a;
}

bool PlotMarker::hitTest(QPoint pos,
                         bool &hLine,
                         bool &vLine) {
    if (plot() == NULL)
        return false;
    hLine = false;
    vLine = false;
    QwtScaleMap yMap = plot()->canvasMap(yAxis());
    QwtScaleMap xMap = plot()->canvasMap(xAxis());
    QPointF sample = value();
    double cx = xMap.transform( sample.x() ) - pos.x();
    double cy = yMap.transform( sample.y() ) - pos.y();
    if (classicMarker)
        if (qAbs(cy) <= 3 || qAbs(cx) <= 3){
            if (qAbs(cy) <= 3) vLine = true;
            if (qAbs(cx) <= 3) hLine = true;
            return true;
        }
    if (!classicMarker) {
        int size = 17;
        if (alignment == Qt::AlignLeft ||
                alignment == Qt::AlignRight) {
            if (alignment == Qt::AlignLeft)
                if (qAbs(cy)<=3 && pos.x()<=size) {
                    vLine = true;
                    return true;
                }
            if (alignment == Qt::AlignRight)
                if (qAbs(cy)<=3 && pos.x()>=xMap.p2()-size) {
                    vLine = true;
                    return true;
                }
        }
        if (alignment == Qt::AlignBottom ||
                alignment == Qt::AlignTop) {
            if (alignment == Qt::AlignTop)
                if (qAbs(cx)<=3 && pos.y()<=size) {
                    hLine = true;
                    return true;
                }
            if (alignment == Qt::AlignBottom)
                if (qAbs(cx)<=3 && pos.y()>=yMap.p1()-size) {
                    hLine = true;
                    return true;
                }
        }
    }
    return false;
}

void PlotMarker::setClassicMode(bool m) {
    if (classicMarker != m) {
        classicMarker = m;
        itemChanged();
    }
}

void PlotMarker::moveMarker(QPointF &pos,
                            bool &moveH,
                            bool &moveV,
                            Plot *p)
{
    if (p == NULL) return;

    QwtScaleMap yMap = p->canvasMap( yAxis() );
    QwtScaleMap xMap = p->canvasMap( xAxis() );
    double yVal = yMap.invTransform(pos.y());
    double xVal = xMap.invTransform(pos.x());
    if (moveH && moveV)
        setValue(xVal, yVal);
    else if (moveH)
        setXValue(xVal);
    else if (moveV)
        setYValue(yVal);
}

//PEAKMARKER  -----------------------------
PeakMarker::PeakMarker()
    :PlotMarker()
{
    peaksCount=0;
}

PeakMarker::~PeakMarker()
{
}

bool PeakMarker::hitTest(QPoint pos, bool &hLine, bool &vLine)
{
    return false;
}

void PeakMarker::drawLines( QPainter *painter,
    const QRectF &canvasRect, const QPointF &pos ) const
{
    if ( /*d_data->style*/lineStyle() == NoLine )
        return;
    qDebug()<<"in peakMarkerDrawLines";
    Plot *plt = (Plot*) plot();
    const bool doAlign = QwtPainter::roundingAlignment( painter );
    painter->setPen( /*d_data->pen*/linePen() );
    if ( /*d_data->style*/lineStyle() == QwtPlotMarker::HLine ||
        /*d_data->style*/lineStyle() == QwtPlotMarker::Cross )
    {
        int h = 6;//element height
        //int w = 6; //element
        double y = pos.y();
        double x = pos.x();
        if ( doAlign )
            y = qRound( y );



        /*p.append(QPointF(x+10, y));
        p.append(QPointF(x+15, y+15));
        p.append(QPointF(x, y+10));
        p.append(QPointF(x, y));*/
        //pth.addPolygon(p);
painter->save();

        QPainterPath pth;
        QPolygonF p;
        //double x1 = peaks[0].x(), y1=peaks[0].y();
        QwtScaleMap xmap = plot()->canvasMap(xAxis());
        QwtScaleMap ymap = plot()->canvasMap(yAxis());
        /*x1 = xmap.transform(peaks[0].x());
        y1 = ymap.transform(peaks[0].y());
        p.append(QPointF(x, y));
        p.append(QPointF(x+h/2, y-h));
        p.append(QPointF(x-h/2, y-h));

        pth.addPolygon(p);
        painter->fillPath(pth, QBrush(Qt::black));
        painter->drawPolygon(p);*/
        //painter->setOpacity(0.8);
        //painter->setPen(QPen(Qt::SolidLine));
        //painter->setPen(linePen().color());

        int textalign = 0;
        int textXDev = 0;
        int textYDev = 0;
        if (alignment == Qt::AlignTop ||
                 alignment == Qt::AlignBottom) {
            if (alignment == Qt::AlignTop) textYDev-=2;
            if (alignment == Qt::AlignBottom) textYDev+=2;
            textalign = alignment | Qt::AlignHCenter;
        }

        for (int i=0; i<peaksCount; i++)
        {
//            double x1 = xmap.transform(peaks[i].x());
//            double y1 = ymap.transform(peaks[i].y());
            double x1 = xmap.transform(xPeakVal[i]*lastXGain);
            double y1 = ymap.transform(yPealVal[i]*lastYGain);
            QPainterPath pth;
            QPolygonF p;

            p.append(QPointF(x1, y1));
            p.append(QPointF(x1+h/2, y1-h));
            p.append(QPointF(x1-h/2, y1-h));
            pth.addPolygon(p);
            painter->fillPath(pth, QBrush(Qt::black)); // todo... setBkColor(...)
            painter->drawPolygon(p);

            QwtPainter::drawText(painter,
                                 p.boundingRect().x()-5,
                                 p.boundingRect().y()-15,
                                 p.boundingRect().width()+10,
                                 p.boundingRect().height()+10,
                                 textalign,
                                 QString::number(i));
        }

//        QwtPainter::drawText(painter,
//                  p.boundingRect().x()-5,
//                  p.boundingRect().y()-15,
//                  p.boundingRect().width()+10,
//                  p.boundingRect().height()+10,
//                  textalign,
//                  textLabel);

        painter->restore();

    }
    if ( /*d_data->style*/lineStyle() == QwtPlotMarker::VLine ||
        /*d_data->style*/lineStyle() == QwtPlotMarker::Cross )
    {
        double x = pos.x();
        if ( doAlign )
            x = qRound( x );

        QwtPainter::drawLine( painter, x,
            canvasRect.top(), x, canvasRect.bottom() - 1.0 );
    }
    qDebug()<<"leaving peakMarkerDrawLines";
}

int PeakMarker::rtti() const
{
    return -1;
}

void PeakMarker::setPeaks(QPointF *pArr) {
    for (int i=0; i<100; i++)
    {
        peaks[i]=pArr[i];
    }
}

void PeakMarker::setPeaks(int *xArr, int *yArr,
                          double gainY,
                          double gainX)
{
    qDebug()<<"in setpeaks int";
    for (int i=0;i<100;i++)
    {
        xPeakVal[i]=xArr[i];
        yPealVal[i]=yArr[i];
    }
    lastXGain = gainX;
    lastYGain = gainY;
    qDebug()<<"leaving setpeaks int";
}

void PeakMarker::setPeaksCount(int c) {
    qDebug()<<"in setpeakscount";
    peaksCount=c;
    qDebug()<<"leaving setpeakscont";
}
