#ifndef PLOTDIALOG_H
#define PLOTDIALOG_H

#include <QDialog>
#include "mode.h"

class MeasureConfigItem;

namespace Ui {
class PlotDialog;
}

class PlotDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit PlotDialog(QWidget *parent = 0);
    ~PlotDialog();
    void fillPlotBox(QString name, Mode* m);
    Mode* getMode();
    void setMode(Mode* m);
    QString getModeName();
    bool isReady();

public slots:
    void setCurrentMode(QString name);
    
private:
    Ui::PlotDialog *ui;
    QMap<QString, Mode*> map;
    Mode *mode;
    QString modeName;
    bool ready;
};

#endif // PLOTDIALOG_H
