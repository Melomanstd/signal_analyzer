#ifndef SPECTREWIDGET_H
#define SPECTREWIDGET_H
#include "basetabwidget.h"

class SpectreWidget : public baseTabWidget
{
public:
    SpectreWidget(QWidget *parent=0);
    QString getName();
};

#endif // SPECTREWIDGET_H
