#include "analyzerlib.h"
#include "QTcpSocket"



//GeneratorLib::GeneratorLib()
//{

//}

QWidget* createMainWindow()
{
    //window = new MainWindow;
    //window->hide();
    //return window;//window->centralWidget();
    return 0;
}

///-------------------------------------
AnalyzerWindow::AnalyzerWindow()
{
    anWin = 0;
}

void AnalyzerWindow::createWindow()
{
    if (anWin) return;
    anWin = new ADcfDemoMainWindow;
//    anWin->hide();
}

void AnalyzerWindow::destroyWindow()
{
    if (!anWin) return;
    delete anWin;
    anWin = 0;
}

QWidget* AnalyzerWindow::getMainWindow()
{
//    return anWin->centralWidget();
    return anWin;
}

QTranslator* AnalyzerWindow::getTranslator()
{
    return &anWin->translator;
}
//-------------------------------------------------

AnalyzerPlugin::AnalyzerPlugin()
{
    anWin = 0;
    core = 0;
    mode=0;
    process=0;
    conf=0;
    classNum = 3;
    drvNum = 3;
    DrvName = "AnalyzerDrv";
    ClassName = "AnalyzerClass";
    DrvAnnotation = "Analyzer Driver";
    DrvVersion = "0.000001";
    CommandsCount = 4;
    WndCount = 1;
    varCount = 11;
}

AnalyzerPlugin::~AnalyzerPlugin()
{}

int AnalyzerPlugin::getInfo(DrvInfo *pDrvInfo)
{
    pDrvInfo->Number = drvNum;
        pDrvInfo->Class = classNum;
        pDrvInfo->Name = DrvName;
        pDrvInfo->NameClass = ClassName;
        pDrvInfo->Annotation = DrvAnnotation;
        pDrvInfo->Version = DrvVersion;
        pDrvInfo->NumCommand = CommandsCount;
        pDrvInfo->NumDRVar = varCount;
        pDrvInfo->NumDOVar = -1;
        pDrvInfo->NumWnd = WndCount;
        pDrvInfo->bCreateProtocol = false;
        pDrvInfo->bResultWindow = false;
        pDrvInfo->bRunNoCustomTest = false;
    return 0;
}

int AnalyzerPlugin::getWndInfo(int nWnd, DrvWndInfo *pWndInfo)
{
    if (nWnd < 0 || nWnd >= WndCount) return -1;
    pWndInfo->wnd = (void*) anWin;
    pWndInfo->name = tr("Analyzer");
    pWndInfo->bShowForAdmin = true;
    pWndInfo->bShowForProgrammer = true;
    pWndInfo->bShowForUser = true;
    pWndInfo->bShowInEditMode = true;
    pWndInfo->bShowInRunMode = true;
    return 0;
}

int AnalyzerPlugin::getCommandInfo(int nCommand, DrvCommandInfo *pCommandInfo)
{
    if(nCommand >= CommandsCount || nCommand < 0) return -1;
    switch (nCommand)
    {
    case 0:
        pCommandInfo->Name = tr("Configure Channel");//"Установить параметры канала";
        pCommandInfo->Annotation = tr("Configure Channel a");//"Установить параметры канала";
        pCommandInfo->NumParam = 4;
        break;
    case 1:
        pCommandInfo->Name = tr("Configure Timescale");//"Установить параметры записи";
        pCommandInfo->Annotation = tr("Configure Timescale a");//"Установить параметры записи";
        pCommandInfo->NumParam = 1;
        break;
    case 2:
        pCommandInfo->Name = tr("Configure Spectre");//"Установить параметры спектра";
        pCommandInfo->Annotation = tr("Configure Spectre a");//"Установить параметры спектра";
        pCommandInfo->NumParam = 3;
        break;
    case 3:
        pCommandInfo->Name = tr("Measure");//"Измерить";
        pCommandInfo->Annotation = tr("Measure a");//"Измерить";
        pCommandInfo->NumParam = 2;
        break;
    }

    return 0;
}

int AnalyzerPlugin::getCommandParamInfo(int nCommand, int nParam, DrvCommandParamInfo *pCommandParamInfo)
{
    if (nCommand < 0 || nCommand >= CommandsCount) return -1;
    switch(nCommand)
    {
    case 0:
        switch(nParam)
        {
        case 0:
            pCommandParamInfo->Name = tr("Channel");//"Канал";
            pCommandParamInfo->Annotation = tr("1\n2\n");
            pCommandParamInfo->Type = TYPE_ENUM;
            pCommandParamInfo->LRange = 0;
            pCommandParamInfo->RRange = 0;
            break;
        case 1:
            pCommandParamInfo->Name = tr("Input");//"Вход";
            pCommandParamInfo->Annotation = tr("50Om\n600Om\n1MOm\n");
            pCommandParamInfo->Type = TYPE_ENUM;
            pCommandParamInfo->LRange = 0;
            pCommandParamInfo->RRange = 0;
            break;
        case 2:
            pCommandParamInfo->Name = tr("Attenuator");//"Делитель(аттенюатор)";
            pCommandParamInfo->Annotation = tr("1/1\n1/10\n1/30\n1/300\n");
            pCommandParamInfo->Type = TYPE_ENUM;
            pCommandParamInfo->LRange = 0;
            pCommandParamInfo->RRange = 0;
            break;
        case 3:
            pCommandParamInfo->Name = tr("Range");//"Диапазон(Вольт/Деление)";
            pCommandParamInfo->Annotation = tr("1mV/s\n2mV/s\n5mV/s\n10mV/s\n20mV/s\n50mV/s\n100mV/s\n200mV/s\n500mV/s\n1V/s\n2V/s\n5V/s\n10V/s\n");
            pCommandParamInfo->Type = TYPE_ENUM;
            pCommandParamInfo->LRange = 0;
            pCommandParamInfo->RRange = 0;
            break;
        }
        break;
    case 1:
        pCommandParamInfo->Name = tr("Timescale");//"Развертка";
        pCommandParamInfo->Annotation = tr("1mkS\n2mkS\n5mkS\n10mkS\n20mkS\n50mkS\n100mkS\n200mkS\n500mkS\n1mS\n2mS\n5mS\n10mS\n20mS\n50mS\n100mS\n200mS\n500mS\n1Sec\n2Sec\n5Sec");
        pCommandParamInfo->Type = TYPE_ENUM;
        pCommandParamInfo->LRange = 0;
        pCommandParamInfo->RRange = 0;
        break;
    case 2:
        switch(nParam)
        {
        case 0:
            pCommandParamInfo->Name = tr("Central Frequency");//"Центральная частота";
            pCommandParamInfo->Annotation = tr("Central Frequency a");//"Центральная частота";
            pCommandParamInfo->Type = TYPE_DOUBLE;
            pCommandParamInfo->LRange = 0;
            pCommandParamInfo->RRange = 0;
            break;
        case 1:
            pCommandParamInfo->Name = tr("Bandwidth");//"Ширина полосы";
            pCommandParamInfo->Annotation = tr("Bandwidth a");//"Ширина полосы";
            pCommandParamInfo->Type = TYPE_DOUBLE;
            pCommandParamInfo->LRange = 0;
            pCommandParamInfo->RRange = 0;
            break;
        case 2:
            pCommandParamInfo->Name = tr("Window");//"Тип окна";
            pCommandParamInfo->Annotation = tr("Отсутствует\nХанна\nХэминга\nБлэкмана\nБлэкмана-Харриса\nС плоской вершиной\n");
            pCommandParamInfo->Type = TYPE_ENUM;
            pCommandParamInfo->LRange = 0;
            pCommandParamInfo->RRange = 0;
            break;
        }
        break;
    case 3:
        switch(nParam)
        {
        case 0:
            pCommandParamInfo->Name = tr("Channel");//"Канал";
            pCommandParamInfo->Annotation = tr("1 Channel\n2 Channel\n");
            pCommandParamInfo->Type = TYPE_ENUM;
            pCommandParamInfo->LRange = 0;
            pCommandParamInfo->RRange = 0;
            break;
        case 1:
            pCommandParamInfo->Name = tr("Measuring Mode");//"Тип измерения";
            pCommandParamInfo->Annotation = tr("Deep\nDeviation\nOscilograph, SKZ, Amplitude, Ti, T\nSpectre Analyzer\nFrequency\nKNI\nDeformation TLG\nPHOSO\n");
            pCommandParamInfo->Type = TYPE_ENUM;
            pCommandParamInfo->LRange = 0;
            pCommandParamInfo->RRange = 0;
            break;
        }
        break;
    }

    return 0;
}

int AnalyzerPlugin::drvOpen(QString strAddr)
{
    if (core) return -1;
    QStringList address = strAddr.split("::");
    if (address.size()!=4) return -3;
    core = new RenderThread(0,0/*m_worker*/, address.at(1), address.at(2), this);
    conf = new MeasureConfigItem;
    Mode::clearConfig(conf);
    processes.append(new OscProcess);
    processes.last()->setID(2);
//    processes.last()->setLimFunc(true);
    processes.append(new SpecProcess);
    processes.last()->setID(3);
//    processes.last()->setLimFunc(true);
    processes.append(new IqAnProcess);
    processes.last()->setID(0);
//    processes.last()->setLimFunc(true);

    for (int i=0;i<3;i++)
    {
        processes.at(i)->setRender(core);
        processes.at(i)->setConfig(conf);
    }

//    getCoreStatus();
    return 0;
}

int AnalyzerPlugin::drvClose()
{
    if(!core) return -1;
    core->stopProcess();
    core->wait(3000);
    delete core;
    core = 0;
    return 0;
}

QStringList AnalyzerPlugin::commands() const
{
    return QStringList("");
}

QStringList AnalyzerPlugin::variables() const
{
    return QStringList("");
}

int AnalyzerPlugin::getDOInfo(int nVar, DrvVariableInfo *pDataVariable)
{
    return 0;
}

int AnalyzerPlugin::getDRInfo(int nVar, DrvVariableInfo *pDataVariable)
{
    if (nVar < 0 || nVar >= varCount) return -1;
    //if (!process) return -1;
        switch(nVar)
        {
        case 0:
            pDataVariable->Name = "Urms";
            pDataVariable->Annotation = "СКЗ";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "В";
            pDataVariable->pValue = (void*) &(processes.at(Mode::OSCI_ID)->SKZ);
            break;
        case 1:
            pDataVariable->Name = "Uavg";
            pDataVariable->Annotation = "Среднее значение";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "В";
            pDataVariable->pValue = (void*) &(processes.at(Mode::OSCI_ID))->AVG;
            break;
        case 2:
            pDataVariable->Name = "Umax";
            pDataVariable->Annotation = "Наибольшее значение сигнала";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "В";
            pDataVariable->pValue = (void*) &(processes.at(Mode::OSCI_ID))->posPeak;
            break;
        case 3:
            pDataVariable->Name = "Umin";
            pDataVariable->Annotation = "Наименьшее значение сигнала";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "В";
            pDataVariable->pValue = (void*) &(processes.at(Mode::OSCI_ID))->negPeak;
            break;
        case 4:
            pDataVariable->Name = "F";
            pDataVariable->Annotation = "Измерянная частота";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "Гц";
            pDataVariable->pValue = (void*) &(processes.at(Mode::SPEC_ID))->F;
            break;
        case 5:
            pDataVariable->Name = "Sinad";
            pDataVariable->Annotation = "Коэфицент нелинейной модуляции";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "%";
            pDataVariable->pValue = (void*) &(processes.at(Mode::SPEC_ID))->sinad;
            break;
        case 6:
            pDataVariable->Name = "kgi";
            pDataVariable->Annotation = "Коэфицент гармонических искажений";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "%";
            pDataVariable->pValue = (void*) &(processes.at(Mode::SPEC_ID))->kgi;
            break;
        case 7:
            pDataVariable->Name = "kni";
            pDataVariable->Annotation = "Коэфицент нелинейной модуляции";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "%";
            pDataVariable->pValue = (void*) &(processes.at(Mode::SPEC_ID))->kni;
            break;
        case 8:
            pDataVariable->Name = "Am";
            pDataVariable->Annotation = "Глубина модуляции";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "%";
            pDataVariable->pValue = (void*) &(processes.at(Mode::IQAN_ID))->Am;
            break;
        case 9:
            pDataVariable->Name = "Fdev";
            pDataVariable->Annotation = "Девиация";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "Гц";
            pDataVariable->pValue = (void*) &(processes.at(Mode::IQAN_ID))->Fdev;
            break;
        case 10:
            pDataVariable->Name = "Ktlg";
            pDataVariable->Annotation = "Коэфицент ТЛГ отклонений";
            pDataVariable->Type = TYPE_DOUBLE;
            pDataVariable->ED = "%";
            pDataVariable->pValue = (void*) &(processes.at(Mode::IQAN_ID))->Am;
            break;
        }
    return 0;
}

int AnalyzerPlugin::makeCommand(int nCommand, QByteArray &pData, int nActiveObject, PasportSaveValMgr *pArrayPasportSaveVal)
{
    if (nCommand<0 || nCommand>CommandsCount) return -1;
    QDataStream data(pData);
    quint8 param1=0;
    quint8 param2=0;
    quint8 param3=0;
    quint8 param4=0;
    double param5=0;
    double param6=0;
    switch(nCommand)
    {
    case 0:
        data>>param1; data>>param2; data>>param3; data>>param4;
        configureChannel(param1,param2, param3,param4);
        break;
    case 1:
        data>>param1;
        configTimescale(param1);
        break;
    case 2:
        data>>param5; data>>param6; data>>param1;
        configureSpecOptions(param5, param6, param1);
        break;
    case 3:
        data>>param1; data>>param2;
        measuring(param1, param2);
        break;
    }

    return 0;
}

int AnalyzerPlugin::drvInit(DrvInitInfo *pDrvInitInfo)
{
    if (!anWin) return -1;
    //anWin->setEnabled(false);
//    anWin->blockGUI(true);
    return 0;
}

int AnalyzerPlugin::drvSuspend()
{
    if (!anWin) return -1;
//    anWin->setEnabled(true);
//    anWin->blockGUI(false);
    return 0;
}

QString AnalyzerPlugin::getErrorStr(int nErrorCode)
{
    return "";
}

int AnalyzerPlugin::createWnd(int nWnd)
{
    if (anWin || !core) return -1;
    if (nWnd<0 || nWnd > WndCount) return -2;
    switch (nWnd)
    {
    case 0:
        anWin = new ADcfDemoMainWindow(core, true);
        anWin->hide();
        break;
    }
    return 0;
}

int AnalyzerPlugin::getCoreStatus()
{
    if (!core) return -1;
    int res = core->checkConnection();
    return res;
}

int AnalyzerPlugin::someInitsFromAnalyzer()
{
    if (!conf) return -1;
    /*double Fs*/conf->fFs =1000000*200;//ui->edit_Fs->text().toDouble();
        conf->fFcentr/*Fcentr*/=1000000*conf->uiFCentr;//ui->edit_Fcentr->text().toDouble();
        conf->fBand/*Band*/=1000*conf->uiBand;//ui->edit_Band->text().toDouble();
    /*double */
        //Fleft=ui->edit_Fleft->text().toDouble();
        conf->Fleft/*Fleft*/=conf->fFcentr/*Fcentr*/-conf->fBand/*Band*//2;
    /*double */
        //Fright=ui->edit_Fright->text().toDouble();
        conf->Fright/*Fright*/=conf->fFcentr/*Fcentr*/+conf->fBand/*Band*//2;
//    /*int */window=ui->combo_WindowType->currentIndex();
    /*int */conf->Nfft/*Nfft*/=FFT_SAMPLES_NUM;

    /*double *//*Fcentr=(Fleft+Fright)/2;*/
    /*double *//*Band=Fright-Fleft;*/

    /*double */conf->Dec_ideal/*Dec_ideal*/=conf->fFs/*Fs*//(2*conf->fBand/*Band*/);
    /*int */conf->Dec_3/*Dec_3*/=(conf->Dec_ideal>=2)? 2 : 1;

    /*int */conf->Dec_1=qFloor(conf->Dec_ideal/conf->Dec_3); //41.2 -> 41
    if(conf->Dec_1>=256)conf->Dec_1=256;

    /*int */conf->Dec_2=qFloor(conf->Dec_ideal/(conf->Dec_1*conf->Dec_3));
    if(conf->Dec_2>=256)conf->Dec_2=256;

    /*unsigned long */conf->Dec_sum=conf->Dec_1*conf->Dec_2*conf->Dec_3;

    /*double */conf->Fdec/*Fdec*/=conf->fFs/*Fs*//conf->Dec_sum;

    /*int */conf->Nband/*Nband*/=(conf->fBand/*Band*/*conf->Nfft)/conf->Fdec/*Fdec*/;

    conf->nTaktADC = 1;
//    conf-
    return 0;
}

int AnalyzerPlugin::configureChannel(quint8 ch,
                                     quint8 entry,
                                     quint8 atten,
                                     quint8 range)
{
    if (!conf) return -1;
    if (ch!=0 && ch!=1) return -2;
    if (ch==0)
    {
        conf->nChType = entry;
        conf->nChAtten=atten;
        conf->sC1 = range;
    }
    if (ch==1)
    {
        conf->nChType2 = entry;
        conf->nChAtten2=atten;
        conf->sC2 = range;
    }
    return 0;
}

int AnalyzerPlugin::configTimescale(quint8 tsc)
{
    if (!conf) return -1;
    conf->tSc = tsc;
    return 0;
}

int AnalyzerPlugin::configureSpecOptions(double fc, double band, quint8 win)
{
    if (!conf) return -1;
    conf->uiFCentr = fc;
    conf->uiBand = band;
    conf->nFftWindow = win;
    return 0;
}

int AnalyzerPlugin::measuring(quint8 ch, quint8 mode)
{
    if (!core || !conf) return -1;
    if (ch!=0 && ch!=1) return -2;
    if (mode<0 || mode >3) return -2;
    someInitsFromAnalyzer();
    if (ch==0)
        conf->Ch1=true;
    if (ch==1)
        conf->Ch2=true;

    conf->subMeasMode = 0;
    conf->bCycle=false;

    switch (mode)
    {
    case 2:
//        core->setProcess(processes.at(Mode::OSCI_ID));
        process = processes.at(Mode::OSCI_ID);
//        processes.at(Mode::OSCI_ID)->setPlot(anWin->currentMode->getPlot());
//        processes.at(Mode::OSCI_ID)->setID(mode);
        break;
    case 3:
    case 4:
    case 5:
    case 7:
//        core->setProcess(processes.at(Mode::SPEC_ID));
        process = processes.at(Mode::SPEC_ID);
//        processes.at(Mode::SPEC_ID)->setPlot(anWin->currentMode->getPlot());
//        processes.at(Mode::SPEC_ID)->setID(mode);
        break;
    case 0:
    case 1:
    case 6:
//        core->setProcess(processes.at(Mode::IQAN_ID));
        process = processes.at(Mode::IQAN_ID);
//        processes.at(Mode::IQAN_ID)->setPlot(anWin->currentMode->getPlot());
//        processes.at(Mode::IQAN_ID)->setID(mode);
        break;
    }
    core->setProcess(process);
    if (anWin)
        process->setPlot(anWin->currentMode->getPlot());
    else process->setPlot(0);
    process->setID(mode);
    process->setLimFunc(!anWin);

    core->setExternalWaitCodition(&m_wait);
    core->startProcess2(conf);
    m_wait.wait(&mutex);


    return 0;
}

//-------------------------------------------------

Q_EXPORT_PLUGIN2(analyzerLib, AnalyzerPlugin)
