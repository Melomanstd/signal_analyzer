#ifndef TIMERCOMBOBOX_H
#define TIMERCOMBOBOX_H

#include <QComboBox>
#include <QTimer>

class TimerComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit TimerComboBox(QWidget *parent = 0,int msec = 200 );
    
signals:
    void itemChanged(int index);
    
public slots:
    void setTimer(int i);
    void changeIndex();

protected:
    QTimer timer;
    
};

#endif // TIMERCOMBOBOX_H
