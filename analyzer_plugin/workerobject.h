/****************************************************************************

****************************************************************************/


#ifndef WORKEROBJECT_H
#define WORKEROBJECT_H

#include <QtCore>
#include <QUdpSocket>

//#include "thread.h"
//#include "renderthread.h"
#include "measconf.h"

#define PI 3.1415926535897932384626433832795

#define FFT_WIN_NUM 6
#define FFT_WIN_LEN 4096

#define RAW_SAMPLES_NUM 16384
#define FFT_SAMPLES_NUM 4096

//class RenderThread;
class WriteRegRequest;

enum GenMode
{
    DDS_OFF,
    DDS_XXX,
    DDS_F1B,
    DDS_G1B,
    DDS_J3E,
    DDS_R3E,
    DDS_A3E,
    DDS_H3E,
    DDS_A1A,
    DDS_F3EA,
    DDS_F3EJ
};

//#define asd

#ifdef asd
class WorkerObject : public QObject
{
    Q_OBJECT
public:
    explicit WorkerObject(QString strIp, QString strOscIp, QObject *parent = 0);
    virtual ~WorkerObject();

//    QThread* getMainThread();
//    void setMainThread(QThread *thr);
    void readData(int &m_nReadPtr, int m_nMeasMode, char* currentData);

signals:
    void recvPacket(const QByteArray &datagram);
    void resumeRead();

public slots:
    void readPendingDatagrams();
    void writeData(const QByteArray &datagram);
    void WriteReg(quint8 BADDR, const QList<QPoint> &WDATA);
//    void readData(RenderThread *r);
    void startDecoding(int nCh, int m_nMeasMode);
    void processDatagram(const QByteArray& datagram, int &m_nReadPtr, char* currentData);
    void processDdcDatagram(QByteArray datagram, int &m_nReadPtr, char* currentData);
    void processFftDatagram(QByteArray datagram, int &m_nReadPtr, char* currentData);
    void processRawDatagram(QByteArray datagram, int &m_nReadPtr, char* currentData);

    void open();
    void close();

    virtual void doWork();

    void initPLL(int step);
    void initDDS();
    void initDDS_XXX(double Fs,double Fc,quint64 Amax);
    void initDDS_J3E(double Fs, double Fc, quint64 Amax, double Fb, quint8 SSBType);
    void initDDS_R3E(double Fs, double Fc, quint64 Amax, double Fb, quint8 SSBType);
    void initDDS_F1B(double Fs, double Fc, quint64 Amax, double Fdev, quint32 Fsym);
    void initDDS_G1B(double Fs, double Fc, quint64 Amax, quint32 Fsym);
    void initDDS_A3E(double Fs, double Fc, quint64 Amax, double Fmod, double K, quint8 N=32);
    void initDDS_H3E(double Fs, double Fc, quint64 Amax, double Fmod, quint8 SSBType, quint8 N=32);
    void initDDS_F3EA(double Fs, double Fc, quint64 Amax, double Fdev, double Fmod, quint32 N=1024);
    void initDDS_F3EJ();
    void initDDS_A1A(double Fs, double Fc, quint64 Amax, quint64 Amin, double Fsym);

    void initGEN(quint8 nMode, quint64 nFc, quint32 nAmax, quint32 nAmin, quint64 nFmod, quint64 nFsym, quint64 nFdev, quint8 nSSBType, qreal fK);

    void initTakt(int nTaktADC);
    void initDevice();
//    void initDDC(bool bFft, RenderThread *r);
//    void initDDC_TLG(RenderThread *r);
//    void initFFT_PHOSO(RenderThread *r);
//    void initFFT_FREQ(RenderThread *r);
    void initDDC(bool bFft, MeasureConfigItem *modeConf);
    void initDDC_TLG(MeasureConfigItem *modeConf);
    void initFFT_PHOSO(MeasureConfigItem *modeConf);
    void initFFT_FREQ(MeasureConfigItem *modeConf);

    void initChannel(int nCh, int nChType, int nChAtten);
    void processQueue();

protected slots:
    void WriteLog(const QList<QPoint>& args);
    void WriteLog(QByteArray frame, bool bIn);

    void cmd_29(quint8 AD_9954_RESET, quint8 MODE, quint8 RAM_ADDR);
    void cmd_30(quint8 DATA_LENGTH, quint8 INSTRUCT_CODE, quint64 DATA);

public:
    int fft_window[FFT_WIN_NUM][FFT_WIN_LEN];
    int pllInitSteps;

protected:
//    Thread*     m_thread;
//    QThread *mainThread;
    QUdpSocket* m_udpSocket;
    QMutex m;
    QQueue<WriteRegRequest *> m_queue;

    QString m_strIp;
    QString m_strOscIp;
    quint32 port;


};

class WriteRegRequest:public QObject {
    Q_OBJECT
public:
    WriteRegRequest(qint8 b, QList<QPoint> data,QObject *parent = 0);
    ~WriteRegRequest();

    qint8 BADDR;
    QList <QPoint> WDATA;
};
#endif
#endif // WORKEROBJECT_H
