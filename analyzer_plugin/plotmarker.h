#ifndef PLOTMARKER_H
#define PLOTMARKER_H

#include "qwt_plot_marker.h"
#include "plot.h"

class Plot;
class CanvasPicker;

class PlotMarker : public QwtPlotMarker
{
public:
    PlotMarker(bool mode = true);
    ~PlotMarker();

    virtual void draw( QPainter *p,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF & ) const;
    void setText(QString text);
    void setAlignment(int a);
    virtual bool hitTest(QPoint pos, bool &hLine, bool &vLine);
    void setClassicMode(bool m);
    void moveMarker(QPointF &pos, bool &moveH, bool &moveV, Plot *p);

protected:
    virtual void drawLines( QPainter *,
        const QRectF &, const QPointF & ) const;

    int alignment;
    QString textLabel;
private:


    bool classicMarker;
};

class PeakMarker: public PlotMarker
{
public:
    PeakMarker();
    ~PeakMarker();
    virtual int rtti() const;
    virtual bool hitTest(QPoint pos, bool &hLine, bool &vLine);
    void setPeaks(QPointF *pArr);
    void setPeaks(int *xArr, int* yArr,
                  double gainY, double gainX);
    void setPeaksCount(int c);


protected:
    virtual void drawLines( QPainter *,
        const QRectF &, const QPointF & ) const;

    QPointF peaks[100];
    int xPeakVal[100];
    int yPealVal[100];
    double lastXGain;
    double lastYGain;
    int peaksCount;
};

#endif // PLOTMARKER_H
