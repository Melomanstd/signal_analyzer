#ifndef PEAKSMARKERSWIDGET_H
#define PEAKSMARKERSWIDGET_H

#include <QWidget>
#include <QPointF>

namespace Ui {
class PeaksMarkersWidget;
}

class PeaksMarkersWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit PeaksMarkersWidget(QWidget *parent = 0);
    ~PeaksMarkersWidget();

    void headersInit(int count);
    void setValues(QPointF *values, int size) const;
    void setValues(int *xArr, int *yArr, int size,
                   double gainY, double gainX) const;
    
private:
    Ui::PeaksMarkersWidget *ui;
};

#endif // PEAKSMARKERSWIDGET_H
