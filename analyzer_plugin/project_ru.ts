<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>ADcfDemoMainWindow</name>
    <message>
        <location filename="adcfdemomainwindow.ui" line="26"/>
        <source>ADcfDemoMainWindow</source>
        <translation type="unfinished">АПАК КД</translation>
    </message>
    <message>
        <source>Decoder State:</source>
        <translation type="obsolete">Состояние:</translation>
    </message>
    <message>
        <source>not connected</source>
        <translation type="obsolete">не подключен</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1508"/>
        <location filename="adcfdemomainwindow.cpp" line="1820"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <source>Scan</source>
        <translation type="obsolete">Сканировать</translation>
    </message>
    <message>
        <source>US &quot;Proliv&quot; frequencies</source>
        <translation type="obsolete">Частоты УС &quot;Пролив&quot;</translation>
    </message>
    <message>
        <source>DPC Limit:</source>
        <translation type="obsolete">Ограничения ДПЧ:</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>Prediction</source>
        <translation type="obsolete">Прогноз</translation>
    </message>
    <message>
        <source>KMS</source>
        <translation type="obsolete">КМС</translation>
    </message>
    <message>
        <source>Net &quot;Vyuga&quot;, &quot;Monolit&quot;, &quot;Proliv&quot;</source>
        <translation type="obsolete">Р/с &quot;Вьюга&quot;, &quot;Монолит&quot;, ОБУ УС &quot;Пролив&quot;</translation>
    </message>
    <message>
        <source>&quot;Vyuga&quot;</source>
        <translation type="obsolete">&quot;Вьюга&quot;</translation>
    </message>
    <message>
        <source>&quot;Monolit&quot;</source>
        <translation type="obsolete">&quot;Монолит&quot;</translation>
    </message>
    <message>
        <source>OBU</source>
        <translation type="obsolete">ОБУ</translation>
    </message>
    <message>
        <source>US &quot;Tribuna&quot; frequencies</source>
        <translation type="obsolete">Частоты УС &quot;Трибуна&quot;</translation>
    </message>
    <message>
        <source>Net &quot;Tribuna&quot;</source>
        <translation type="obsolete">Р/с ОБУ УС &quot;Трибуна&quot;</translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="199"/>
        <source>Записать</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="219"/>
        <source>Параметры тактирования</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="255"/>
        <source>Тактирование АЦП:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="278"/>
        <source>Тактирование PLL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="adcfdemomainwindow.ui" line="301"/>
        <source>Выход «CLK OUT»:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>APAK KD - auto mode</source>
        <translation type="obsolete">АПАК КД - автоматический режим</translation>
    </message>
    <message>
        <source>Switch to manual mode</source>
        <translation type="obsolete">Перейти в ручной режим</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Подключение</translation>
    </message>
    <message>
        <source>IP address:</source>
        <translation type="obsolete">IP адрес:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation type="obsolete">Порт:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="388"/>
        <location filename="adcfdemomainwindow.cpp" line="1216"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="522"/>
        <source>Channel settings</source>
        <translation>Параметры канала</translation>
    </message>
    <message>
        <source>Channel:</source>
        <translation type="obsolete">Канал:</translation>
    </message>
    <message>
        <source>Input:</source>
        <translation type="obsolete">Вход:</translation>
    </message>
    <message>
        <source>Attenuator:</source>
        <translation type="obsolete">Аттенюатор:</translation>
    </message>
    <message>
        <source>Spectr analyze</source>
        <translation type="obsolete">Анализ спектра</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="970"/>
        <source>Fs, MHz:</source>
        <translation type="unfinished">Дискретизация, MHz:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1003"/>
        <source>Fcentr, MHz:</source>
        <translation type="unfinished">Центр. частота, MHz:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1036"/>
        <source>Bandwidth, kHz:</source>
        <translation type="unfinished">Ширина полосы, KHz:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1075"/>
        <source>FFT window:</source>
        <translation>Тип окна:</translation>
    </message>
    <message>
        <source>Generatot</source>
        <translation type="obsolete">Генератор</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="118"/>
        <location filename="adcfdemomainwindow.cpp" line="1141"/>
        <source>Osci</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="123"/>
        <location filename="adcfdemomainwindow.cpp" line="1142"/>
        <source>Spec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="438"/>
        <source>Repeat</source>
        <translation>Цикл</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="451"/>
        <source>Check All</source>
        <translation>Опрашивать все</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="464"/>
        <location filename="adcfdemomainwindow.cpp" line="2539"/>
        <source>StartChecking</source>
        <translation>Начать опрос</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="601"/>
        <source>Chanel1</source>
        <translation>Канал 1</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="618"/>
        <source>Chanel2</source>
        <translation>Канал 2</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="752"/>
        <source>Timescale</source>
        <translation>Делений на секцию:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="783"/>
        <source>Trigger</source>
        <translation>Триггер</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="828"/>
        <location filename="adcfdemomainwindow.ui" line="1140"/>
        <source>Mode:</source>
        <translation type="unfinished">Режим:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="858"/>
        <source>Slope:</source>
        <translation>Наклон</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="888"/>
        <source>Source: </source>
        <translation>Источник</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="918"/>
        <source>Level:</source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="944"/>
        <source>Spectre analyze</source>
        <translation>Анализ спектра</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1109"/>
        <source>Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1182"/>
        <source>n/d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1212"/>
        <source>Fmod:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1244"/>
        <source>Fsym:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1273"/>
        <source>Fdev:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1319"/>
        <source>Fc:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1348"/>
        <source>K:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1377"/>
        <source>Neg/Pos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1421"/>
        <source>Cursors</source>
        <translation>Курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1434"/>
        <source>Measure</source>
        <translation>Измерение</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1466"/>
        <source>Cursor Show</source>
        <translation>Вкл. курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1482"/>
        <source>Peaks</source>
        <translation>Вкл. пики</translation>
    </message>
    <message>
        <source>Mode: </source>
        <translation type="obsolete">Режим:</translation>
    </message>
    <message>
        <source>Result2:</source>
        <translation type="obsolete">Мощность, дБм:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1495"/>
        <source>Cycle</source>
        <translation type="unfinished">Цикл</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1555"/>
        <source>Quit</source>
        <translation type="unfinished">Выход</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1560"/>
        <source>Set System Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.ui" line="1565"/>
        <source>Reset Decoder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>connecting ...</source>
        <translation type="obsolete">подключение...</translation>
    </message>
    <message>
        <source>connected to %1</source>
        <translation type="obsolete">подключен к %1</translation>
    </message>
    <message>
        <source>no HF device found</source>
        <translation type="obsolete">нет доступных устройств</translation>
    </message>
    <message>
        <source>decoding data</source>
        <translation type="obsolete">измерение</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1120"/>
        <source>f1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1121"/>
        <source>f2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1122"/>
        <source>f3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1123"/>
        <source>f4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1126"/>
        <location filename="adcfdemomainwindow.cpp" line="1132"/>
        <source>Number</source>
        <translation type="unfinished">Номер</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1127"/>
        <location filename="adcfdemomainwindow.cpp" line="1130"/>
        <location filename="adcfdemomainwindow.cpp" line="1133"/>
        <location filename="adcfdemomainwindow.cpp" line="1136"/>
        <source>Frequency</source>
        <translation type="unfinished">Частота</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1128"/>
        <location filename="adcfdemomainwindow.cpp" line="1131"/>
        <location filename="adcfdemomainwindow.cpp" line="1134"/>
        <location filename="adcfdemomainwindow.cpp" line="1137"/>
        <source>dBm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1129"/>
        <location filename="adcfdemomainwindow.cpp" line="1135"/>
        <source>Net</source>
        <translation type="unfinished">Р/с</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1803"/>
        <source>Stop</source>
        <translation type="unfinished">Стоп</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1660"/>
        <location filename="adcfdemomainwindow.cpp" line="1857"/>
        <source>File error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Channel 1</source>
        <translation type="obsolete">Канал 1</translation>
    </message>
    <message>
        <source>Channel 2</source>
        <translation type="obsolete">Канал 2</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="71"/>
        <source>Show Gen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="128"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="129"/>
        <source>Normal</source>
        <translation>Нормальный</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="132"/>
        <source>Chanel 1</source>
        <translation>Канал 1</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="133"/>
        <source>Chanel 2</source>
        <translation>Канал 2</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="134"/>
        <source>External</source>
        <translation>Внешний</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="157"/>
        <source>Freeze Cursors</source>
        <translation>Блок. курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="162"/>
        <source>Default</source>
        <translation>По умолч.</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="167"/>
        <source>Cursor 1</source>
        <translation>Курсор 1</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="169"/>
        <source>Cursor 2</source>
        <translation>Курсор 2</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="310"/>
        <location filename="adcfdemomainwindow.cpp" line="315"/>
        <source>50 Ohm</source>
        <translation>50 Ом</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="311"/>
        <location filename="adcfdemomainwindow.cpp" line="316"/>
        <source>600 Ohm Diff.</source>
        <translation>600 Ом</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="312"/>
        <location filename="adcfdemomainwindow.cpp" line="317"/>
        <source>1 MOhm/20pF</source>
        <translation>1 МОм</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="395"/>
        <source>1 Mks</source>
        <translation>1 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="396"/>
        <source>2 Mks</source>
        <translation>2 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="397"/>
        <source>5 Mks</source>
        <translation>5 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="398"/>
        <source>10 Mks</source>
        <translation>10 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="399"/>
        <source>20 Mks</source>
        <translation>20 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="400"/>
        <source>50 Mks</source>
        <translation>50 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="401"/>
        <source>100 Mks</source>
        <translation>100 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="402"/>
        <source>200 Mks</source>
        <translation>200 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="403"/>
        <source>500 Mks</source>
        <translation>500 мкС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="404"/>
        <source>1 Ms</source>
        <translation>1 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="405"/>
        <source>2 Ms</source>
        <translation>2 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="406"/>
        <source>5 Ms</source>
        <translation>5 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="407"/>
        <source>10 Ms</source>
        <translation>10 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="408"/>
        <source>20 Ms</source>
        <translation>20 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="409"/>
        <source>50 Ms</source>
        <translation>50 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="410"/>
        <source>100 Ms</source>
        <translation>100 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="411"/>
        <source>200 Ms</source>
        <translation>200 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="412"/>
        <source>500 Ms</source>
        <translation>500 мС</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="413"/>
        <source>1 Sec</source>
        <translation>1 Сек</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="414"/>
        <source>2 Sec</source>
        <translation>2 Сек</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="415"/>
        <source>5 Sec</source>
        <translation>5 Сек</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="422"/>
        <source>1 mV/d</source>
        <translation>1 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="423"/>
        <source>2 mV/d</source>
        <translation>2 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="424"/>
        <source>5 mV/d</source>
        <translation>5 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="425"/>
        <source>10 mV/d</source>
        <translation>10 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="426"/>
        <source>20 mV/d</source>
        <translation>20 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="427"/>
        <source>50 mV/d</source>
        <translation>50 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="428"/>
        <source>100 mV/d</source>
        <translation>100 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="429"/>
        <source>200 mV/d</source>
        <translation>200 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="430"/>
        <source>500 mV/d</source>
        <translation>500 мВ/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="431"/>
        <source>1 V/d</source>
        <translation>1 В/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="432"/>
        <source>2 V/d</source>
        <translation>2 В/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="433"/>
        <source>5 V/d</source>
        <translation>5 В/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="434"/>
        <source>10 V/d</source>
        <translation>10 В/д</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="448"/>
        <source>DC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="449"/>
        <source>AC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="450"/>
        <source>GND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="460"/>
        <source>rectangular</source>
        <translation type="unfinished">Отсутствует</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="461"/>
        <source>hann</source>
        <translation type="unfinished">Ханна</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="462"/>
        <source>hamming</source>
        <translation type="unfinished">Хэминга</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="463"/>
        <source>blackman</source>
        <translation type="unfinished">Блэкмана</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="464"/>
        <source>blackmanharris</source>
        <translation type="unfinished">Блэкмена-Харриса</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="465"/>
        <source>flattopwin</source>
        <translation type="unfinished">С плоской вершиной</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="506"/>
        <source>Negative</source>
        <translation type="unfinished">Негатив</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="507"/>
        <source>Positive</source>
        <translation type="unfinished">Позитив</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1140"/>
        <source>MAIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1143"/>
        <source>IQ Analyzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1146"/>
        <source>Add Tab</source>
        <translation>Добавить вкладку</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2272"/>
        <source>Main</source>
        <translation type="unfinished">Мульти окно</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2531"/>
        <source>StopChecking</source>
        <translation>Завершить опрос</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2659"/>
        <source>Hide Cursors</source>
        <translation>Выкл. курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2667"/>
        <source>Show Cursors</source>
        <translation>Вкл. курсоры</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2748"/>
        <source>Hide peaks</source>
        <translation>Выкл. пики</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="2751"/>
        <source>Show peaks</source>
        <translation>Вкл. пики</translation>
    </message>
    <message>
        <source>Am</source>
        <translation type="obsolete">Глубина АМ</translation>
    </message>
    <message>
        <source>Fm</source>
        <translation type="obsolete">Девиация ЧМ</translation>
    </message>
    <message>
        <source>Osc</source>
        <translation type="obsolete">Осциллограмма</translation>
    </message>
    <message>
        <source>Fft</source>
        <translation type="obsolete">Спектр</translation>
    </message>
    <message>
        <source>Freq</source>
        <translation type="obsolete">Частотомер</translation>
    </message>
    <message>
        <source>SINAD</source>
        <translation type="obsolete">КНИ/SINAD</translation>
    </message>
    <message>
        <source>TLG</source>
        <translation type="obsolete">Искажения ТЛГ</translation>
    </message>
    <message>
        <source>Psopho</source>
        <translation type="obsolete">Псофометр</translation>
    </message>
    <message>
        <source>Sens SINAD</source>
        <translation type="obsolete">Чувствительность SINAD</translation>
    </message>
    <message>
        <source>Sens TLG</source>
        <translation type="obsolete">Чувствительность ТЛГ</translation>
    </message>
    <message>
        <source>AFC</source>
        <translation type="obsolete">АЧХ</translation>
    </message>
    <message>
        <source>Deep AM, %:</source>
        <translation type="obsolete">Глубина АМ, %:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="809"/>
        <location filename="adcfdemomainwindow.cpp" line="925"/>
        <location filename="adcfdemomainwindow.cpp" line="984"/>
        <source>V</source>
        <translation type="unfinished">В</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="810"/>
        <location filename="adcfdemomainwindow.cpp" line="926"/>
        <location filename="adcfdemomainwindow.cpp" line="985"/>
        <source>mks</source>
        <translation type="unfinished">мкс</translation>
    </message>
    <message>
        <source>Deviation FM, Hz:</source>
        <translation type="obsolete">Девиация ЧМ, Гц:</translation>
    </message>
    <message>
        <source>SKZ, V:</source>
        <translation type="obsolete">СКЗ, В:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="854"/>
        <location filename="adcfdemomainwindow.cpp" line="882"/>
        <location filename="adcfdemomainwindow.cpp" line="904"/>
        <location filename="adcfdemomainwindow.cpp" line="943"/>
        <location filename="adcfdemomainwindow.cpp" line="966"/>
        <location filename="adcfdemomainwindow.cpp" line="1003"/>
        <source>dBV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="855"/>
        <location filename="adcfdemomainwindow.cpp" line="905"/>
        <location filename="adcfdemomainwindow.cpp" line="967"/>
        <location filename="adcfdemomainwindow.cpp" line="1004"/>
        <source>MHz</source>
        <translation type="unfinished">МГц</translation>
    </message>
    <message>
        <source>Freq, Hz:</source>
        <translation type="obsolete">Частота, Гц:</translation>
    </message>
    <message>
        <source>KNI, %:</source>
        <translation type="obsolete">КНИ/КГИ, %:</translation>
    </message>
    <message>
        <source>KVP, %:</source>
        <translation type="obsolete">КВП, %:</translation>
    </message>
    <message>
        <source>Upsoph, V:</source>
        <translation type="obsolete">Uпсоф, В:</translation>
    </message>
    <message>
        <source>U, mkV:</source>
        <translation type="obsolete">U, мкВ:</translation>
    </message>
    <message>
        <source>Nonliner, dB:</source>
        <translation type="obsolete">Нелинейность, dB:</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1216"/>
        <source>Close</source>
        <translation type="unfinished">Закрыть</translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1661"/>
        <location filename="adcfdemomainwindow.cpp" line="1858"/>
        <source>Failed to open
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="adcfdemomainwindow.cpp" line="1950"/>
        <source> mkV</source>
        <translation type="unfinished">мкВ</translation>
    </message>
    <message>
        <source>Switch to auto mode</source>
        <translation type="obsolete">Перейти в автоматический режим</translation>
    </message>
    <message>
        <source>APAK KD - manual mode</source>
        <translation type="obsolete">АПАК КД - ручной режим</translation>
    </message>
</context>
<context>
    <name>AutoWindow</name>
    <message>
        <source>Listen port:</source>
        <translation type="obsolete">Порт:</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="obsolete">Сервер</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="obsolete">Старт</translation>
    </message>
    <message>
        <source>Server IP:</source>
        <translation type="obsolete">IP адрес:</translation>
    </message>
    <message>
        <source>ARM ARK</source>
        <translation type="obsolete">АРМ АРК</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Дата</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Время</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="obsolete">Сообщение</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="obsolete">Стоп</translation>
    </message>
    <message>
        <source>Server stopped</source>
        <translation type="obsolete">Остановка сервера</translation>
    </message>
    <message>
        <source>Server started</source>
        <translation type="obsolete">Запуск сервера</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="table1.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="32"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="39"/>
        <source>Listen port:</source>
        <translation type="unfinished">Порт:</translation>
    </message>
    <message>
        <location filename="table1.ui" line="59"/>
        <location filename="table1.ui" line="127"/>
        <source>Start</source>
        <translation type="unfinished">Старт</translation>
    </message>
    <message>
        <location filename="table1.ui" line="72"/>
        <source>Server</source>
        <translation type="unfinished">Сервер</translation>
    </message>
    <message>
        <location filename="table1.ui" line="108"/>
        <source>Mulfunction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="134"/>
        <source>Server port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="160"/>
        <source>Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="180"/>
        <source>Server IP:</source>
        <translation type="unfinished">IP адрес:</translation>
    </message>
    <message>
        <location filename="table1.ui" line="187"/>
        <source>Archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="194"/>
        <source>Calibrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table1.ui" line="201"/>
        <source>Measure</source>
        <translation type="unfinished">Измерение</translation>
    </message>
</context>
<context>
    <name>FortuneServer</name>
    <message>
        <location filename="fortuneserver.cpp" line="50"/>
        <source>You&apos;ve been leading a dog&apos;s life. Stay off the furniture.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="51"/>
        <source>You&apos;ve got to think about tomorrow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="52"/>
        <source>You will be surprised by a loud noise.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="53"/>
        <source>You will feel hungry again in another hour.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="54"/>
        <source>You might have mail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="55"/>
        <source>You cannot kill time without injuring eternity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fortuneserver.cpp" line="56"/>
        <source>Computers are not intelligent. They only think they are.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IqAnMode</name>
    <message>
        <location filename="mode.cpp" line="723"/>
        <source>IQ Analayzer</source>
        <translation>IQ анализатор</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="725"/>
        <source>Deep</source>
        <translation>Глубина</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="726"/>
        <source>Deviaton</source>
        <translation>Девиация</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="727"/>
        <source>Deformation TLG</source>
        <translation>Деформация TLG</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="728"/>
        <source>IQANTEST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mode.cpp" line="729"/>
        <source>IQ-Vector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mode.cpp" line="737"/>
        <source>IQANALYZER</source>
        <translation>IQ анализатор</translation>
    </message>
</context>
<context>
    <name>IqAnWidget</name>
    <message>
        <location filename="iqanwidget.cpp" line="6"/>
        <source>IQ Analayzer</source>
        <translation>IQ анализатор</translation>
    </message>
</context>
<context>
    <name>Mode</name>
    <message>
        <location filename="mode.cpp" line="24"/>
        <source>Oscillograph</source>
        <translation>Осциллограф</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="25"/>
        <source>Spectre</source>
        <translation>Анализатор спектра</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="26"/>
        <source>IqAnalyzer</source>
        <translation>IQ анализатор</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="297"/>
        <location filename="mode.cpp" line="298"/>
        <source>Cursor: %1</source>
        <translation>Курсор: %1</translation>
    </message>
</context>
<context>
    <name>ModeDialog</name>
    <message>
        <location filename="modedialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modedialog.ui" line="49"/>
        <source>Ok</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="modedialog.ui" line="56"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="modedialog.cpp" line="9"/>
        <source>New mode</source>
        <translation>Новый режим</translation>
    </message>
</context>
<context>
    <name>ModsMainWidget</name>
    <message>
        <location filename="modsmainwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modsmainwidget.ui" line="77"/>
        <location filename="modsmainwidget.ui" line="148"/>
        <location filename="modsmainwidget.ui" line="213"/>
        <location filename="modsmainwidget.ui" line="278"/>
        <source>NONE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modsmainwidget.ui" line="97"/>
        <source>Dialog1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modsmainwidget.cpp" line="10"/>
        <location filename="modsmainwidget.cpp" line="11"/>
        <location filename="modsmainwidget.cpp" line="12"/>
        <location filename="modsmainwidget.cpp" line="13"/>
        <source>Insert mode</source>
        <translation>Добавить наблюдаемый режим</translation>
    </message>
    <message>
        <location filename="modsmainwidget.cpp" line="14"/>
        <location filename="modsmainwidget.cpp" line="15"/>
        <location filename="modsmainwidget.cpp" line="16"/>
        <location filename="modsmainwidget.cpp" line="17"/>
        <source>Remove mode</source>
        <translation>Удалить наблюдаемый режим</translation>
    </message>
    <message>
        <location filename="modsmainwidget.cpp" line="18"/>
        <location filename="modsmainwidget.cpp" line="19"/>
        <location filename="modsmainwidget.cpp" line="20"/>
        <location filename="modsmainwidget.cpp" line="21"/>
        <source>Move to tab</source>
        <translation>Перейти на вкладку</translation>
    </message>
</context>
<context>
    <name>OscMode</name>
    <message>
        <location filename="mode.cpp" line="511"/>
        <location filename="mode.cpp" line="513"/>
        <location filename="mode.cpp" line="517"/>
        <source>Oscillograph</source>
        <translation>Осциллограф</translation>
    </message>
</context>
<context>
    <name>OsciWidget</name>
    <message>
        <location filename="osciwidget.cpp" line="6"/>
        <source>Oscillograph</source>
        <oldsource>Oscillographrewtre</oldsource>
        <translation>Осциллограф</translation>
    </message>
</context>
<context>
    <name>PeaksMarkersWidget</name>
    <message>
        <location filename="peaksmarkerswidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlotDialog</name>
    <message>
        <location filename="plotdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Server</name>
    <message>
        <source>Server: Incoming connection</source>
        <translation type="obsolete">Сервер: новое подключение</translation>
    </message>
    <message>
        <source>Server: Reject incoming connection</source>
        <translation type="obsolete">Сервер: новое подключение игнорировано</translation>
    </message>
    <message>
        <source>Server: Close connection</source>
        <translation type="obsolete">Сервер: закрытие соединения</translation>
    </message>
</context>
<context>
    <name>ServerWorker</name>
    <message>
        <source> Server: Connect received</source>
        <translation type="obsolete">Сервер: открытие сеанса связи</translation>
    </message>
    <message>
        <source> Server: Disconnect received</source>
        <translation type="obsolete">Сервер: закрытие еанса связи</translation>
    </message>
    <message>
        <source> Server: Request received</source>
        <translation type="obsolete">Сервер: новый запрос</translation>
    </message>
    <message>
        <source> Server: Request finished</source>
        <translation type="obsolete">Сервер: запрос выполнен</translation>
    </message>
</context>
<context>
    <name>SpecMode</name>
    <message>
        <location filename="mode.cpp" line="595"/>
        <location filename="mode.cpp" line="597"/>
        <source>Spectre</source>
        <translation>Анализатор спектра</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="598"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="mode.cpp" line="599"/>
        <source>KNI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mode.cpp" line="600"/>
        <source>PHOSO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mode.cpp" line="607"/>
        <source>SPECTRE</source>
        <translation>Анализатор спектра</translation>
    </message>
</context>
<context>
    <name>SpectreWidget</name>
    <message>
        <location filename="spectrewidget.cpp" line="6"/>
        <source>Spectre</source>
        <translation>Анализатор спектра</translation>
    </message>
</context>
<context>
    <name>baseTabWidget</name>
    <message>
        <location filename="basetabwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="basetabwidget.ui" line="31"/>
        <source>GroupBox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
