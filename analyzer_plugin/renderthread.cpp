/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include "renderthread.h"
#include "analyzerworkerlib.h"

RenderThread::RenderThread(WorkerObject *w,
                           QString strIp,
                           QString strOscIp,
                           QObject *parent)
    : QThread(parent)
{
    externalWait = 0;
    //process=0;
    //m_Ch1=false;
    //m_Ch2= false;
    m_abort = false;
    m_bDelWorker=false;
    suspended = false;
    stopThreadF = false;
    exitThreadF = false;
    suspF = false;
    m_strIp=strIp;
    m_strOscIp=strOscIp;
    m_worker=0;//w;
    initDone = false;
/*    if(!m_worker)
    {
        m_worker = new WorkerObject(m_strIp, m_strOscIp);
        m_worker->open();
        m_bDelWorker=true;
    }
*/
    m_data2.resize(RAW_SAMPLES_NUM*4);
    //m_data2.fill(1);
    m_data.resize(RAW_SAMPLES_NUM*4);

    m_nReadPtr=0;
    m_nFftGain=0;
#ifdef unused
    d_plot=NULL;
#endif
    //m_mw=mw;

    //m_bCycle=false;

    udp_client=NULL;
    mainThread = 0;
//    udp_client=new QUdpSocket();
//    udp_client->bind(QHostAddress(/*"192.168.100.1"*/m_strIp), 50001);
    //udp_client->bind(QHostAddress::LocalHost, 50001);
    //udp_client->connectToHost(ui->edit_IP->text(), ui->edit_Port->text().toInt()/*QHostAddress::LocalHost, 7755*/);
    //udp_client->moveToThread(this);

    //initPLL();
}

RenderThread::RenderThread(ProcessObject *p,
                           WorkerObject *w,
                           QString strIp,
                           QString strOscIp,
                           QObject *parent)
    : QThread(parent)
{
//    initDrv();

//    QByteArray osc, ip;
//    osc = strOscIp.toAscii();
//    ip = strIp.toAscii();
//    ::openDrw(osc.data(), ip.data());

    externalWait = 0;
    process=p;
    //m_Ch1=false;
    //m_Ch2= false;
    mainThread = 0;
    m_abort = false;
    m_bDelWorker=false;
    suspended = false;
    stopThreadF = false;
    exitThreadF = false;
    initDone = false;
    suspF = false;
    m_strIp=strIp;
    m_strOscIp=strOscIp;
    m_worker=0;//new WorkerObject(m_strIp, m_strOscIp);//w;
    m_data2.resize(RAW_SAMPLES_NUM*4);
    //m_data2.fill(1);
    m_data.resize(RAW_SAMPLES_NUM*4);
    m_nReadPtr=0;
    m_nFftGain=0;
    modeConf=0;
#ifdef unused
    d_plot=NULL;
#endif
    //m_bCycle=false;

    udp_client=NULL;
    //initPLL();
}


RenderThread::~RenderThread()
{
    mutex.lock();
    m_abort = true;
    mutex.unlock();

    wait();

    if(udp_client)
    {
        udp_client->close();
        delete udp_client;
    }

    if(m_worker && m_bDelWorker)
    {
        delete m_worker;
        m_worker=NULL;
    }
}

//![processing the image (start)]
void RenderThread::startProcess(quint8 nMeasMode,
                                quint8 nCh,
                                quint8 nChType,
                                quint8 nChAtten,
                                quint8 nTaktADC,
                                quint8 nGenMode,
                                double fFc,
                                quint32 nAmax,
                                quint32 nAmin,
                                double fFdev,
                                double fFsym,
                                double fFmod,
                                quint8 nSSBType,
                                qreal  fK,
                                double fFs,
                                double fFcentr,
                                double fBand,
                                int nFftWindow,//*pFftWindow,
#ifdef unused
                                Plot *pPlot,
#endif
                                bool bCycle)
{
    qDebug("render starting ");
    m_nMeasMode=nMeasMode;
    //m_nCh=nCh;
    //m_nChType=nChType;
    //m_nChAtten=nChAtten;
    //m_nTaktADC=nTaktADC;
    //m_nGenMode=nGenMode;
    //m_fFc=fFc;
    //m_nAmax=nAmax;
    //m_nAmin=nAmin;
    //m_fFdev=fFdev;
    //m_fFsym=fFsym;
    //m_fFmod=fFmod;
    //m_nSSBType=nSSBType;
    //m_fK=fK;

    //Fs=fFs;
    //Fcentr=fFcentr;
    //Band=fBand;
    //window=pFftWindow;
    fft_window=::getNFTWindow(nFftWindow);//&m_worker->fft_window[nFftWindow][0];//pFftWindow;
#ifdef unused
    d_plot=pPlot;
#endif
    //m_bCycle=bCycle;

    //Fleft=modeConf->fFcentr-modeConf->fBand/2;
    //Fright=modeConf->fFcentr+modeConf->fBand/2;
    //Nfft=FFT_SAMPLES_NUM;

    //udp_client=pSock;
    if(udp_client)
        udp_client->moveToThread(this);

    //m_condition = condition;
    m_abort = false;
    start();
}

void RenderThread::run()
{
//    emit renderStarted();
    QByteArray osc, ip;
    osc = m_strOscIp.toAscii();
    ip = m_strIp.toAscii();
    ::openDrw(osc.data(), ip.data());

//    for (int i = 0;i<FFT_WIN_NUM;i++)
//        for (int j =0; j<FFT_WIN_LEN; j++)
//            ::setFFTWindow(i,j, fft_windowBuff[i][j]);

//    fft_window=::getNFTWindow(modeConf->nFftWindow);

//    for(int i=0; i<4; i++)
//    {
//        ::initPLL(i);
//        msleep(500);
//    }
    initDrv();
  process->initRenderVar(0);
  int nFrameLen;
//    m_worker->initDevice();
  ::initDevice();
  msleep(200);//500
//    m_worker->initTakt(modeConf->nTaktADC);
  ::initTakt(modeConf->nTaktADC);
  msleep(200);//1000
//process->processDataDDC(nFrameLen);
  //m_worker->processQueue();
  while (1)
  {
#ifdef unused
//    for (int i=0;i<modeConf->plots->size();i++) {
//        modeConf->plots->at(i)->signalData(0).clearValues();
//        modeConf->plots->at(i)->signalData(1).clearValues();
//        modeConf->plots->at(i)->signalData(2).clearValues();
//    }
#endif
//    initPLL();


    while(!m_abort)
    {
        //m_worker->processQueue();
        if(modeConf->nPass==0)
            process->initRenderVar(1);
        process->processDataDDC(nFrameLen);

//------------->
        int c=0; //chanel counter
        bool bResult;
        bool ch1Used=false;
        bool ch2Used=false;
        if (modeConf->Ch1) {c++;
            ch1Used=true;
        }
        if (modeConf->Ch2) {c++;
            ch2Used=true;
        }

      for (int x=0; x<c; x++) {
        if (ch1Used) {
            currentData = &m_data;
//            m_worker->initChannel(0, modeConf->nChType,
//                        modeConf->nChAtten);
            ::initChanel(0,modeConf->nChType,modeConf->nChAtten);
        }
        else if (ch2Used) {
            currentData = &m_data2;
//            m_worker->initChannel(1, modeConf->nChType2,
//                        modeConf->nChAtten2);
            ::initChanel(1,modeConf->nChType2,modeConf->nChAtten2);
        }

        bResult=true;
        for(int i=0;i<3;i++)
        {
        //!!!!!!!!!!!!!
        // start write data BUGED
            if (ch1Used)
                this->startDecoding(0);
            else if (ch2Used)
                this->startDecoding(1);
            //startDecoding(x);
//---------------
        // delay for fft frame to be captured
        if(nFrameLen==FFT_SAMPLES_NUM) // fft
            msleep(100+FFT_SAMPLES_NUM*1000/modeConf->Fdec);

qDebug("render start %d ", m_nReadPtr);
        //makeTestDataFFT();
        //m_nReadPtr=FFT_SAMPLES_NUM;
//#ifdef sdsfsdfs
        while(m_nReadPtr<nFrameLen)
        {
            mutex.lock();
            int n=m_nReadPtr;
            readData();
            qDebug("render ............ %d ", m_nReadPtr);
            //m_nReadPtr+=128;
           //m_worker->waitFor();

            //if(n==m_nReadPtr)
            //m_codition.wait(&mutex, 1000);
                    //msleep(100);
            mutex.unlock();

            if(m_abort)break;
        }
//#endif
qDebug("render stop %d ", m_nReadPtr);
        if(m_abort)break;

        if(m_nMeasMode!=4)
            break;
        }
        //bool bResult=true;
        if(m_nReadPtr>=nFrameLen)
        {
            //bwait = true
            if (ch1Used) {
                bResult=processData(0);
                ch1Used=false;
            }
            else if (ch2Used) {
                bResult=processData(1);
                ch2Used=false;
            }
            //bResult=processData(x);

            if( bResult) // todo remove m_nMeasMode==4
            {
            //m_mw->dataReady();
                modeConf->max_y=0;
                modeConf->max_y2=0;
                modeConf->min_y=0xffff;
                modeConf->min_y2=0xffff;

                //msleep(100);
                mutex.lock();
                //if bwait
                if (process->bWait)
                    m_codition.wait(&mutex, 100);//
                mutex.unlock();
            }
        }
        m_nReadPtr=0;
      }
//-----------------------<

        if(m_abort)break;

        if(/*!modeConf->bCycle &&*/ bResult/*nPass>=(nPassNum-1)*/)break;

        if(m_nMeasMode==4)
        {
            if(!bResult/*nPass<(nPassNum-1)*/)modeConf->nPass++;
            else modeConf->nPass=0;
        }

    }
//    enableGEN(false);
//    while(!stopThreadF)
//    {

//    }
    if (suspF)
    {
        suspended = true;
        m_codition_run.wait(&mutex);
        suspended = false;
    }
    if(exitThreadF)
        break;
    else m_abort = false;
  }

    //udp_client->close();
    // return socket to the main thread
    if(udp_client)
        udp_client->moveToThread(QApplication::instance()->thread());

    qDebug("render QUIT *********************************************************8 ");

    closeDrw();
    if (externalWait) externalWait->wakeAll();
    externalWait = 0;
//    qDebug()<<"move to "<<thread();
    //m_worker->moveToThread(mainThread/*m_worker->getMainThread()*/);
}
//![processing the image (finish)]

void RenderThread::stopProcess()
{
    mutex.lock();
    m_abort = true;
    exitThreadF = true;
    mutex.unlock();
    wait();
}

void RenderThread::resumeProcess()
{
    mutex.lock();
    //bwait =false
    if (process)
        process->bWait = false;
    m_codition.wakeAll();
    mutex.unlock();
}

void RenderThread::startDecoding(int nCh)
{
    qDebug("start decoding %i chn",nCh+1);
    ::startDecoding(nCh, m_nMeasMode);
    currentData->fill(0);
    m_nReadPtr=0;
    // use timer for getting status updates
}

void RenderThread::readData()
{
    ::readData(m_nReadPtr, m_nMeasMode,(char*) currentData/*->data()*/);
    // read
#ifdef asddasd
    if (udp_client->waitForReadyRead(/*-1*/1000))
    {
        while(udp_client->hasPendingDatagrams()) {
            QByteArray datagram;
            datagram.resize(udp_client->pendingDatagramSize());
            QHostAddress sender;
            quint16 senderPort;

            udp_client->readDatagram(datagram.data(), datagram.size(),
                                     &sender, &senderPort);
            //qout << "datagram received from " << sender.toString() << endl;

            processDatagram(datagram);
        }
    }
#endif
}

void RenderThread::WriteReg(quint8 BADDR, const QList<QPoint> &WDATA)
{
//#ifdef qweqwewq
    bool b2byte=false;
    quint16 LDATA=0;
    quint8 CMD=1;

    //if(BADDR>3 && BADDR<8) b2byte=true; // упр АЦП 2байт адрес

    LDATA=b2byte ? WDATA.size()*3 : WDATA.size()*2;

    //QByteArray msg;
    //msg.resize(LDATA+4);
    //unsigned char* msg=(uchar*)datagram.data();


    unsigned char msg[1024]={0};

    //qToBigEndian(LDATA, msg);
    //memcpy(msg, &LDATA, 2);
    msg[0]=LDATA>>8;
    msg[1]=LDATA&0x00ff;
    msg[2]=CMD;
    msg[3]=BADDR;

    for(int i=0;i<WDATA.size();i++)
    {
        QPoint item=WDATA.at(i);

        quint16 a=item.x();
        quint8 d=item.y();

        if(b2byte)
        {
            msg[4+i*3]=a>>8;
            msg[4+i*3+1]=a&0x00FF;
            msg[4+i*3+2]=d;
        }
        else
        {
            msg[4+i*2]=a&0x00FF;
            msg[4+i*2+1]=d;
        }
    }
// todo if !release
    //WriteLog(LDATA, CMD, BADDR, WDATA);
    QByteArray datagram((const char*)msg, LDATA+4);
    //QMetaObject::invokeMethod(m_worker, "writeData",  Qt::BlockingQueuedConnection,
    //                          Q_ARG(QByteArray, datagram) );
    //m_worker->writeData(datagram);
#ifdef sdasdasd
    if(udp_client)
        //udp_client->writeDatagram((const char*)msg, LDATA+4, QHostAddress::LocalHost, 50000);
    udp_client->writeDatagram((const char*)msg, LDATA+4, QHostAddress(/*"192.168.100.2"*/m_strOscIp), 50000);
#endif
}

bool RenderThread::processData(int nCh)
{
    bool bRes=false;
    QByteArray *temp;
    if (nCh==0) temp=&m_data;
    if (nCh==1) temp=&m_data2;

    bRes=process->processData(currentData,nCh);
    /*case 0:// глубина ам //iqan
    case 1:// девиация чм  //iqan
    case 2: // вч напряжение  //osci
    case 3: // спектр  //spec
    case 4: // частота  //spec
    case 5: // кни  //spec
    case 6: // тлг //iqan
    case 7: // псоф  //spec
    case 8: // чувствит
    case 9: // чувствит
        bRes=processFftDataSens(nCh);
    case 10:// ачх
        bRes=processFftDataAFC(nCh);*/

    return bRes;
}

void RenderThread::WriteLog(quint16 LDATA, quint8 CMD, quint8 BADDR, const QList<QPoint> &WDATA)
{
    //if(!ui->text_Log->isVisible())return;

    QString str, str1;
    str.sprintf("PC->OSC: LDATA=0x%04X CMD=0x%02X BADDR=0x%02X | ", LDATA, CMD, BADDR);

    for(int i=0;i<WDATA.size();i++)
    {
        QPoint item=WDATA.at(i);

        quint16 a=item.x();
        quint8 d=item.y();

        str1.sprintf("0x%04X 0x%02X ", a,d);
        str+=str1;
    }
    //str+="\n";

    //ui->text_Log->appendPlainText(str);
    //ui->text_Log->ensureCursorVisible();
    qDebug()<<str<<endl;
}

void RenderThread::WriteLog(QByteArray datagram)
{
    //if(!ui->text_Log->isVisible())return;

    QString str;
    QTextStream stream(&str);
    stream << hex << qSetFieldWidth(2) << qSetPadChar('0') << right;
    for(int i=0;i<datagram.size();i++)
    {
        stream << qSetFieldWidth(2) << static_cast<unsigned char>(datagram[i]) << qSetFieldWidth(0) <<" ";
        //stream << static_cast<unsigned char>(datagram[i]) <<" ";
    }
    str.prepend("OSC->PC: ");
    //ui->text_Log->appendPlainText("OSC->PC: "+/*datagram*/str + QString("\n"));
    //ui->text_Log->ensureCursorVisible();
    qDebug()<<str<<endl;
}

double RenderThread::getGainY(int ch)
{
    if (!modeConf) return 0;
    //return 1.0;
    //перевод в вольты
    int nAtten;//ui->combo_Ch1Atten->currentIndex();
    int nChType=-1;
    int chScale;
    mutex.lock();
    if (ch==0) {
        nAtten = modeConf->nChAtten;
        nChType=modeConf->nChType;
        chScale = modeConf->sC1;}
    if (ch==1) {
        nAtten = modeConf->nChAtten2;
        nChType=modeConf->nChType2;
        chScale = modeConf->sC2;}
    mutex.unlock();
    int N=1;
    double V=0;
    switch(nAtten)
    {
    case 0:N=1;
        break;
    case 1:N=10;
        break;
    case 2:N=30;
        break;
    case 3:N=300;
        break;
    }

    switch (chScale) {
        case 0:
            V=0.001; break;
        case 1:
            V=0.002; break;
        case 2:
            V=0.005; break;
        case 3:
            V=0.01; break;
        case 4:
            V=0.02; break;
        case 5:
            V=0.05; break;
        case 6:
            V=0.1; break;
        case 7:
            V=0.2; break;
        case 8:
            V=0.5; break;
        case 9:
            V= 1; break;
        case 10:
            V= 2; break;
        case 11:
            V= 5; break;
        case 12:
            V= 10; break;
    }

    double val=(1.25*N)/(m_nMeasMode==3||m_nMeasMode==4||m_nMeasMode==5||m_nMeasMode==7||m_nMeasMode==8||m_nMeasMode==10?qPow(2,28):32767); // todo *2 for spectr

    if(nChType==0)val=val*7/8; // 50 ohm
    else if(nChType==1)val=val*0.5; //600 ohm
    else if(nChType==2)val=val*0.5; //1 Mohm
    //val*10/цена деления В
    //val=val*10/V;
    return val;
}

double RenderThread::getGainX()
{
    if (!modeConf) return 0;
    double val=0.005;  // 200 MHz
    //ms val/1000

    if (process->getID()==2)
    {
        if (modeConf->tSc>8 && modeConf->tSc<18)
           val=val/1000;
        if (modeConf->tSc>17)
           val=val/1000000;
    }

    //перевод в mks
    switch(m_nMeasMode)
    {
    case 0:// глубина ам
    case 1:// девиация чм
    case 6:// квп
    case 9:// чувств тлг квп
        val=1000000/modeConf->Fdec;
        break;
    case 2: // вч напряжение
        break;
    case 3: // спектр
    case 4:// частота
    case 5:// кни
    case 7:// псоф
    case 8:// чувств
    case 10:// ачх
        val=modeConf->fBand/modeConf->Nband;
        break;
    }

    return val;
}

void RenderThread::makeTestDataFFT(unsigned int *F)
{
    QStringList files;
    files.append("KNI_data_4096.txt");

    for(int i=0;i<files.size();i++)
    {
        QString dir=QCoreApplication::applicationDirPath ();
        QString fileName = dir+"/"+files.at(i);//QFileDialog::getOpenFileName(this);
        if (fileName.isEmpty())
            return;

        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            //QMessageBox::warning(this,tr("File error"),tr("Failed to open\n%1").arg(fileName));
            return;
        }
        int j=0;
        while (!file.atEnd())
        {
            QString str = file.readLine().trimmed();
            if (!str.isEmpty())
            {
                unsigned int y=str.toInt();

                m_data[j*4]=(y>>24)&0xff; //m_data[jj*4]<<24;
                m_data[j*4+1]=(y>>16)&0xff; //y|=(0xff&m_data[jj*4+1])<<16;
                m_data[j*4+2]=(y>>8)&0xff; //y|=(0xff&m_data[jj*4+2])<<8;
                m_data[j*4+3]=(y)&0xff; //y|=0xff&m_data[jj*4+3];

                F[j]=y;
                unsigned int y1=m_data[j*4]<<24;
                y1|=(0xff&m_data[j*4+1])<<16;
                y1|=(0xff&m_data[j*4+2])<<8;
                y1|=0xff&m_data[j*4+3];

                if(y!=y1)
                {
                    y=0;
                    return;
                }
            }
            j++;
        }
        file.close();
    }
}

//REALISATION OF STARTPROCESS COPY
void RenderThread::startProcess2(
                  MeasureConfigItem *conf)
{
    qDebug("copy of render starting ");
    modeConf = conf;

    m_nMeasMode=process->getID();
    fft_window=::getNFTWindow(conf->nFftWindow);//&m_worker->fft_window[conf->nFftWindow][0];//pFftWindow;
#ifdef unused
//    d_plot=conf->plots->last();
#endif
    //m_bCycle=conf->bCycle;

    modeConf->Fleft=modeConf->fFc/*fFcentr*/-modeConf->fBand/2;
    modeConf->Fright=modeConf->fFc/*fFcentr*/+modeConf->fBand/2;
    modeConf->Nfft=FFT_SAMPLES_NUM;

    //udp_client=pSock;
    if(udp_client)
        udp_client->moveToThread(this);

    //m_condition = condition;
    m_abort = false;
    exitThreadF = !conf->bCycle;
    start();
}

void RenderThread::initRender(MeasureConfigItem *conf)
{
    modeConf = conf;

    m_nMeasMode=process->getID();
    fft_window=::getNFTWindow(conf->nFftWindow);//&m_worker->fft_window[conf->nFftWindow][0];//pFftWindow;
#ifdef unused
//    d_plot=conf->plots->last();
#endif
    //m_bCycle=conf->bCycle;

    modeConf->Fleft=modeConf->fFcentr-modeConf->fBand/2;
    modeConf->Fright=modeConf->fFcentr+modeConf->fBand/2;
    modeConf->Nfft=FFT_SAMPLES_NUM;

    //udp_client=pSock;
//    if(udp_client)
//        udp_client->moveToThread(this);

//    m_condition = condition;
//    m_abort = false;
//    exitThreadF = !conf->bCycle;
}

void RenderThread::setScale(int ch ,int sc) {
    if (!modeConf) return;
    //if (modeConf->empty)
    //    return;
    mutex.lock();
    if (ch==0) {
        //m_Scale1 = sc;
        modeConf->sC1=sc;
    }

    if (ch==1) {
        //m_Scale2 = sc;
        modeConf->sC2=sc;
    }
    mutex.unlock();
}

void RenderThread::setTimeScale(int val) {
    if (!modeConf) return;
    //if (modeConf->empty)
    //    return;
    mutex.lock();
    //m_TimeScale = val;
    modeConf->tSc=val;
    mutex.unlock();
}

QByteArray *RenderThread::getData() {
    return &m_data;
}

QByteArray *RenderThread::getData2() {
    return &m_data2;
}

QByteArray *RenderThread::getCurrentData() {
    return currentData;
}

void RenderThread::setMainThread(QThread *thr)
{
    mainThread = thr;
}

void RenderThread::setProcess(ProcessObject *p)
{
    process = p;
}

void RenderThread::stop()
{
    suspendProcess();
    exitThreadF = true;
    resume();
}

void RenderThread::resume()
{
    suspF = false;
    m_codition_run.wakeAll();
}

void RenderThread::suspendProcess()
{
    suspF = true;
    m_abort = true;
    //stopThreadF = true;
}

void RenderThread::setUdpPropetries(QString oscIp, QString ip, quint32 port)
{
    m_strOscIp = oscIp;
    m_strIp = ip;
    this->port = port;
}

WorkerObject* RenderThread::getWorker()
{
    return m_worker;
}

void RenderThread::setFFTWindow(int i, int j,int res)
{
    static bool done = false;
    if (!done)
    {
        memset(fft_windowBuff, 0, FFT_WIN_NUM*FFT_WIN_LEN);
        done = true;
    }
    fft_windowBuff[i][j] = res;
//    ::setFFTWindow(i,j, res);
}

void RenderThread::initDrv()
{
    QByteArray osc, ip;
    osc = m_strOscIp.toAscii();
    ip = m_strIp.toAscii();
    ::openDrw(osc.data(), ip.data());


//    if (!initDone)
    for (int i = 0;i<FFT_WIN_NUM;i++)
        for (int j =0; j<FFT_WIN_LEN; j++)
            ::setFFTWindow(i,j, fft_windowBuff[i][j]);

    fft_window=::getNFTWindow(modeConf->nFftWindow);

    if (initDone) return;
//    for(int i=0; i<4; i++)
    {
        ::initPLL(0/*i*/);
        msleep(500);
    }
    initDone = true;
}

void RenderThread::setExternalWaitCodition(QWaitCondition *wait)
{
    externalWait = wait;
}

int RenderThread::checkConnection()
{
    return ::checkConnection();
}
