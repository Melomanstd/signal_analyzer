#ifndef CHECKINGTHREAD_H
#define CHECKINGTHREAD_H

#include <QThread>
#include "modsmainwidget.h"

class CheckingThread : public QThread
{
    Q_OBJECT
public:
    explicit CheckingThread(ModsMainWidget *tab,
                            QObject *parent = 0);
    void startChecking(bool c, bool a);
    void setModes(QList<Mode *> *l); //set mods in main tab
    void setFullModes(QList<Mode *> *l); //set full mode list

protected:
    void run();

signals:
    
public slots:
    void setCheckingCycle(bool c);
    void stopProcess();
    void pauseProcess();
    void checkingMode(Mode *m);
    void modeChecked();

public:
    int currentModeIndex;

protected:
    QList<Mode*> *mainModes;
    QList<Mode*> *allModes;
    bool cycle;
    bool checkAll;
    QWaitCondition pause;
    QPushButton *btn;
    Mode *currentMode;
    QMutex mut;

};

#endif // CHECKINGTHREAD_H
