#ifndef MODEDIALOG_H
#define MODEDIALOG_H

#include <QDialog>
#include "mode.h"

class MeasureConfigItem;

namespace Ui {
class ModeDialog;
}

class ModeDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ModeDialog(QWidget *parent = 0);
    ~ModeDialog();

    unsigned getResult();

public slots:
    void setMode(int m);

private:
    Ui::ModeDialog *ui;
    unsigned result;
};

#endif // MODEDIALOG_H
