#include "plotdialog.h"
#include "ui_plotdialog.h"

PlotDialog::PlotDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlotDialog)
{
    ui->setupUi(this);
    mode = 0;
    modeName = "";
    ready=false;
    connect(ui->modeBox, SIGNAL(currentIndexChanged(QString)),
            this,SLOT(setCurrentMode(QString)));
}

PlotDialog::~PlotDialog()
{
    delete ui;
}

void PlotDialog::fillPlotBox(QString name, Mode *m){
    if (m->isUsed()) return;
    map.insert(name, m);
    ui->modeBox->addItem(name);
    if (!ready) ready=true;
}

void PlotDialog::setCurrentMode(QString name) {
    if (mode) mode->setUsed(false);
    mode = map.value(name);
    modeName = name;
}

Mode* PlotDialog::getMode() {
    return mode;
}

QString PlotDialog::getModeName() {
    return modeName;
}

bool PlotDialog::isReady() {
    return ready;
}

void PlotDialog::setMode(Mode *m) {
    mode = m;
}
