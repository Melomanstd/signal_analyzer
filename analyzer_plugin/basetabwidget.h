#ifndef BASETABWIDGET_H
#define BASETABWIDGET_H

#include <QWidget>
#include <QString>
#include <QLayout>
#include <QPushButton>
#include <QLabel>
#include "plot.h"
#include <qwt_plot.h>

namespace Ui {
class baseTabWidget;
}

class baseTabWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit baseTabWidget(QWidget *parent = 0);
    ~baseTabWidget();

    virtual QString getName();
    virtual QWidget* createHeader(QString text);
    virtual QWidget* createFooter(QString text);
    virtual QWidget* createSideBar(QStringList names,
                                   QStringList btnsNames);
    QWidget* header();
    QWidget* footer();
    QWidget* sideBar();
    void setName(QString name);
    virtual void setPlot(QList<Plot*> *plots);
    virtual void insertHead(QWidget* head);
    virtual void insertFoot(QWidget* head);
    void insertSideBar();
    QWidget *getOutputWidget();
    QHBoxLayout *getLay();
    QWidget* btnGroup();
    Plot* getPlot();
    QList<QPushButton*>* btnsList();
    void addWidget(QWidget *w);
    QVBoxLayout* getOutputLay();
    void createToolBarActions(QStringList &names);
    QList<QAction*> *getActions();
    
protected:
    QString name;
    Plot *plot;
    QWidget *headerPtr;
    QWidget *footerPtr;
    QWidget *sideBarPtr;
    QList<QPushButton*> btnList;
    QWidget *outputWidget;
    QVBoxLayout *outputLay;
    Ui::baseTabWidget *ui;

    QList<QAction*> actionList;
private:

};

#endif // BASETABWIDGET_H
