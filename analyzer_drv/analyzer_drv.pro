#-------------------------------------------------
#
# Project created by QtCreator 2014-02-24T16:47:19
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = sa_drv
TEMPLATE = lib

DEFINES += ANALYZERWORKERLIB_LIBRARY

PROJECT_PATH=\"$$PWD\"

#DESTDIR = C:/Projects/pluginsProject/Release
CONFIG(debug, debug|release) {
        #DESTDIR = $$quote($${PROJECT_PATH}/../../pluginsProject)
        #DEFINES += QT_NO_DEBUG_OUTPUT
        DESTDIR = ../../bin
}else{
        #DESTDIR = $$quote($${PROJECT_PATH}/../../pluginsProject/Release)
        #DEFINES += QT_NO_DEBUG_OUTPUT
        DESTDIR = ../../bin/Release
}

SOURCES += analyzerworkerlib.cpp \
    workerobject.cpp

HEADERS += analyzerworkerlib.h\
        AnalyzerWorkerLib_global.h \
    workerobject.h \
    measconf.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
