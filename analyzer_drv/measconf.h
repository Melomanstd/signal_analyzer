/****************************************************************************
**
** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/


#ifndef MEASCONF_H
#define MEASCONF_H

#include <QtCore>
//#include "plot.h"
//#include "mode.h"


// -------------------------------------
//class Mode;
//class RenderThread;
struct MeasureConfigItem
{
    //bool empty;
    bool markersShow;// = false;
    bool markersFreeze;// = false;
    bool generatorState;// = false; //t-showed/f-hidden

    int *fft_window;

    bool Ch1;// = true;
    bool Ch2;// = false;
    quint8 nMeasMode;
    quint8 nCh;
    quint8 nChType;// = 0;
    quint8 nChType2;// = 0;
    quint8 nChAtten;//= 0;
    quint8 nChAtten2;// = 0;
    quint8 nTaktADC;
    quint8 nGenMode;
    double fFc;
    quint32 nAmax;
    quint32 nAmin;
    double fFdev;
    double fFsym;
    double fFmod;
    quint8 nSSBType;
    qreal  fK;
    double fFs;
    double fFcentr;
    double fBand;
    int nFftWindow;
    quint8 sC1;// = -1;
    quint8 sC2;// = -1;
    quint8 tSc;// = -1;

    bool bCycle;// = false;
    int tabIndex;

    double uiFCentr;
    double uiBand;

    int subMeasMode;// = -1;

    //render variables
    double Fleft;
    double Fright;
    int Nfft;

    double Dec_ideal;
    int Dec_3;
    int Dec_1;
    int Dec_2;

    unsigned long Dec_sum;
    double Fdec;
    int Nband;

    int nPass;
    int nPassNum;

    double Ft0;
    double Ft1;
    double Ft2;
    double Ft3;

    double dFt1;
    double dFt2;
    double dFt3;

    short max_y;
    short max_y2;

    short min_y;
    short min_y2;

    // psophometer data
    quint64 m_Fa[32];

    // sensometr data
    quint32 m_nSmax;
    quint32 m_nSmin;
    quint32 m_nSc;

    qreal m_fSINAD_bound;
    qreal m_fKvp_bound;

    // afc data
    qreal m_FdB[32];
    int hardGain;
    int hardGain2;
    double G;
    double G2;

    //quint8 m_nCh;       // канал
    //quint8 m_nChType;   // тип канала
    //quint8 m_nParam;// условный номер измеряемого параметра
};
//class MeasureConfigItem : public QObject
//{
//    Q_OBJECT
//public:
//    explicit MeasureConfigItem(QObject *parent = 0);
//    ~MeasureConfigItem();
//    void dataInit(bool Ch1,
//                  bool Ch2,
//                  quint8 nMeasMode,
//                  quint8 nChType,
//                  quint8 nChType2,
//                  quint8 nChAtten,
//                  quint8 m_nChAtten2,
//                  quint8 nTaktADC,
//                  quint8 nGenMode,
//                  double fFc,
//                  quint32 nAmax,
//                  quint32 nAmin,
//                  double fFdev,
//                  double fFsym,
//                  double fFmod,
//                  quint8 nSSBType,
//                  qreal fK,
//                  double fFs,
//                  double fFcentr,
//                  double fBand,
//                  int nFftWindow,
//                  quint8 sC1,
//                  quint8 sC2,
//                  quint8 tSc,
//                  //Plot *pPlot,
//                  bool bCycle=false);

//public slots:
//    //void setFc(QString x);

//public:
//    //bool empty;
//    bool markersShow;
//    bool markersFreeze;
//    bool generatorState; //t-showed/f-hidden

//    bool Ch1;
//    bool Ch2;
//    quint8 nMeasMode;
//    quint8 nCh;
//    quint8 nChType;
//    quint8 nChType2;
//    quint8 nChAtten;
//    quint8 nChAtten2;
//    quint8 nTaktADC;
//    quint8 nGenMode;
//    double fFc;
//    quint32 nAmax;
//    quint32 nAmin;
//    double fFdev;
//    double fFsym;
//    double fFmod;
//    quint8 nSSBType;
//    qreal  fK;
//    double fFs;
//    double fFcentr;
//    double fBand;
//    int nFftWindow;
//    quint8 sC1;
//    quint8 sC2;
//    quint8 tSc;
//    //Plot *pPlot;
////    QList<Plot*> *plots;
//    bool bCycle;
//    int tabIndex;
////    QString tabName;
////    char* tabName;
//    //Mode *currMode;

//    double uiFCentr;
//    double uiBand;

//    int subMeasMode;

//    //render variables
//    double Fleft;
//    double Fright;
//    int Nfft;

//    double Dec_ideal;
//    int Dec_3;
//    int Dec_1;
//    int Dec_2;

//    unsigned long Dec_sum;
//    double Fdec;
//    int Nband;

//    int nPass;
//    int nPassNum;

//    double Ft0;
//    double Ft1;
//    double Ft2;
//    double Ft3;

//    double dFt1;
//    double dFt2;
//    double dFt3;

//    short max_y;
//    short max_y2;

//    short min_y;
//    short min_y2;

//    // psophometer data
//    quint64 m_Fa[32];

//    // sensometr data
//    quint32 m_nSmax;
//    quint32 m_nSmin;
//    quint32 m_nSc;

//    qreal m_fSINAD_bound;
//    qreal m_fKvp_bound;

//    // afc data
//    qreal m_FdB[32];

//    //quint8 m_nCh;       // канал
//    //quint8 m_nChType;   // тип канала
//    //quint8 m_nParam;// условный номер измеряемого параметра
//};

// -------------------------------------
//typedef QMap<quint8, MeasureConfigItem> MeasureConfig;
// -------------------------------------

#endif // MEASCONF_H
