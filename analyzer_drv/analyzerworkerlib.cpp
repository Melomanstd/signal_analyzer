#include "analyzerworkerlib.h"


//AnalyzerWorkerLib::AnalyzerWorkerLib()
//{
//}

WorkerObjectLib *lib_worker = 0;

int openDrw(char *oscIp, char *ip)
{
    if(lib_worker) return -1;
    lib_worker = new WorkerObjectLib(QString(ip),QString(oscIp),0);
    memset(lib_worker->fft_window, 0, FFT_WIN_NUM*FFT_WIN_LEN);
    return 0;
}

int closeDrw()
{
    if (!lib_worker) return -1;
    lib_worker->close();
    delete lib_worker;
    lib_worker = 0;
    return 0;
}

int initDevice()
{
    if(!lib_worker) return -1;
    lib_worker->initDevice();
    return 0;
}

int initTakt(unsigned char nTaktADC)
{
    if(!lib_worker) return -1;
    lib_worker->initTakt(nTaktADC);
    return 0;
}

int initPLL(int step)
{
    if(!lib_worker) return -1;
    lib_worker->OSC_Init();
    lib_worker->initPLL13(/*step*/);
    return 0;
}

int* getNFTWindow(int nFftWindow)
{
    if (!lib_worker)
        return 0;
    return &lib_worker->fft_window[nFftWindow][0];
}

int initDDC(int mode, MeasureConfigItem* modeConf)
{
    if (!lib_worker) return -1;
    switch (mode)
    {
    case 0:
        lib_worker->initDDC(false, modeConf);
        break;
    case 1:
        lib_worker->initDDC(true,modeConf);
        break;
    case 2:
        lib_worker->initFFT_PHOSO(modeConf);
        break;
    case 3:
        lib_worker->initFFT_FREQ(modeConf);
        break;
    case 4:
        lib_worker->initDDC_TLG(modeConf);
        break;
    }

    return 0;
}

int initChanel(unsigned int nCh, unsigned int type, unsigned int att)
{
    if(!lib_worker) return -1;
    lib_worker->initChannel13(nCh, type, att,0);
    return 0;
}

int startDecoding(int nCh, int meas)
{
    if(!lib_worker) return -1;
    lib_worker->startDecoding(nCh, meas);
    return 0;
}

int readData(int &readPtr, int meas, char *cData)
{
    if (!lib_worker) return -1;
    lib_worker->readData(readPtr, meas, cData);
    return 0;
}

int initDDS()
{
    if (!lib_worker) return -1;
    lib_worker->initDDS();
    return 0;
}

int setFFTWindow(int i, int j,int res)
{
    if(!lib_worker) return -1;
    lib_worker->fft_window[i][j] = res;
    return 0;
}

int checkConnection()
{
    if (!lib_worker) return -1;
    return lib_worker->checkConnectoin();
}
