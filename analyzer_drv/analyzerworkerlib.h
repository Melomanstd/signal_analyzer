#ifndef ANALYZERWORKERLIB_H
#define ANALYZERWORKERLIB_H

#include "AnalyzerWorkerLib_global.h"
#include "workerobject.h"

//class ANALYZERWORKERLIBSHARED_EXPORT AnalyzerWorkerLib
//{
//public:
//    AnalyzerWorkerLib();
//};

extern "C"
{
ANALYZERWORKERLIBSHARED_EXPORT int openDrw(char* oscIp, char* ip);
ANALYZERWORKERLIBSHARED_EXPORT int closeDrw();
ANALYZERWORKERLIBSHARED_EXPORT int initDevice();
ANALYZERWORKERLIBSHARED_EXPORT int initDDS();
ANALYZERWORKERLIBSHARED_EXPORT int initTakt(unsigned char nTaktADC);
ANALYZERWORKERLIBSHARED_EXPORT int initPLL(int step);
ANALYZERWORKERLIBSHARED_EXPORT int* getNFTWindow(int nFftWindow);
ANALYZERWORKERLIBSHARED_EXPORT int initDDC(int mode, MeasureConfigItem *modeConf);
ANALYZERWORKERLIBSHARED_EXPORT int initChanel(unsigned int nCh,
                                              unsigned int type,
                                              unsigned int att );
ANALYZERWORKERLIBSHARED_EXPORT int startDecoding(int nCh, int meas);
ANALYZERWORKERLIBSHARED_EXPORT int readData(int &readPtr, int meas, char* cData);
ANALYZERWORKERLIBSHARED_EXPORT int setFFTWindow(int i, int j, int res);
ANALYZERWORKERLIBSHARED_EXPORT int checkConnection();
}


#endif // ANALYZERWORKERLIB_H
